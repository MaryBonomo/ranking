# Ranking

The folder contains material related to a paradigm for the discovery of hidden knowledge from biological networks,  based  on  compact  views  obtained  from  the  rank  induced  by  topological  measures.  The inputs are six networks involving three