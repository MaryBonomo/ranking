Creator "igraph version 1.2.5 Mon Nov  9 14:28:24 2020"
Version 1
graph
[
  directed 0


node
  [
    id 1
    name "1"
    gene "A2M"
    phclass "Neurological"
  ]
  node
  [
    id 2
    name "2"
    gene "APP"
    phclass "Grey"
  ]
  node
  [
    id 3
    name "3"
    gene "APOE"
    phclass "Grey"
  ]
  node
  [
    id 4
    name "4"
    gene "PSEN2"
    phclass "Neurological"
  ]
  node
  [
    id 5
    name "5"
    gene "APBB2"
    phclass "Neurological"
  ]
  node
  [
    id 6
    name "6"
    gene "NOS3"
    phclass "Grey"
  ]
  node
  [
    id 7
    name "7"
    gene "PLAU"
    phclass "Neurological"
  ]
  node
  [
    id 8
    name "8"
    gene "ACE"
    phclass "Grey"
  ]
  node
  [
    id 9
    name "9"
    gene "MPO"
    phclass "Grey"
  ]
  node
  [
    id 10
    name "10"
    gene "PAXIP1"
    phclass "Neurological"
  ]
  node
  [
    id 11
    name "11"
    gene "BLMH"
    phclass "Neurological"
  ]
  node
  [
    id 12
    name "12"
    gene "PSEN1"
    phclass "Neurological"
  ]
  node
  [
    id 13
    name "13"
    gene "ABCA1"
    phclass "Grey"
  ]
  node
  [
    id 14
    name "14"
    gene "CST3"
    phclass "Neurological"
  ]
  node
  [
    id 15
    name "15"
    gene "MEF2A"
    phclass "Cardiovascular"
  ]
  node
  [
    id 16
    name "16"
    gene "KL"
    phclass "Cardiovascular"
  ]
  node
  [
    id 17
    name "17"
    gene "PON1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 18
    name "18"
    gene "PON2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 19
    name "19"
    gene "MMP3"
    phclass "Cardiovascular"
  ]
  node
  [
    id 20
    name "20"
    gene "ESR1"
    phclass "Grey"
  ]
  node
  [
    id 21
    name "21"
    gene "ABCB7"
    phclass "Hematological"
  ]
  node
  [
    id 22
    name "22"
    gene "CDAN1"
    phclass "Hematological"
  ]
  node
  [
    id 23
    name "23"
    gene "RPS19"
    phclass "Hematological"
  ]
  node
  [
    id 24
    name "24"
    gene "PKLR"
    phclass "Hematological"
  ]
  node
  [
    id 25
    name "25"
    gene "NT5C3"
    phclass "Hematological"
  ]
  node
  [
    id 26
    name "26"
    gene "RHAG"
    phclass "Hematological"
  ]
  node
  [
    id 27
    name "27"
    gene "SLC11A2"
    phclass "Hematological"
  ]
  node
  [
    id 28
    name "28"
    gene "SPTB"
    phclass "Hematological"
  ]
  node
  [
    id 29
    name "29"
    gene "ALAS2"
    phclass "Hematological"
  ]
  node
  [
    id 30
    name "30"
    gene "ABCA4"
    phclass "Ophthamological"
  ]
  node
  [
    id 31
    name "31"
    gene "RPGR"
    phclass "Ophthamological"
  ]
  node
  [
    id 32
    name "32"
    gene "GUCA1A"
    phclass "Ophthamological"
  ]
  node
  [
    id 33
    name "33"
    gene "AIPL1"
    phclass "Ophthamological"
  ]
  node
  [
    id 34
    name "34"
    gene "GUCY2D"
    phclass "Ophthamological"
  ]
  node
  [
    id 35
    name "35"
    gene "RPGRIP1"
    phclass "Ophthamological"
  ]
  node
  [
    id 36
    name "36"
    gene "CRX"
    phclass "Ophthamological"
  ]
  node
  [
    id 37
    name "37"
    gene "RDH5"
    phclass "Ophthamological"
  ]
  node
  [
    id 38
    name "38"
    gene "RLBP1"
    phclass "Ophthamological"
  ]
  node
  [
    id 39
    name "39"
    gene "IMPDH1"
    phclass "Ophthamological"
  ]
  node
  [
    id 40
    name "40"
    gene "PRPF31"
    phclass "Ophthamological"
  ]
  node
  [
    id 41
    name "41"
    gene "RP1"
    phclass "Grey"
  ]
  node
  [
    id 42
    name "42"
    gene "CRB1"
    phclass "Ophthamological"
  ]
  node
  [
    id 43
    name "43"
    gene "PRPF8"
    phclass "Ophthamological"
  ]
  node
  [
    id 44
    name "44"
    gene "TULP1"
    phclass "Ophthamological"
  ]
  node
  [
    id 45
    name "45"
    gene "CA4"
    phclass "Ophthamological"
  ]
  node
  [
    id 46
    name "46"
    gene "PRPF3"
    phclass "Ophthamological"
  ]
  node
  [
    id 47
    name "47"
    gene "RPE65"
    phclass "Ophthamological"
  ]
  node
  [
    id 48
    name "48"
    gene "RP2"
    phclass "Ophthamological"
  ]
  node
  [
    id 49
    name "49"
    gene "CERKL"
    phclass "Ophthamological"
  ]
  node
  [
    id 50
    name "50"
    gene "NRL"
    phclass "Ophthamological"
  ]
  node
  [
    id 51
    name "51"
    gene "FSCN2"
    phclass "Ophthamological"
  ]
  node
  [
    id 52
    name "52"
    gene "RHO"
    phclass "Ophthamological"
  ]
  node
  [
    id 53
    name "53"
    gene "RDS"
    phclass "Ophthamological"
  ]
  node
  [
    id 54
    name "54"
    gene "RP9"
    phclass "Ophthamological"
  ]
  node
  [
    id 55
    name "55"
    gene "USH2A"
    phclass "Grey"
  ]
  node
  [
    id 56
    name "56"
    gene "RGR"
    phclass "Ophthamological"
  ]
  node
  [
    id 57
    name "57"
    gene "CNGB1"
    phclass "Ophthamological"
  ]
  node
  [
    id 58
    name "58"
    gene "CNGA1"
    phclass "Ophthamological"
  ]
  node
  [
    id 59
    name "59"
    gene "PDE6A"
    phclass "Ophthamological"
  ]
  node
  [
    id 60
    name "60"
    gene "PDE6B"
    phclass "Ophthamological"
  ]
  node
  [
    id 61
    name "61"
    gene "ROM1"
    phclass "Ophthamological"
  ]
  node
  [
    id 62
    name "62"
    gene "NR2E3"
    phclass "Ophthamological"
  ]
  node
  [
    id 63
    name "63"
    gene "MERTK"
    phclass "Ophthamological"
  ]
  node
  [
    id 64
    name "64"
    gene "ELOVL4"
    phclass "Ophthamological"
  ]
  node
  [
    id 65
    name "65"
    gene "CHST6"
    phclass "Ophthamological"
  ]
  node
  [
    id 66
    name "66"
    gene "VMD2"
    phclass "Ophthamological"
  ]
  node
  [
    id 67
    name "67"
    gene "ABL1"
    phclass "Cancer"
  ]
  node
  [
    id 68
    name "68"
    gene "TAL1"
    phclass "Cancer"
  ]
  node
  [
    id 69
    name "69"
    gene "TAL2"
    phclass "Cancer"
  ]
  node
  [
    id 70
    name "70"
    gene "FLT3"
    phclass "Cancer"
  ]
  node
  [
    id 71
    name "71"
    gene "NBN"
    phclass "Grey"
  ]
  node
  [
    id 72
    name "72"
    gene "ZNFN1A1"
    phclass "Cancer"
  ]
  node
  [
    id 73
    name "73"
    gene "HOXD4"
    phclass "Cancer"
  ]
  node
  [
    id 74
    name "74"
    gene "BCR"
    phclass "Cancer"
  ]
  node
  [
    id 75
    name "75"
    gene "ARNT"
    phclass "Cancer"
  ]
  node
  [
    id 76
    name "76"
    gene "KRAS"
    phclass "Cancer"
  ]
  node
  [
    id 77
    name "77"
    gene "GMPS"
    phclass "Cancer"
  ]
  node
  [
    id 78
    name "78"
    gene "MLLT10"
    phclass "Cancer"
  ]
  node
  [
    id 79
    name "79"
    gene "ARHGEF12"
    phclass "Cancer"
  ]
  node
  [
    id 80
    name "80"
    gene "PICALM"
    phclass "Cancer"
  ]
  node
  [
    id 81
    name "81"
    gene "CEBPA"
    phclass "Cancer"
  ]
  node
  [
    id 82
    name "82"
    gene "CHIC2"
    phclass "Cancer"
  ]
  node
  [
    id 83
    name "83"
    gene "KIT"
    phclass "Grey"
  ]
  node
  [
    id 84
    name "84"
    gene "LPP"
    phclass "Cancer"
  ]
  node
  [
    id 85
    name "85"
    gene "NPM1"
    phclass "Cancer"
  ]
  node
  [
    id 86
    name "86"
    gene "NUP214"
    phclass "Cancer"
  ]
  node
  [
    id 87
    name "87"
    gene "RUNX1"
    phclass "Grey"
  ]
  node
  [
    id 88
    name "88"
    gene "WHSC1L1"
    phclass "Cancer"
  ]
  node
  [
    id 89
    name "89"
    gene "MLLT11"
    phclass "Cancer"
  ]
  node
  [
    id 90
    name "90"
    gene "NUMA1"
    phclass "Cancer"
  ]
  node
  [
    id 91
    name "91"
    gene "ZBTB16"
    phclass "Cancer"
  ]
  node
  [
    id 92
    name "92"
    gene "PML"
    phclass "Cancer"
  ]
  node
  [
    id 93
    name "93"
    gene "STAT5B"
    phclass "Grey"
  ]
  node
  [
    id 94
    name "94"
    gene "ARL11"
    phclass "Cancer"
  ]
  node
  [
    id 95
    name "95"
    gene "P2RX7"
    phclass "Cancer"
  ]
  node
  [
    id 96
    name "96"
    gene "ARHGAP26"
    phclass "Cancer"
  ]
  node
  [
    id 97
    name "97"
    gene "NF1"
    phclass "Cancer"
  ]
  node
  [
    id 98
    name "98"
    gene "PTPN11"
    phclass "Grey"
  ]
  node
  [
    id 99
    name "99"
    gene "BCL2"
    phclass "Cancer"
  ]
  node
  [
    id 100
    name "100"
    gene "CCND1"
    phclass "Cancer"
  ]
  node
  [
    id 101
    name "101"
    gene "TRA@"
    phclass "Cancer"
  ]
  node
  [
    id 102
    name "102"
    gene "GATA1"
    phclass "Grey"
  ]
  node
  [
    id 103
    name "103"
    gene "NQO1"
    phclass "Grey"
  ]
  node
  [
    id 104
    name "104"
    gene "ABO"
    phclass "Hematological"
  ]
  node
  [
    id 105
    name "105"
    gene "LU"
    phclass "Hematological"
  ]
  node
  [
    id 106
    name "106"
    gene "AQP1"
    phclass "Grey"
  ]
  node
  [
    id 107
    name "107"
    gene "DAF"
    phclass "Hematological"
  ]
  node
  [
    id 108
    name "108"
    gene "SLC4A1"
    phclass "Grey"
  ]
  node
  [
    id 109
    name "109"
    gene "DO"
    phclass "Hematological"
  ]
  node
  [
    id 110
    name "110"
    gene "GYPC"
    phclass "Grey"
  ]
  node
  [
    id 111
    name "111"
    gene "AQP3"
    phclass "Hematological"
  ]
  node
  [
    id 112
    name "112"
    gene "GCNT2"
    phclass "Hematological"
  ]
  node
  [
    id 113
    name "113"
    gene "CD44"
    phclass "Hematological"
  ]
  node
  [
    id 114
    name "114"
    gene "KEL"
    phclass "Hematological"
  ]
  node
  [
    id 115
    name "115"
    gene "SLC14A1"
    phclass "Hematological"
  ]
  node
  [
    id 116
    name "116"
    gene "CR1"
    phclass "Hematological"
  ]
  node
  [
    id 117
    name "117"
    gene "ICAM4"
    phclass "Hematological"
  ]
  node
  [
    id 118
    name "118"
    gene "FUT3"
    phclass "Hematological"
  ]
  node
  [
    id 119
    name "119"
    gene "GYPA"
    phclass "Hematological"
  ]
  node
  [
    id 120
    name "120"
    gene "BSG"
    phclass "Hematological"
  ]
  node
  [
    id 121
    name "121"
    gene "A4GALT"
    phclass "Hematological"
  ]
  node
  [
    id 122
    name "122"
    gene "B3GALT3"
    phclass "Hematological"
  ]
  node
  [
    id 123
    name "123"
    gene "RHCE"
    phclass "Hematological"
  ]
  node
  [
    id 124
    name "124"
    gene "GYPB"
    phclass "Hematological"
  ]
  node
  [
    id 125
    name "125"
    gene "XG"
    phclass "Hematological"
  ]
  node
  [
    id 126
    name "126"
    gene "ACHE"
    phclass "Hematological"
  ]
  node
  [
    id 127
    name "127"
    gene "ACTA1"
    phclass "Muscular"
  ]
  node
  [
    id 128
    name "128"
    gene "CRYAB"
    phclass "Grey"
  ]
  node
  [
    id 129
    name "129"
    gene "MYF6"
    phclass "Muscular"
  ]
  node
  [
    id 130
    name "130"
    gene "ITGA7"
    phclass "Muscular"
  ]
  node
  [
    id 131
    name "131"
    gene "DES"
    phclass "Grey"
  ]
  node
  [
    id 132
    name "132"
    gene "DYSF"
    phclass "Muscular"
  ]
  node
  [
    id 133
    name "133"
    gene "CAV3"
    phclass "Grey"
  ]
  node
  [
    id 134
    name "134"
    gene "CPT2"
    phclass "Grey"
  ]
  node
  [
    id 135
    name "135"
    gene "PGAM2"
    phclass "Muscular"
  ]
  node
  [
    id 136
    name "136"
    gene "MYH7"
    phclass "Grey"
  ]
  node
  [
    id 137
    name "137"
    gene "ACTC"
    phclass "Cardiovascular"
  ]
  node
  [
    id 138
    name "138"
    gene "MYL3"
    phclass "Cardiovascular"
  ]
  node
  [
    id 139
    name "139"
    gene "LMNA"
    phclass "Grey"
  ]
  node
  [
    id 140
    name "140"
    gene "TNNT2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 141
    name "141"
    gene "TTN"
    phclass "Grey"
  ]
  node
  [
    id 142
    name "142"
    gene "EYA4"
    phclass "Grey"
  ]
  node
  [
    id 143
    name "143"
    gene "SGCD"
    phclass "Grey"
  ]
  node
  [
    id 144
    name "144"
    gene "CSRP3"
    phclass "Cardiovascular"
  ]
  node
  [
    id 145
    name "145"
    gene "TCAP"
    phclass "Grey"
  ]
  node
  [
    id 146
    name "146"
    gene "ABCC9"
    phclass "Cardiovascular"
  ]
  node
  [
    id 147
    name "147"
    gene "DMD"
    phclass "Grey"
  ]
  node
  [
    id 148
    name "148"
    gene "MYL2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 149
    name "149"
    gene "MYH6"
    phclass "Cardiovascular"
  ]
  node
  [
    id 150
    name "150"
    gene "TNNC1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 151
    name "151"
    gene "TPM1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 152
    name "152"
    gene "TNNI3"
    phclass "Cardiovascular"
  ]
  node
  [
    id 153
    name "153"
    gene "MYBPC3"
    phclass "Cardiovascular"
  ]
  node
  [
    id 154
    name "154"
    gene "COX15"
    phclass "Grey"
  ]
  node
  [
    id 155
    name "155"
    gene "MYLK2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 156
    name "156"
    gene "PRKAG2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 157
    name "157"
    gene "PLN"
    phclass "Cardiovascular"
  ]
  node
  [
    id 158
    name "158"
    gene "TAZ"
    phclass "Grey"
  ]
  node
  [
    id 159
    name "159"
    gene "ACTG1"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 160
    name "160"
    gene "DIAPH1"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 161
    name "161"
    gene "MYO7A"
    phclass "Grey"
  ]
  node
  [
    id 162
    name "162"
    gene "TECTA"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 163
    name "163"
    gene "COL11A2"
    phclass "Grey"
  ]
  node
  [
    id 164
    name "164"
    gene "POU4F3"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 165
    name "165"
    gene "MYH9"
    phclass "Grey"
  ]
  node
  [
    id 166
    name "166"
    gene "MYO6"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 167
    name "167"
    gene "GJB3"
    phclass "Grey"
  ]
  node
  [
    id 168
    name "168"
    gene "KCNQ4"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 169
    name "169"
    gene "GRHL2"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 170
    name "170"
    gene "GJB2"
    phclass "Grey"
  ]
  node
  [
    id 171
    name "171"
    gene "GJB6"
    phclass "Grey"
  ]
  node
  [
    id 172
    name "172"
    gene "TMC1"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 173
    name "173"
    gene "DSPP"
    phclass "Grey"
  ]
  node
  [
    id 174
    name "174"
    gene "CRYM"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 175
    name "175"
    gene "MYH14"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 176
    name "176"
    gene "DFNA5"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 177
    name "177"
    gene "COCH"
    phclass "Grey"
  ]
  node
  [
    id 178
    name "178"
    gene "MYO1A"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 179
    name "179"
    gene "TMPRSS3"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 180
    name "180"
    gene "CDH23"
    phclass "Grey"
  ]
  node
  [
    id 181
    name "181"
    gene "ATP2B2"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 182
    name "182"
    gene "STRC"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 183
    name "183"
    gene "USH1C"
    phclass "Grey"
  ]
  node
  [
    id 184
    name "184"
    gene "OTOA"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 185
    name "185"
    gene "PCDH15"
    phclass "Grey"
  ]
  node
  [
    id 186
    name "186"
    gene "CLDN14"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 187
    name "187"
    gene "MYO3A"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 188
    name "188"
    gene "DFNB31"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 189
    name "189"
    gene "MYO15A"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 190
    name "190"
    gene "ESPN"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 191
    name "191"
    gene "SLC26A4"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 192
    name "192"
    gene "SLC26A5"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 193
    name "193"
    gene "TMIE"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 194
    name "194"
    gene "OTOF"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 195
    name "195"
    gene "JAG1"
    phclass "Grey"
  ]
  node
  [
    id 196
    name "196"
    gene "KIAA1199"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 197
    name "197"
    gene "TIMM8A"
    phclass "Grey"
  ]
  node
  [
    id 198
    name "198"
    gene "POU3F4"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 199
    name "199"
    gene "ACVR1B"
    phclass "Cancer"
  ]
  node
  [
    id 200
    name "200"
    gene "ARMET"
    phclass "Cancer"
  ]
  node
  [
    id 201
    name "201"
    gene "BRCA2"
    phclass "Grey"
  ]
  node
  [
    id 202
    name "202"
    gene "TP53"
    phclass "Cancer"
  ]
  node
  [
    id 203
    name "203"
    gene "SMAD4"
    phclass "Cancer"
  ]
  node
  [
    id 204
    name "204"
    gene "CDKN2A"
    phclass "Cancer"
  ]
  node
  [
    id 205
    name "205"
    gene "STK11"
    phclass "Cancer"
  ]
  node
  [
    id 206
    name "206"
    gene "RBBP8"
    phclass "Cancer"
  ]
  node
  [
    id 207
    name "207"
    gene "ADA"
    phclass "Immunological"
  ]
  node
  [
    id 208
    name "208"
    gene "DCLRE1C"
    phclass "Immunological"
  ]
  node
  [
    id 209
    name "209"
    gene "RAG1"
    phclass "Immunological"
  ]
  node
  [
    id 210
    name "210"
    gene "RAG2"
    phclass "Immunological"
  ]
  node
  [
    id 211
    name "211"
    gene "PTPRC"
    phclass "Grey"
  ]
  node
  [
    id 212
    name "212"
    gene "IL7R"
    phclass "Immunological"
  ]
  node
  [
    id 213
    name "213"
    gene "CD3D"
    phclass "Immunological"
  ]
  node
  [
    id 214
    name "214"
    gene "IL2RG"
    phclass "Immunological"
  ]
  node
  [
    id 215
    name "215"
    gene "ADD1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 216
    name "216"
    gene "KCNMB1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 217
    name "217"
    gene "NR3C2"
    phclass "Grey"
  ]
  node
  [
    id 218
    name "218"
    gene "AGTR1"
    phclass "Grey"
  ]
  node
  [
    id 219
    name "219"
    gene "PTGIS"
    phclass "Cardiovascular"
  ]
  node
  [
    id 220
    name "220"
    gene "AGT"
    phclass "Grey"
  ]
  node
  [
    id 221
    name "221"
    gene "ECE1"
    phclass "Grey"
  ]
  node
  [
    id 222
    name "222"
    gene "GNB3"
    phclass "Cardiovascular"
  ]
  node
  [
    id 223
    name "223"
    gene "RETN"
    phclass "Grey"
  ]
  node
  [
    id 224
    name "224"
    gene "HSD11B2"
    phclass "Grey"
  ]
  node
  [
    id 225
    name "225"
    gene "CYP3A5"
    phclass "Cardiovascular"
  ]
  node
  [
    id 226
    name "226"
    gene "ADRB2"
    phclass "Grey"
  ]
  node
  [
    id 227
    name "227"
    gene "PHF11"
    phclass "Grey"
  ]
  node
  [
    id 228
    name "228"
    gene "MS4A2"
    phclass "Respiratory"
  ]
  node
  [
    id 229
    name "229"
    gene "ALOX5"
    phclass "Grey"
  ]
  node
  [
    id 230
    name "230"
    gene "PTGDR"
    phclass "Respiratory"
  ]
  node
  [
    id 231
    name "231"
    gene "GPR154"
    phclass "Respiratory"
  ]
  node
  [
    id 232
    name "232"
    gene "HNMT"
    phclass "Respiratory"
  ]
  node
  [
    id 233
    name "233"
    gene "IL12B"
    phclass "Respiratory"
  ]
  node
  [
    id 234
    name "234"
    gene "IL13"
    phclass "Grey"
  ]
  node
  [
    id 235
    name "235"
    gene "PLA2G7"
    phclass "Grey"
  ]
  node
  [
    id 236
    name "236"
    gene "SCGB3A2"
    phclass "Respiratory"
  ]
  node
  [
    id 237
    name "237"
    gene "TNF"
    phclass "Grey"
  ]
  node
  [
    id 238
    name "238"
    gene "SCGB1A1"
    phclass "Respiratory"
  ]
  node
  [
    id 239
    name "239"
    gene "POMC"
    phclass "Nutritional"
  ]
  node
  [
    id 240
    name "240"
    gene "MC4R"
    phclass "Nutritional"
  ]
  node
  [
    id 241
    name "241"
    gene "AKR1C2"
    phclass "Nutritional"
  ]
  node
  [
    id 242
    name "242"
    gene "NTRK2"
    phclass "Nutritional"
  ]
  node
  [
    id 243
    name "243"
    gene "AGRP"
    phclass "Nutritional"
  ]
  node
  [
    id 244
    name "244"
    gene "NR0B2"
    phclass "Nutritional"
  ]
  node
  [
    id 245
    name "245"
    gene "LEP"
    phclass "Nutritional"
  ]
  node
  [
    id 246
    name "246"
    gene "LEPR"
    phclass "Nutritional"
  ]
  node
  [
    id 247
    name "247"
    gene "PPARG"
    phclass "Grey"
  ]
  node
  [
    id 248
    name "248"
    gene "SIM1"
    phclass "Nutritional"
  ]
  node
  [
    id 249
    name "249"
    gene "UCP3"
    phclass "Nutritional"
  ]
  node
  [
    id 250
    name "250"
    gene "MC3R"
    phclass "Nutritional"
  ]
  node
  [
    id 251
    name "251"
    gene "SLC6A14"
    phclass "Nutritional"
  ]
  node
  [
    id 252
    name "252"
    gene "ADRB3"
    phclass "Nutritional"
  ]
  node
  [
    id 253
    name "253"
    gene "CART"
    phclass "Nutritional"
  ]
  node
  [
    id 254
    name "254"
    gene "ENPP1"
    phclass "Grey"
  ]
  node
  [
    id 255
    name "255"
    gene "GHRL"
    phclass "Nutritional"
  ]
  node
  [
    id 256
    name "256"
    gene "UCP1"
    phclass "Nutritional"
  ]
  node
  [
    id 257
    name "257"
    gene "UCP2"
    phclass "Nutritional"
  ]
  node
  [
    id 258
    name "258"
    gene "PCSK1"
    phclass "Nutritional"
  ]
  node
  [
    id 259
    name "259"
    gene "AGC1"
    phclass "Skeletal"
  ]
  node
  [
    id 260
    name "260"
    gene "MATN3"
    phclass "Grey"
  ]
  node
  [
    id 261
    name "261"
    gene "CHST3"
    phclass "Skeletal"
  ]
  node
  [
    id 262
    name "262"
    gene "TRAPPC2"
    phclass "Skeletal"
  ]
  node
  [
    id 263
    name "263"
    gene "WISP3"
    phclass "Grey"
  ]
  node
  [
    id 264
    name "264"
    gene "ZFPM2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 265
    name "265"
    gene "NKX2-5"
    phclass "Cardiovascular"
  ]
  node
  [
    id 266
    name "266"
    gene "STOX1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 267
    name "267"
    gene "EPHX1"
    phclass "Grey"
  ]
  node
  [
    id 268
    name "268"
    gene "REN"
    phclass "Grey"
  ]
  node
  [
    id 269
    name "269"
    gene "AGTR2"
    phclass "Neurological"
  ]
  node
  [
    id 270
    name "270"
    gene "PRSS12"
    phclass "Neurological"
  ]
  node
  [
    id 271
    name "271"
    gene "CRBN"
    phclass "Neurological"
  ]
  node
  [
    id 272
    name "272"
    gene "NLGN4X"
    phclass "Grey"
  ]
  node
  [
    id 273
    name "273"
    gene "MECP2"
    phclass "Grey"
  ]
  node
  [
    id 274
    name "274"
    gene "PAK3"
    phclass "Neurological"
  ]
  node
  [
    id 275
    name "275"
    gene "IL1RAPL1"
    phclass "Neurological"
  ]
  node
  [
    id 276
    name "276"
    gene "ARX"
    phclass "Grey"
  ]
  node
  [
    id 277
    name "277"
    gene "SLC6A8"
    phclass "Neurological"
  ]
  node
  [
    id 278
    name "278"
    gene "FTSJ1"
    phclass "Neurological"
  ]
  node
  [
    id 279
    name "279"
    gene "ZNF81"
    phclass "Neurological"
  ]
  node
  [
    id 280
    name "280"
    gene "TSPAN7"
    phclass "Neurological"
  ]
  node
  [
    id 281
    name "281"
    gene "OPHN1"
    phclass "Neurological"
  ]
  node
  [
    id 282
    name "282"
    gene "AFF2"
    phclass "Neurological"
  ]
  node
  [
    id 283
    name "283"
    gene "SMCX"
    phclass "Neurological"
  ]
  node
  [
    id 284
    name "284"
    gene "GDI1"
    phclass "Neurological"
  ]
  node
  [
    id 285
    name "285"
    gene "ACSL4"
    phclass "Neurological"
  ]
  node
  [
    id 286
    name "286"
    gene "RPS6KA3"
    phclass "Grey"
  ]
  node
  [
    id 287
    name "287"
    gene "ARHGEF6"
    phclass "Neurological"
  ]
  node
  [
    id 288
    name "288"
    gene "FGD1"
    phclass "Grey"
  ]
  node
  [
    id 289
    name "289"
    gene "ZNF41"
    phclass "Neurological"
  ]
  node
  [
    id 290
    name "290"
    gene "DLG3"
    phclass "Neurological"
  ]
  node
  [
    id 291
    name "291"
    gene "SMS"
    phclass "Neurological"
  ]
  node
  [
    id 292
    name "292"
    gene "SOX3"
    phclass "Grey"
  ]
  node
  [
    id 293
    name "293"
    gene "AK1"
    phclass "Hematological"
  ]
  node
  [
    id 294
    name "294"
    gene "BPGM"
    phclass "Hematological"
  ]
  node
  [
    id 295
    name "295"
    gene "G6PD"
    phclass "Grey"
  ]
  node
  [
    id 296
    name "296"
    gene "GCLC"
    phclass "Hematological"
  ]
  node
  [
    id 297
    name "297"
    gene "GPI"
    phclass "Hematological"
  ]
  node
  [
    id 298
    name "298"
    gene "GSS"
    phclass "Grey"
  ]
  node
  [
    id 299
    name "299"
    gene "HK1"
    phclass "Hematological"
  ]
  node
  [
    id 300
    name "300"
    gene "PGK1"
    phclass "Grey"
  ]
  node
  [
    id 301
    name "301"
    gene "TPI1"
    phclass "Hematological"
  ]
  node
  [
    id 302
    name "302"
    gene "AKT2"
    phclass "Endocrine"
  ]
  node
  [
    id 303
    name "303"
    gene "ABCC8"
    phclass "Grey"
  ]
  node
  [
    id 304
    name "304"
    gene "TCF1"
    phclass "Grey"
  ]
  node
  [
    id 305
    name "305"
    gene "SUMO4"
    phclass "Endocrine"
  ]
  node
  [
    id 306
    name "306"
    gene "PTPN22"
    phclass "Grey"
  ]
  node
  [
    id 307
    name "307"
    gene "INSR"
    phclass "Grey"
  ]
  node
  [
    id 308
    name "308"
    gene "GCK"
    phclass "Grey"
  ]
  node
  [
    id 309
    name "309"
    gene "GCGR"
    phclass "Endocrine"
  ]
  node
  [
    id 310
    name "310"
    gene "GPD2"
    phclass "Endocrine"
  ]
  node
  [
    id 311
    name "311"
    gene "HNF4A"
    phclass "Endocrine"
  ]
  node
  [
    id 312
    name "312"
    gene "IRS2"
    phclass "Endocrine"
  ]
  node
  [
    id 313
    name "313"
    gene "MAPK8IP1"
    phclass "Endocrine"
  ]
  node
  [
    id 314
    name "314"
    gene "NEUROD1"
    phclass "Endocrine"
  ]
  node
  [
    id 315
    name "315"
    gene "TCF2"
    phclass "Grey"
  ]
  node
  [
    id 316
    name "316"
    gene "IRS1"
    phclass "Endocrine"
  ]
  node
  [
    id 317
    name "317"
    gene "SLC2A2"
    phclass "Grey"
  ]
  node
  [
    id 318
    name "318"
    gene "SLC2A4"
    phclass "Endocrine"
  ]
  node
  [
    id 319
    name "319"
    gene "CAPN10"
    phclass "Endocrine"
  ]
  node
  [
    id 320
    name "320"
    gene "PTF1A"
    phclass "Endocrine"
  ]
  node
  [
    id 321
    name "321"
    gene "KCNJ11"
    phclass "Grey"
  ]
  node
  [
    id 322
    name "322"
    gene "IPF1"
    phclass "Grey"
  ]
  node
  [
    id 323
    name "323"
    gene "FOXP3"
    phclass "Grey"
  ]
  node
  [
    id 324
    name "324"
    gene "VEGF"
    phclass "Endocrine"
  ]
  node
  [
    id 325
    name "325"
    gene "ALOX5AP"
    phclass "Cardiovascular"
  ]
  node
  [
    id 326
    name "326"
    gene "F7"
    phclass "Grey"
  ]
  node
  [
    id 327
    name "327"
    gene "LGALS2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 328
    name "328"
    gene "LTA"
    phclass "Cardiovascular"
  ]
  node
  [
    id 329
    name "329"
    gene "OLR1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 330
    name "330"
    gene "THBD"
    phclass "Grey"
  ]
  node
  [
    id 331
    name "331"
    gene "GCLM"
    phclass "Cardiovascular"
  ]
  node
  [
    id 332
    name "332"
    gene "TNFSF4"
    phclass "Cardiovascular"
  ]
  node
  [
    id 333
    name "333"
    gene "PDE4D"
    phclass "Cardiovascular"
  ]
  node
  [
    id 334
    name "334"
    gene "ALOX12B"
    phclass "Dermatological"
  ]
  node
  [
    id 335
    name "335"
    gene "TGM1"
    phclass "Dermatological"
  ]
  node
  [
    id 336
    name "336"
    gene "ALOXE3"
    phclass "Dermatological"
  ]
  node
  [
    id 337
    name "337"
    gene "ANK1"
    phclass "Hematological"
  ]
  node
  [
    id 338
    name "338"
    gene "EPB42"
    phclass "Hematological"
  ]
  node
  [
    id 339
    name "339"
    gene "SPTA1"
    phclass "Hematological"
  ]
  node
  [
    id 340
    name "340"
    gene "ANK2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 341
    name "341"
    gene "KCNQ1"
    phclass "Grey"
  ]
  node
  [
    id 342
    name "342"
    gene "KCNH2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 343
    name "343"
    gene "SCN5A"
    phclass "Cardiovascular"
  ]
  node
  [
    id 344
    name "344"
    gene "KCNE1"
    phclass "Grey"
  ]
  node
  [
    id 345
    name "345"
    gene "KCNE2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 346
    name "346"
    gene "KCNJ2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 347
    name "347"
    gene "APC"
    phclass "Cancer"
  ]
  node
  [
    id 348
    name "348"
    gene "MUTYH"
    phclass "Cancer"
  ]
  node
  [
    id 349
    name "349"
    gene "PLAG1"
    phclass "Cancer"
  ]
  node
  [
    id 350
    name "350"
    gene "RAD54B"
    phclass "Cancer"
  ]
  node
  [
    id 351
    name "351"
    gene "RAD54L"
    phclass "Cancer"
  ]
  node
  [
    id 352
    name "352"
    gene "BCL10"
    phclass "Cancer"
  ]
  node
  [
    id 353
    name "353"
    gene "PTPN12"
    phclass "Cancer"
  ]
  node
  [
    id 354
    name "354"
    gene "TGFBR2"
    phclass "Grey"
  ]
  node
  [
    id 355
    name "355"
    gene "SRC"
    phclass "Cancer"
  ]
  node
  [
    id 356
    name "356"
    gene "MLH3"
    phclass "Cancer"
  ]
  node
  [
    id 357
    name "357"
    gene "PTPRJ"
    phclass "Cancer"
  ]
  node
  [
    id 358
    name "358"
    gene "ODC1"
    phclass "Cancer"
  ]
  node
  [
    id 359
    name "359"
    gene "AXIN2"
    phclass "Cancer"
  ]
  node
  [
    id 360
    name "360"
    gene "BUB1B"
    phclass "Cancer"
  ]
  node
  [
    id 361
    name "361"
    gene "EP300"
    phclass "Grey"
  ]
  node
  [
    id 362
    name "362"
    gene "PDGFRL"
    phclass "Cancer"
  ]
  node
  [
    id 363
    name "363"
    gene "PIK3CA"
    phclass "Cancer"
  ]
  node
  [
    id 364
    name "364"
    gene "BAX"
    phclass "Cancer"
  ]
  node
  [
    id 365
    name "365"
    gene "CTNNB1"
    phclass "Cancer"
  ]
  node
  [
    id 366
    name "366"
    gene "DCC"
    phclass "Cancer"
  ]
  node
  [
    id 367
    name "367"
    gene "MCC"
    phclass "Cancer"
  ]
  node
  [
    id 368
    name "368"
    gene "NRAS"
    phclass "Cancer"
  ]
  node
  [
    id 369
    name "369"
    gene "MSH2"
    phclass "Cancer"
  ]
  node
  [
    id 370
    name "370"
    gene "MLH1"
    phclass "Cancer"
  ]
  node
  [
    id 371
    name "371"
    gene "PMS1"
    phclass "Cancer"
  ]
  node
  [
    id 372
    name "372"
    gene "PMS2"
    phclass "Grey"
  ]
  node
  [
    id 373
    name "373"
    gene "MSH6"
    phclass "Cancer"
  ]
  node
  [
    id 374
    name "374"
    gene "FGFR3"
    phclass "Grey"
  ]
  node
  [
    id 375
    name "375"
    gene "FLCN"
    phclass "Grey"
  ]
  node
  [
    id 376
    name "376"
    gene "BRAF"
    phclass "Cancer"
  ]
  node
  [
    id 377
    name "377"
    gene "DLC1"
    phclass "Cancer"
  ]
  node
  [
    id 378
    name "378"
    gene "PLA2G2A"
    phclass "Cancer"
  ]
  node
  [
    id 379
    name "379"
    gene "BUB1"
    phclass "Cancer"
  ]
  node
  [
    id 380
    name "380"
    gene "IRF1"
    phclass "Grey"
  ]
  node
  [
    id 381
    name "381"
    gene "CDH1"
    phclass "Grey"
  ]
  node
  [
    id 382
    name "382"
    gene "IL1B"
    phclass "Cancer"
  ]
  node
  [
    id 383
    name "383"
    gene "IL1RN"
    phclass "Cancer"
  ]
  node
  [
    id 384
    name "384"
    gene "CASP10"
    phclass "Grey"
  ]
  node
  [
    id 385
    name "385"
    gene "ERBB2"
    phclass "Cancer"
  ]
  node
  [
    id 386
    name "386"
    gene "FGFR2"
    phclass "Grey"
  ]
  node
  [
    id 387
    name "387"
    gene "KLF6"
    phclass "Cancer"
  ]
  node
  [
    id 388
    name "388"
    gene "AIRE"
    phclass "Immunological"
  ]
  node
  [
    id 389
    name "389"
    gene "FAS"
    phclass "Grey"
  ]
  node
  [
    id 390
    name "390"
    gene "CASP8"
    phclass "Grey"
  ]
  node
  [
    id 391
    name "391"
    gene "APOA1"
    phclass "Grey"
  ]
  node
  [
    id 392
    name "392"
    gene "GSN"
    phclass "Neurological"
  ]
  node
  [
    id 393
    name "393"
    gene "FGA"
    phclass "Grey"
  ]
  node
  [
    id 394
    name "394"
    gene "LYZ"
    phclass "Neurological"
  ]
  node
  [
    id 395
    name "395"
    gene "TTR"
    phclass "Grey"
  ]
  node
  [
    id 396
    name "396"
    gene "APOA2"
    phclass "Metabolic"
  ]
  node
  [
    id 397
    name "397"
    gene "APOC3"
    phclass "Metabolic"
  ]
  node
  [
    id 398
    name "398"
    gene "APOH"
    phclass "Metabolic"
  ]
  node
  [
    id 399
    name "399"
    gene "TGFBI"
    phclass "Ophthamological"
  ]
  node
  [
    id 400
    name "400"
    gene "TACSTD2"
    phclass "Ophthamological"
  ]
  node
  [
    id 401
    name "401"
    gene "VSX1"
    phclass "Grey"
  ]
  node
  [
    id 402
    name "402"
    gene "COL8A2"
    phclass "Ophthamological"
  ]
  node
  [
    id 403
    name "403"
    gene "PIP5K3"
    phclass "Ophthamological"
  ]
  node
  [
    id 404
    name "404"
    gene "KERA"
    phclass "Ophthamological"
  ]
  node
  [
    id 405
    name "405"
    gene "APOA5"
    phclass "Metabolic"
  ]
  node
  [
    id 406
    name "406"
    gene "LIPI"
    phclass "Metabolic"
  ]
  node
  [
    id 407
    name "407"
    gene "APOB"
    phclass "Metabolic"
  ]
  node
  [
    id 408
    name "408"
    gene "LDLR"
    phclass "Metabolic"
  ]
  node
  [
    id 409
    name "409"
    gene "PCSK9"
    phclass "Metabolic"
  ]
  node
  [
    id 410
    name "410"
    gene "LDLRAP1"
    phclass "Metabolic"
  ]
  node
  [
    id 411
    name "411"
    gene "EPHX2"
    phclass "Metabolic"
  ]
  node
  [
    id 412
    name "412"
    gene "C7orf16"
    phclass "Metabolic"
  ]
  node
  [
    id 413
    name "413"
    gene "ITIH4"
    phclass "Metabolic"
  ]
  node
  [
    id 414
    name "414"
    gene "MTP"
    phclass "Metabolic"
  ]
  node
  [
    id 415
    name "415"
    gene "APOC2"
    phclass "Metabolic"
  ]
  node
  [
    id 416
    name "416"
    gene "DISC1"
    phclass "Psychiatric"
  ]
  node
  [
    id 417
    name "417"
    gene "TAAR6"
    phclass "Psychiatric"
  ]
  node
  [
    id 418
    name "418"
    gene "COMT"
    phclass "Psychiatric"
  ]
  node
  [
    id 419
    name "419"
    gene "HTR2A"
    phclass "Grey"
  ]
  node
  [
    id 420
    name "420"
    gene "RTN4R"
    phclass "Psychiatric"
  ]
  node
  [
    id 421
    name "421"
    gene "SYN2"
    phclass "Psychiatric"
  ]
  node
  [
    id 422
    name "422"
    gene "ENTH"
    phclass "Psychiatric"
  ]
  node
  [
    id 423
    name "423"
    gene "PRODH"
    phclass "Grey"
  ]
  node
  [
    id 424
    name "424"
    gene "APRT"
    phclass "Metabolic"
  ]
  node
  [
    id 425
    name "425"
    gene "SLC34A1"
    phclass "Grey"
  ]
  node
  [
    id 426
    name "426"
    gene "ING1"
    phclass "Cancer"
  ]
  node
  [
    id 427
    name "427"
    gene "TNFRSF10B"
    phclass "Cancer"
  ]
  node
  [
    id 428
    name "428"
    gene "FASLG"
    phclass "Immunological"
  ]
  node
  [
    id 429
    name "429"
    gene "DNASE1"
    phclass "Immunological"
  ]
  node
  [
    id 430
    name "430"
    gene "PDCD1"
    phclass "Immunological"
  ]
  node
  [
    id 431
    name "431"
    gene "AR"
    phclass "Grey"
  ]
  node
  [
    id 432
    name "432"
    gene "CHEK2"
    phclass "Cancer"
  ]
  node
  [
    id 433
    name "433"
    gene "PPM1D"
    phclass "Cancer"
  ]
  node
  [
    id 434
    name "434"
    gene "SLC22A18"
    phclass "Cancer"
  ]
  node
  [
    id 435
    name "435"
    gene "BRCA1"
    phclass "Cancer"
  ]
  node
  [
    id 436
    name "436"
    gene "TSG101"
    phclass "Cancer"
  ]
  node
  [
    id 437
    name "437"
    gene "BRIP1"
    phclass "Grey"
  ]
  node
  [
    id 438
    name "438"
    gene "RB1CC1"
    phclass "Cancer"
  ]
  node
  [
    id 439
    name "439"
    gene "PHB"
    phclass "Cancer"
  ]
  node
  [
    id 440
    name "440"
    gene "ATM"
    phclass "Grey"
  ]
  node
  [
    id 441
    name "441"
    gene "BARD1"
    phclass "Cancer"
  ]
  node
  [
    id 442
    name "442"
    gene "RAD51"
    phclass "Cancer"
  ]
  node
  [
    id 443
    name "443"
    gene "XRCC3"
    phclass "Cancer"
  ]
  node
  [
    id 444
    name "444"
    gene "RNASEL"
    phclass "Cancer"
  ]
  node
  [
    id 445
    name "445"
    gene "PTEN"
    phclass "Grey"
  ]
  node
  [
    id 446
    name "446"
    gene "MSR1"
    phclass "Cancer"
  ]
  node
  [
    id 447
    name "447"
    gene "EPHB2"
    phclass "Cancer"
  ]
  node
  [
    id 448
    name "448"
    gene "MAD1L1"
    phclass "Cancer"
  ]
  node
  [
    id 449
    name "449"
    gene "ATBF1"
    phclass "Cancer"
  ]
  node
  [
    id 450
    name "450"
    gene "ELAC2"
    phclass "Cancer"
  ]
  node
  [
    id 451
    name "451"
    gene "MXI1"
    phclass "Cancer"
  ]
  node
  [
    id 452
    name "452"
    gene "VAPB"
    phclass "Grey"
  ]
  node
  [
    id 453
    name "453"
    gene "SMN1"
    phclass "Muscular"
  ]
  node
  [
    id 454
    name "454"
    gene "BSCL2"
    phclass "Grey"
  ]
  node
  [
    id 455
    name "455"
    gene "GARS"
    phclass "Grey"
  ]
  node
  [
    id 456
    name "456"
    gene "HEXB"
    phclass "Grey"
  ]
  node
  [
    id 457
    name "457"
    gene "IGHMBP2"
    phclass "Muscular"
  ]
  node
  [
    id 458
    name "458"
    gene "STS"
    phclass "Grey"
  ]
  node
  [
    id 459
    name "459"
    gene "KRT2A"
    phclass "Dermatological"
  ]
  node
  [
    id 460
    name "460"
    gene "ICHTHYIN"
    phclass "Dermatological"
  ]
  node
  [
    id 461
    name "461"
    gene "KRT10"
    phclass "Dermatological"
  ]
  node
  [
    id 462
    name "462"
    gene "ABCA12"
    phclass "Dermatological"
  ]
  node
  [
    id 463
    name "463"
    gene "KRT1"
    phclass "Dermatological"
  ]
  node
  [
    id 464
    name "464"
    gene "MRE11A"
    phclass "Immunological"
  ]
  node
  [
    id 465
    name "465"
    gene "RAP1GDS1"
    phclass "Cancer"
  ]
  node
  [
    id 466
    name "466"
    gene "BCL8"
    phclass "Cancer"
  ]
  node
  [
    id 467
    name "467"
    gene "FCGR2B"
    phclass "Cancer"
  ]
  node
  [
    id 468
    name "468"
    gene "SH2D1A"
    phclass "Cancer"
  ]
  node
  [
    id 469
    name "469"
    gene "ATP1A2"
    phclass "Neurological"
  ]
  node
  [
    id 470
    name "470"
    gene "EDNRA"
    phclass "Neurological"
  ]
  node
  [
    id 471
    name "471"
    gene "ATP6V1B1"
    phclass "Renal"
  ]
  node
  [
    id 472
    name "472"
    gene "ATP6V0A4"
    phclass "Renal"
  ]
  node
  [
    id 473
    name "473"
    gene "CA2"
    phclass "Renal"
  ]
  node
  [
    id 474
    name "474"
    gene "SLC4A4"
    phclass "Renal"
  ]
  node
  [
    id 475
    name "475"
    gene "ATP7A"
    phclass "Grey"
  ]
  node
  [
    id 476
    name "476"
    gene "ELN"
    phclass "Grey"
  ]
  node
  [
    id 477
    name "477"
    gene "FBLN5"
    phclass "Grey"
  ]
  node
  [
    id 478
    name "478"
    gene "BAAT"
    phclass "Gastrointestinal"
  ]
  node
  [
    id 479
    name "479"
    gene "TJP2"
    phclass "Gastrointestinal"
  ]
  node
  [
    id 480
    name "480"
    gene "BCKDHA"
    phclass "Metabolic"
  ]
  node
  [
    id 481
    name "481"
    gene "BCKDHB"
    phclass "Metabolic"
  ]
  node
  [
    id 482
    name "482"
    gene "DBT"
    phclass "Metabolic"
  ]
  node
  [
    id 483
    name "483"
    gene "GCSL"
    phclass "Grey"
  ]
  node
  [
    id 484
    name "484"
    gene "VHL"
    phclass "Grey"
  ]
  node
  [
    id 485
    name "485"
    gene "BCS1L"
    phclass "Grey"
  ]
  node
  [
    id 486
    name "486"
    gene "NDUFS3"
    phclass "Neurological"
  ]
  node
  [
    id 487
    name "487"
    gene "NDUFS4"
    phclass "Grey"
  ]
  node
  [
    id 488
    name "488"
    gene "NDUFS7"
    phclass "Neurological"
  ]
  node
  [
    id 489
    name "489"
    gene "NDUFS8"
    phclass "Neurological"
  ]
  node
  [
    id 490
    name "490"
    gene "NDUFV1"
    phclass "Grey"
  ]
  node
  [
    id 491
    name "491"
    gene "SDHA"
    phclass "Grey"
  ]
  node
  [
    id 492
    name "492"
    gene "SURF1"
    phclass "Neurological"
  ]
  node
  [
    id 493
    name "493"
    gene "LRPPRC"
    phclass "Neurological"
  ]
  node
  [
    id 494
    name "494"
    gene "PDHA1"
    phclass "Grey"
  ]
  node
  [
    id 495
    name "495"
    gene "NDUFS1"
    phclass "multiple"
  ]
  node
  [
    id 496
    name "496"
    gene "NDUFS2"
    phclass "multiple"
  ]
  node
  [
    id 497
    name "497"
    gene "UQCRB"
    phclass "multiple"
  ]
  node
  [
    id 498
    name "498"
    gene "BDNF"
    phclass "Grey"
  ]
  node
  [
    id 499
    name "499"
    gene "GDNF"
    phclass "Grey"
  ]
  node
  [
    id 500
    name "500"
    gene "EDN3"
    phclass "Grey"
  ]
  node
  [
    id 501
    name "501"
    gene "PHOX2B"
    phclass "Grey"
  ]
  node
  [
    id 502
    name "502"
    gene "RET"
    phclass "Grey"
  ]
  node
  [
    id 503
    name "503"
    gene "SLC6A4"
    phclass "Psychiatric"
  ]
  node
  [
    id 504
    name "504"
    gene "BMPR1A"
    phclass "Cancer"
  ]
  node
  [
    id 505
    name "505"
    gene "RRAS2"
    phclass "Cancer"
  ]
  node
  [
    id 506
    name "506"
    gene "EGFR"
    phclass "Cancer"
  ]
  node
  [
    id 507
    name "507"
    gene "PARK2"
    phclass "Grey"
  ]
  node
  [
    id 508
    name "508"
    gene "CDK4"
    phclass "Cancer"
  ]
  node
  [
    id 509
    name "509"
    gene "FANCA"
    phclass "multiple"
  ]
  node
  [
    id 510
    name "510"
    gene "FANCB"
    phclass "multiple"
  ]
  node
  [
    id 511
    name "511"
    gene "FANCC"
    phclass "multiple"
  ]
  node
  [
    id 512
    name "512"
    gene "FANCD2"
    phclass "multiple"
  ]
  node
  [
    id 513
    name "513"
    gene "FANCE"
    phclass "multiple"
  ]
  node
  [
    id 514
    name "514"
    gene "FANCF"
    phclass "multiple"
  ]
  node
  [
    id 515
    name "515"
    gene "FANCG"
    phclass "multiple"
  ]
  node
  [
    id 516
    name "516"
    gene "FANCL"
    phclass "multiple"
  ]
  node
  [
    id 517
    name "517"
    gene "FANCM"
    phclass "multiple"
  ]
  node
  [
    id 518
    name "518"
    gene "GPC3"
    phclass "Grey"
  ]
  node
  [
    id 519
    name "519"
    gene "POU6F2"
    phclass "Cancer"
  ]
  node
  [
    id 520
    name "520"
    gene "WT1"
    phclass "Grey"
  ]
  node
  [
    id 521
    name "521"
    gene "C1QA"
    phclass "Immunological"
  ]
  node
  [
    id 522
    name "522"
    gene "C1QB"
    phclass "Immunological"
  ]
  node
  [
    id 523
    name "523"
    gene "C1QG"
    phclass "Immunological"
  ]
  node
  [
    id 524
    name "524"
    gene "C1S"
    phclass "Immunological"
  ]
  node
  [
    id 525
    name "525"
    gene "C2"
    phclass "Immunological"
  ]
  node
  [
    id 526
    name "526"
    gene "IF"
    phclass "Immunological"
  ]
  node
  [
    id 527
    name "527"
    gene "C3"
    phclass "Immunological"
  ]
  node
  [
    id 528
    name "528"
    gene "C4A"
    phclass "Immunological"
  ]
  node
  [
    id 529
    name "529"
    gene "C4B"
    phclass "Immunological"
  ]
  node
  [
    id 530
    name "530"
    gene "C6"
    phclass "Immunological"
  ]
  node
  [
    id 531
    name "531"
    gene "C7"
    phclass "Immunological"
  ]
  node
  [
    id 532
    name "532"
    gene "C8B"
    phclass "Immunological"
  ]
  node
  [
    id 533
    name "533"
    gene "C9"
    phclass "Immunological"
  ]
  node
  [
    id 534
    name "534"
    gene "CACNA1A"
    phclass "Neurological"
  ]
  node
  [
    id 535
    name "535"
    gene "CP"
    phclass "Grey"
  ]
  node
  [
    id 536
    name "536"
    gene "KCNA1"
    phclass "Neurological"
  ]
  node
  [
    id 537
    name "537"
    gene "ATXN10"
    phclass "Neurological"
  ]
  node
  [
    id 538
    name "538"
    gene "ATXN1"
    phclass "Neurological"
  ]
  node
  [
    id 539
    name "539"
    gene "PPP2R2B"
    phclass "Neurological"
  ]
  node
  [
    id 540
    name "540"
    gene "PRKCG"
    phclass "Neurological"
  ]
  node
  [
    id 541
    name "541"
    gene "TBP"
    phclass "Neurological"
  ]
  node
  [
    id 542
    name "542"
    gene "ATXN2"
    phclass "Neurological"
  ]
  node
  [
    id 543
    name "543"
    gene "SCA25"
    phclass "Neurological"
  ]
  node
  [
    id 544
    name "544"
    gene "FGF14"
    phclass "Neurological"
  ]
  node
  [
    id 545
    name "545"
    gene "PLEKHG4"
    phclass "Neurological"
  ]
  node
  [
    id 546
    name "546"
    gene "ATXN7"
    phclass "Neurological"
  ]
  node
  [
    id 547
    name "547"
    gene "KLHL1AS"
    phclass "Neurological"
  ]
  node
  [
    id 548
    name "548"
    gene "TDP1"
    phclass "Neurological"
  ]
  node
  [
    id 549
    name "549"
    gene "CACNA1F"
    phclass "Ophthamological"
  ]
  node
  [
    id 550
    name "550"
    gene "GNAT1"
    phclass "Ophthamological"
  ]
  node
  [
    id 551
    name "551"
    gene "NYX"
    phclass "Ophthamological"
  ]
  node
  [
    id 552
    name "552"
    gene "CACNA1S"
    phclass "Grey"
  ]
  node
  [
    id 553
    name "553"
    gene "KCNE3"
    phclass "Renal"
  ]
  node
  [
    id 554
    name "554"
    gene "SCN4A"
    phclass "Grey"
  ]
  node
  [
    id 555
    name "555"
    gene "RYR1"
    phclass "Grey"
  ]
  node
  [
    id 556
    name "556"
    gene "CACNB4"
    phclass "Neurological"
  ]
  node
  [
    id 557
    name "557"
    gene "ATCAY"
    phclass "Neurological"
  ]
  node
  [
    id 558
    name "558"
    gene "APTX"
    phclass "Neurological"
  ]
  node
  [
    id 559
    name "559"
    gene "ALS4"
    phclass "Neurological"
  ]
  node
  [
    id 560
    name "560"
    gene "TTPA"
    phclass "Neurological"
  ]
  node
  [
    id 561
    name "561"
    gene "KCNQ2"
    phclass "Neurological"
  ]
  node
  [
    id 562
    name "562"
    gene "KCNQ3"
    phclass "Neurological"
  ]
  node
  [
    id 563
    name "563"
    gene "GABRG2"
    phclass "Neurological"
  ]
  node
  [
    id 564
    name "564"
    gene "CLCN2"
    phclass "Neurological"
  ]
  node
  [
    id 565
    name "565"
    gene "JRK"
    phclass "Neurological"
  ]
  node
  [
    id 566
    name "566"
    gene "SCN1A"
    phclass "Neurological"
  ]
  node
  [
    id 567
    name "567"
    gene "ME2"
    phclass "Neurological"
  ]
  node
  [
    id 568
    name "568"
    gene "GABRA1"
    phclass "Neurological"
  ]
  node
  [
    id 569
    name "569"
    gene "EPM2A"
    phclass "Neurological"
  ]
  node
  [
    id 570
    name "570"
    gene "NHLRC1"
    phclass "Neurological"
  ]
  node
  [
    id 571
    name "571"
    gene "SLC25A22"
    phclass "Neurological"
  ]
  node
  [
    id 572
    name "572"
    gene "CHRNA4"
    phclass "Grey"
  ]
  node
  [
    id 573
    name "573"
    gene "CHRNB2"
    phclass "Neurological"
  ]
  node
  [
    id 574
    name "574"
    gene "LGI1"
    phclass "Grey"
  ]
  node
  [
    id 575
    name "575"
    gene "CSTB"
    phclass "Neurological"
  ]
  node
  [
    id 576
    name "576"
    gene "SYN1"
    phclass "Neurological"
  ]
  node
  [
    id 577
    name "577"
    gene "CALCA"
    phclass "Bone"
  ]
  node
  [
    id 578
    name "578"
    gene "COL1A1"
    phclass "Grey"
  ]
  node
  [
    id 579
    name "579"
    gene "LRP5"
    phclass "Grey"
  ]
  node
  [
    id 580
    name "580"
    gene "COL1A2"
    phclass "Grey"
  ]
  node
  [
    id 581
    name "581"
    gene "CALCR"
    phclass "Bone"
  ]
  node
  [
    id 582
    name "582"
    gene "CAPN3"
    phclass "Muscular"
  ]
  node
  [
    id 583
    name "583"
    gene "FKRP"
    phclass "Muscular"
  ]
  node
  [
    id 584
    name "584"
    gene "LAMA2"
    phclass "Muscular"
  ]
  node
  [
    id 585
    name "585"
    gene "LARGE"
    phclass "Muscular"
  ]
  node
  [
    id 586
    name "586"
    gene "FCMD"
    phclass "Grey"
  ]
  node
  [
    id 587
    name "587"
    gene "MYOT"
    phclass "Muscular"
  ]
  node
  [
    id 588
    name "588"
    gene "SGCG"
    phclass "Muscular"
  ]
  node
  [
    id 589
    name "589"
    gene "SGCA"
    phclass "Muscular"
  ]
  node
  [
    id 590
    name "590"
    gene "SGCB"
    phclass "Muscular"
  ]
  node
  [
    id 591
    name "591"
    gene "TRIM32"
    phclass "Muscular"
  ]
  node
  [
    id 592
    name "592"
    gene "POMT1"
    phclass "Grey"
  ]
  node
  [
    id 593
    name "593"
    gene "SEPN1"
    phclass "Muscular"
  ]
  node
  [
    id 594
    name "594"
    gene "PLEC1"
    phclass "Grey"
  ]
  node
  [
    id 595
    name "595"
    gene "AXIN1"
    phclass "Cancer"
  ]
  node
  [
    id 596
    name "596"
    gene "IGF2R"
    phclass "Cancer"
  ]
  node
  [
    id 597
    name "597"
    gene "MET"
    phclass "Cancer"
  ]
  node
  [
    id 598
    name "598"
    gene "CASQ2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 599
    name "599"
    gene "GNAI2"
    phclass "Grey"
  ]
  node
  [
    id 600
    name "600"
    gene "RYR2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 601
    name "601"
    gene "CASR"
    phclass "Endocrine"
  ]
  node
  [
    id 602
    name "602"
    gene "MEN1"
    phclass "Grey"
  ]
  node
  [
    id 603
    name "603"
    gene "CDC73"
    phclass "Grey"
  ]
  node
  [
    id 604
    name "604"
    gene "P2RY12"
    phclass "Hematological"
  ]
  node
  [
    id 605
    name "605"
    gene "CD36"
    phclass "Grey"
  ]
  node
  [
    id 606
    name "606"
    gene "IL10"
    phclass "Grey"
  ]
  node
  [
    id 607
    name "607"
    gene "CIITA"
    phclass "Grey"
  ]
  node
  [
    id 608
    name "608"
    gene "NFKBIL1"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 609
    name "609"
    gene "PADI4"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 610
    name "610"
    gene "SLC22A4"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 611
    name "611"
    gene "MIF"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 612
    name "612"
    gene "CBFB"
    phclass "Cancer"
  ]
  node
  [
    id 613
    name "613"
    gene "ACSL6"
    phclass "Grey"
  ]
  node
  [
    id 614
    name "614"
    gene "CSF1R"
    phclass "Cancer"
  ]
  node
  [
    id 615
    name "615"
    gene "CBS"
    phclass "Grey"
  ]
  node
  [
    id 616
    name "616"
    gene "MTHFR"
    phclass "Metabolic"
  ]
  node
  [
    id 617
    name "617"
    gene "F5"
    phclass "Hematological"
  ]
  node
  [
    id 618
    name "618"
    gene "SERPIND1"
    phclass "Hematological"
  ]
  node
  [
    id 619
    name "619"
    gene "HRG"
    phclass "Hematological"
  ]
  node
  [
    id 620
    name "620"
    gene "PROC"
    phclass "Hematological"
  ]
  node
  [
    id 621
    name "621"
    gene "FGB"
    phclass "Hematological"
  ]
  node
  [
    id 622
    name "622"
    gene "FGG"
    phclass "Hematological"
  ]
  node
  [
    id 623
    name "623"
    gene "ADAMTS13"
    phclass "Hematological"
  ]
  node
  [
    id 624
    name "624"
    gene "ICAM1"
    phclass "Immunological"
  ]
  node
  [
    id 625
    name "625"
    gene "NOS2A"
    phclass "Immunological"
  ]
  node
  [
    id 626
    name "626"
    gene "MSH3"
    phclass "Cancer"
  ]
  node
  [
    id 627
    name "627"
    gene "CHAT"
    phclass "Muscular"
  ]
  node
  [
    id 628
    name "628"
    gene "CHRNB1"
    phclass "Muscular"
  ]
  node
  [
    id 629
    name "629"
    gene "CHRNE"
    phclass "Muscular"
  ]
  node
  [
    id 630
    name "630"
    gene "RAPSN"
    phclass "Muscular"
  ]
  node
  [
    id 631
    name "631"
    gene "CHRNA1"
    phclass "Muscular"
  ]
  node
  [
    id 632
    name "632"
    gene "CHRND"
    phclass "Muscular"
  ]
  node
  [
    id 633
    name "633"
    gene "CYP2A6"
    phclass "Grey"
  ]
  node
  [
    id 634
    name "634"
    gene "GPR51"
    phclass "Psychiatric"
  ]
  node
  [
    id 635
    name "635"
    gene "CLCN1"
    phclass "Muscular"
  ]
  node
  [
    id 636
    name "636"
    gene "CLCN7"
    phclass "Bone"
  ]
  node
  [
    id 637
    name "637"
    gene "OSTM1"
    phclass "Bone"
  ]
  node
  [
    id 638
    name "638"
    gene "TCIRG1"
    phclass "Bone"
  ]
  node
  [
    id 639
    name "639"
    gene "CCR2"
    phclass "Immunological"
  ]
  node
  [
    id 640
    name "640"
    gene "CCL5"
    phclass "Immunological"
  ]
  node
  [
    id 641
    name "641"
    gene "CCR5"
    phclass "Immunological"
  ]
  node
  [
    id 642
    name "642"
    gene "CNGA3"
    phclass "Ophthamological"
  ]
  node
  [
    id 643
    name "643"
    gene "CNGB3"
    phclass "Ophthamological"
  ]
  node
  [
    id 644
    name "644"
    gene "GNAT2"
    phclass "Ophthamological"
  ]
  node
  [
    id 645
    name "645"
    gene "TNXB"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 646
    name "646"
    gene "B4GALT7"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 647
    name "647"
    gene "COL5A1"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 648
    name "648"
    gene "COL5A2"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 649
    name "649"
    gene "COL3A1"
    phclass "Grey"
  ]
  node
  [
    id 650
    name "650"
    gene "PLOD1"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 651
    name "651"
    gene "ADAMTS2"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 652
    name "652"
    gene "FBN1"
    phclass "Grey"
  ]
  node
  [
    id 653
    name "653"
    gene "COL2A1"
    phclass "Grey"
  ]
  node
  [
    id 654
    name "654"
    gene "FRZB"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 655
    name "655"
    gene "ASPN"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 656
    name "656"
    gene "COL11A1"
    phclass "multiple"
  ]
  node
  [
    id 657
    name "657"
    gene "COL7A1"
    phclass "Dermatological"
  ]
  node
  [
    id 658
    name "658"
    gene "COL17A1"
    phclass "Dermatological"
  ]
  node
  [
    id 659
    name "659"
    gene "ITGB4"
    phclass "Dermatological"
  ]
  node
  [
    id 660
    name "660"
    gene "LAMA3"
    phclass "Grey"
  ]
  node
  [
    id 661
    name "661"
    gene "LAMB3"
    phclass "Dermatological"
  ]
  node
  [
    id 662
    name "662"
    gene "LAMC2"
    phclass "Dermatological"
  ]
  node
  [
    id 663
    name "663"
    gene "ITGA6"
    phclass "Dermatological"
  ]
  node
  [
    id 664
    name "664"
    gene "DSP"
    phclass "Grey"
  ]
  node
  [
    id 665
    name "665"
    gene "KRT14"
    phclass "Dermatological"
  ]
  node
  [
    id 666
    name "666"
    gene "KRT5"
    phclass "Dermatological"
  ]
  node
  [
    id 667
    name "667"
    gene "COL9A1"
    phclass "Bone"
  ]
  node
  [
    id 668
    name "668"
    gene "COMP"
    phclass "Grey"
  ]
  node
  [
    id 669
    name "669"
    gene "SLC26A2"
    phclass "Grey"
  ]
  node
  [
    id 670
    name "670"
    gene "COL9A3"
    phclass "Grey"
  ]
  node
  [
    id 671
    name "671"
    gene "COL9A2"
    phclass "Grey"
  ]
  node
  [
    id 672
    name "672"
    gene "MAP3K8"
    phclass "Cancer"
  ]
  node
  [
    id 673
    name "673"
    gene "PPP2R1B"
    phclass "Cancer"
  ]
  node
  [
    id 674
    name "674"
    gene "CPT1A"
    phclass "Metabolic"
  ]
  node
  [
    id 675
    name "675"
    gene "CREBBP"
    phclass "multiple"
  ]
  node
  [
    id 676
    name "676"
    gene "RDH12"
    phclass "Ophthamological"
  ]
  node
  [
    id 677
    name "677"
    gene "CRYAA"
    phclass "Ophthamological"
  ]
  node
  [
    id 678
    name "678"
    gene "CRYBB2"
    phclass "Ophthamological"
  ]
  node
  [
    id 679
    name "679"
    gene "PITX3"
    phclass "Ophthamological"
  ]
  node
  [
    id 680
    name "680"
    gene "BFSP2"
    phclass "Ophthamological"
  ]
  node
  [
    id 681
    name "681"
    gene "PAX6"
    phclass "Grey"
  ]
  node
  [
    id 682
    name "682"
    gene "CRYBA1"
    phclass "Ophthamological"
  ]
  node
  [
    id 683
    name "683"
    gene "CRYGC"
    phclass "Ophthamological"
  ]
  node
  [
    id 684
    name "684"
    gene "LIM2"
    phclass "Ophthamological"
  ]
  node
  [
    id 685
    name "685"
    gene "CRYGD"
    phclass "Ophthamological"
  ]
  node
  [
    id 686
    name "686"
    gene "HSF4"
    phclass "Ophthamological"
  ]
  node
  [
    id 687
    name "687"
    gene "MIP"
    phclass "Ophthamological"
  ]
  node
  [
    id 688
    name "688"
    gene "CRYBB1"
    phclass "Ophthamological"
  ]
  node
  [
    id 689
    name "689"
    gene "GJA8"
    phclass "Ophthamological"
  ]
  node
  [
    id 690
    name "690"
    gene "GJA3"
    phclass "Ophthamological"
  ]
  node
  [
    id 691
    name "691"
    gene "GATA4"
    phclass "Cardiovascular"
  ]
  node
  [
    id 692
    name "692"
    gene "GJA1"
    phclass "Grey"
  ]
  node
  [
    id 693
    name "693"
    gene "CRELD1"
    phclass "Cardiovascular"
  ]
  node
  [
    id 694
    name "694"
    gene "CTLA4"
    phclass "Endocrine"
  ]
  node
  [
    id 695
    name "695"
    gene "GC"
    phclass "Endocrine"
  ]
  node
  [
    id 696
    name "696"
    gene "SLC5A5"
    phclass "Endocrine"
  ]
  node
  [
    id 697
    name "697"
    gene "DUOX2"
    phclass "Endocrine"
  ]
  node
  [
    id 698
    name "698"
    gene "PAX8"
    phclass "Endocrine"
  ]
  node
  [
    id 699
    name "699"
    gene "TSHR"
    phclass "Grey"
  ]
  node
  [
    id 700
    name "700"
    gene "TG"
    phclass "Endocrine"
  ]
  node
  [
    id 701
    name "701"
    gene "TSHB"
    phclass "Endocrine"
  ]
  node
  [
    id 702
    name "702"
    gene "CYP1B1"
    phclass "Grey"
  ]
  node
  [
    id 703
    name "703"
    gene "MYOC"
    phclass "Ophthamological"
  ]
  node
  [
    id 704
    name "704"
    gene "OPTN"
    phclass "Ophthamological"
  ]
  node
  [
    id 705
    name "705"
    gene "OPA1"
    phclass "Ophthamological"
  ]
  node
  [
    id 706
    name "706"
    gene "DBH"
    phclass "Grey"
  ]
  node
  [
    id 707
    name "707"
    gene "NR4A2"
    phclass "Neurological"
  ]
  node
  [
    id 708
    name "708"
    gene "SNCAIP"
    phclass "Neurological"
  ]
  node
  [
    id 709
    name "709"
    gene "SNCA"
    phclass "Neurological"
  ]
  node
  [
    id 710
    name "710"
    gene "PARK7"
    phclass "Neurological"
  ]
  node
  [
    id 711
    name "711"
    gene "LRRK2"
    phclass "Neurological"
  ]
  node
  [
    id 712
    name "712"
    gene "PINK1"
    phclass "Neurological"
  ]
  node
  [
    id 713
    name "713"
    gene "UCHL1"
    phclass "Neurological"
  ]
  node
  [
    id 714
    name "714"
    gene "NDUFV2"
    phclass "Neurological"
  ]
  node
  [
    id 715
    name "715"
    gene "DCTN1"
    phclass "Neurological"
  ]
  node
  [
    id 716
    name "716"
    gene "SOD1"
    phclass "Neurological"
  ]
  node
  [
    id 717
    name "717"
    gene "ALS2"
    phclass "Neurological"
  ]
  node
  [
    id 718
    name "718"
    gene "NEFH"
    phclass "Neurological"
  ]
  node
  [
    id 719
    name "719"
    gene "PRPH"
    phclass "Neurological"
  ]
  node
  [
    id 720
    name "720"
    gene "DCX"
    phclass "Neurological"
  ]
  node
  [
    id 721
    name "721"
    gene "PAFAH1B1"
    phclass "Neurological"
  ]
  node
  [
    id 722
    name "722"
    gene "RELN"
    phclass "Neurological"
  ]
  node
  [
    id 723
    name "723"
    gene "DMBT1"
    phclass "Cancer"
  ]
  node
  [
    id 724
    name "724"
    gene "DNM2"
    phclass "Neurological"
  ]
  node
  [
    id 725
    name "725"
    gene "HSPB1"
    phclass "Neurological"
  ]
  node
  [
    id 726
    name "726"
    gene "MPZ"
    phclass "Grey"
  ]
  node
  [
    id 727
    name "727"
    gene "HOXD10"
    phclass "Grey"
  ]
  node
  [
    id 728
    name "728"
    gene "GDAP1"
    phclass "Neurological"
  ]
  node
  [
    id 729
    name "729"
    gene "PMP22"
    phclass "Grey"
  ]
  node
  [
    id 730
    name "730"
    gene "LITAF"
    phclass "Neurological"
  ]
  node
  [
    id 731
    name "731"
    gene "EGR2"
    phclass "Grey"
  ]
  node
  [
    id 732
    name "732"
    gene "NEFL"
    phclass "Neurological"
  ]
  node
  [
    id 733
    name "733"
    gene "KIF1B"
    phclass "Neurological"
  ]
  node
  [
    id 734
    name "734"
    gene "MFN2"
    phclass "Neurological"
  ]
  node
  [
    id 735
    name "735"
    gene "RAB7"
    phclass "Neurological"
  ]
  node
  [
    id 736
    name "736"
    gene "MTMR2"
    phclass "Neurological"
  ]
  node
  [
    id 737
    name "737"
    gene "SBF2"
    phclass "Neurological"
  ]
  node
  [
    id 738
    name "738"
    gene "SH3TC2"
    phclass "Neurological"
  ]
  node
  [
    id 739
    name "739"
    gene "NDRG1"
    phclass "Neurological"
  ]
  node
  [
    id 740
    name "740"
    gene "GJB1"
    phclass "Neurological"
  ]
  node
  [
    id 741
    name "741"
    gene "DSG1"
    phclass "Dermatological"
  ]
  node
  [
    id 742
    name "742"
    gene "SAT"
    phclass "Dermatological"
  ]
  node
  [
    id 743
    name "743"
    gene "PKP2"
    phclass "Cardiovascular"
  ]
  node
  [
    id 744
    name "744"
    gene "FLNB"
    phclass "Grey"
  ]
  node
  [
    id 745
    name "745"
    gene "NRTN"
    phclass "Gastrointestinal"
  ]
  node
  [
    id 746
    name "746"
    gene "EDNRB"
    phclass "Grey"
  ]
  node
  [
    id 747
    name "747"
    gene "EDA"
    phclass "Dermatological"
  ]
  node
  [
    id 748
    name "748"
    gene "EDARADD"
    phclass "Dermatological"
  ]
  node
  [
    id 749
    name "749"
    gene "IKBKG"
    phclass "Dermatological"
  ]
  node
  [
    id 750
    name "750"
    gene "NFKBIA"
    phclass "Dermatological"
  ]
  node
  [
    id 751
    name "751"
    gene "EDAR"
    phclass "Dermatological"
  ]
  node
  [
    id 752
    name "752"
    gene "PVRL1"
    phclass "Grey"
  ]
  node
  [
    id 753
    name "753"
    gene "PKP1"
    phclass "Dermatological"
  ]
  node
  [
    id 754
    name "754"
    gene "SOX10"
    phclass "Grey"
  ]
  node
  [
    id 755
    name "755"
    gene "PRX"
    phclass "multiple"
  ]
  node
  [
    id 756
    name "756"
    gene "HSPB8"
    phclass "Neurological"
  ]
  node
  [
    id 757
    name "757"
    gene "SPTLC1"
    phclass "Neurological"
  ]
  node
  [
    id 758
    name "758"
    gene "NGFB"
    phclass "Neurological"
  ]
  node
  [
    id 759
    name "759"
    gene "HSN2"
    phclass "Neurological"
  ]
  node
  [
    id 760
    name "760"
    gene "ELA2"
    phclass "Hematological"
  ]
  node
  [
    id 761
    name "761"
    gene "FCGR3A"
    phclass "Grey"
  ]
  node
  [
    id 762
    name "762"
    gene "GFI1"
    phclass "Hematological"
  ]
  node
  [
    id 763
    name "763"
    gene "WAS"
    phclass "Grey"
  ]
  node
  [
    id 764
    name "764"
    gene "EMD"
    phclass "Muscular"
  ]
  node
  [
    id 765
    name "765"
    gene "EPB41"
    phclass "Hematological"
  ]
  node
  [
    id 766
    name "766"
    gene "EYA1"
    phclass "Grey"
  ]
  node
  [
    id 767
    name "767"
    gene "FOXE3"
    phclass "Ophthamological"
  ]
  node
  [
    id 768
    name "768"
    gene "FOXC1"
    phclass "Grey"
  ]
  node
  [
    id 769
    name "769"
    gene "SERPINA1"
    phclass "Grey"
  ]
  node
  [
    id 770
    name "770"
    gene "MASTL"
    phclass "Hematological"
  ]
  node
  [
    id 771
    name "771"
    gene "MPL"
    phclass "Hematological"
  ]
  node
  [
    id 772
    name "772"
    gene "CFH"
    phclass "Grey"
  ]
  node
  [
    id 773
    name "773"
    gene "MCFD2"
    phclass "Hematological"
  ]
  node
  [
    id 774
    name "774"
    gene "F10"
    phclass "Hematological"
  ]
  node
  [
    id 775
    name "775"
    gene "F11"
    phclass "Hematological"
  ]
  node
  [
    id 776
    name "776"
    gene "F12"
    phclass "Hematological"
  ]
  node
  [
    id 777
    name "777"
    gene "F13A1"
    phclass "Hematological"
  ]
  node
  [
    id 778
    name "778"
    gene "F13B"
    phclass "Hematological"
  ]
  node
  [
    id 779
    name "779"
    gene "ADAMTS10"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 780
    name "780"
    gene "OAS1"
    phclass "Immunological"
  ]
  node
  [
    id 781
    name "781"
    gene "RAB3GAP1"
    phclass "multiple"
  ]
  node
  [
    id 782
    name "782"
    gene "FGFR1"
    phclass "Grey"
  ]
  node
  [
    id 783
    name "783"
    gene "KAL1"
    phclass "multiple"
  ]
  node
  [
    id 784
    name "784"
    gene "RB1"
    phclass "Cancer"
  ]
  node
  [
    id 785
    name "785"
    gene "HRAS"
    phclass "Grey"
  ]
  node
  [
    id 786
    name "786"
    gene "MSX2"
    phclass "Skeletal"
  ]
  node
  [
    id 787
    name "787"
    gene "TWIST1"
    phclass "Developmental"
  ]
  node
  [
    id 788
    name "788"
    gene "FGFR4"
    phclass "Cancer"
  ]
  node
  [
    id 789
    name "789"
    gene "PITX2"
    phclass "Grey"
  ]
  node
  [
    id 790
    name "790"
    gene "FOXO1A"
    phclass "Cancer"
  ]
  node
  [
    id 791
    name "791"
    gene "PAX3"
    phclass "Grey"
  ]
  node
  [
    id 792
    name "792"
    gene "PAX7"
    phclass "Cancer"
  ]
  node
  [
    id 793
    name "793"
    gene "GABRB3"
    phclass "Psychiatric"
  ]
  node
  [
    id 794
    name "794"
    gene "PRNP"
    phclass "Grey"
  ]
  node
  [
    id 795
    name "795"
    gene "EFHC1"
    phclass "Neurological"
  ]
  node
  [
    id 796
    name "796"
    gene "INS"
    phclass "Endocrine"
  ]
  node
  [
    id 797
    name "797"
    gene "GFAP"
    phclass "multiple"
  ]
  node
  [
    id 798
    name "798"
    gene "GHRHR"
    phclass "Endocrine"
  ]
  node
  [
    id 799
    name "799"
    gene "LOR"
    phclass "Grey"
  ]
  node
  [
    id 800
    name "800"
    gene "GJB4"
    phclass "Dermatological"
  ]
  node
  [
    id 801
    name "801"
    gene "GLO1"
    phclass "Psychiatric"
  ]
  node
  [
    id 802
    name "802"
    gene "NLGN3"
    phclass "Psychiatric"
  ]
  node
  [
    id 803
    name "803"
    gene "GNAS"
    phclass "Grey"
  ]
  node
  [
    id 804
    name "804"
    gene "THRA"
    phclass "Cancer"
  ]
  node
  [
    id 805
    name "805"
    gene "SSTR5"
    phclass "Endocrine"
  ]
  node
  [
    id 806
    name "806"
    gene "HD"
    phclass "Neurological"
  ]
  node
  [
    id 807
    name "807"
    gene "JPH3"
    phclass "Neurological"
  ]
  node
  [
    id 808
    name "808"
    gene "HMCN1"
    phclass "Ophthamological"
  ]
  node
  [
    id 809
    name "809"
    gene "HLA-DQB1"
    phclass "Neurological"
  ]
  node
  [
    id 810
    name "810"
    gene "MINPP1"
    phclass "Cancer"
  ]
  node
  [
    id 811
    name "811"
    gene "GOLGA5"
    phclass "Cancer"
  ]
  node
  [
    id 812
    name "812"
    gene "NCOA4"
    phclass "Cancer"
  ]
  node
  [
    id 813
    name "813"
    gene "PCM1"
    phclass "Cancer"
  ]
  node
  [
    id 814
    name "814"
    gene "PRKAR1A"
    phclass "Grey"
  ]
  node
  [
    id 815
    name "815"
    gene "TRIM33"
    phclass "Cancer"
  ]
  node
  [
    id 816
    name "816"
    gene "TRIM24"
    phclass "Cancer"
  ]
  node
  [
    id 817
    name "817"
    gene "HSPD1"
    phclass "Neurological"
  ]
  node
  [
    id 818
    name "818"
    gene "SACS"
    phclass "Neurological"
  ]
  node
  [
    id 819
    name "819"
    gene "KIF5A"
    phclass "Neurological"
  ]
  node
  [
    id 820
    name "820"
    gene "PLP1"
    phclass "Neurological"
  ]
  node
  [
    id 821
    name "821"
    gene "SPG3A"
    phclass "Neurological"
  ]
  node
  [
    id 822
    name "822"
    gene "SPAST"
    phclass "Neurological"
  ]
  node
  [
    id 823
    name "823"
    gene "NIPA1"
    phclass "Neurological"
  ]
  node
  [
    id 824
    name "824"
    gene "SPG7"
    phclass "Neurological"
  ]
  node
  [
    id 825
    name "825"
    gene "IL4R"
    phclass "Immunological"
  ]
  node
  [
    id 826
    name "826"
    gene "SPINK5"
    phclass "Grey"
  ]
  node
  [
    id 827
    name "827"
    gene "HAVCR1"
    phclass "Immunological"
  ]
  node
  [
    id 828
    name "828"
    gene "SELP"
    phclass "Immunological"
  ]
  node
  [
    id 829
    name "829"
    gene "MDS1"
    phclass "Muscular"
  ]
  node
  [
    id 830
    name "830"
    gene "JAK2"
    phclass "Hematological"
  ]
  node
  [
    id 831
    name "831"
    gene "THPO"
    phclass "Hematological"
  ]
  node
  [
    id 832
    name "832"
    gene "PDGFRA"
    phclass "Grey"
  ]
  node
  [
    id 833
    name "833"
    gene "KRT9"
    phclass "Dermatological"
  ]
  node
  [
    id 834
    name "834"
    gene "AGPAT2"
    phclass "Metabolic"
  ]
  node
  [
    id 835
    name "835"
    gene "PPARGC1A"
    phclass "Metabolic"
  ]
  node
  [
    id 836
    name "836"
    gene "HMGA2"
    phclass "Cancer"
  ]
  node
  [
    id 837
    name "837"
    gene "FZD4"
    phclass "Ophthamological"
  ]
  node
  [
    id 838
    name "838"
    gene "NDP"
    phclass "Grey"
  ]
  node
  [
    id 839
    name "839"
    gene "MAPT"
    phclass "Neurological"
  ]
  node
  [
    id 840
    name "840"
    gene "ITM2B"
    phclass "Neurological"
  ]
  node
  [
    id 841
    name "841"
    gene "SNCB"
    phclass "Neurological"
  ]
  node
  [
    id 842
    name "842"
    gene "UBE3A"
    phclass "Developmental"
  ]
  node
  [
    id 843
    name "843"
    gene "CDKL5"
    phclass "Neurological"
  ]
  node
  [
    id 844
    name "844"
    gene "RNF139"
    phclass "Cancer"
  ]
  node
  [
    id 845
    name "845"
    gene "OGG1"
    phclass "Cancer"
  ]
  node
  [
    id 846
    name "846"
    gene "PRCC"
    phclass "Cancer"
  ]
  node
  [
    id 847
    name "847"
    gene "TFE3"
    phclass "Cancer"
  ]
  node
  [
    id 848
    name "848"
    gene "TAPBP"
    phclass "Immunological"
  ]
  node
  [
    id 849
    name "849"
    gene "TAP2"
    phclass "Immunological"
  ]
  node
  [
    id 850
    name "850"
    gene "RFX5"
    phclass "Immunological"
  ]
  node
  [
    id 851
    name "851"
    gene "RFXAP"
    phclass "Immunological"
  ]
  node
  [
    id 852
    name "852"
    gene "MITF"
    phclass "multiple"
  ]
  node
  [
    id 853
    name "853"
    gene "TYR"
    phclass "Grey"
  ]
  node
  [
    id 854
    name "854"
    gene "SNAI2"
    phclass "multiple"
  ]
  node
  [
    id 855
    name "855"
    gene "SCNN1A"
    phclass "Endocrine"
  ]
  node
  [
    id 856
    name "856"
    gene "SCNN1B"
    phclass "Grey"
  ]
  node
  [
    id 857
    name "857"
    gene "SCNN1G"
    phclass "Grey"
  ]
  node
  [
    id 858
    name "858"
    gene "WNK4"
    phclass "Endocrine"
  ]
  node
  [
    id 859
    name "859"
    gene "WNK1"
    phclass "Endocrine"
  ]
  node
  [
    id 860
    name "860"
    gene "MN1"
    phclass "Cancer"
  ]
  node
  [
    id 861
    name "861"
    gene "NF2"
    phclass "Cancer"
  ]
  node
  [
    id 862
    name "862"
    gene "PDGFB"
    phclass "Cancer"
  ]
  node
  [
    id 863
    name "863"
    gene "MSX1"
    phclass "Grey"
  ]
  node
  [
    id 864
    name "864"
    gene "TBX22"
    phclass "Developmental"
  ]
  node
  [
    id 865
    name "865"
    gene "PAX9"
    phclass "Skeletal"
  ]
  node
  [
    id 866
    name "866"
    gene "ALX4"
    phclass "Skeletal"
  ]
  node
  [
    id 867
    name "867"
    gene "MYH8"
    phclass "multiple"
  ]
  node
  [
    id 868
    name "868"
    gene "USH1G"
    phclass "multiple"
  ]
  node
  [
    id 869
    name "869"
    gene "USH3A"
    phclass "multiple"
  ]
  node
  [
    id 870
    name "870"
    gene "MASS1"
    phclass "Grey"
  ]
  node
  [
    id 871
    name "871"
    gene "NME1"
    phclass "Cancer"
  ]
  node
  [
    id 872
    name "872"
    gene "PROM1"
    phclass "Ophthamological"
  ]
  node
  [
    id 873
    name "873"
    gene "C1QTNF5"
    phclass "Ophthamological"
  ]
  node
  [
    id 874
    name "874"
    gene "LRAT"
    phclass "Ophthamological"
  ]
  node
  [
    id 875
    name "875"
    gene "NTRK1"
    phclass "Grey"
  ]
  node
  [
    id 876
    name "876"
    gene "OCA2"
    phclass "Dermatological"
  ]
  node
  [
    id 877
    name "877"
    gene "TYRP1"
    phclass "Dermatological"
  ]
  node
  [
    id 878
    name "878"
    gene "OPA3"
    phclass "Grey"
  ]
  node
  [
    id 879
    name "879"
    gene "SHH"
    phclass "Grey"
  ]
  node
  [
    id 880
    name "880"
    gene "PC"
    phclass "Metabolic"
  ]
  node
  [
    id 881
    name "881"
    gene "PDHB"
    phclass "Metabolic"
  ]
  node
  [
    id 882
    name "882"
    gene "PPP1R3A"
    phclass "Metabolic"
  ]
  node
  [
    id 883
    name "883"
    gene "PTPN1"
    phclass "Metabolic"
  ]
  node
  [
    id 884
    name "884"
    gene "ALDH4A1"
    phclass "Metabolic"
  ]
  node
  [
    id 885
    name "885"
    gene "PTCH"
    phclass "Grey"
  ]
  node
  [
    id 886
    name "886"
    gene "RASA1"
    phclass "Grey"
  ]
  node
  [
    id 887
    name "887"
    gene "PTCH2"
    phclass "Cancer"
  ]
  node
  [
    id 888
    name "888"
    gene "SMO"
    phclass "Cancer"
  ]
  node
  [
    id 889
    name "889"
    gene "SIX3"
    phclass "Developmental"
  ]
  node
  [
    id 890
    name "890"
    gene "TGIF"
    phclass "Developmental"
  ]
  node
  [
    id 891
    name "891"
    gene "ZIC2"
    phclass "Developmental"
  ]
  node
  [
    id 892
    name "892"
    gene "RHD"
    phclass "Hematological"
  ]
  node
  [
    id 893
    name "893"
    gene "RNF6"
    phclass "Cancer"
  ]
  node
  [
    id 894
    name "894"
    gene "LZTS1"
    phclass "Cancer"
  ]
  node
  [
    id 895
    name "895"
    gene "WWOX"
    phclass "Cancer"
  ]
  node
  [
    id 896
    name "896"
    gene "SDHB"
    phclass "Cancer"
  ]
  node
  [
    id 897
    name "897"
    gene "SDHD"
    phclass "Cancer"
  ]
  node
  [
    id 898
    name "898"
    gene "SDHC"
    phclass "Cancer"
  ]
  node
  [
    id 899
    name "899"
    gene "TPO"
    phclass "Endocrine"
  ]
  node
  [
    id 900
    name "900"
    gene "TGFBR1"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 901
    name "901"
    gene "THRB"
    phclass "Endocrine"
  ]
  node
  [
    id 902
    name "902"
    gene "CASP12P1"
    phclass "Immunological"
  ]
  node
  [
    id 903
    name "903"
    gene "SUFU"
    phclass "Cancer"
  ]
    edge
    [
      rank 2
      source 2
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "1"
      id 1
    ]
    edge
    [
      rank 4
      source 3
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "2"
      id 2
    ]
    edge
    [
      rank 5
      source 4
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "3"
      id 3
    ]
    edge
    [
      rank 7
      source 5
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "4"
      id 4
    ]
    edge
    [
      rank 9
      source 6
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "5"
      id 5
    ]
    edge
    [
      rank 10
      source 7
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "6"
      id 6
    ]
    edge
    [
      rank 11
      source 8
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "7"
      id 7
    ]
    edge
    [
      rank 13
      source 9
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "8"
      id 8
    ]
    edge
    [
      rank 15
      source 10
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "9"
      id 9
    ]
    edge
    [
      rank 16
      source 11
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "10"
      id 10
    ]
    edge
    [
      rank 17
      source 12
      target 1
      measure 1
      diseases "Alzheimer_disease"
      name "11"
      id 11
    ]
    edge
    [
      rank 18
      source 3
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "12"
      id 12
    ]
    edge
    [
      rank 19
      source 4
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "13"
      id 13
    ]
    edge
    [
      rank 19
      source 5
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "14"
      id 14
    ]
    edge
    [
      rank 19
      source 6
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "15"
      id 15
    ]
    edge
    [
      rank 22
      source 7
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "16"
      id 16
    ]
    edge
    [
      rank 25
      source 8
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "17"
      id 17
    ]
    edge
    [
      rank 27
      source 9
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "18"
      id 18
    ]
    edge
    [
      rank 27
      source 10
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "19"
      id 19
    ]
    edge
    [
      rank 27
      source 11
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "20"
      id 20
    ]
    edge
    [
      rank 31
      source 12
      target 2
      measure 1
      diseases "Alzheimer_disease"
      name "21"
      id 21
    ]
    edge
    [
      rank 33
      source 391
      target 2
      measure 1
      diseases "Amyloidosis"
      name "22"
      id 22
    ]
    edge
    [
      rank 34
      source 392
      target 2
      measure 1
      diseases "Amyloidosis"
      name "23"
      id 23
    ]
    edge
    [
      rank 35
      source 393
      target 2
      measure 1
      diseases "Amyloidosis"
      name "24"
      id 24
    ]
    edge
    [
      rank 36
      source 394
      target 2
      measure 1
      diseases "Amyloidosis"
      name "25"
      id 25
    ]
    edge
    [
      rank 38
      source 395
      target 2
      measure 1
      diseases "Amyloidosis"
      name "26"
      id 26
    ]
    edge
    [
      rank 38
      source 416
      target 2
      measure 1
      diseases "Schizophrenia"
      name "27"
      id 27
    ]
    edge
    [
      rank 38
      source 417
      target 2
      measure 1
      diseases "Schizophrenia"
      name "28"
      id 28
    ]
    edge
    [
      rank 38
      source 418
      target 2
      measure 1
      diseases "Schizophrenia"
      name "29"
      id 29
    ]
    edge
    [
      rank 38
      source 419
      target 2
      measure 1
      diseases "Schizophrenia"
      name "30"
      id 30
    ]
    edge
    [
      rank 38
      source 420
      target 2
      measure 1
      diseases "Schizophrenia"
      name "31"
      id 31
    ]
    edge
    [
      rank 38
      source 421
      target 2
      measure 1
      diseases "Schizophrenia"
      name "32"
      id 32
    ]
    edge
    [
      rank 38
      source 422
      target 2
      measure 1
      diseases "Schizophrenia"
      name "33"
      id 33
    ]
    edge
    [
      rank 38
      source 423
      target 2
      measure 1
      diseases "Schizophrenia"
      name "34"
      id 34
    ]
    edge
    [
      rank 38
      source 4
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "35"
      id 35
    ]
    edge
    [
      rank 38
      source 5
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "36"
      id 36
    ]
    edge
    [
      rank 39
      source 6
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "37"
      id 37
    ]
    edge
    [
      rank 39
      source 7
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "38"
      id 38
    ]
    edge
    [
      rank 40
      source 8
      target 3
      measure 2
      diseases "Alzheimer_disease,Myocardial_infarction"
      name "39"
      id 39
    ]
    edge
    [
      rank 40
      source 9
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "40"
      id 40
    ]
    edge
    [
      rank 41
      source 10
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "41"
      id 41
    ]
    edge
    [
      rank 41
      source 11
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "42"
      id 42
    ]
    edge
    [
      rank 43
      source 12
      target 3
      measure 1
      diseases "Alzheimer_disease"
      name "43"
      id 43
    ]
    edge
    [
      rank 43
      source 325
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "44"
      id 44
    ]
    edge
    [
      rank 44
      source 326
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "45"
      id 45
    ]
    edge
    [
      rank 44
      source 327
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "46"
      id 46
    ]
    edge
    [
      rank 44
      source 328
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "47"
      id 47
    ]
    edge
    [
      rank 44
      source 329
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "48"
      id 48
    ]
    edge
    [
      rank 44
      source 330
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "49"
      id 49
    ]
    edge
    [
      rank 47
      source 331
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "50"
      id 50
    ]
    edge
    [
      rank 47
      source 332
      target 3
      measure 1
      diseases "Myocardial_infarction"
      name "51"
      id 51
    ]
    edge
    [
      rank 48
      source 415
      target 3
      measure 1
      diseases "Hyperlipoproteinemia"
      name "52"
      id 52
    ]
    edge
    [
      rank 48
      source 5
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "53"
      id 53
    ]
    edge
    [
      rank 49
      source 6
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "54"
      id 54
    ]
    edge
    [
      rank 49
      source 7
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "55"
      id 55
    ]
    edge
    [
      rank 49
      source 8
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "56"
      id 56
    ]
    edge
    [
      rank 49
      source 9
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "57"
      id 57
    ]
    edge
    [
      rank 49
      source 10
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "58"
      id 58
    ]
    edge
    [
      rank 50
      source 11
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "59"
      id 59
    ]
    edge
    [
      rank 50
      source 12
      target 4
      measure 1
      diseases "Alzheimer_disease"
      name "60"
      id 60
    ]
    edge
    [
      rank 50
      source 6
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "61"
      id 61
    ]
    edge
    [
      rank 51
      source 7
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "62"
      id 62
    ]
    edge
    [
      rank 51
      source 8
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "63"
      id 63
    ]
    edge
    [
      rank 51
      source 9
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "64"
      id 64
    ]
    edge
    [
      rank 54
      source 10
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "65"
      id 65
    ]
    edge
    [
      rank 55
      source 11
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "66"
      id 66
    ]
    edge
    [
      rank 55
      source 12
      target 5
      measure 1
      diseases "Alzheimer_disease"
      name "67"
      id 67
    ]
    edge
    [
      rank 55
      source 7
      target 6
      measure 1
      diseases "Alzheimer_disease"
      name "68"
      id 68
    ]
    edge
    [
      rank 56
      source 8
      target 6
      measure 1
      diseases "Alzheimer_disease"
      name "69"
      id 69
    ]
    edge
    [
      rank 56
      source 9
      target 6
      measure 1
      diseases "Alzheimer_disease"
      name "70"
      id 70
    ]
    edge
    [
      rank 57
      source 10
      target 6
      measure 1
      diseases "Alzheimer_disease"
      name "71"
      id 71
    ]
    edge
    [
      rank 61
      source 11
      target 6
      measure 1
      diseases "Alzheimer_disease"
      name "72"
      id 72
    ]
    edge
    [
      rank 61
      source 12
      target 6
      measure 1
      diseases "Alzheimer_disease"
      name "73"
      id 73
    ]
    edge
    [
      rank 61
      source 215
      target 6
      measure 1
      diseases "Hypertension"
      name "74"
      id 74
    ]
    edge
    [
      rank 62
      source 216
      target 6
      measure 1
      diseases "Hypertension"
      name "75"
      id 75
    ]
    edge
    [
      rank 63
      source 217
      target 6
      measure 1
      diseases "Hypertension"
      name "76"
      id 76
    ]
    edge
    [
      rank 63
      source 218
      target 6
      measure 1
      diseases "Hypertension"
      name "77"
      id 77
    ]
    edge
    [
      rank 64
      source 219
      target 6
      measure 1
      diseases "Hypertension"
      name "78"
      id 78
    ]
    edge
    [
      rank 65
      source 220
      target 6
      measure 1
      diseases "Hypertension"
      name "79"
      id 79
    ]
    edge
    [
      rank 66
      source 221
      target 6
      measure 1
      diseases "Hypertension"
      name "80"
      id 80
    ]
    edge
    [
      rank 67
      source 222
      target 6
      measure 1
      diseases "Hypertension"
      name "81"
      id 81
    ]
    edge
    [
      rank 67
      source 223
      target 6
      measure 1
      diseases "Hypertension"
      name "82"
      id 82
    ]
    edge
    [
      rank 67
      source 224
      target 6
      measure 1
      diseases "Hypertension"
      name "83"
      id 83
    ]
    edge
    [
      rank 68
      source 225
      target 6
      measure 1
      diseases "Hypertension"
      name "84"
      id 84
    ]
    edge
    [
      rank 71
      source 8
      target 7
      measure 1
      diseases "Alzheimer_disease"
      name "85"
      id 85
    ]
    edge
    [
      rank 73
      source 9
      target 7
      measure 1
      diseases "Alzheimer_disease"
      name "86"
      id 86
    ]
    edge
    [
      rank 73
      source 10
      target 7
      measure 1
      diseases "Alzheimer_disease"
      name "87"
      id 87
    ]
    edge
    [
      rank 73
      source 11
      target 7
      measure 1
      diseases "Alzheimer_disease"
      name "88"
      id 88
    ]
    edge
    [
      rank 73
      source 12
      target 7
      measure 1
      diseases "Alzheimer_disease"
      name "89"
      id 89
    ]
    edge
    [
      rank 73
      source 9
      target 8
      measure 1
      diseases "Alzheimer_disease"
      name "90"
      id 90
    ]
    edge
    [
      rank 74
      source 10
      target 8
      measure 1
      diseases "Alzheimer_disease"
      name "91"
      id 91
    ]
    edge
    [
      rank 75
      source 11
      target 8
      measure 1
      diseases "Alzheimer_disease"
      name "92"
      id 92
    ]
    edge
    [
      rank 75
      source 12
      target 8
      measure 1
      diseases "Alzheimer_disease"
      name "93"
      id 93
    ]
    edge
    [
      rank 76
      source 218
      target 8
      measure 1
      diseases "Renal_tubular_dysgenesis"
      name "94"
      id 94
    ]
    edge
    [
      rank 76
      source 220
      target 8
      measure 1
      diseases "Renal_tubular_dysgenesis"
      name "95"
      id 95
    ]
    edge
    [
      rank 76
      source 223
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "96"
      id 96
    ]
    edge
    [
      rank 77
      source 247
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "97"
      id 97
    ]
    edge
    [
      rank 78
      source 254
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "98"
      id 98
    ]
    edge
    [
      rank 79
      source 268
      target 8
      measure 1
      diseases "Renal_tubular_dysgenesis"
      name "99"
      id 99
    ]
    edge
    [
      rank 80
      source 302
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "100"
      id 100
    ]
    edge
    [
      rank 81
      source 303
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "101"
      id 101
    ]
    edge
    [
      rank 81
      source 304
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "102"
      id 102
    ]
    edge
    [
      rank 81
      source 305
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "103"
      id 103
    ]
    edge
    [
      rank 81
      source 306
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "104"
      id 104
    ]
    edge
    [
      rank 81
      source 307
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "105"
      id 105
    ]
    edge
    [
      rank 81
      source 308
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "106"
      id 106
    ]
    edge
    [
      rank 81
      source 309
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "107"
      id 107
    ]
    edge
    [
      rank 81
      source 310
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "108"
      id 108
    ]
    edge
    [
      rank 81
      source 311
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "109"
      id 109
    ]
    edge
    [
      rank 81
      source 312
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "110"
      id 110
    ]
    edge
    [
      rank 81
      source 313
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "111"
      id 111
    ]
    edge
    [
      rank 81
      source 314
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "112"
      id 112
    ]
    edge
    [
      rank 82
      source 315
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "113"
      id 113
    ]
    edge
    [
      rank 82
      source 316
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "114"
      id 114
    ]
    edge
    [
      rank 82
      source 317
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "115"
      id 115
    ]
    edge
    [
      rank 83
      source 318
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "116"
      id 116
    ]
    edge
    [
      rank 84
      source 319
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "117"
      id 117
    ]
    edge
    [
      rank 86
      source 320
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "118"
      id 118
    ]
    edge
    [
      rank 87
      source 321
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "119"
      id 119
    ]
    edge
    [
      rank 87
      source 322
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "120"
      id 120
    ]
    edge
    [
      rank 87
      source 323
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "121"
      id 121
    ]
    edge
    [
      rank 88
      source 324
      target 8
      measure 1
      diseases "Diabetes_mellitus"
      name "122"
      id 122
    ]
    edge
    [
      rank 88
      source 325
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "123"
      id 123
    ]
    edge
    [
      rank 88
      source 326
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "124"
      id 124
    ]
    edge
    [
      rank 88
      source 327
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "125"
      id 125
    ]
    edge
    [
      rank 88
      source 328
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "126"
      id 126
    ]
    edge
    [
      rank 88
      source 329
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "127"
      id 127
    ]
    edge
    [
      rank 90
      source 330
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "128"
      id 128
    ]
    edge
    [
      rank 90
      source 331
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "129"
      id 129
    ]
    edge
    [
      rank 90
      source 332
      target 8
      measure 1
      diseases "Myocardial_infarction"
      name "130"
      id 130
    ]
    edge
    [
      rank 91
      source 10
      target 9
      measure 1
      diseases "Alzheimer_disease"
      name "131"
      id 131
    ]
    edge
    [
      rank 91
      source 11
      target 9
      measure 1
      diseases "Alzheimer_disease"
      name "132"
      id 132
    ]
    edge
    [
      rank 91
      source 12
      target 9
      measure 1
      diseases "Alzheimer_disease"
      name "133"
      id 133
    ]
    edge
    [
      rank 91
      source 11
      target 10
      measure 1
      diseases "Alzheimer_disease"
      name "134"
      id 134
    ]
    edge
    [
      rank 91
      source 12
      target 10
      measure 1
      diseases "Alzheimer_disease"
      name "135"
      id 135
    ]
    edge
    [
      rank 91
      source 12
      target 11
      measure 1
      diseases "Alzheimer_disease"
      name "136"
      id 136
    ]
    edge
    [
      rank 92
      source 237
      target 12
      measure 1
      diseases "Dementia"
      name "137"
      id 137
    ]
    edge
    [
      rank 92
      source 709
      target 12
      measure 1
      diseases "Dementia"
      name "138"
      id 138
    ]
    edge
    [
      rank 92
      source 839
      target 12
      measure 1
      diseases "Dementia"
      name "139"
      id 139
    ]
    edge
    [
      rank 92
      source 840
      target 12
      measure 1
      diseases "Dementia"
      name "140"
      id 140
    ]
    edge
    [
      rank 92
      source 841
      target 12
      measure 1
      diseases "Dementia"
      name "141"
      id 141
    ]
    edge
    [
      rank 93
      source 14
      target 13
      measure 1
      diseases "Cerebral_amyloid_angiopathy"
      name "142"
      id 142
    ]
    edge
    [
      rank 94
      source 15
      target 13
      measure 1
      diseases "Coronary_artery_disease"
      name "143"
      id 143
    ]
    edge
    [
      rank 95
      source 16
      target 13
      measure 1
      diseases "Coronary_artery_disease"
      name "144"
      id 144
    ]
    edge
    [
      rank 96
      source 17
      target 13
      measure 1
      diseases "Coronary_artery_disease"
      name "145"
      id 145
    ]
    edge
    [
      rank 96
      source 18
      target 13
      measure 1
      diseases "Coronary_artery_disease"
      name "146"
      id 146
    ]
    edge
    [
      rank 97
      source 19
      target 13
      measure 1
      diseases "Coronary_artery_disease"
      name "147"
      id 147
    ]
    edge
    [
      rank 97
      source 20
      target 13
      measure 1
      diseases "HDL_cholesterol_level_QTL"
      name "148"
      id 148
    ]
    edge
    [
      rank 97
      source 16
      target 15
      measure 1
      diseases "Coronary_artery_disease"
      name "149"
      id 149
    ]
    edge
    [
      rank 97
      source 17
      target 15
      measure 1
      diseases "Coronary_artery_disease"
      name "150"
      id 150
    ]
    edge
    [
      rank 97
      source 18
      target 15
      measure 1
      diseases "Coronary_artery_disease"
      name "151"
      id 151
    ]
    edge
    [
      rank 98
      source 19
      target 15
      measure 1
      diseases "Coronary_artery_disease"
      name "152"
      id 152
    ]
    edge
    [
      rank 99
      source 17
      target 16
      measure 1
      diseases "Coronary_artery_disease"
      name "153"
      id 153
    ]
    edge
    [
      rank 100
      source 18
      target 16
      measure 1
      diseases "Coronary_artery_disease"
      name "154"
      id 154
    ]
    edge
    [
      rank 102
      source 19
      target 16
      measure 1
      diseases "Coronary_artery_disease"
      name "155"
      id 155
    ]
    edge
    [
      rank 103
      source 18
      target 17
      measure 1
      diseases "Coronary_artery_disease"
      name "156"
      id 156
    ]
    edge
    [
      rank 103
      source 19
      target 17
      measure 1
      diseases "Coronary_artery_disease"
      name "157"
      id 157
    ]
    edge
    [
      rank 104
      source 19
      target 18
      measure 1
      diseases "Coronary_artery_disease"
      name "158"
      id 158
    ]
    edge
    [
      rank 106
      source 237
      target 20
      measure 1
      diseases "Migraine"
      name "159"
      id 159
    ]
    edge
    [
      rank 106
      source 469
      target 20
      measure 1
      diseases "Migraine"
      name "160"
      id 160
    ]
    edge
    [
      rank 107
      source 470
      target 20
      measure 1
      diseases "Migraine"
      name "161"
      id 161
    ]
    edge
    [
      rank 108
      source 22
      target 21
      measure 1
      diseases "Anemia"
      name "162"
      id 162
    ]
    edge
    [
      rank 109
      source 23
      target 21
      measure 1
      diseases "Anemia"
      name "163"
      id 163
    ]
    edge
    [
      rank 109
      source 24
      target 21
      measure 1
      diseases "Anemia"
      name "164"
      id 164
    ]
    edge
    [
      rank 109
      source 25
      target 21
      measure 1
      diseases "Anemia"
      name "165"
      id 165
    ]
    edge
    [
      rank 109
      source 26
      target 21
      measure 1
      diseases "Anemia"
      name "166"
      id 166
    ]
    edge
    [
      rank 109
      source 27
      target 21
      measure 1
      diseases "Anemia"
      name "167"
      id 167
    ]
    edge
    [
      rank 110
      source 28
      target 21
      measure 1
      diseases "Anemia"
      name "168"
      id 168
    ]
    edge
    [
      rank 110
      source 29
      target 21
      measure 1
      diseases "Anemia"
      name "169"
      id 169
    ]
    edge
    [
      rank 110
      source 23
      target 22
      measure 1
      diseases "Anemia"
      name "170"
      id 170
    ]
    edge
    [
      rank 110
      source 24
      target 22
      measure 1
      diseases "Anemia"
      name "171"
      id 171
    ]
    edge
    [
      rank 110
      source 25
      target 22
      measure 1
      diseases "Anemia"
      name "172"
      id 172
    ]
    edge
    [
      rank 110
      source 26
      target 22
      measure 1
      diseases "Anemia"
      name "173"
      id 173
    ]
    edge
    [
      rank 111
      source 27
      target 22
      measure 1
      diseases "Anemia"
      name "174"
      id 174
    ]
    edge
    [
      rank 111
      source 28
      target 22
      measure 1
      diseases "Anemia"
      name "175"
      id 175
    ]
    edge
    [
      rank 111
      source 29
      target 22
      measure 1
      diseases "Anemia"
      name "176"
      id 176
    ]
    edge
    [
      rank 111
      source 24
      target 23
      measure 1
      diseases "Anemia"
      name "177"
      id 177
    ]
    edge
    [
      rank 111
      source 25
      target 23
      measure 1
      diseases "Anemia"
      name "178"
      id 178
    ]
    edge
    [
      rank 111
      source 26
      target 23
      measure 1
      diseases "Anemia"
      name "179"
      id 179
    ]
    edge
    [
      rank 111
      source 27
      target 23
      measure 1
      diseases "Anemia"
      name "180"
      id 180
    ]
    edge
    [
      rank 111
      source 28
      target 23
      measure 1
      diseases "Anemia"
      name "181"
      id 181
    ]
    edge
    [
      rank 112
      source 29
      target 23
      measure 1
      diseases "Anemia"
      name "182"
      id 182
    ]
    edge
    [
      rank 113
      source 25
      target 24
      measure 1
      diseases "Anemia"
      name "183"
      id 183
    ]
    edge
    [
      rank 114
      source 26
      target 24
      measure 1
      diseases "Anemia"
      name "184"
      id 184
    ]
    edge
    [
      rank 114
      source 27
      target 24
      measure 1
      diseases "Anemia"
      name "185"
      id 185
    ]
    edge
    [
      rank 114
      source 28
      target 24
      measure 1
      diseases "Anemia"
      name "186"
      id 186
    ]
    edge
    [
      rank 114
      source 29
      target 24
      measure 1
      diseases "Anemia"
      name "187"
      id 187
    ]
    edge
    [
      rank 114
      source 26
      target 25
      measure 1
      diseases "Anemia"
      name "188"
      id 188
    ]
    edge
    [
      rank 116
      source 27
      target 25
      measure 1
      diseases "Anemia"
      name "189"
      id 189
    ]
    edge
    [
      rank 118
      source 28
      target 25
      measure 1
      diseases "Anemia"
      name "190"
      id 190
    ]
    edge
    [
      rank 118
      source 29
      target 25
      measure 1
      diseases "Anemia"
      name "191"
      id 191
    ]
    edge
    [
      rank 119
      source 27
      target 26
      measure 1
      diseases "Anemia"
      name "192"
      id 192
    ]
    edge
    [
      rank 119
      source 28
      target 26
      measure 1
      diseases "Anemia"
      name "193"
      id 193
    ]
    edge
    [
      rank 120
      source 29
      target 26
      measure 1
      diseases "Anemia"
      name "194"
      id 194
    ]
    edge
    [
      rank 121
      source 28
      target 27
      measure 1
      diseases "Anemia"
      name "195"
      id 195
    ]
    edge
    [
      rank 121
      source 29
      target 27
      measure 1
      diseases "Anemia"
      name "196"
      id 196
    ]
    edge
    [
      rank 122
      source 29
      target 28
      measure 1
      diseases "Anemia"
      name "197"
      id 197
    ]
    edge
    [
      rank 123
      source 108
      target 28
      measure 2
      diseases "Elliptocytosis,Spherocytosis"
      name "198"
      id 198
    ]
    edge
    [
      rank 123
      source 337
      target 28
      measure 1
      diseases "Spherocytosis"
      name "199"
      id 199
    ]
    edge
    [
      rank 123
      source 338
      target 28
      measure 1
      diseases "Spherocytosis"
      name "200"
      id 200
    ]
    edge
    [
      rank 123
      source 339
      target 28
      measure 2
      diseases "Elliptocytosis,Spherocytosis"
      name "201"
      id 201
    ]
    edge
    [
      rank 123
      source 765
      target 28
      measure 1
      diseases "Elliptocytosis"
      name "202"
      id 202
    ]
    edge
    [
      rank 124
      source 31
      target 30
      measure 2
      diseases "Cone_dystrophy,Retinitis_pigmentosa"
      name "203"
      id 203
    ]
    edge
    [
      rank 125
      source 32
      target 30
      measure 1
      diseases "Cone_dystrophy"
      name "204"
      id 204
    ]
    edge
    [
      rank 125
      source 33
      target 30
      measure 2
      diseases "Cone_dystrophy,Retinitis_pigmentosa"
      name "205"
      id 205
    ]
    edge
    [
      rank 127
      source 34
      target 30
      measure 1
      diseases "Cone_dystrophy"
      name "206"
      id 206
    ]
    edge
    [
      rank 127
      source 35
      target 30
      measure 1
      diseases "Cone_dystrophy"
      name "207"
      id 207
    ]
    edge
    [
      rank 128
      source 36
      target 30
      measure 2
      diseases "Cone_dystrophy,Retinitis_pigmentosa"
      name "208"
      id 208
    ]
    edge
    [
      rank 129
      source 37
      target 30
      measure 1
      diseases "Fundus_albipunctatus"
      name "209"
      id 209
    ]
    edge
    [
      rank 129
      source 38
      target 30
      measure 2
      diseases "Fundus_albipunctatus,Retinitis_pigmentosa"
      name "210"
      id 210
    ]
    edge
    [
      rank 129
      source 39
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "211"
      id 211
    ]
    edge
    [
      rank 129
      source 40
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "212"
      id 212
    ]
    edge
    [
      rank 130
      source 41
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "213"
      id 213
    ]
    edge
    [
      rank 130
      source 42
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "214"
      id 214
    ]
    edge
    [
      rank 130
      source 43
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "215"
      id 215
    ]
    edge
    [
      rank 130
      source 44
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "216"
      id 216
    ]
    edge
    [
      rank 130
      source 45
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "217"
      id 217
    ]
    edge
    [
      rank 130
      source 46
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "218"
      id 218
    ]
    edge
    [
      rank 130
      source 47
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "219"
      id 219
    ]
    edge
    [
      rank 131
      source 48
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "220"
      id 220
    ]
    edge
    [
      rank 131
      source 49
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "221"
      id 221
    ]
    edge
    [
      rank 131
      source 50
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "222"
      id 222
    ]
    edge
    [
      rank 131
      source 51
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "223"
      id 223
    ]
    edge
    [
      rank 131
      source 52
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "224"
      id 224
    ]
    edge
    [
      rank 131
      source 53
      target 30
      measure 2
      diseases "Retinitis_pigmentosa,Macular_dystrophy"
      name "225"
      id 225
    ]
    edge
    [
      rank 131
      source 54
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "226"
      id 226
    ]
    edge
    [
      rank 132
      source 55
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "227"
      id 227
    ]
    edge
    [
      rank 132
      source 56
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "228"
      id 228
    ]
    edge
    [
      rank 133
      source 57
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "229"
      id 229
    ]
    edge
    [
      rank 133
      source 58
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "230"
      id 230
    ]
    edge
    [
      rank 133
      source 59
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "231"
      id 231
    ]
    edge
    [
      rank 133
      source 60
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "232"
      id 232
    ]
    edge
    [
      rank 133
      source 61
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "233"
      id 233
    ]
    edge
    [
      rank 134
      source 62
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "234"
      id 234
    ]
    edge
    [
      rank 134
      source 63
      target 30
      measure 1
      diseases "Retinitis_pigmentosa"
      name "235"
      id 235
    ]
    edge
    [
      rank 134
      source 64
      target 30
      measure 2
      diseases "Stargardt_disease,Macular_dystrophy"
      name "236"
      id 236
    ]
    edge
    [
      rank 134
      source 65
      target 30
      measure 1
      diseases "Macular_dystrophy"
      name "237"
      id 237
    ]
    edge
    [
      rank 134
      source 66
      target 30
      measure 1
      diseases "Macular_dystrophy"
      name "238"
      id 238
    ]
    edge
    [
      rank 134
      source 32
      target 31
      measure 1
      diseases "Cone_dystrophy"
      name "239"
      id 239
    ]
    edge
    [
      rank 134
      source 33
      target 31
      measure 2
      diseases "Cone_dystrophy,Retinitis_pigmentosa"
      name "240"
      id 240
    ]
    edge
    [
      rank 134
      source 34
      target 31
      measure 1
      diseases "Cone_dystrophy"
      name "241"
      id 241
    ]
    edge
    [
      rank 134
      source 35
      target 31
      measure 1
      diseases "Cone_dystrophy"
      name "242"
      id 242
    ]
    edge
    [
      rank 134
      source 36
      target 31
      measure 2
      diseases "Cone_dystrophy,Retinitis_pigmentosa"
      name "243"
      id 243
    ]
    edge
    [
      rank 134
      source 38
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "244"
      id 244
    ]
    edge
    [
      rank 134
      source 39
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "245"
      id 245
    ]
    edge
    [
      rank 134
      source 40
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "246"
      id 246
    ]
    edge
    [
      rank 135
      source 41
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "247"
      id 247
    ]
    edge
    [
      rank 135
      source 42
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "248"
      id 248
    ]
    edge
    [
      rank 135
      source 43
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "249"
      id 249
    ]
    edge
    [
      rank 135
      source 44
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "250"
      id 250
    ]
    edge
    [
      rank 135
      source 45
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "251"
      id 251
    ]
    edge
    [
      rank 135
      source 46
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "252"
      id 252
    ]
    edge
    [
      rank 136
      source 47
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "253"
      id 253
    ]
    edge
    [
      rank 136
      source 48
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "254"
      id 254
    ]
    edge
    [
      rank 136
      source 49
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "255"
      id 255
    ]
    edge
    [
      rank 136
      source 50
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "256"
      id 256
    ]
    edge
    [
      rank 136
      source 51
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "257"
      id 257
    ]
    edge
    [
      rank 136
      source 52
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "258"
      id 258
    ]
    edge
    [
      rank 136
      source 53
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "259"
      id 259
    ]
    edge
    [
      rank 136
      source 54
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "260"
      id 260
    ]
    edge
    [
      rank 136
      source 55
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "261"
      id 261
    ]
    edge
    [
      rank 136
      source 56
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "262"
      id 262
    ]
    edge
    [
      rank 136
      source 57
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "263"
      id 263
    ]
    edge
    [
      rank 136
      source 58
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "264"
      id 264
    ]
    edge
    [
      rank 136
      source 59
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "265"
      id 265
    ]
    edge
    [
      rank 136
      source 60
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "266"
      id 266
    ]
    edge
    [
      rank 137
      source 61
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "267"
      id 267
    ]
    edge
    [
      rank 137
      source 62
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "268"
      id 268
    ]
    edge
    [
      rank 137
      source 63
      target 31
      measure 1
      diseases "Retinitis_pigmentosa"
      name "269"
      id 269
    ]
    edge
    [
      rank 137
      source 477
      target 31
      measure 1
      diseases "Macular_degeneration"
      name "270"
      id 270
    ]
    edge
    [
      rank 137
      source 643
      target 31
      measure 1
      diseases "Macular_degeneration"
      name "271"
      id 271
    ]
    edge
    [
      rank 138
      source 772
      target 31
      measure 1
      diseases "Macular_degeneration"
      name "272"
      id 272
    ]
    edge
    [
      rank 140
      source 808
      target 31
      measure 1
      diseases "Macular_degeneration"
      name "273"
      id 273
    ]
    edge
    [
      rank 143
      source 33
      target 32
      measure 1
      diseases "Cone_dystrophy"
      name "274"
      id 274
    ]
    edge
    [
      rank 145
      source 34
      target 32
      measure 1
      diseases "Cone_dystrophy"
      name "275"
      id 275
    ]
    edge
    [
      rank 146
      source 35
      target 32
      measure 1
      diseases "Cone_dystrophy"
      name "276"
      id 276
    ]
    edge
    [
      rank 147
      source 36
      target 32
      measure 1
      diseases "Cone_dystrophy"
      name "277"
      id 277
    ]
    edge
    [
      rank 148
      source 34
      target 33
      measure 2
      diseases "Cone_dystrophy,Leber_congenital_amaurosis"
      name "278"
      id 278
    ]
    edge
    [
      rank 148
      source 35
      target 33
      measure 2
      diseases "Cone_dystrophy,Leber_congenital_amaurosis"
      name "279"
      id 279
    ]
    edge
    [
      rank 150
      source 36
      target 33
      measure 3
      diseases "Cone_dystrophy,Leber_congenital_amaurosis,Retinitis_pigmentosa"
      name "280"
      id 280
    ]
    edge
    [
      rank 153
      source 38
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "281"
      id 281
    ]
    edge
    [
      rank 153
      source 39
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "282"
      id 282
    ]
    edge
    [
      rank 154
      source 40
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "283"
      id 283
    ]
    edge
    [
      rank 155
      source 41
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "284"
      id 284
    ]
    edge
    [
      rank 156
      source 42
      target 33
      measure 2
      diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
      name "285"
      id 285
    ]
    edge
    [
      rank 156
      source 43
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "286"
      id 286
    ]
    edge
    [
      rank 157
      source 44
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "287"
      id 287
    ]
    edge
    [
      rank 157
      source 45
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "288"
      id 288
    ]
    edge
    [
      rank 157
      source 46
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "289"
      id 289
    ]
    edge
    [
      rank 157
      source 47
      target 33
      measure 2
      diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
      name "290"
      id 290
    ]
    edge
    [
      rank 157
      source 48
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "291"
      id 291
    ]
    edge
    [
      rank 157
      source 49
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "292"
      id 292
    ]
    edge
    [
      rank 157
      source 50
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "293"
      id 293
    ]
    edge
    [
      rank 157
      source 51
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "294"
      id 294
    ]
    edge
    [
      rank 157
      source 52
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "295"
      id 295
    ]
    edge
    [
      rank 158
      source 53
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "296"
      id 296
    ]
    edge
    [
      rank 158
      source 54
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "297"
      id 297
    ]
    edge
    [
      rank 159
      source 55
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "298"
      id 298
    ]
    edge
    [
      rank 159
      source 56
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "299"
      id 299
    ]
    edge
    [
      rank 159
      source 57
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "300"
      id 300
    ]
    edge
    [
      rank 160
      source 58
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "301"
      id 301
    ]
    edge
    [
      rank 160
      source 59
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "302"
      id 302
    ]
    edge
    [
      rank 160
      source 60
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "303"
      id 303
    ]
    edge
    [
      rank 160
      source 61
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "304"
      id 304
    ]
    edge
    [
      rank 160
      source 62
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "305"
      id 305
    ]
    edge
    [
      rank 161
      source 63
      target 33
      measure 1
      diseases "Retinitis_pigmentosa"
      name "306"
      id 306
    ]
    edge
    [
      rank 161
      source 676
      target 33
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "307"
      id 307
    ]
    edge
    [
      rank 162
      source 35
      target 34
      measure 2
      diseases "Cone_dystrophy,Leber_congenital_amaurosis"
      name "308"
      id 308
    ]
    edge
    [
      rank 162
      source 36
      target 34
      measure 2
      diseases "Cone_dystrophy,Leber_congenital_amaurosis"
      name "309"
      id 309
    ]
    edge
    [
      rank 162
      source 42
      target 34
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "310"
      id 310
    ]
    edge
    [
      rank 164
      source 47
      target 34
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "311"
      id 311
    ]
    edge
    [
      rank 164
      source 676
      target 34
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "312"
      id 312
    ]
    edge
    [
      rank 165
      source 36
      target 35
      measure 2
      diseases "Cone_dystrophy,Leber_congenital_amaurosis"
      name "313"
      id 313
    ]
    edge
    [
      rank 165
      source 42
      target 35
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "314"
      id 314
    ]
    edge
    [
      rank 166
      source 47
      target 35
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "315"
      id 315
    ]
    edge
    [
      rank 167
      source 676
      target 35
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "316"
      id 316
    ]
    edge
    [
      rank 168
      source 38
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "317"
      id 317
    ]
    edge
    [
      rank 168
      source 39
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "318"
      id 318
    ]
    edge
    [
      rank 169
      source 40
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "319"
      id 319
    ]
    edge
    [
      rank 170
      source 41
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "320"
      id 320
    ]
    edge
    [
      rank 170
      source 42
      target 36
      measure 2
      diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
      name "321"
      id 321
    ]
    edge
    [
      rank 170
      source 43
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "322"
      id 322
    ]
    edge
    [
      rank 170
      source 44
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "323"
      id 323
    ]
    edge
    [
      rank 170
      source 45
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "324"
      id 324
    ]
    edge
    [
      rank 170
      source 46
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "325"
      id 325
    ]
    edge
    [
      rank 171
      source 47
      target 36
      measure 2
      diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
      name "326"
      id 326
    ]
    edge
    [
      rank 171
      source 48
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "327"
      id 327
    ]
    edge
    [
      rank 172
      source 49
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "328"
      id 328
    ]
    edge
    [
      rank 172
      source 50
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "329"
      id 329
    ]
    edge
    [
      rank 173
      source 51
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "330"
      id 330
    ]
    edge
    [
      rank 173
      source 52
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "331"
      id 331
    ]
    edge
    [
      rank 173
      source 53
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "332"
      id 332
    ]
    edge
    [
      rank 173
      source 54
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "333"
      id 333
    ]
    edge
    [
      rank 173
      source 55
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "334"
      id 334
    ]
    edge
    [
      rank 173
      source 56
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "335"
      id 335
    ]
    edge
    [
      rank 173
      source 57
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "336"
      id 336
    ]
    edge
    [
      rank 174
      source 58
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "337"
      id 337
    ]
    edge
    [
      rank 174
      source 59
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "338"
      id 338
    ]
    edge
    [
      rank 175
      source 60
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "339"
      id 339
    ]
    edge
    [
      rank 175
      source 61
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "340"
      id 340
    ]
    edge
    [
      rank 175
      source 62
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "341"
      id 341
    ]
    edge
    [
      rank 175
      source 63
      target 36
      measure 1
      diseases "Retinitis_pigmentosa"
      name "342"
      id 342
    ]
    edge
    [
      rank 175
      source 676
      target 36
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "343"
      id 343
    ]
    edge
    [
      rank 176
      source 38
      target 37
      measure 1
      diseases "Fundus_albipunctatus"
      name "344"
      id 344
    ]
    edge
    [
      rank 176
      source 39
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "345"
      id 345
    ]
    edge
    [
      rank 177
      source 40
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "346"
      id 346
    ]
    edge
    [
      rank 179
      source 41
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "347"
      id 347
    ]
    edge
    [
      rank 180
      source 42
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "348"
      id 348
    ]
    edge
    [
      rank 182
      source 43
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "349"
      id 349
    ]
    edge
    [
      rank 183
      source 44
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "350"
      id 350
    ]
    edge
    [
      rank 183
      source 45
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "351"
      id 351
    ]
    edge
    [
      rank 183
      source 46
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "352"
      id 352
    ]
    edge
    [
      rank 184
      source 47
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "353"
      id 353
    ]
    edge
    [
      rank 185
      source 48
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "354"
      id 354
    ]
    edge
    [
      rank 185
      source 49
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "355"
      id 355
    ]
    edge
    [
      rank 185
      source 50
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "356"
      id 356
    ]
    edge
    [
      rank 185
      source 51
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "357"
      id 357
    ]
    edge
    [
      rank 186
      source 52
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "358"
      id 358
    ]
    edge
    [
      rank 186
      source 53
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "359"
      id 359
    ]
    edge
    [
      rank 186
      source 54
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "360"
      id 360
    ]
    edge
    [
      rank 186
      source 55
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "361"
      id 361
    ]
    edge
    [
      rank 186
      source 56
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "362"
      id 362
    ]
    edge
    [
      rank 187
      source 57
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "363"
      id 363
    ]
    edge
    [
      rank 187
      source 58
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "364"
      id 364
    ]
    edge
    [
      rank 187
      source 59
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "365"
      id 365
    ]
    edge
    [
      rank 187
      source 60
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "366"
      id 366
    ]
    edge
    [
      rank 187
      source 61
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "367"
      id 367
    ]
    edge
    [
      rank 187
      source 62
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "368"
      id 368
    ]
    edge
    [
      rank 188
      source 63
      target 38
      measure 1
      diseases "Retinitis_pigmentosa"
      name "369"
      id 369
    ]
    edge
    [
      rank 188
      source 40
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "370"
      id 370
    ]
    edge
    [
      rank 188
      source 41
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "371"
      id 371
    ]
    edge
    [
      rank 188
      source 42
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "372"
      id 372
    ]
    edge
    [
      rank 188
      source 43
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "373"
      id 373
    ]
    edge
    [
      rank 188
      source 44
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "374"
      id 374
    ]
    edge
    [
      rank 188
      source 45
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "375"
      id 375
    ]
    edge
    [
      rank 188
      source 46
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "376"
      id 376
    ]
    edge
    [
      rank 188
      source 47
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "377"
      id 377
    ]
    edge
    [
      rank 189
      source 48
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "378"
      id 378
    ]
    edge
    [
      rank 190
      source 49
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "379"
      id 379
    ]
    edge
    [
      rank 191
      source 50
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "380"
      id 380
    ]
    edge
    [
      rank 191
      source 51
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "381"
      id 381
    ]
    edge
    [
      rank 191
      source 52
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "382"
      id 382
    ]
    edge
    [
      rank 192
      source 53
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "383"
      id 383
    ]
    edge
    [
      rank 192
      source 54
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "384"
      id 384
    ]
    edge
    [
      rank 192
      source 55
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "385"
      id 385
    ]
    edge
    [
      rank 193
      source 56
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "386"
      id 386
    ]
    edge
    [
      rank 193
      source 57
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "387"
      id 387
    ]
    edge
    [
      rank 193
      source 58
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "388"
      id 388
    ]
    edge
    [
      rank 196
      source 59
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "389"
      id 389
    ]
    edge
    [
      rank 196
      source 60
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "390"
      id 390
    ]
    edge
    [
      rank 196
      source 61
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "391"
      id 391
    ]
    edge
    [
      rank 197
      source 62
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "392"
      id 392
    ]
    edge
    [
      rank 197
      source 63
      target 39
      measure 1
      diseases "Retinitis_pigmentosa"
      name "393"
      id 393
    ]
    edge
    [
      rank 198
      source 41
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "394"
      id 394
    ]
    edge
    [
      rank 198
      source 42
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "395"
      id 395
    ]
    edge
    [
      rank 199
      source 43
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "396"
      id 396
    ]
    edge
    [
      rank 199
      source 44
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "397"
      id 397
    ]
    edge
    [
      rank 200
      source 45
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "398"
      id 398
    ]
    edge
    [
      rank 201
      source 46
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "399"
      id 399
    ]
    edge
    [
      rank 202
      source 47
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "400"
      id 400
    ]
    edge
    [
      rank 203
      source 48
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "401"
      id 401
    ]
    edge
    [
      rank 204
      source 49
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "402"
      id 402
    ]
    edge
    [
      rank 205
      source 50
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "403"
      id 403
    ]
    edge
    [
      rank 207
      source 51
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "404"
      id 404
    ]
    edge
    [
      rank 208
      source 52
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "405"
      id 405
    ]
    edge
    [
      rank 208
      source 53
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "406"
      id 406
    ]
    edge
    [
      rank 208
      source 54
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "407"
      id 407
    ]
    edge
    [
      rank 208
      source 55
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "408"
      id 408
    ]
    edge
    [
      rank 208
      source 56
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "409"
      id 409
    ]
    edge
    [
      rank 208
      source 57
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "410"
      id 410
    ]
    edge
    [
      rank 208
      source 58
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "411"
      id 411
    ]
    edge
    [
      rank 209
      source 59
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "412"
      id 412
    ]
    edge
    [
      rank 209
      source 60
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "413"
      id 413
    ]
    edge
    [
      rank 209
      source 61
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "414"
      id 414
    ]
    edge
    [
      rank 209
      source 62
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "415"
      id 415
    ]
    edge
    [
      rank 209
      source 63
      target 40
      measure 1
      diseases "Retinitis_pigmentosa"
      name "416"
      id 416
    ]
    edge
    [
      rank 211
      source 42
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "417"
      id 417
    ]
    edge
    [
      rank 213
      source 43
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "418"
      id 418
    ]
    edge
    [
      rank 214
      source 44
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "419"
      id 419
    ]
    edge
    [
      rank 214
      source 45
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "420"
      id 420
    ]
    edge
    [
      rank 215
      source 46
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "421"
      id 421
    ]
    edge
    [
      rank 215
      source 47
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "422"
      id 422
    ]
    edge
    [
      rank 215
      source 48
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "423"
      id 423
    ]
    edge
    [
      rank 215
      source 49
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "424"
      id 424
    ]
    edge
    [
      rank 215
      source 50
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "425"
      id 425
    ]
    edge
    [
      rank 215
      source 51
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "426"
      id 426
    ]
    edge
    [
      rank 216
      source 52
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "427"
      id 427
    ]
    edge
    [
      rank 216
      source 53
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "428"
      id 428
    ]
    edge
    [
      rank 217
      source 54
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "429"
      id 429
    ]
    edge
    [
      rank 217
      source 55
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "430"
      id 430
    ]
    edge
    [
      rank 217
      source 56
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "431"
      id 431
    ]
    edge
    [
      rank 217
      source 57
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "432"
      id 432
    ]
    edge
    [
      rank 217
      source 58
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "433"
      id 433
    ]
    edge
    [
      rank 217
      source 59
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "434"
      id 434
    ]
    edge
    [
      rank 217
      source 60
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "435"
      id 435
    ]
    edge
    [
      rank 218
      source 61
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "436"
      id 436
    ]
    edge
    [
      rank 218
      source 62
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "437"
      id 437
    ]
    edge
    [
      rank 218
      source 63
      target 41
      measure 1
      diseases "Retinitis_pigmentosa"
      name "438"
      id 438
    ]
    edge
    [
      rank 218
      source 391
      target 41
      measure 1
      diseases "Hypertriglyceridemia"
      name "439"
      id 439
    ]
    edge
    [
      rank 219
      source 405
      target 41
      measure 1
      diseases "Hypertriglyceridemia"
      name "440"
      id 440
    ]
    edge
    [
      rank 219
      source 406
      target 41
      measure 1
      diseases "Hypertriglyceridemia"
      name "441"
      id 441
    ]
    edge
    [
      rank 219
      source 43
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "442"
      id 442
    ]
    edge
    [
      rank 219
      source 44
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "443"
      id 443
    ]
    edge
    [
      rank 219
      source 45
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "444"
      id 444
    ]
    edge
    [
      rank 220
      source 46
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "445"
      id 445
    ]
    edge
    [
      rank 220
      source 47
      target 42
      measure 2
      diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
      name "446"
      id 446
    ]
    edge
    [
      rank 220
      source 48
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "447"
      id 447
    ]
    edge
    [
      rank 220
      source 49
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "448"
      id 448
    ]
    edge
    [
      rank 220
      source 50
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "449"
      id 449
    ]
    edge
    [
      rank 220
      source 51
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "450"
      id 450
    ]
    edge
    [
      rank 220
      source 52
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "451"
      id 451
    ]
    edge
    [
      rank 220
      source 53
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "452"
      id 452
    ]
    edge
    [
      rank 220
      source 54
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "453"
      id 453
    ]
    edge
    [
      rank 220
      source 55
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "454"
      id 454
    ]
    edge
    [
      rank 221
      source 56
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "455"
      id 455
    ]
    edge
    [
      rank 221
      source 57
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "456"
      id 456
    ]
    edge
    [
      rank 221
      source 58
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "457"
      id 457
    ]
    edge
    [
      rank 221
      source 59
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "458"
      id 458
    ]
    edge
    [
      rank 221
      source 60
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "459"
      id 459
    ]
    edge
    [
      rank 221
      source 61
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "460"
      id 460
    ]
    edge
    [
      rank 221
      source 62
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "461"
      id 461
    ]
    edge
    [
      rank 222
      source 63
      target 42
      measure 1
      diseases "Retinitis_pigmentosa"
      name "462"
      id 462
    ]
    edge
    [
      rank 222
      source 676
      target 42
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "463"
      id 463
    ]
    edge
    [
      rank 222
      source 44
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "464"
      id 464
    ]
    edge
    [
      rank 223
      source 45
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "465"
      id 465
    ]
    edge
    [
      rank 223
      source 46
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "466"
      id 466
    ]
    edge
    [
      rank 223
      source 47
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "467"
      id 467
    ]
    edge
    [
      rank 223
      source 48
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "468"
      id 468
    ]
    edge
    [
      rank 223
      source 49
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "469"
      id 469
    ]
    edge
    [
      rank 223
      source 50
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "470"
      id 470
    ]
    edge
    [
      rank 223
      source 51
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "471"
      id 471
    ]
    edge
    [
      rank 223
      source 52
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "472"
      id 472
    ]
    edge
    [
      rank 223
      source 53
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "473"
      id 473
    ]
    edge
    [
      rank 223
      source 54
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "474"
      id 474
    ]
    edge
    [
      rank 224
      source 55
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "475"
      id 475
    ]
    edge
    [
      rank 224
      source 56
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "476"
      id 476
    ]
    edge
    [
      rank 224
      source 57
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "477"
      id 477
    ]
    edge
    [
      rank 224
      source 58
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "478"
      id 478
    ]
    edge
    [
      rank 224
      source 59
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "479"
      id 479
    ]
    edge
    [
      rank 224
      source 60
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "480"
      id 480
    ]
    edge
    [
      rank 224
      source 61
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "481"
      id 481
    ]
    edge
    [
      rank 224
      source 62
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "482"
      id 482
    ]
    edge
    [
      rank 225
      source 63
      target 43
      measure 1
      diseases "Retinitis_pigmentosa"
      name "483"
      id 483
    ]
    edge
    [
      rank 225
      source 45
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "484"
      id 484
    ]
    edge
    [
      rank 225
      source 46
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "485"
      id 485
    ]
    edge
    [
      rank 225
      source 47
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "486"
      id 486
    ]
    edge
    [
      rank 225
      source 48
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "487"
      id 487
    ]
    edge
    [
      rank 225
      source 49
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "488"
      id 488
    ]
    edge
    [
      rank 225
      source 50
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "489"
      id 489
    ]
    edge
    [
      rank 225
      source 51
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "490"
      id 490
    ]
    edge
    [
      rank 226
      source 52
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "491"
      id 491
    ]
    edge
    [
      rank 226
      source 53
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "492"
      id 492
    ]
    edge
    [
      rank 226
      source 54
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "493"
      id 493
    ]
    edge
    [
      rank 226
      source 55
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "494"
      id 494
    ]
    edge
    [
      rank 226
      source 56
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "495"
      id 495
    ]
    edge
    [
      rank 226
      source 57
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "496"
      id 496
    ]
    edge
    [
      rank 226
      source 58
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "497"
      id 497
    ]
    edge
    [
      rank 226
      source 59
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "498"
      id 498
    ]
    edge
    [
      rank 226
      source 60
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "499"
      id 499
    ]
    edge
    [
      rank 226
      source 61
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "500"
      id 500
    ]
    edge
    [
      rank 226
      source 62
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "501"
      id 501
    ]
    edge
    [
      rank 226
      source 63
      target 44
      measure 1
      diseases "Retinitis_pigmentosa"
      name "502"
      id 502
    ]
    edge
    [
      rank 227
      source 46
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "503"
      id 503
    ]
    edge
    [
      rank 227
      source 47
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "504"
      id 504
    ]
    edge
    [
      rank 227
      source 48
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "505"
      id 505
    ]
    edge
    [
      rank 227
      source 49
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "506"
      id 506
    ]
    edge
    [
      rank 227
      source 50
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "507"
      id 507
    ]
    edge
    [
      rank 228
      source 51
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "508"
      id 508
    ]
    edge
    [
      rank 228
      source 52
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "509"
      id 509
    ]
    edge
    [
      rank 230
      source 53
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "510"
      id 510
    ]
    edge
    [
      rank 230
      source 54
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "511"
      id 511
    ]
    edge
    [
      rank 230
      source 55
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "512"
      id 512
    ]
    edge
    [
      rank 230
      source 56
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "513"
      id 513
    ]
    edge
    [
      rank 231
      source 57
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "514"
      id 514
    ]
    edge
    [
      rank 231
      source 58
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "515"
      id 515
    ]
    edge
    [
      rank 231
      source 59
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "516"
      id 516
    ]
    edge
    [
      rank 232
      source 60
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "517"
      id 517
    ]
    edge
    [
      rank 232
      source 61
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "518"
      id 518
    ]
    edge
    [
      rank 232
      source 62
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "519"
      id 519
    ]
    edge
    [
      rank 232
      source 63
      target 45
      measure 1
      diseases "Retinitis_pigmentosa"
      name "520"
      id 520
    ]
    edge
    [
      rank 232
      source 47
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "521"
      id 521
    ]
    edge
    [
      rank 232
      source 48
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "522"
      id 522
    ]
    edge
    [
      rank 232
      source 49
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "523"
      id 523
    ]
    edge
    [
      rank 232
      source 50
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "524"
      id 524
    ]
    edge
    [
      rank 232
      source 51
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "525"
      id 525
    ]
    edge
    [
      rank 232
      source 52
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "526"
      id 526
    ]
    edge
    [
      rank 232
      source 53
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "527"
      id 527
    ]
    edge
    [
      rank 232
      source 54
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "528"
      id 528
    ]
    edge
    [
      rank 232
      source 55
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "529"
      id 529
    ]
    edge
    [
      rank 232
      source 56
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "530"
      id 530
    ]
    edge
    [
      rank 232
      source 57
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "531"
      id 531
    ]
    edge
    [
      rank 233
      source 58
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "532"
      id 532
    ]
    edge
    [
      rank 233
      source 59
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "533"
      id 533
    ]
    edge
    [
      rank 233
      source 60
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "534"
      id 534
    ]
    edge
    [
      rank 233
      source 61
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "535"
      id 535
    ]
    edge
    [
      rank 233
      source 62
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "536"
      id 536
    ]
    edge
    [
      rank 233
      source 63
      target 46
      measure 1
      diseases "Retinitis_pigmentosa"
      name "537"
      id 537
    ]
    edge
    [
      rank 233
      source 48
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "538"
      id 538
    ]
    edge
    [
      rank 233
      source 49
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "539"
      id 539
    ]
    edge
    [
      rank 234
      source 50
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "540"
      id 540
    ]
    edge
    [
      rank 234
      source 51
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "541"
      id 541
    ]
    edge
    [
      rank 234
      source 52
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "542"
      id 542
    ]
    edge
    [
      rank 234
      source 53
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "543"
      id 543
    ]
    edge
    [
      rank 234
      source 54
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "544"
      id 544
    ]
    edge
    [
      rank 234
      source 55
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "545"
      id 545
    ]
    edge
    [
      rank 234
      source 56
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "546"
      id 546
    ]
    edge
    [
      rank 236
      source 57
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "547"
      id 547
    ]
    edge
    [
      rank 236
      source 58
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "548"
      id 548
    ]
    edge
    [
      rank 236
      source 59
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "549"
      id 549
    ]
    edge
    [
      rank 236
      source 60
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "550"
      id 550
    ]
    edge
    [
      rank 237
      source 61
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "551"
      id 551
    ]
    edge
    [
      rank 237
      source 62
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "552"
      id 552
    ]
    edge
    [
      rank 237
      source 63
      target 47
      measure 1
      diseases "Retinitis_pigmentosa"
      name "553"
      id 553
    ]
    edge
    [
      rank 238
      source 676
      target 47
      measure 1
      diseases "Leber_congenital_amaurosis"
      name "554"
      id 554
    ]
    edge
    [
      rank 238
      source 49
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "555"
      id 555
    ]
    edge
    [
      rank 239
      source 50
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "556"
      id 556
    ]
    edge
    [
      rank 241
      source 51
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "557"
      id 557
    ]
    edge
    [
      rank 242
      source 52
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "558"
      id 558
    ]
    edge
    [
      rank 242
      source 53
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "559"
      id 559
    ]
    edge
    [
      rank 242
      source 54
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "560"
      id 560
    ]
    edge
    [
      rank 243
      source 55
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "561"
      id 561
    ]
    edge
    [
      rank 245
      source 56
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "562"
      id 562
    ]
    edge
    [
      rank 246
      source 57
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "563"
      id 563
    ]
    edge
    [
      rank 246
      source 58
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "564"
      id 564
    ]
    edge
    [
      rank 246
      source 59
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "565"
      id 565
    ]
    edge
    [
      rank 246
      source 60
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "566"
      id 566
    ]
    edge
    [
      rank 247
      source 61
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "567"
      id 567
    ]
    edge
    [
      rank 247
      source 62
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "568"
      id 568
    ]
    edge
    [
      rank 247
      source 63
      target 48
      measure 1
      diseases "Retinitis_pigmentosa"
      name "569"
      id 569
    ]
    edge
    [
      rank 247
      source 50
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "570"
      id 570
    ]
    edge
    [
      rank 247
      source 51
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "571"
      id 571
    ]
    edge
    [
      rank 247
      source 52
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "572"
      id 572
    ]
    edge
    [
      rank 248
      source 53
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "573"
      id 573
    ]
    edge
    [
      rank 248
      source 54
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "574"
      id 574
    ]
    edge
    [
      rank 248
      source 55
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "575"
      id 575
    ]
    edge
    [
      rank 248
      source 56
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "576"
      id 576
    ]
    edge
    [
      rank 249
      source 57
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "577"
      id 577
    ]
    edge
    [
      rank 249
      source 58
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "578"
      id 578
    ]
    edge
    [
      rank 249
      source 59
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "579"
      id 579
    ]
    edge
    [
      rank 249
      source 60
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "580"
      id 580
    ]
    edge
    [
      rank 249
      source 61
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "581"
      id 581
    ]
    edge
    [
      rank 249
      source 62
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "582"
      id 582
    ]
    edge
    [
      rank 250
      source 63
      target 49
      measure 1
      diseases "Retinitis_pigmentosa"
      name "583"
      id 583
    ]
    edge
    [
      rank 250
      source 51
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "584"
      id 584
    ]
    edge
    [
      rank 250
      source 52
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "585"
      id 585
    ]
    edge
    [
      rank 250
      source 53
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "586"
      id 586
    ]
    edge
    [
      rank 251
      source 54
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "587"
      id 587
    ]
    edge
    [
      rank 251
      source 55
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "588"
      id 588
    ]
    edge
    [
      rank 251
      source 56
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "589"
      id 589
    ]
    edge
    [
      rank 251
      source 57
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "590"
      id 590
    ]
    edge
    [
      rank 251
      source 58
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "591"
      id 591
    ]
    edge
    [
      rank 252
      source 59
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "592"
      id 592
    ]
    edge
    [
      rank 253
      source 60
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "593"
      id 593
    ]
    edge
    [
      rank 253
      source 61
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "594"
      id 594
    ]
    edge
    [
      rank 253
      source 62
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "595"
      id 595
    ]
    edge
    [
      rank 253
      source 63
      target 50
      measure 1
      diseases "Retinitis_pigmentosa"
      name "596"
      id 596
    ]
    edge
    [
      rank 254
      source 872
      target 50
      measure 1
      diseases "Retinal_cone_dsytrophy"
      name "597"
      id 597
    ]
    edge
    [
      rank 255
      source 873
      target 50
      measure 1
      diseases "Retinal_cone_dsytrophy"
      name "598"
      id 598
    ]
    edge
    [
      rank 257
      source 874
      target 50
      measure 1
      diseases "Retinal_cone_dsytrophy"
      name "599"
      id 599
    ]
    edge
    [
      rank 257
      source 52
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "600"
      id 600
    ]
    edge
    [
      rank 257
      source 53
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "601"
      id 601
    ]
    edge
    [
      rank 258
      source 54
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "602"
      id 602
    ]
    edge
    [
      rank 258
      source 55
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "603"
      id 603
    ]
    edge
    [
      rank 258
      source 56
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "604"
      id 604
    ]
    edge
    [
      rank 258
      source 57
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "605"
      id 605
    ]
    edge
    [
      rank 258
      source 58
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "606"
      id 606
    ]
    edge
    [
      rank 258
      source 59
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "607"
      id 607
    ]
    edge
    [
      rank 258
      source 60
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "608"
      id 608
    ]
    edge
    [
      rank 258
      source 61
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "609"
      id 609
    ]
    edge
    [
      rank 258
      source 62
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "610"
      id 610
    ]
    edge
    [
      rank 258
      source 63
      target 51
      measure 1
      diseases "Retinitis_pigmentosa"
      name "611"
      id 611
    ]
    edge
    [
      rank 258
      source 53
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "612"
      id 612
    ]
    edge
    [
      rank 259
      source 54
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "613"
      id 613
    ]
    edge
    [
      rank 259
      source 55
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "614"
      id 614
    ]
    edge
    [
      rank 259
      source 56
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "615"
      id 615
    ]
    edge
    [
      rank 260
      source 57
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "616"
      id 616
    ]
    edge
    [
      rank 260
      source 58
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "617"
      id 617
    ]
    edge
    [
      rank 260
      source 59
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "618"
      id 618
    ]
    edge
    [
      rank 261
      source 60
      target 52
      measure 2
      diseases "Night_blindness,Retinitis_pigmentosa"
      name "619"
      id 619
    ]
    edge
    [
      rank 261
      source 61
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "620"
      id 620
    ]
    edge
    [
      rank 262
      source 62
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "621"
      id 621
    ]
    edge
    [
      rank 262
      source 63
      target 52
      measure 1
      diseases "Retinitis_pigmentosa"
      name "622"
      id 622
    ]
    edge
    [
      rank 262
      source 549
      target 52
      measure 1
      diseases "Night_blindness"
      name "623"
      id 623
    ]
    edge
    [
      rank 262
      source 550
      target 52
      measure 1
      diseases "Night_blindness"
      name "624"
      id 624
    ]
    edge
    [
      rank 262
      source 551
      target 52
      measure 1
      diseases "Night_blindness"
      name "625"
      id 625
    ]
    edge
    [
      rank 263
      source 54
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "626"
      id 626
    ]
    edge
    [
      rank 263
      source 55
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "627"
      id 627
    ]
    edge
    [
      rank 263
      source 56
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "628"
      id 628
    ]
    edge
    [
      rank 264
      source 57
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "629"
      id 629
    ]
    edge
    [
      rank 264
      source 58
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "630"
      id 630
    ]
    edge
    [
      rank 265
      source 59
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "631"
      id 631
    ]
    edge
    [
      rank 265
      source 60
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "632"
      id 632
    ]
    edge
    [
      rank 265
      source 61
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "633"
      id 633
    ]
    edge
    [
      rank 266
      source 62
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "634"
      id 634
    ]
    edge
    [
      rank 266
      source 63
      target 53
      measure 1
      diseases "Retinitis_pigmentosa"
      name "635"
      id 635
    ]
    edge
    [
      rank 266
      source 64
      target 53
      measure 1
      diseases "Macular_dystrophy"
      name "636"
      id 636
    ]
    edge
    [
      rank 266
      source 65
      target 53
      measure 1
      diseases "Macular_dystrophy"
      name "637"
      id 637
    ]
    edge
    [
      rank 266
      source 66
      target 53
      measure 1
      diseases "Macular_dystrophy"
      name "638"
      id 638
    ]
    edge
    [
      rank 266
      source 55
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "639"
      id 639
    ]
    edge
    [
      rank 266
      source 56
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "640"
      id 640
    ]
    edge
    [
      rank 266
      source 57
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "641"
      id 641
    ]
    edge
    [
      rank 266
      source 58
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "642"
      id 642
    ]
    edge
    [
      rank 266
      source 59
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "643"
      id 643
    ]
    edge
    [
      rank 266
      source 60
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "644"
      id 644
    ]
    edge
    [
      rank 266
      source 61
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "645"
      id 645
    ]
    edge
    [
      rank 267
      source 62
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "646"
      id 646
    ]
    edge
    [
      rank 267
      source 63
      target 54
      measure 1
      diseases "Retinitis_pigmentosa"
      name "647"
      id 647
    ]
    edge
    [
      rank 267
      source 56
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "648"
      id 648
    ]
    edge
    [
      rank 267
      source 57
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "649"
      id 649
    ]
    edge
    [
      rank 267
      source 58
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "650"
      id 650
    ]
    edge
    [
      rank 267
      source 59
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "651"
      id 651
    ]
    edge
    [
      rank 267
      source 60
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "652"
      id 652
    ]
    edge
    [
      rank 268
      source 61
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "653"
      id 653
    ]
    edge
    [
      rank 268
      source 62
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "654"
      id 654
    ]
    edge
    [
      rank 268
      source 63
      target 55
      measure 1
      diseases "Retinitis_pigmentosa"
      name "655"
      id 655
    ]
    edge
    [
      rank 268
      source 161
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "656"
      id 656
    ]
    edge
    [
      rank 268
      source 180
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "657"
      id 657
    ]
    edge
    [
      rank 268
      source 183
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "658"
      id 658
    ]
    edge
    [
      rank 268
      source 185
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "659"
      id 659
    ]
    edge
    [
      rank 268
      source 868
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "660"
      id 660
    ]
    edge
    [
      rank 268
      source 869
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "661"
      id 661
    ]
    edge
    [
      rank 268
      source 870
      target 55
      measure 1
      diseases "Usher_syndrome"
      name "662"
      id 662
    ]
    edge
    [
      rank 268
      source 57
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "663"
      id 663
    ]
    edge
    [
      rank 268
      source 58
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "664"
      id 664
    ]
    edge
    [
      rank 268
      source 59
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "665"
      id 665
    ]
    edge
    [
      rank 268
      source 60
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "666"
      id 666
    ]
    edge
    [
      rank 268
      source 61
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "667"
      id 667
    ]
    edge
    [
      rank 268
      source 62
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "668"
      id 668
    ]
    edge
    [
      rank 268
      source 63
      target 56
      measure 1
      diseases "Retinitis_pigmentosa"
      name "669"
      id 669
    ]
    edge
    [
      rank 268
      source 58
      target 57
      measure 1
      diseases "Retinitis_pigmentosa"
      name "670"
      id 670
    ]
    edge
    [
      rank 268
      source 59
      target 57
      measure 1
      diseases "Retinitis_pigmentosa"
      name "671"
      id 671
    ]
    edge
    [
      rank 268
      source 60
      target 57
      measure 1
      diseases "Retinitis_pigmentosa"
      name "672"
      id 672
    ]
    edge
    [
      rank 268
      source 61
      target 57
      measure 1
      diseases "Retinitis_pigmentosa"
      name "673"
      id 673
    ]
    edge
    [
      rank 268
      source 62
      target 57
      measure 1
      diseases "Retinitis_pigmentosa"
      name "674"
      id 674
    ]
    edge
    [
      rank 270
      source 63
      target 57
      measure 1
      diseases "Retinitis_pigmentosa"
      name "675"
      id 675
    ]
    edge
    [
      rank 271
      source 59
      target 58
      measure 1
      diseases "Retinitis_pigmentosa"
      name "676"
      id 676
    ]
    edge
    [
      rank 271
      source 60
      target 58
      measure 1
      diseases "Retinitis_pigmentosa"
      name "677"
      id 677
    ]
    edge
    [
      rank 271
      source 61
      target 58
      measure 1
      diseases "Retinitis_pigmentosa"
      name "678"
      id 678
    ]
    edge
    [
      rank 271
      source 62
      target 58
      measure 1
      diseases "Retinitis_pigmentosa"
      name "679"
      id 679
    ]
    edge
    [
      rank 271
      source 63
      target 58
      measure 1
      diseases "Retinitis_pigmentosa"
      name "680"
      id 680
    ]
    edge
    [
      rank 272
      source 60
      target 59
      measure 1
      diseases "Retinitis_pigmentosa"
      name "681"
      id 681
    ]
    edge
    [
      rank 272
      source 61
      target 59
      measure 1
      diseases "Retinitis_pigmentosa"
      name "682"
      id 682
    ]
    edge
    [
      rank 273
      source 62
      target 59
      measure 1
      diseases "Retinitis_pigmentosa"
      name "683"
      id 683
    ]
    edge
    [
      rank 273
      source 63
      target 59
      measure 1
      diseases "Retinitis_pigmentosa"
      name "684"
      id 684
    ]
    edge
    [
      rank 274
      source 61
      target 60
      measure 1
      diseases "Retinitis_pigmentosa"
      name "685"
      id 685
    ]
    edge
    [
      rank 274
      source 62
      target 60
      measure 1
      diseases "Retinitis_pigmentosa"
      name "686"
      id 686
    ]
    edge
    [
      rank 274
      source 63
      target 60
      measure 1
      diseases "Retinitis_pigmentosa"
      name "687"
      id 687
    ]
    edge
    [
      rank 275
      source 549
      target 60
      measure 1
      diseases "Night_blindness"
      name "688"
      id 688
    ]
    edge
    [
      rank 276
      source 550
      target 60
      measure 1
      diseases "Night_blindness"
      name "689"
      id 689
    ]
    edge
    [
      rank 276
      source 551
      target 60
      measure 1
      diseases "Night_blindness"
      name "690"
      id 690
    ]
    edge
    [
      rank 276
      source 62
      target 61
      measure 1
      diseases "Retinitis_pigmentosa"
      name "691"
      id 691
    ]
    edge
    [
      rank 276
      source 63
      target 61
      measure 1
      diseases "Retinitis_pigmentosa"
      name "692"
      id 692
    ]
    edge
    [
      rank 276
      source 63
      target 62
      measure 1
      diseases "Retinitis_pigmentosa"
      name "693"
      id 693
    ]
    edge
    [
      rank 276
      source 65
      target 64
      measure 1
      diseases "Macular_dystrophy"
      name "694"
      id 694
    ]
    edge
    [
      rank 276
      source 66
      target 64
      measure 1
      diseases "Macular_dystrophy"
      name "695"
      id 695
    ]
    edge
    [
      rank 276
      source 66
      target 65
      measure 1
      diseases "Macular_dystrophy"
      name "696"
      id 696
    ]
    edge
    [
      rank 276
      source 68
      target 67
      measure 1
      diseases "Leukemia"
      name "697"
      id 697
    ]
    edge
    [
      rank 276
      source 69
      target 67
      measure 1
      diseases "Leukemia"
      name "698"
      id 698
    ]
    edge
    [
      rank 276
      source 70
      target 67
      measure 1
      diseases "Leukemia"
      name "699"
      id 699
    ]
    edge
    [
      rank 276
      source 71
      target 67
      measure 1
      diseases "Leukemia"
      name "700"
      id 700
    ]
    edge
    [
      rank 277
      source 72
      target 67
      measure 1
      diseases "Leukemia"
      name "701"
      id 701
    ]
    edge
    [
      rank 277
      source 73
      target 67
      measure 1
      diseases "Leukemia"
      name "702"
      id 702
    ]
    edge
    [
      rank 278
      source 74
      target 67
      measure 1
      diseases "Leukemia"
      name "703"
      id 703
    ]
    edge
    [
      rank 278
      source 75
      target 67
      measure 1
      diseases "Leukemia"
      name "704"
      id 704
    ]
    edge
    [
      rank 278
      source 76
      target 67
      measure 1
      diseases "Leukemia"
      name "705"
      id 705
    ]
    edge
    [
      rank 279
      source 77
      target 67
      measure 1
      diseases "Leukemia"
      name "706"
      id 706
    ]
    edge
    [
      rank 279
      source 78
      target 67
      measure 1
      diseases "Leukemia"
      name "707"
      id 707
    ]
    edge
    [
      rank 279
      source 79
      target 67
      measure 1
      diseases "Leukemia"
      name "708"
      id 708
    ]
    edge
    [
      rank 279
      source 80
      target 67
      measure 1
      diseases "Leukemia"
      name "709"
      id 709
    ]
    edge
    [
      rank 279
      source 81
      target 67
      measure 1
      diseases "Leukemia"
      name "710"
      id 710
    ]
    edge
    [
      rank 279
      source 82
      target 67
      measure 1
      diseases "Leukemia"
      name "711"
      id 711
    ]
    edge
    [
      rank 279
      source 83
      target 67
      measure 1
      diseases "Leukemia"
      name "712"
      id 712
    ]
    edge
    [
      rank 279
      source 84
      target 67
      measure 1
      diseases "Leukemia"
      name "713"
      id 713
    ]
    edge
    [
      rank 280
      source 85
      target 67
      measure 1
      diseases "Leukemia"
      name "714"
      id 714
    ]
    edge
    [
      rank 280
      source 86
      target 67
      measure 1
      diseases "Leukemia"
      name "715"
      id 715
    ]
    edge
    [
      rank 280
      source 87
      target 67
      measure 1
      diseases "Leukemia"
      name "716"
      id 716
    ]
    edge
    [
      rank 280
      source 88
      target 67
      measure 1
      diseases "Leukemia"
      name "717"
      id 717
    ]
    edge
    [
      rank 280
      source 89
      target 67
      measure 1
      diseases "Leukemia"
      name "718"
      id 718
    ]
    edge
    [
      rank 280
      source 90
      target 67
      measure 1
      diseases "Leukemia"
      name "719"
      id 719
    ]
    edge
    [
      rank 280
      source 91
      target 67
      measure 1
      diseases "Leukemia"
      name "720"
      id 720
    ]
    edge
    [
      rank 280
      source 92
      target 67
      measure 1
      diseases "Leukemia"
      name "721"
      id 721
    ]
    edge
    [
      rank 281
      source 93
      target 67
      measure 1
      diseases "Leukemia"
      name "722"
      id 722
    ]
    edge
    [
      rank 281
      source 94
      target 67
      measure 1
      diseases "Leukemia"
      name "723"
      id 723
    ]
    edge
    [
      rank 281
      source 95
      target 67
      measure 1
      diseases "Leukemia"
      name "724"
      id 724
    ]
    edge
    [
      rank 281
      source 96
      target 67
      measure 1
      diseases "Leukemia"
      name "725"
      id 725
    ]
    edge
    [
      rank 283
      source 97
      target 67
      measure 1
      diseases "Leukemia"
      name "726"
      id 726
    ]
    edge
    [
      rank 283
      source 98
      target 67
      measure 1
      diseases "Leukemia"
      name "727"
      id 727
    ]
    edge
    [
      rank 284
      source 99
      target 67
      measure 1
      diseases "Leukemia"
      name "728"
      id 728
    ]
    edge
    [
      rank 285
      source 100
      target 67
      measure 1
      diseases "Leukemia"
      name "729"
      id 729
    ]
    edge
    [
      rank 286
      source 101
      target 67
      measure 1
      diseases "Leukemia"
      name "730"
      id 730
    ]
    edge
    [
      rank 288
      source 102
      target 67
      measure 1
      diseases "Leukemia"
      name "731"
      id 731
    ]
    edge
    [
      rank 289
      source 103
      target 67
      measure 1
      diseases "Leukemia"
      name "732"
      id 732
    ]
    edge
    [
      rank 289
      source 69
      target 68
      measure 1
      diseases "Leukemia"
      name "733"
      id 733
    ]
    edge
    [
      rank 290
      source 70
      target 68
      measure 1
      diseases "Leukemia"
      name "734"
      id 734
    ]
    edge
    [
      rank 292
      source 71
      target 68
      measure 1
      diseases "Leukemia"
      name "735"
      id 735
    ]
    edge
    [
      rank 294
      source 72
      target 68
      measure 1
      diseases "Leukemia"
      name "736"
      id 736
    ]
    edge
    [
      rank 298
      source 73
      target 68
      measure 1
      diseases "Leukemia"
      name "737"
      id 737
    ]
    edge
    [
      rank 299
      source 74
      target 68
      measure 1
      diseases "Leukemia"
      name "738"
      id 738
    ]
    edge
    [
      rank 299
      source 75
      target 68
      measure 1
      diseases "Leukemia"
      name "739"
      id 739
    ]
    edge
    [
      rank 299
      source 76
      target 68
      measure 1
      diseases "Leukemia"
      name "740"
      id 740
    ]
    edge
    [
      rank 299
      source 77
      target 68
      measure 1
      diseases "Leukemia"
      name "741"
      id 741
    ]
    edge
    [
      rank 299
      source 78
      target 68
      measure 1
      diseases "Leukemia"
      name "742"
      id 742
    ]
    edge
    [
      rank 301
      source 79
      target 68
      measure 1
      diseases "Leukemia"
      name "743"
      id 743
    ]
    edge
    [
      rank 301
      source 80
      target 68
      measure 1
      diseases "Leukemia"
      name "744"
      id 744
    ]
    edge
    [
      rank 302
      source 81
      target 68
      measure 1
      diseases "Leukemia"
      name "745"
      id 745
    ]
    edge
    [
      rank 302
      source 82
      target 68
      measure 1
      diseases "Leukemia"
      name "746"
      id 746
    ]
    edge
    [
      rank 303
      source 83
      target 68
      measure 1
      diseases "Leukemia"
      name "747"
      id 747
    ]
    edge
    [
      rank 303
      source 84
      target 68
      measure 1
      diseases "Leukemia"
      name "748"
      id 748
    ]
    edge
    [
      rank 304
      source 85
      target 68
      measure 1
      diseases "Leukemia"
      name "749"
      id 749
    ]
    edge
    [
      rank 304
      source 86
      target 68
      measure 1
      diseases "Leukemia"
      name "750"
      id 750
    ]
    edge
    [
      rank 305
      source 87
      target 68
      measure 1
      diseases "Leukemia"
      name "751"
      id 751
    ]
]