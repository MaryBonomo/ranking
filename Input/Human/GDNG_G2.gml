Creator "igraph version 1.2.5 Mon Nov  9 14:28:25 2020"
Version 1
graph
[
  directed 0
  node
  [
    id 1
    gene "A2M"
    name "1"
    phclass "Neurological"
  ]
  node
  [
    id 2
    gene "APP"
    name "2"
    phclass "Grey"
  ]
  node
  [
    id 3
    gene "APOE"
    name "3"
    phclass "Grey"
  ]
  node
  [
    id 4
    gene "PSEN2"
    name "4"
    phclass "Neurological"
  ]
  node
  [
    id 5
    gene "APBB2"
    name "5"
    phclass "Neurological"
  ]
  node
  [
    id 6
    gene "NOS3"
    name "6"
    phclass "Grey"
  ]
  node
  [
    id 7
    gene "PLAU"
    name "7"
    phclass "Neurological"
  ]
  node
  [
    id 8
    gene "ACE"
    name "8"
    phclass "Grey"
  ]
  node
  [
    id 9
    gene "MPO"
    name "9"
    phclass "Grey"
  ]
  node
  [
    id 10
    gene "PAXIP1"
    name "10"
    phclass "Neurological"
  ]
  node
  [
    id 11
    gene "BLMH"
    name "11"
    phclass "Neurological"
  ]
  node
  [
    id 12
    gene "PSEN1"
    name "12"
    phclass "Neurological"
  ]
  node
  [
    id 13
    gene "ABCA1"
    name "13"
    phclass "Grey"
  ]
  node
  [
    id 14
    gene "CST3"
    name "14"
    phclass "Neurological"
  ]
  node
  [
    id 15
    gene "MEF2A"
    name "15"
    phclass "Cardiovascular"
  ]
  node
  [
    id 16
    gene "PON1"
    name "16"
    phclass "Cardiovascular"
  ]
  node
  [
    id 17
    gene "PON2"
    name "17"
    phclass "Cardiovascular"
  ]
  node
  [
    id 18
    gene "MMP3"
    name "18"
    phclass "Cardiovascular"
  ]
  node
  [
    id 19
    gene "ESR1"
    name "19"
    phclass "Grey"
  ]
  node
  [
    id 20
    gene "ABCB7"
    name "20"
    phclass "Hematological"
  ]
  node
  [
    id 21
    gene "CDAN1"
    name "21"
    phclass "Hematological"
  ]
  node
  [
    id 22
    gene "RPS19"
    name "22"
    phclass "Hematological"
  ]
  node
  [
    id 23
    gene "PKLR"
    name "23"
    phclass "Hematological"
  ]
  node
  [
    id 24
    gene "RHAG"
    name "24"
    phclass "Hematological"
  ]
  node
  [
    id 25
    gene "SLC11A2"
    name "25"
    phclass "Hematological"
  ]
  node
  [
    id 26
    gene "SPTB"
    name "26"
    phclass "Hematological"
  ]
  node
  [
    id 27
    gene "ALAS2"
    name "27"
    phclass "Hematological"
  ]
  node
  [
    id 28
    gene "ABCA4"
    name "28"
    phclass "Ophthamological"
  ]
  node
  [
    id 29
    gene "RPGR"
    name "29"
    phclass "Ophthamological"
  ]
  node
  [
    id 30
    gene "GUCA1A"
    name "30"
    phclass "Ophthamological"
  ]
  node
  [
    id 31
    gene "AIPL1"
    name "31"
    phclass "Ophthamological"
  ]
  node
  [
    id 32
    gene "GUCY2D"
    name "32"
    phclass "Ophthamological"
  ]
  node
  [
    id 33
    gene "RPGRIP1"
    name "33"
    phclass "Ophthamological"
  ]
  node
  [
    id 34
    gene "CRX"
    name "34"
    phclass "Ophthamological"
  ]
  node
  [
    id 35
    gene "RDH5"
    name "35"
    phclass "Ophthamological"
  ]
  node
  [
    id 36
    gene "RLBP1"
    name "36"
    phclass "Ophthamological"
  ]
  node
  [
    id 37
    gene "IMPDH1"
    name "37"
    phclass "Ophthamological"
  ]
  node
  [
    id 38
    gene "PRPF31"
    name "38"
    phclass "Ophthamological"
  ]
  node
  [
    id 39
    gene "RP1"
    name "39"
    phclass "Grey"
  ]
  node
  [
    id 40
    gene "CRB1"
    name "40"
    phclass "Ophthamological"
  ]
  node
  [
    id 41
    gene "PRPF8"
    name "41"
    phclass "Ophthamological"
  ]
  node
  [
    id 42
    gene "CA4"
    name "42"
    phclass "Ophthamological"
  ]
  node
  [
    id 43
    gene "PRPF3"
    name "43"
    phclass "Ophthamological"
  ]
  node
  [
    id 44
    gene "RPE65"
    name "44"
    phclass "Ophthamological"
  ]
  node
  [
    id 45
    gene "RP2"
    name "45"
    phclass "Ophthamological"
  ]
  node
  [
    id 46
    gene "CERKL"
    name "46"
    phclass "Ophthamological"
  ]
  node
  [
    id 47
    gene "NRL"
    name "47"
    phclass "Ophthamological"
  ]
  node
  [
    id 48
    gene "RHO"
    name "48"
    phclass "Ophthamological"
  ]
  node
  [
    id 49
    gene "RP9"
    name "49"
    phclass "Ophthamological"
  ]
  node
  [
    id 50
    gene "USH2A"
    name "50"
    phclass "Grey"
  ]
  node
  [
    id 51
    gene "RGR"
    name "51"
    phclass "Ophthamological"
  ]
  node
  [
    id 52
    gene "CNGB1"
    name "52"
    phclass "Ophthamological"
  ]
  node
  [
    id 53
    gene "CNGA1"
    name "53"
    phclass "Ophthamological"
  ]
  node
  [
    id 54
    gene "PDE6A"
    name "54"
    phclass "Ophthamological"
  ]
  node
  [
    id 55
    gene "PDE6B"
    name "55"
    phclass "Ophthamological"
  ]
  node
  [
    id 56
    gene "ROM1"
    name "56"
    phclass "Ophthamological"
  ]
  node
  [
    id 57
    gene "NR2E3"
    name "57"
    phclass "Ophthamological"
  ]
  node
  [
    id 58
    gene "MERTK"
    name "58"
    phclass "Ophthamological"
  ]
  node
  [
    id 59
    gene "ELOVL4"
    name "59"
    phclass "Ophthamological"
  ]
  node
  [
    id 60
    gene "ABL1"
    name "60"
    phclass "Cancer"
  ]
  node
  [
    id 61
    gene "TAL1"
    name "61"
    phclass "Cancer"
  ]
  node
  [
    id 62
    gene "TAL2"
    name "62"
    phclass "Cancer"
  ]
  node
  [
    id 63
    gene "FLT3"
    name "63"
    phclass "Cancer"
  ]
  node
  [
    id 64
    gene "NBN"
    name "64"
    phclass "Grey"
  ]
  node
  [
    id 65
    gene "HOXD4"
    name "65"
    phclass "Cancer"
  ]
  node
  [
    id 66
    gene "BCR"
    name "66"
    phclass "Cancer"
  ]
  node
  [
    id 67
    gene "ARNT"
    name "67"
    phclass "Cancer"
  ]
  node
  [
    id 68
    gene "KRAS"
    name "68"
    phclass "Cancer"
  ]
  node
  [
    id 69
    gene "GMPS"
    name "69"
    phclass "Cancer"
  ]
  node
  [
    id 70
    gene "MLLT10"
    name "70"
    phclass "Cancer"
  ]
  node
  [
    id 71
    gene "ARHGEF12"
    name "71"
    phclass "Cancer"
  ]
  node
  [
    id 72
    gene "PICALM"
    name "72"
    phclass "Cancer"
  ]
  node
  [
    id 73
    gene "CEBPA"
    name "73"
    phclass "Cancer"
  ]
  node
  [
    id 74
    gene "CHIC2"
    name "74"
    phclass "Cancer"
  ]
  node
  [
    id 75
    gene "KIT"
    name "75"
    phclass "Grey"
  ]
  node
  [
    id 76
    gene "LPP"
    name "76"
    phclass "Cancer"
  ]
  node
  [
    id 77
    gene "NPM1"
    name "77"
    phclass "Cancer"
  ]
  node
  [
    id 78
    gene "NUP214"
    name "78"
    phclass "Cancer"
  ]
  node
  [
    id 79
    gene "RUNX1"
    name "79"
    phclass "Grey"
  ]
  node
  [
    id 80
    gene "WHSC1L1"
    name "80"
    phclass "Cancer"
  ]
  node
  [
    id 81
    gene "MLLT11"
    name "81"
    phclass "Cancer"
  ]
  node
  [
    id 82
    gene "NUMA1"
    name "82"
    phclass "Cancer"
  ]
  node
  [
    id 83
    gene "ZBTB16"
    name "83"
    phclass "Cancer"
  ]
  node
  [
    id 84
    gene "PML"
    name "84"
    phclass "Cancer"
  ]
  node
  [
    id 85
    gene "STAT5B"
    name "85"
    phclass "Grey"
  ]
  node
  [
    id 86
    gene "ARL11"
    name "86"
    phclass "Cancer"
  ]
  node
  [
    id 87
    gene "P2RX7"
    name "87"
    phclass "Cancer"
  ]
  node
  [
    id 88
    gene "ARHGAP26"
    name "88"
    phclass "Cancer"
  ]
  node
  [
    id 89
    gene "NF1"
    name "89"
    phclass "Cancer"
  ]
  node
  [
    id 90
    gene "PTPN11"
    name "90"
    phclass "Grey"
  ]
  node
  [
    id 91
    gene "BCL2"
    name "91"
    phclass "Cancer"
  ]
  node
  [
    id 92
    gene "CCND1"
    name "92"
    phclass "Cancer"
  ]
  node
  [
    id 93
    gene "GATA1"
    name "93"
    phclass "Grey"
  ]
  node
  [
    id 94
    gene "NQO1"
    name "94"
    phclass "Grey"
  ]
  node
  [
    id 95
    gene "ABO"
    name "95"
    phclass "Hematological"
  ]
  node
  [
    id 96
    gene "AQP1"
    name "96"
    phclass "Grey"
  ]
  node
  [
    id 97
    gene "SLC4A1"
    name "97"
    phclass "Grey"
  ]
  node
  [
    id 98
    gene "GYPC"
    name "98"
    phclass "Grey"
  ]
  node
  [
    id 99
    gene "AQP3"
    name "99"
    phclass "Hematological"
  ]
  node
  [
    id 100
    gene "GCNT2"
    name "100"
    phclass "Hematological"
  ]
  node
  [
    id 101
    gene "CD44"
    name "101"
    phclass "Hematological"
  ]
  node
  [
    id 102
    gene "KEL"
    name "102"
    phclass "Hematological"
  ]
  node
  [
    id 103
    gene "CR1"
    name "103"
    phclass "Hematological"
  ]
  node
  [
    id 104
    gene "ICAM4"
    name "104"
    phclass "Hematological"
  ]
  node
  [
    id 105
    gene "GYPA"
    name "105"
    phclass "Hematological"
  ]
  node
  [
    id 106
    gene "BSG"
    name "106"
    phclass "Hematological"
  ]
  node
  [
    id 107
    gene "A4GALT"
    name "107"
    phclass "Hematological"
  ]
  node
  [
    id 108
    gene "GYPB"
    name "108"
    phclass "Hematological"
  ]
  node
  [
    id 109
    gene "ACHE"
    name "109"
    phclass "Hematological"
  ]
  node
  [
    id 110
    gene "ACTA1"
    name "110"
    phclass "Muscular"
  ]
  node
  [
    id 111
    gene "CRYAB"
    name "111"
    phclass "Grey"
  ]
  node
  [
    id 112
    gene "MYF6"
    name "112"
    phclass "Muscular"
  ]
  node
  [
    id 113
    gene "ITGA7"
    name "113"
    phclass "Muscular"
  ]
  node
  [
    id 114
    gene "DES"
    name "114"
    phclass "Grey"
  ]
  node
  [
    id 115
    gene "DYSF"
    name "115"
    phclass "Muscular"
  ]
  node
  [
    id 116
    gene "CAV3"
    name "116"
    phclass "Grey"
  ]
  node
  [
    id 117
    gene "CPT2"
    name "117"
    phclass "Grey"
  ]
  node
  [
    id 118
    gene "PGAM2"
    name "118"
    phclass "Muscular"
  ]
  node
  [
    id 119
    gene "MYH7"
    name "119"
    phclass "Grey"
  ]
  node
  [
    id 120
    gene "MYL3"
    name "120"
    phclass "Cardiovascular"
  ]
  node
  [
    id 121
    gene "LMNA"
    name "121"
    phclass "Grey"
  ]
  node
  [
    id 122
    gene "TNNT2"
    name "122"
    phclass "Cardiovascular"
  ]
  node
  [
    id 123
    gene "TTN"
    name "123"
    phclass "Grey"
  ]
  node
  [
    id 124
    gene "EYA4"
    name "124"
    phclass "Grey"
  ]
  node
  [
    id 125
    gene "SGCD"
    name "125"
    phclass "Grey"
  ]
  node
  [
    id 126
    gene "CSRP3"
    name "126"
    phclass "Cardiovascular"
  ]
  node
  [
    id 127
    gene "TCAP"
    name "127"
    phclass "Grey"
  ]
  node
  [
    id 128
    gene "ABCC9"
    name "128"
    phclass "Cardiovascular"
  ]
  node
  [
    id 129
    gene "DMD"
    name "129"
    phclass "Grey"
  ]
  node
  [
    id 130
    gene "MYL2"
    name "130"
    phclass "Cardiovascular"
  ]
  node
  [
    id 131
    gene "MYH6"
    name "131"
    phclass "Cardiovascular"
  ]
  node
  [
    id 132
    gene "TNNC1"
    name "132"
    phclass "Cardiovascular"
  ]
  node
  [
    id 133
    gene "TPM1"
    name "133"
    phclass "Cardiovascular"
  ]
  node
  [
    id 134
    gene "TNNI3"
    name "134"
    phclass "Cardiovascular"
  ]
  node
  [
    id 135
    gene "MYBPC3"
    name "135"
    phclass "Cardiovascular"
  ]
  node
  [
    id 136
    gene "COX15"
    name "136"
    phclass "Grey"
  ]
  node
  [
    id 137
    gene "MYLK2"
    name "137"
    phclass "Cardiovascular"
  ]
  node
  [
    id 138
    gene "PRKAG2"
    name "138"
    phclass "Cardiovascular"
  ]
  node
  [
    id 139
    gene "PLN"
    name "139"
    phclass "Cardiovascular"
  ]
  node
  [
    id 140
    gene "TAZ"
    name "140"
    phclass "Grey"
  ]
  node
  [
    id 141
    gene "ACTG1"
    name "141"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 142
    gene "DIAPH1"
    name "142"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 143
    gene "MYO7A"
    name "143"
    phclass "Grey"
  ]
  node
  [
    id 144
    gene "COL11A2"
    name "144"
    phclass "Grey"
  ]
  node
  [
    id 145
    gene "MYH9"
    name "145"
    phclass "Grey"
  ]
  node
  [
    id 146
    gene "MYO6"
    name "146"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 147
    gene "GJB3"
    name "147"
    phclass "Grey"
  ]
  node
  [
    id 148
    gene "KCNQ4"
    name "148"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 149
    gene "GRHL2"
    name "149"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 150
    gene "GJB2"
    name "150"
    phclass "Grey"
  ]
  node
  [
    id 151
    gene "GJB6"
    name "151"
    phclass "Grey"
  ]
  node
  [
    id 152
    gene "TMC1"
    name "152"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 153
    gene "CRYM"
    name "153"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 154
    gene "MYH14"
    name "154"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 155
    gene "DFNA5"
    name "155"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 156
    gene "COCH"
    name "156"
    phclass "Grey"
  ]
  node
  [
    id 157
    gene "MYO1A"
    name "157"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 158
    gene "TMPRSS3"
    name "158"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 159
    gene "CDH23"
    name "159"
    phclass "Grey"
  ]
  node
  [
    id 160
    gene "ATP2B2"
    name "160"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 161
    gene "USH1C"
    name "161"
    phclass "Grey"
  ]
  node
  [
    id 162
    gene "PCDH15"
    name "162"
    phclass "Grey"
  ]
  node
  [
    id 163
    gene "MYO3A"
    name "163"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 164
    gene "MYO15A"
    name "164"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 165
    gene "OTOF"
    name "165"
    phclass "Ear,Nose,Throat"
  ]
  node
  [
    id 166
    gene "JAG1"
    name "166"
    phclass "Grey"
  ]
  node
  [
    id 167
    gene "TIMM8A"
    name "167"
    phclass "Grey"
  ]
  node
  [
    id 168
    gene "ACVR1B"
    name "168"
    phclass "Cancer"
  ]
  node
  [
    id 169
    gene "BRCA2"
    name "169"
    phclass "Grey"
  ]
  node
  [
    id 170
    gene "TP53"
    name "170"
    phclass "Cancer"
  ]
  node
  [
    id 171
    gene "SMAD4"
    name "171"
    phclass "Cancer"
  ]
  node
  [
    id 172
    gene "CDKN2A"
    name "172"
    phclass "Cancer"
  ]
  node
  [
    id 173
    gene "STK11"
    name "173"
    phclass "Cancer"
  ]
  node
  [
    id 174
    gene "RBBP8"
    name "174"
    phclass "Cancer"
  ]
  node
  [
    id 175
    gene "ADA"
    name "175"
    phclass "Immunological"
  ]
  node
  [
    id 176
    gene "DCLRE1C"
    name "176"
    phclass "Immunological"
  ]
  node
  [
    id 177
    gene "RAG1"
    name "177"
    phclass "Immunological"
  ]
  node
  [
    id 178
    gene "RAG2"
    name "178"
    phclass "Immunological"
  ]
  node
  [
    id 179
    gene "PTPRC"
    name "179"
    phclass "Grey"
  ]
  node
  [
    id 180
    gene "IL7R"
    name "180"
    phclass "Immunological"
  ]
  node
  [
    id 181
    gene "CD3D"
    name "181"
    phclass "Immunological"
  ]
  node
  [
    id 182
    gene "IL2RG"
    name "182"
    phclass "Immunological"
  ]
  node
  [
    id 183
    gene "ADD1"
    name "183"
    phclass "Cardiovascular"
  ]
  node
  [
    id 184
    gene "KCNMB1"
    name "184"
    phclass "Cardiovascular"
  ]
  node
  [
    id 185
    gene "NR3C2"
    name "185"
    phclass "Grey"
  ]
  node
  [
    id 186
    gene "AGTR1"
    name "186"
    phclass "Grey"
  ]
  node
  [
    id 187
    gene "PTGIS"
    name "187"
    phclass "Cardiovascular"
  ]
  node
  [
    id 188
    gene "AGT"
    name "188"
    phclass "Grey"
  ]
  node
  [
    id 189
    gene "ECE1"
    name "189"
    phclass "Grey"
  ]
  node
  [
    id 190
    gene "GNB3"
    name "190"
    phclass "Cardiovascular"
  ]
  node
  [
    id 191
    gene "RETN"
    name "191"
    phclass "Grey"
  ]
  node
  [
    id 192
    gene "HSD11B2"
    name "192"
    phclass "Grey"
  ]
  node
  [
    id 193
    gene "CYP3A5"
    name "193"
    phclass "Cardiovascular"
  ]
  node
  [
    id 194
    gene "ADRB2"
    name "194"
    phclass "Grey"
  ]
  node
  [
    id 195
    gene "PHF11"
    name "195"
    phclass "Grey"
  ]
  node
  [
    id 196
    gene "MS4A2"
    name "196"
    phclass "Respiratory"
  ]
  node
  [
    id 197
    gene "ALOX5"
    name "197"
    phclass "Grey"
  ]
  node
  [
    id 198
    gene "PTGDR"
    name "198"
    phclass "Respiratory"
  ]
  node
  [
    id 199
    gene "HNMT"
    name "199"
    phclass "Respiratory"
  ]
  node
  [
    id 200
    gene "IL12B"
    name "200"
    phclass "Respiratory"
  ]
  node
  [
    id 201
    gene "PLA2G7"
    name "201"
    phclass "Grey"
  ]
  node
  [
    id 202
    gene "SCGB3A2"
    name "202"
    phclass "Respiratory"
  ]
  node
  [
    id 203
    gene "TNF"
    name "203"
    phclass "Grey"
  ]
  node
  [
    id 204
    gene "SCGB1A1"
    name "204"
    phclass "Respiratory"
  ]
  node
  [
    id 205
    gene "POMC"
    name "205"
    phclass "Nutritional"
  ]
  node
  [
    id 206
    gene "MC4R"
    name "206"
    phclass "Nutritional"
  ]
  node
  [
    id 207
    gene "AKR1C2"
    name "207"
    phclass "Nutritional"
  ]
  node
  [
    id 208
    gene "NTRK2"
    name "208"
    phclass "Nutritional"
  ]
  node
  [
    id 209
    gene "AGRP"
    name "209"
    phclass "Nutritional"
  ]
  node
  [
    id 210
    gene "NR0B2"
    name "210"
    phclass "Nutritional"
  ]
  node
  [
    id 211
    gene "LEP"
    name "211"
    phclass "Nutritional"
  ]
  node
  [
    id 212
    gene "LEPR"
    name "212"
    phclass "Nutritional"
  ]
  node
  [
    id 213
    gene "PPARG"
    name "213"
    phclass "Grey"
  ]
  node
  [
    id 214
    gene "SIM1"
    name "214"
    phclass "Nutritional"
  ]
  node
  [
    id 215
    gene "UCP3"
    name "215"
    phclass "Nutritional"
  ]
  node
  [
    id 216
    gene "MC3R"
    name "216"
    phclass "Nutritional"
  ]
  node
  [
    id 217
    gene "ADRB3"
    name "217"
    phclass "Nutritional"
  ]
  node
  [
    id 218
    gene "ENPP1"
    name "218"
    phclass "Grey"
  ]
  node
  [
    id 219
    gene "GHRL"
    name "219"
    phclass "Nutritional"
  ]
  node
  [
    id 220
    gene "UCP1"
    name "220"
    phclass "Nutritional"
  ]
  node
  [
    id 221
    gene "UCP2"
    name "221"
    phclass "Nutritional"
  ]
  node
  [
    id 222
    gene "PCSK1"
    name "222"
    phclass "Nutritional"
  ]
  node
  [
    id 223
    gene "MATN3"
    name "223"
    phclass "Grey"
  ]
  node
  [
    id 224
    gene "CHST3"
    name "224"
    phclass "Skeletal"
  ]
  node
  [
    id 225
    gene "TRAPPC2"
    name "225"
    phclass "Skeletal"
  ]
  node
  [
    id 226
    gene "WISP3"
    name "226"
    phclass "Grey"
  ]
  node
  [
    id 227
    gene "ZFPM2"
    name "227"
    phclass "Cardiovascular"
  ]
  node
  [
    id 228
    gene "NKX2-5"
    name "228"
    phclass "Cardiovascular"
  ]
  node
  [
    id 229
    gene "STOX1"
    name "229"
    phclass "Cardiovascular"
  ]
  node
  [
    id 230
    gene "EPHX1"
    name "230"
    phclass "Grey"
  ]
  node
  [
    id 231
    gene "REN"
    name "231"
    phclass "Grey"
  ]
  node
  [
    id 232
    gene "AGTR2"
    name "232"
    phclass "Neurological"
  ]
  node
  [
    id 233
    gene "PRSS12"
    name "233"
    phclass "Neurological"
  ]
  node
  [
    id 234
    gene "CRBN"
    name "234"
    phclass "Neurological"
  ]
  node
  [
    id 235
    gene "NLGN4X"
    name "235"
    phclass "Grey"
  ]
  node
  [
    id 236
    gene "MECP2"
    name "236"
    phclass "Grey"
  ]
  node
  [
    id 237
    gene "PAK3"
    name "237"
    phclass "Neurological"
  ]
  node
  [
    id 238
    gene "IL1RAPL1"
    name "238"
    phclass "Neurological"
  ]
  node
  [
    id 239
    gene "ARX"
    name "239"
    phclass "Grey"
  ]
  node
  [
    id 240
    gene "SLC6A8"
    name "240"
    phclass "Neurological"
  ]
  node
  [
    id 241
    gene "FTSJ1"
    name "241"
    phclass "Neurological"
  ]
  node
  [
    id 242
    gene "TSPAN7"
    name "242"
    phclass "Neurological"
  ]
  node
  [
    id 243
    gene "OPHN1"
    name "243"
    phclass "Neurological"
  ]
  node
  [
    id 244
    gene "GDI1"
    name "244"
    phclass "Neurological"
  ]
  node
  [
    id 245
    gene "ACSL4"
    name "245"
    phclass "Neurological"
  ]
  node
  [
    id 246
    gene "RPS6KA3"
    name "246"
    phclass "Grey"
  ]
  node
  [
    id 247
    gene "ARHGEF6"
    name "247"
    phclass "Neurological"
  ]
  node
  [
    id 248
    gene "FGD1"
    name "248"
    phclass "Grey"
  ]
  node
  [
    id 249
    gene "ZNF41"
    name "249"
    phclass "Neurological"
  ]
  node
  [
    id 250
    gene "DLG3"
    name "250"
    phclass "Neurological"
  ]
  node
  [
    id 251
    gene "SMS"
    name "251"
    phclass "Neurological"
  ]
  node
  [
    id 252
    gene "SOX3"
    name "252"
    phclass "Grey"
  ]
  node
  [
    id 253
    gene "AK1"
    name "253"
    phclass "Hematological"
  ]
  node
  [
    id 254
    gene "BPGM"
    name "254"
    phclass "Hematological"
  ]
  node
  [
    id 255
    gene "G6PD"
    name "255"
    phclass "Grey"
  ]
  node
  [
    id 256
    gene "GCLC"
    name "256"
    phclass "Hematological"
  ]
  node
  [
    id 257
    gene "GPI"
    name "257"
    phclass "Hematological"
  ]
  node
  [
    id 258
    gene "GSS"
    name "258"
    phclass "Grey"
  ]
  node
  [
    id 259
    gene "HK1"
    name "259"
    phclass "Hematological"
  ]
  node
  [
    id 260
    gene "PGK1"
    name "260"
    phclass "Grey"
  ]
  node
  [
    id 261
    gene "TPI1"
    name "261"
    phclass "Hematological"
  ]
  node
  [
    id 262
    gene "AKT2"
    name "262"
    phclass "Endocrine"
  ]
  node
  [
    id 263
    gene "ABCC8"
    name "263"
    phclass "Grey"
  ]
  node
  [
    id 264
    gene "SUMO4"
    name "264"
    phclass "Endocrine"
  ]
  node
  [
    id 265
    gene "PTPN22"
    name "265"
    phclass "Grey"
  ]
  node
  [
    id 266
    gene "INSR"
    name "266"
    phclass "Grey"
  ]
  node
  [
    id 267
    gene "GCK"
    name "267"
    phclass "Grey"
  ]
  node
  [
    id 268
    gene "GCGR"
    name "268"
    phclass "Endocrine"
  ]
  node
  [
    id 269
    gene "GPD2"
    name "269"
    phclass "Endocrine"
  ]
  node
  [
    id 270
    gene "HNF4A"
    name "270"
    phclass "Endocrine"
  ]
  node
  [
    id 271
    gene "IRS2"
    name "271"
    phclass "Endocrine"
  ]
  node
  [
    id 272
    gene "MAPK8IP1"
    name "272"
    phclass "Endocrine"
  ]
  node
  [
    id 273
    gene "NEUROD1"
    name "273"
    phclass "Endocrine"
  ]
  node
  [
    id 274
    gene "IRS1"
    name "274"
    phclass "Endocrine"
  ]
  node
  [
    id 275
    gene "SLC2A2"
    name "275"
    phclass "Grey"
  ]
  node
  [
    id 276
    gene "SLC2A4"
    name "276"
    phclass "Endocrine"
  ]
  node
  [
    id 277
    gene "CAPN10"
    name "277"
    phclass "Endocrine"
  ]
  node
  [
    id 278
    gene "PTF1A"
    name "278"
    phclass "Endocrine"
  ]
  node
  [
    id 279
    gene "KCNJ11"
    name "279"
    phclass "Grey"
  ]
  node
  [
    id 280
    gene "FOXP3"
    name "280"
    phclass "Grey"
  ]
  node
  [
    id 281
    gene "ALOX5AP"
    name "281"
    phclass "Cardiovascular"
  ]
  node
  [
    id 282
    gene "F7"
    name "282"
    phclass "Grey"
  ]
  node
  [
    id 283
    gene "LGALS2"
    name "283"
    phclass "Cardiovascular"
  ]
  node
  [
    id 284
    gene "LTA"
    name "284"
    phclass "Cardiovascular"
  ]
  node
  [
    id 285
    gene "OLR1"
    name "285"
    phclass "Cardiovascular"
  ]
  node
  [
    id 286
    gene "THBD"
    name "286"
    phclass "Grey"
  ]
  node
  [
    id 287
    gene "GCLM"
    name "287"
    phclass "Cardiovascular"
  ]
  node
  [
    id 288
    gene "PDE4D"
    name "288"
    phclass "Cardiovascular"
  ]
  node
  [
    id 289
    gene "ALOX12B"
    name "289"
    phclass "Dermatological"
  ]
  node
  [
    id 290
    gene "TGM1"
    name "290"
    phclass "Dermatological"
  ]
  node
  [
    id 291
    gene "ALOXE3"
    name "291"
    phclass "Dermatological"
  ]
  node
  [
    id 292
    gene "ANK1"
    name "292"
    phclass "Hematological"
  ]
  node
  [
    id 293
    gene "EPB42"
    name "293"
    phclass "Hematological"
  ]
  node
  [
    id 294
    gene "SPTA1"
    name "294"
    phclass "Hematological"
  ]
  node
  [
    id 295
    gene "ANK2"
    name "295"
    phclass "Cardiovascular"
  ]
  node
  [
    id 296
    gene "KCNQ1"
    name "296"
    phclass "Grey"
  ]
  node
  [
    id 297
    gene "KCNH2"
    name "297"
    phclass "Cardiovascular"
  ]
  node
  [
    id 298
    gene "SCN5A"
    name "298"
    phclass "Cardiovascular"
  ]
  node
  [
    id 299
    gene "KCNE1"
    name "299"
    phclass "Grey"
  ]
  node
  [
    id 300
    gene "KCNE2"
    name "300"
    phclass "Cardiovascular"
  ]
  node
  [
    id 301
    gene "KCNJ2"
    name "301"
    phclass "Cardiovascular"
  ]
  node
  [
    id 302
    gene "APC"
    name "302"
    phclass "Cancer"
  ]
  node
  [
    id 303
    gene "MUTYH"
    name "303"
    phclass "Cancer"
  ]
  node
  [
    id 304
    gene "PLAG1"
    name "304"
    phclass "Cancer"
  ]
  node
  [
    id 305
    gene "RAD54B"
    name "305"
    phclass "Cancer"
  ]
  node
  [
    id 306
    gene "RAD54L"
    name "306"
    phclass "Cancer"
  ]
  node
  [
    id 307
    gene "BCL10"
    name "307"
    phclass "Cancer"
  ]
  node
  [
    id 308
    gene "PTPN12"
    name "308"
    phclass "Cancer"
  ]
  node
  [
    id 309
    gene "TGFBR2"
    name "309"
    phclass "Grey"
  ]
  node
  [
    id 310
    gene "SRC"
    name "310"
    phclass "Cancer"
  ]
  node
  [
    id 311
    gene "MLH3"
    name "311"
    phclass "Cancer"
  ]
  node
  [
    id 312
    gene "PTPRJ"
    name "312"
    phclass "Cancer"
  ]
  node
  [
    id 313
    gene "ODC1"
    name "313"
    phclass "Cancer"
  ]
  node
  [
    id 314
    gene "AXIN2"
    name "314"
    phclass "Cancer"
  ]
  node
  [
    id 315
    gene "BUB1B"
    name "315"
    phclass "Cancer"
  ]
  node
  [
    id 316
    gene "EP300"
    name "316"
    phclass "Grey"
  ]
  node
  [
    id 317
    gene "PIK3CA"
    name "317"
    phclass "Cancer"
  ]
  node
  [
    id 318
    gene "BAX"
    name "318"
    phclass "Cancer"
  ]
  node
  [
    id 319
    gene "CTNNB1"
    name "319"
    phclass "Cancer"
  ]
  node
  [
    id 320
    gene "DCC"
    name "320"
    phclass "Cancer"
  ]
  node
  [
    id 321
    gene "MCC"
    name "321"
    phclass "Cancer"
  ]
  node
  [
    id 322
    gene "NRAS"
    name "322"
    phclass "Cancer"
  ]
  node
  [
    id 323
    gene "MSH2"
    name "323"
    phclass "Cancer"
  ]
  node
  [
    id 324
    gene "MLH1"
    name "324"
    phclass "Cancer"
  ]
  node
  [
    id 325
    gene "PMS1"
    name "325"
    phclass "Cancer"
  ]
  node
  [
    id 326
    gene "PMS2"
    name "326"
    phclass "Grey"
  ]
  node
  [
    id 327
    gene "MSH6"
    name "327"
    phclass "Cancer"
  ]
  node
  [
    id 328
    gene "FGFR3"
    name "328"
    phclass "Grey"
  ]
  node
  [
    id 329
    gene "FLCN"
    name "329"
    phclass "Grey"
  ]
  node
  [
    id 330
    gene "BRAF"
    name "330"
    phclass "Cancer"
  ]
  node
  [
    id 331
    gene "DLC1"
    name "331"
    phclass "Cancer"
  ]
  node
  [
    id 332
    gene "PLA2G2A"
    name "332"
    phclass "Cancer"
  ]
  node
  [
    id 333
    gene "BUB1"
    name "333"
    phclass "Cancer"
  ]
  node
  [
    id 334
    gene "IRF1"
    name "334"
    phclass "Grey"
  ]
  node
  [
    id 335
    gene "CDH1"
    name "335"
    phclass "Grey"
  ]
  node
  [
    id 336
    gene "IL1B"
    name "336"
    phclass "Cancer"
  ]
  node
  [
    id 337
    gene "IL1RN"
    name "337"
    phclass "Cancer"
  ]
  node
  [
    id 338
    gene "CASP10"
    name "338"
    phclass "Grey"
  ]
  node
  [
    id 339
    gene "ERBB2"
    name "339"
    phclass "Cancer"
  ]
  node
  [
    id 340
    gene "FGFR2"
    name "340"
    phclass "Grey"
  ]
  node
  [
    id 341
    gene "KLF6"
    name "341"
    phclass "Cancer"
  ]
  node
  [
    id 342
    gene "AIRE"
    name "342"
    phclass "Immunological"
  ]
  node
  [
    id 343
    gene "FAS"
    name "343"
    phclass "Grey"
  ]
  node
  [
    id 344
    gene "CASP8"
    name "344"
    phclass "Grey"
  ]
  node
  [
    id 345
    gene "APOA1"
    name "345"
    phclass "Grey"
  ]
  node
  [
    id 346
    gene "GSN"
    name "346"
    phclass "Neurological"
  ]
  node
  [
    id 347
    gene "FGA"
    name "347"
    phclass "Grey"
  ]
  node
  [
    id 348
    gene "LYZ"
    name "348"
    phclass "Neurological"
  ]
  node
  [
    id 349
    gene "TTR"
    name "349"
    phclass "Grey"
  ]
  node
  [
    id 350
    gene "APOA2"
    name "350"
    phclass "Metabolic"
  ]
  node
  [
    id 351
    gene "APOC3"
    name "351"
    phclass "Metabolic"
  ]
  node
  [
    id 352
    gene "APOH"
    name "352"
    phclass "Metabolic"
  ]
  node
  [
    id 353
    gene "TGFBI"
    name "353"
    phclass "Ophthamological"
  ]
  node
  [
    id 354
    gene "TACSTD2"
    name "354"
    phclass "Ophthamological"
  ]
  node
  [
    id 355
    gene "VSX1"
    name "355"
    phclass "Grey"
  ]
  node
  [
    id 356
    gene "COL8A2"
    name "356"
    phclass "Ophthamological"
  ]
  node
  [
    id 357
    gene "KERA"
    name "357"
    phclass "Ophthamological"
  ]
  node
  [
    id 358
    gene "APOA5"
    name "358"
    phclass "Metabolic"
  ]
  node
  [
    id 359
    gene "APOB"
    name "359"
    phclass "Metabolic"
  ]
  node
  [
    id 360
    gene "LDLR"
    name "360"
    phclass "Metabolic"
  ]
  node
  [
    id 361
    gene "PCSK9"
    name "361"
    phclass "Metabolic"
  ]
  node
  [
    id 362
    gene "LDLRAP1"
    name "362"
    phclass "Metabolic"
  ]
  node
  [
    id 363
    gene "EPHX2"
    name "363"
    phclass "Metabolic"
  ]
  node
  [
    id 364
    gene "ITIH4"
    name "364"
    phclass "Metabolic"
  ]
  node
  [
    id 365
    gene "APOC2"
    name "365"
    phclass "Metabolic"
  ]
  node
  [
    id 366
    gene "DISC1"
    name "366"
    phclass "Psychiatric"
  ]
  node
  [
    id 367
    gene "COMT"
    name "367"
    phclass "Psychiatric"
  ]
  node
  [
    id 368
    gene "HTR2A"
    name "368"
    phclass "Grey"
  ]
  node
  [
    id 369
    gene "RTN4R"
    name "369"
    phclass "Psychiatric"
  ]
  node
  [
    id 370
    gene "SYN2"
    name "370"
    phclass "Psychiatric"
  ]
  node
  [
    id 371
    gene "PRODH"
    name "371"
    phclass "Grey"
  ]
  node
  [
    id 372
    gene "APRT"
    name "372"
    phclass "Metabolic"
  ]
  node
  [
    id 373
    gene "SLC34A1"
    name "373"
    phclass "Grey"
  ]
  node
  [
    id 374
    gene "ING1"
    name "374"
    phclass "Cancer"
  ]
  node
  [
    id 375
    gene "TNFRSF10B"
    name "375"
    phclass "Cancer"
  ]
  node
  [
    id 376
    gene "FASLG"
    name "376"
    phclass "Immunological"
  ]
  node
  [
    id 377
    gene "DNASE1"
    name "377"
    phclass "Immunological"
  ]
  node
  [
    id 378
    gene "PDCD1"
    name "378"
    phclass "Immunological"
  ]
  node
  [
    id 379
    gene "AR"
    name "379"
    phclass "Grey"
  ]
  node
  [
    id 380
    gene "CHEK2"
    name "380"
    phclass "Cancer"
  ]
  node
  [
    id 381
    gene "PPM1D"
    name "381"
    phclass "Cancer"
  ]
  node
  [
    id 382
    gene "SLC22A18"
    name "382"
    phclass "Cancer"
  ]
  node
  [
    id 383
    gene "BRCA1"
    name "383"
    phclass "Cancer"
  ]
  node
  [
    id 384
    gene "TSG101"
    name "384"
    phclass "Cancer"
  ]
  node
  [
    id 385
    gene "BRIP1"
    name "385"
    phclass "Grey"
  ]
  node
  [
    id 386
    gene "RB1CC1"
    name "386"
    phclass "Cancer"
  ]
  node
  [
    id 387
    gene "PHB"
    name "387"
    phclass "Cancer"
  ]
  node
  [
    id 388
    gene "ATM"
    name "388"
    phclass "Grey"
  ]
  node
  [
    id 389
    gene "BARD1"
    name "389"
    phclass "Cancer"
  ]
  node
  [
    id 390
    gene "RAD51"
    name "390"
    phclass "Cancer"
  ]
  node
  [
    id 391
    gene "XRCC3"
    name "391"
    phclass "Cancer"
  ]
  node
  [
    id 392
    gene "RNASEL"
    name "392"
    phclass "Cancer"
  ]
  node
  [
    id 393
    gene "PTEN"
    name "393"
    phclass "Grey"
  ]
  node
  [
    id 394
    gene "MSR1"
    name "394"
    phclass "Cancer"
  ]
  node
  [
    id 395
    gene "EPHB2"
    name "395"
    phclass "Cancer"
  ]
  node
  [
    id 396
    gene "MAD1L1"
    name "396"
    phclass "Cancer"
  ]
  node
  [
    id 397
    gene "ELAC2"
    name "397"
    phclass "Cancer"
  ]
  node
  [
    id 398
    gene "MXI1"
    name "398"
    phclass "Cancer"
  ]
  node
  [
    id 399
    gene "VAPB"
    name "399"
    phclass "Grey"
  ]
  node
  [
    id 400
    gene "SMN1"
    name "400"
    phclass "Muscular"
  ]
  node
  [
    id 401
    gene "BSCL2"
    name "401"
    phclass "Grey"
  ]
  node
  [
    id 402
    gene "GARS"
    name "402"
    phclass "Grey"
  ]
  node
  [
    id 403
    gene "HEXB"
    name "403"
    phclass "Grey"
  ]
  node
  [
    id 404
    gene "IGHMBP2"
    name "404"
    phclass "Muscular"
  ]
  node
  [
    id 405
    gene "KRT10"
    name "405"
    phclass "Dermatological"
  ]
  node
  [
    id 406
    gene "KRT1"
    name "406"
    phclass "Dermatological"
  ]
  node
  [
    id 407
    gene "MRE11A"
    name "407"
    phclass "Immunological"
  ]
  node
  [
    id 408
    gene "RAP1GDS1"
    name "408"
    phclass "Cancer"
  ]
  node
  [
    id 409
    gene "FCGR2B"
    name "409"
    phclass "Cancer"
  ]
  node
  [
    id 410
    gene "SH2D1A"
    name "410"
    phclass "Cancer"
  ]
  node
  [
    id 411
    gene "ATP1A2"
    name "411"
    phclass "Neurological"
  ]
  node
  [
    id 412
    gene "EDNRA"
    name "412"
    phclass "Neurological"
  ]
  node
  [
    id 413
    gene "ATP6V1B1"
    name "413"
    phclass "Renal"
  ]
  node
  [
    id 414
    gene "ATP6V0A4"
    name "414"
    phclass "Renal"
  ]
  node
  [
    id 415
    gene "CA2"
    name "415"
    phclass "Renal"
  ]
  node
  [
    id 416
    gene "SLC4A4"
    name "416"
    phclass "Renal"
  ]
  node
  [
    id 417
    gene "ATP7A"
    name "417"
    phclass "Grey"
  ]
  node
  [
    id 418
    gene "ELN"
    name "418"
    phclass "Grey"
  ]
  node
  [
    id 419
    gene "FBLN5"
    name "419"
    phclass "Grey"
  ]
  node
  [
    id 420
    gene "BAAT"
    name "420"
    phclass "Gastrointestinal"
  ]
  node
  [
    id 421
    gene "TJP2"
    name "421"
    phclass "Gastrointestinal"
  ]
  node
  [
    id 422
    gene "BCKDHA"
    name "422"
    phclass "Metabolic"
  ]
  node
  [
    id 423
    gene "BCKDHB"
    name "423"
    phclass "Metabolic"
  ]
  node
  [
    id 424
    gene "DBT"
    name "424"
    phclass "Metabolic"
  ]
  node
  [
    id 425
    gene "VHL"
    name "425"
    phclass "Grey"
  ]
  node
  [
    id 426
    gene "BCS1L"
    name "426"
    phclass "Grey"
  ]
  node
  [
    id 427
    gene "NDUFS3"
    name "427"
    phclass "Neurological"
  ]
  node
  [
    id 428
    gene "NDUFS4"
    name "428"
    phclass "Grey"
  ]
  node
  [
    id 429
    gene "NDUFS7"
    name "429"
    phclass "Neurological"
  ]
  node
  [
    id 430
    gene "NDUFS8"
    name "430"
    phclass "Neurological"
  ]
  node
  [
    id 431
    gene "NDUFV1"
    name "431"
    phclass "Grey"
  ]
  node
  [
    id 432
    gene "SDHA"
    name "432"
    phclass "Grey"
  ]
  node
  [
    id 433
    gene "SURF1"
    name "433"
    phclass "Neurological"
  ]
  node
  [
    id 434
    gene "LRPPRC"
    name "434"
    phclass "Neurological"
  ]
  node
  [
    id 435
    gene "PDHA1"
    name "435"
    phclass "Grey"
  ]
  node
  [
    id 436
    gene "NDUFS1"
    name "436"
    phclass "multiple"
  ]
  node
  [
    id 437
    gene "NDUFS2"
    name "437"
    phclass "multiple"
  ]
  node
  [
    id 438
    gene "UQCRB"
    name "438"
    phclass "multiple"
  ]
  node
  [
    id 439
    gene "BDNF"
    name "439"
    phclass "Grey"
  ]
  node
  [
    id 440
    gene "GDNF"
    name "440"
    phclass "Grey"
  ]
  node
  [
    id 441
    gene "EDN3"
    name "441"
    phclass "Grey"
  ]
  node
  [
    id 442
    gene "PHOX2B"
    name "442"
    phclass "Grey"
  ]
  node
  [
    id 443
    gene "RET"
    name "443"
    phclass "Grey"
  ]
  node
  [
    id 444
    gene "SLC6A4"
    name "444"
    phclass "Psychiatric"
  ]
  node
  [
    id 445
    gene "BMPR1A"
    name "445"
    phclass "Cancer"
  ]
  node
  [
    id 446
    gene "RRAS2"
    name "446"
    phclass "Cancer"
  ]
  node
  [
    id 447
    gene "EGFR"
    name "447"
    phclass "Cancer"
  ]
  node
  [
    id 448
    gene "PARK2"
    name "448"
    phclass "Grey"
  ]
  node
  [
    id 449
    gene "CDK4"
    name "449"
    phclass "Cancer"
  ]
  node
  [
    id 450
    gene "FANCA"
    name "450"
    phclass "multiple"
  ]
  node
  [
    id 451
    gene "FANCB"
    name "451"
    phclass "multiple"
  ]
  node
  [
    id 452
    gene "FANCC"
    name "452"
    phclass "multiple"
  ]
  node
  [
    id 453
    gene "FANCD2"
    name "453"
    phclass "multiple"
  ]
  node
  [
    id 454
    gene "FANCE"
    name "454"
    phclass "multiple"
  ]
  node
  [
    id 455
    gene "FANCF"
    name "455"
    phclass "multiple"
  ]
  node
  [
    id 456
    gene "FANCG"
    name "456"
    phclass "multiple"
  ]
  node
  [
    id 457
    gene "FANCL"
    name "457"
    phclass "multiple"
  ]
  node
  [
    id 458
    gene "FANCM"
    name "458"
    phclass "multiple"
  ]
  node
  [
    id 459
    gene "GPC3"
    name "459"
    phclass "Grey"
  ]
  node
  [
    id 460
    gene "WT1"
    name "460"
    phclass "Grey"
  ]
  node
  [
    id 461
    gene "C1QA"
    name "461"
    phclass "Immunological"
  ]
  node
  [
    id 462
    gene "C1QB"
    name "462"
    phclass "Immunological"
  ]
  node
  [
    id 463
    gene "C1S"
    name "463"
    phclass "Immunological"
  ]
  node
  [
    id 464
    gene "C2"
    name "464"
    phclass "Immunological"
  ]
  node
  [
    id 465
    gene "C3"
    name "465"
    phclass "Immunological"
  ]
  node
  [
    id 466
    gene "C4A"
    name "466"
    phclass "Immunological"
  ]
  node
  [
    id 467
    gene "C4B"
    name "467"
    phclass "Immunological"
  ]
  node
  [
    id 468
    gene "C6"
    name "468"
    phclass "Immunological"
  ]
  node
  [
    id 469
    gene "C7"
    name "469"
    phclass "Immunological"
  ]
  node
  [
    id 470
    gene "C8B"
    name "470"
    phclass "Immunological"
  ]
  node
  [
    id 471
    gene "C9"
    name "471"
    phclass "Immunological"
  ]
  node
  [
    id 472
    gene "CACNA1A"
    name "472"
    phclass "Neurological"
  ]
  node
  [
    id 473
    gene "CP"
    name "473"
    phclass "Grey"
  ]
  node
  [
    id 474
    gene "KCNA1"
    name "474"
    phclass "Neurological"
  ]
  node
  [
    id 475
    gene "ATXN10"
    name "475"
    phclass "Neurological"
  ]
  node
  [
    id 476
    gene "ATXN1"
    name "476"
    phclass "Neurological"
  ]
  node
  [
    id 477
    gene "PPP2R2B"
    name "477"
    phclass "Neurological"
  ]
  node
  [
    id 478
    gene "PRKCG"
    name "478"
    phclass "Neurological"
  ]
  node
  [
    id 479
    gene "TBP"
    name "479"
    phclass "Neurological"
  ]
  node
  [
    id 480
    gene "ATXN2"
    name "480"
    phclass "Neurological"
  ]
  node
  [
    id 481
    gene "PLEKHG4"
    name "481"
    phclass "Neurological"
  ]
  node
  [
    id 482
    gene "ATXN7"
    name "482"
    phclass "Neurological"
  ]
  node
  [
    id 483
    gene "TDP1"
    name "483"
    phclass "Neurological"
  ]
  node
  [
    id 484
    gene "CACNA1F"
    name "484"
    phclass "Ophthamological"
  ]
  node
  [
    id 485
    gene "GNAT1"
    name "485"
    phclass "Ophthamological"
  ]
  node
  [
    id 486
    gene "NYX"
    name "486"
    phclass "Ophthamological"
  ]
  node
  [
    id 487
    gene "CACNA1S"
    name "487"
    phclass "Grey"
  ]
  node
  [
    id 488
    gene "KCNE3"
    name "488"
    phclass "Renal"
  ]
  node
  [
    id 489
    gene "SCN4A"
    name "489"
    phclass "Grey"
  ]
  node
  [
    id 490
    gene "RYR1"
    name "490"
    phclass "Grey"
  ]
  node
  [
    id 491
    gene "CACNB4"
    name "491"
    phclass "Neurological"
  ]
  node
  [
    id 492
    gene "ATCAY"
    name "492"
    phclass "Neurological"
  ]
  node
  [
    id 493
    gene "APTX"
    name "493"
    phclass "Neurological"
  ]
  node
  [
    id 494
    gene "TTPA"
    name "494"
    phclass "Neurological"
  ]
  node
  [
    id 495
    gene "KCNQ2"
    name "495"
    phclass "Neurological"
  ]
  node
  [
    id 496
    gene "KCNQ3"
    name "496"
    phclass "Neurological"
  ]
  node
  [
    id 497
    gene "GABRG2"
    name "497"
    phclass "Neurological"
  ]
  node
  [
    id 498
    gene "CLCN2"
    name "498"
    phclass "Neurological"
  ]
  node
  [
    id 499
    gene "JRK"
    name "499"
    phclass "Neurological"
  ]
  node
  [
    id 500
    gene "SCN1A"
    name "500"
    phclass "Neurological"
  ]
  node
  [
    id 501
    gene "ME2"
    name "501"
    phclass "Neurological"
  ]
  node
  [
    id 502
    gene "GABRA1"
    name "502"
    phclass "Neurological"
  ]
  node
  [
    id 503
    gene "EPM2A"
    name "503"
    phclass "Neurological"
  ]
  node
  [
    id 504
    gene "NHLRC1"
    name "504"
    phclass "Neurological"
  ]
  node
  [
    id 505
    gene "SLC25A22"
    name "505"
    phclass "Neurological"
  ]
  node
  [
    id 506
    gene "CHRNA4"
    name "506"
    phclass "Grey"
  ]
  node
  [
    id 507
    gene "LGI1"
    name "507"
    phclass "Grey"
  ]
  node
  [
    id 508
    gene "CSTB"
    name "508"
    phclass "Neurological"
  ]
  node
  [
    id 509
    gene "SYN1"
    name "509"
    phclass "Neurological"
  ]
  node
  [
    id 510
    gene "CALCA"
    name "510"
    phclass "Bone"
  ]
  node
  [
    id 511
    gene "COL1A1"
    name "511"
    phclass "Grey"
  ]
  node
  [
    id 512
    gene "LRP5"
    name "512"
    phclass "Grey"
  ]
  node
  [
    id 513
    gene "COL1A2"
    name "513"
    phclass "Grey"
  ]
  node
  [
    id 514
    gene "CALCR"
    name "514"
    phclass "Bone"
  ]
  node
  [
    id 515
    gene "CAPN3"
    name "515"
    phclass "Muscular"
  ]
  node
  [
    id 516
    gene "FKRP"
    name "516"
    phclass "Muscular"
  ]
  node
  [
    id 517
    gene "LAMA2"
    name "517"
    phclass "Muscular"
  ]
  node
  [
    id 518
    gene "MYOT"
    name "518"
    phclass "Muscular"
  ]
  node
  [
    id 519
    gene "SGCG"
    name "519"
    phclass "Muscular"
  ]
  node
  [
    id 520
    gene "SGCA"
    name "520"
    phclass "Muscular"
  ]
  node
  [
    id 521
    gene "SGCB"
    name "521"
    phclass "Muscular"
  ]
  node
  [
    id 522
    gene "TRIM32"
    name "522"
    phclass "Muscular"
  ]
  node
  [
    id 523
    gene "POMT1"
    name "523"
    phclass "Grey"
  ]
  node
  [
    id 524
    gene "SEPN1"
    name "524"
    phclass "Muscular"
  ]
  node
  [
    id 525
    gene "AXIN1"
    name "525"
    phclass "Cancer"
  ]
  node
  [
    id 526
    gene "IGF2R"
    name "526"
    phclass "Cancer"
  ]
  node
  [
    id 527
    gene "MET"
    name "527"
    phclass "Cancer"
  ]
  node
  [
    id 528
    gene "CASQ2"
    name "528"
    phclass "Cardiovascular"
  ]
  node
  [
    id 529
    gene "GNAI2"
    name "529"
    phclass "Grey"
  ]
  node
  [
    id 530
    gene "RYR2"
    name "530"
    phclass "Cardiovascular"
  ]
  node
  [
    id 531
    gene "CASR"
    name "531"
    phclass "Endocrine"
  ]
  node
  [
    id 532
    gene "MEN1"
    name "532"
    phclass "Grey"
  ]
  node
  [
    id 533
    gene "CDC73"
    name "533"
    phclass "Grey"
  ]
  node
  [
    id 534
    gene "P2RY12"
    name "534"
    phclass "Hematological"
  ]
  node
  [
    id 535
    gene "CD36"
    name "535"
    phclass "Grey"
  ]
  node
  [
    id 536
    gene "IL10"
    name "536"
    phclass "Grey"
  ]
  node
  [
    id 537
    gene "CIITA"
    name "537"
    phclass "Grey"
  ]
  node
  [
    id 538
    gene "NFKBIL1"
    name "538"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 539
    gene "PADI4"
    name "539"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 540
    gene "SLC22A4"
    name "540"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 541
    gene "MIF"
    name "541"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 542
    gene "CBFB"
    name "542"
    phclass "Cancer"
  ]
  node
  [
    id 543
    gene "ACSL6"
    name "543"
    phclass "Grey"
  ]
  node
  [
    id 544
    gene "CSF1R"
    name "544"
    phclass "Cancer"
  ]
  node
  [
    id 545
    gene "CBS"
    name "545"
    phclass "Grey"
  ]
  node
  [
    id 546
    gene "MTHFR"
    name "546"
    phclass "Metabolic"
  ]
  node
  [
    id 547
    gene "F5"
    name "547"
    phclass "Hematological"
  ]
  node
  [
    id 548
    gene "SERPIND1"
    name "548"
    phclass "Hematological"
  ]
  node
  [
    id 549
    gene "HRG"
    name "549"
    phclass "Hematological"
  ]
  node
  [
    id 550
    gene "PROC"
    name "550"
    phclass "Hematological"
  ]
  node
  [
    id 551
    gene "FGB"
    name "551"
    phclass "Hematological"
  ]
  node
  [
    id 552
    gene "FGG"
    name "552"
    phclass "Hematological"
  ]
  node
  [
    id 553
    gene "ICAM1"
    name "553"
    phclass "Immunological"
  ]
  node
  [
    id 554
    gene "MSH3"
    name "554"
    phclass "Cancer"
  ]
  node
  [
    id 555
    gene "CHAT"
    name "555"
    phclass "Muscular"
  ]
  node
  [
    id 556
    gene "CHRNE"
    name "556"
    phclass "Muscular"
  ]
  node
  [
    id 557
    gene "RAPSN"
    name "557"
    phclass "Muscular"
  ]
  node
  [
    id 558
    gene "CHRNA1"
    name "558"
    phclass "Muscular"
  ]
  node
  [
    id 559
    gene "CHRND"
    name "559"
    phclass "Muscular"
  ]
  node
  [
    id 560
    gene "CYP2A6"
    name "560"
    phclass "Grey"
  ]
  node
  [
    id 561
    gene "CLCN1"
    name "561"
    phclass "Muscular"
  ]
  node
  [
    id 562
    gene "CLCN7"
    name "562"
    phclass "Bone"
  ]
  node
  [
    id 563
    gene "OSTM1"
    name "563"
    phclass "Bone"
  ]
  node
  [
    id 564
    gene "TCIRG1"
    name "564"
    phclass "Bone"
  ]
  node
  [
    id 565
    gene "CCR2"
    name "565"
    phclass "Immunological"
  ]
  node
  [
    id 566
    gene "CCL5"
    name "566"
    phclass "Immunological"
  ]
  node
  [
    id 567
    gene "CCR5"
    name "567"
    phclass "Immunological"
  ]
  node
  [
    id 568
    gene "CNGA3"
    name "568"
    phclass "Ophthamological"
  ]
  node
  [
    id 569
    gene "GNAT2"
    name "569"
    phclass "Ophthamological"
  ]
  node
  [
    id 570
    gene "TNXB"
    name "570"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 571
    gene "B4GALT7"
    name "571"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 572
    gene "COL5A1"
    name "572"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 573
    gene "COL5A2"
    name "573"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 574
    gene "COL3A1"
    name "574"
    phclass "Grey"
  ]
  node
  [
    id 575
    gene "PLOD1"
    name "575"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 576
    gene "ADAMTS2"
    name "576"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 577
    gene "FBN1"
    name "577"
    phclass "Grey"
  ]
  node
  [
    id 578
    gene "COL2A1"
    name "578"
    phclass "Grey"
  ]
  node
  [
    id 579
    gene "FRZB"
    name "579"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 580
    gene "COL11A1"
    name "580"
    phclass "multiple"
  ]
  node
  [
    id 581
    gene "COL7A1"
    name "581"
    phclass "Dermatological"
  ]
  node
  [
    id 582
    gene "COL17A1"
    name "582"
    phclass "Dermatological"
  ]
  node
  [
    id 583
    gene "ITGB4"
    name "583"
    phclass "Dermatological"
  ]
  node
  [
    id 584
    gene "LAMA3"
    name "584"
    phclass "Grey"
  ]
  node
  [
    id 585
    gene "LAMB3"
    name "585"
    phclass "Dermatological"
  ]
  node
  [
    id 586
    gene "LAMC2"
    name "586"
    phclass "Dermatological"
  ]
  node
  [
    id 587
    gene "ITGA6"
    name "587"
    phclass "Dermatological"
  ]
  node
  [
    id 588
    gene "DSP"
    name "588"
    phclass "Grey"
  ]
  node
  [
    id 589
    gene "KRT14"
    name "589"
    phclass "Dermatological"
  ]
  node
  [
    id 590
    gene "KRT5"
    name "590"
    phclass "Dermatological"
  ]
  node
  [
    id 591
    gene "COL9A1"
    name "591"
    phclass "Bone"
  ]
  node
  [
    id 592
    gene "COMP"
    name "592"
    phclass "Grey"
  ]
  node
  [
    id 593
    gene "SLC26A2"
    name "593"
    phclass "Grey"
  ]
  node
  [
    id 594
    gene "COL9A3"
    name "594"
    phclass "Grey"
  ]
  node
  [
    id 595
    gene "COL9A2"
    name "595"
    phclass "Grey"
  ]
  node
  [
    id 596
    gene "MAP3K8"
    name "596"
    phclass "Cancer"
  ]
  node
  [
    id 597
    gene "PPP2R1B"
    name "597"
    phclass "Cancer"
  ]
  node
  [
    id 598
    gene "CPT1A"
    name "598"
    phclass "Metabolic"
  ]
  node
  [
    id 599
    gene "CREBBP"
    name "599"
    phclass "multiple"
  ]
  node
  [
    id 600
    gene "RDH12"
    name "600"
    phclass "Ophthamological"
  ]
  node
  [
    id 601
    gene "CRYAA"
    name "601"
    phclass "Ophthamological"
  ]
  node
  [
    id 602
    gene "CRYBB2"
    name "602"
    phclass "Ophthamological"
  ]
  node
  [
    id 603
    gene "PITX3"
    name "603"
    phclass "Ophthamological"
  ]
  node
  [
    id 604
    gene "BFSP2"
    name "604"
    phclass "Ophthamological"
  ]
  node
  [
    id 605
    gene "PAX6"
    name "605"
    phclass "Grey"
  ]
  node
  [
    id 606
    gene "CRYBA1"
    name "606"
    phclass "Ophthamological"
  ]
  node
  [
    id 607
    gene "CRYGC"
    name "607"
    phclass "Ophthamological"
  ]
  node
  [
    id 608
    gene "CRYGD"
    name "608"
    phclass "Ophthamological"
  ]
  node
  [
    id 609
    gene "HSF4"
    name "609"
    phclass "Ophthamological"
  ]
  node
  [
    id 610
    gene "MIP"
    name "610"
    phclass "Ophthamological"
  ]
  node
  [
    id 611
    gene "CRYBB1"
    name "611"
    phclass "Ophthamological"
  ]
  node
  [
    id 612
    gene "GJA8"
    name "612"
    phclass "Ophthamological"
  ]
  node
  [
    id 613
    gene "GJA3"
    name "613"
    phclass "Ophthamological"
  ]
  node
  [
    id 614
    gene "GATA4"
    name "614"
    phclass "Cardiovascular"
  ]
  node
  [
    id 615
    gene "GJA1"
    name "615"
    phclass "Grey"
  ]
  node
  [
    id 616
    gene "CRELD1"
    name "616"
    phclass "Cardiovascular"
  ]
  node
  [
    id 617
    gene "CTLA4"
    name "617"
    phclass "Endocrine"
  ]
  node
  [
    id 618
    gene "GC"
    name "618"
    phclass "Endocrine"
  ]
  node
  [
    id 619
    gene "SLC5A5"
    name "619"
    phclass "Endocrine"
  ]
  node
  [
    id 620
    gene "PAX8"
    name "620"
    phclass "Endocrine"
  ]
  node
  [
    id 621
    gene "TSHR"
    name "621"
    phclass "Grey"
  ]
  node
  [
    id 622
    gene "TG"
    name "622"
    phclass "Endocrine"
  ]
  node
  [
    id 623
    gene "CYP1B1"
    name "623"
    phclass "Grey"
  ]
  node
  [
    id 624
    gene "MYOC"
    name "624"
    phclass "Ophthamological"
  ]
  node
  [
    id 625
    gene "OPTN"
    name "625"
    phclass "Ophthamological"
  ]
  node
  [
    id 626
    gene "OPA1"
    name "626"
    phclass "Ophthamological"
  ]
  node
  [
    id 627
    gene "DBH"
    name "627"
    phclass "Grey"
  ]
  node
  [
    id 628
    gene "NR4A2"
    name "628"
    phclass "Neurological"
  ]
  node
  [
    id 629
    gene "SNCAIP"
    name "629"
    phclass "Neurological"
  ]
  node
  [
    id 630
    gene "SNCA"
    name "630"
    phclass "Neurological"
  ]
  node
  [
    id 631
    gene "PARK7"
    name "631"
    phclass "Neurological"
  ]
  node
  [
    id 632
    gene "LRRK2"
    name "632"
    phclass "Neurological"
  ]
  node
  [
    id 633
    gene "PINK1"
    name "633"
    phclass "Neurological"
  ]
  node
  [
    id 634
    gene "UCHL1"
    name "634"
    phclass "Neurological"
  ]
  node
  [
    id 635
    gene "NDUFV2"
    name "635"
    phclass "Neurological"
  ]
  node
  [
    id 636
    gene "DCTN1"
    name "636"
    phclass "Neurological"
  ]
  node
  [
    id 637
    gene "SOD1"
    name "637"
    phclass "Neurological"
  ]
  node
  [
    id 638
    gene "ALS2"
    name "638"
    phclass "Neurological"
  ]
  node
  [
    id 639
    gene "NEFH"
    name "639"
    phclass "Neurological"
  ]
  node
  [
    id 640
    gene "PRPH"
    name "640"
    phclass "Neurological"
  ]
  node
  [
    id 641
    gene "DCX"
    name "641"
    phclass "Neurological"
  ]
  node
  [
    id 642
    gene "PAFAH1B1"
    name "642"
    phclass "Neurological"
  ]
  node
  [
    id 643
    gene "RELN"
    name "643"
    phclass "Neurological"
  ]
  node
  [
    id 644
    gene "DMBT1"
    name "644"
    phclass "Cancer"
  ]
  node
  [
    id 645
    gene "DNM2"
    name "645"
    phclass "Neurological"
  ]
  node
  [
    id 646
    gene "HSPB1"
    name "646"
    phclass "Neurological"
  ]
  node
  [
    id 647
    gene "MPZ"
    name "647"
    phclass "Grey"
  ]
  node
  [
    id 648
    gene "HOXD10"
    name "648"
    phclass "Grey"
  ]
  node
  [
    id 649
    gene "GDAP1"
    name "649"
    phclass "Neurological"
  ]
  node
  [
    id 650
    gene "PMP22"
    name "650"
    phclass "Grey"
  ]
  node
  [
    id 651
    gene "LITAF"
    name "651"
    phclass "Neurological"
  ]
  node
  [
    id 652
    gene "EGR2"
    name "652"
    phclass "Grey"
  ]
  node
  [
    id 653
    gene "NEFL"
    name "653"
    phclass "Neurological"
  ]
  node
  [
    id 654
    gene "KIF1B"
    name "654"
    phclass "Neurological"
  ]
  node
  [
    id 655
    gene "MFN2"
    name "655"
    phclass "Neurological"
  ]
  node
  [
    id 656
    gene "MTMR2"
    name "656"
    phclass "Neurological"
  ]
  node
  [
    id 657
    gene "SBF2"
    name "657"
    phclass "Neurological"
  ]
  node
  [
    id 658
    gene "NDRG1"
    name "658"
    phclass "Neurological"
  ]
  node
  [
    id 659
    gene "GJB1"
    name "659"
    phclass "Neurological"
  ]
  node
  [
    id 660
    gene "DSG1"
    name "660"
    phclass "Dermatological"
  ]
  node
  [
    id 661
    gene "PKP2"
    name "661"
    phclass "Cardiovascular"
  ]
  node
  [
    id 662
    gene "FLNB"
    name "662"
    phclass "Grey"
  ]
  node
  [
    id 663
    gene "NRTN"
    name "663"
    phclass "Gastrointestinal"
  ]
  node
  [
    id 664
    gene "EDNRB"
    name "664"
    phclass "Grey"
  ]
  node
  [
    id 665
    gene "EDA"
    name "665"
    phclass "Dermatological"
  ]
  node
  [
    id 666
    gene "EDARADD"
    name "666"
    phclass "Dermatological"
  ]
  node
  [
    id 667
    gene "IKBKG"
    name "667"
    phclass "Dermatological"
  ]
  node
  [
    id 668
    gene "NFKBIA"
    name "668"
    phclass "Dermatological"
  ]
  node
  [
    id 669
    gene "EDAR"
    name "669"
    phclass "Dermatological"
  ]
  node
  [
    id 670
    gene "PKP1"
    name "670"
    phclass "Dermatological"
  ]
  node
  [
    id 671
    gene "SOX10"
    name "671"
    phclass "Grey"
  ]
  node
  [
    id 672
    gene "PRX"
    name "672"
    phclass "multiple"
  ]
  node
  [
    id 673
    gene "HSPB8"
    name "673"
    phclass "Neurological"
  ]
  node
  [
    id 674
    gene "SPTLC1"
    name "674"
    phclass "Neurological"
  ]
  node
  [
    id 675
    gene "FCGR3A"
    name "675"
    phclass "Grey"
  ]
  node
  [
    id 676
    gene "GFI1"
    name "676"
    phclass "Hematological"
  ]
  node
  [
    id 677
    gene "WAS"
    name "677"
    phclass "Grey"
  ]
  node
  [
    id 678
    gene "EMD"
    name "678"
    phclass "Muscular"
  ]
  node
  [
    id 679
    gene "EPB41"
    name "679"
    phclass "Hematological"
  ]
  node
  [
    id 680
    gene "EYA1"
    name "680"
    phclass "Grey"
  ]
  node
  [
    id 681
    gene "FOXC1"
    name "681"
    phclass "Grey"
  ]
  node
  [
    id 682
    gene "SERPINA1"
    name "682"
    phclass "Grey"
  ]
  node
  [
    id 683
    gene "MASTL"
    name "683"
    phclass "Hematological"
  ]
  node
  [
    id 684
    gene "MPL"
    name "684"
    phclass "Hematological"
  ]
  node
  [
    id 685
    gene "CFH"
    name "685"
    phclass "Grey"
  ]
  node
  [
    id 686
    gene "MCFD2"
    name "686"
    phclass "Hematological"
  ]
  node
  [
    id 687
    gene "F10"
    name "687"
    phclass "Hematological"
  ]
  node
  [
    id 688
    gene "F11"
    name "688"
    phclass "Hematological"
  ]
  node
  [
    id 689
    gene "F12"
    name "689"
    phclass "Hematological"
  ]
  node
  [
    id 690
    gene "F13A1"
    name "690"
    phclass "Hematological"
  ]
  node
  [
    id 691
    gene "F13B"
    name "691"
    phclass "Hematological"
  ]
  node
  [
    id 692
    gene "ADAMTS10"
    name "692"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 693
    gene "OAS1"
    name "693"
    phclass "Immunological"
  ]
  node
  [
    id 694
    gene "RAB3GAP1"
    name "694"
    phclass "multiple"
  ]
  node
  [
    id 695
    gene "FGFR1"
    name "695"
    phclass "Grey"
  ]
  node
  [
    id 696
    gene "RB1"
    name "696"
    phclass "Cancer"
  ]
  node
  [
    id 697
    gene "HRAS"
    name "697"
    phclass "Grey"
  ]
  node
  [
    id 698
    gene "MSX2"
    name "698"
    phclass "Skeletal"
  ]
  node
  [
    id 699
    gene "TWIST1"
    name "699"
    phclass "Developmental"
  ]
  node
  [
    id 700
    gene "FGFR4"
    name "700"
    phclass "Cancer"
  ]
  node
  [
    id 701
    gene "PITX2"
    name "701"
    phclass "Grey"
  ]
  node
  [
    id 702
    gene "PAX3"
    name "702"
    phclass "Grey"
  ]
  node
  [
    id 703
    gene "PAX7"
    name "703"
    phclass "Cancer"
  ]
  node
  [
    id 704
    gene "GABRB3"
    name "704"
    phclass "Psychiatric"
  ]
  node
  [
    id 705
    gene "PRNP"
    name "705"
    phclass "Grey"
  ]
  node
  [
    id 706
    gene "EFHC1"
    name "706"
    phclass "Neurological"
  ]
  node
  [
    id 707
    gene "INS"
    name "707"
    phclass "Endocrine"
  ]
  node
  [
    id 708
    gene "GFAP"
    name "708"
    phclass "multiple"
  ]
  node
  [
    id 709
    gene "GHRHR"
    name "709"
    phclass "Endocrine"
  ]
  node
  [
    id 710
    gene "LOR"
    name "710"
    phclass "Grey"
  ]
  node
  [
    id 711
    gene "GLO1"
    name "711"
    phclass "Psychiatric"
  ]
  node
  [
    id 712
    gene "NLGN3"
    name "712"
    phclass "Psychiatric"
  ]
  node
  [
    id 713
    gene "GNAS"
    name "713"
    phclass "Grey"
  ]
  node
  [
    id 714
    gene "THRA"
    name "714"
    phclass "Cancer"
  ]
  node
  [
    id 715
    gene "SSTR5"
    name "715"
    phclass "Endocrine"
  ]
  node
  [
    id 716
    gene "JPH3"
    name "716"
    phclass "Neurological"
  ]
  node
  [
    id 717
    gene "HMCN1"
    name "717"
    phclass "Ophthamological"
  ]
  node
  [
    id 718
    gene "MINPP1"
    name "718"
    phclass "Cancer"
  ]
  node
  [
    id 719
    gene "GOLGA5"
    name "719"
    phclass "Cancer"
  ]
  node
  [
    id 720
    gene "NCOA4"
    name "720"
    phclass "Cancer"
  ]
  node
  [
    id 721
    gene "PCM1"
    name "721"
    phclass "Cancer"
  ]
  node
  [
    id 722
    gene "PRKAR1A"
    name "722"
    phclass "Grey"
  ]
  node
  [
    id 723
    gene "TRIM33"
    name "723"
    phclass "Cancer"
  ]
  node
  [
    id 724
    gene "TRIM24"
    name "724"
    phclass "Cancer"
  ]
  node
  [
    id 725
    gene "HSPD1"
    name "725"
    phclass "Neurological"
  ]
  node
  [
    id 726
    gene "SACS"
    name "726"
    phclass "Neurological"
  ]
  node
  [
    id 727
    gene "KIF5A"
    name "727"
    phclass "Neurological"
  ]
  node
  [
    id 728
    gene "PLP1"
    name "728"
    phclass "Neurological"
  ]
  node
  [
    id 729
    gene "SPAST"
    name "729"
    phclass "Neurological"
  ]
  node
  [
    id 730
    gene "SPG7"
    name "730"
    phclass "Neurological"
  ]
  node
  [
    id 731
    gene "IL4R"
    name "731"
    phclass "Immunological"
  ]
  node
  [
    id 732
    gene "SPINK5"
    name "732"
    phclass "Grey"
  ]
  node
  [
    id 733
    gene "HAVCR1"
    name "733"
    phclass "Immunological"
  ]
  node
  [
    id 734
    gene "SELP"
    name "734"
    phclass "Immunological"
  ]
  node
  [
    id 735
    gene "JAK2"
    name "735"
    phclass "Hematological"
  ]
  node
  [
    id 736
    gene "THPO"
    name "736"
    phclass "Hematological"
  ]
  node
  [
    id 737
    gene "PDGFRA"
    name "737"
    phclass "Grey"
  ]
  node
  [
    id 738
    gene "KRT9"
    name "738"
    phclass "Dermatological"
  ]
  node
  [
    id 739
    gene "AGPAT2"
    name "739"
    phclass "Metabolic"
  ]
  node
  [
    id 740
    gene "PPARGC1A"
    name "740"
    phclass "Metabolic"
  ]
  node
  [
    id 741
    gene "HMGA2"
    name "741"
    phclass "Cancer"
  ]
  node
  [
    id 742
    gene "FZD4"
    name "742"
    phclass "Ophthamological"
  ]
  node
  [
    id 743
    gene "NDP"
    name "743"
    phclass "Grey"
  ]
  node
  [
    id 744
    gene "MAPT"
    name "744"
    phclass "Neurological"
  ]
  node
  [
    id 745
    gene "ITM2B"
    name "745"
    phclass "Neurological"
  ]
  node
  [
    id 746
    gene "SNCB"
    name "746"
    phclass "Neurological"
  ]
  node
  [
    id 747
    gene "UBE3A"
    name "747"
    phclass "Developmental"
  ]
  node
  [
    id 748
    gene "CDKL5"
    name "748"
    phclass "Neurological"
  ]
  node
  [
    id 749
    gene "RNF139"
    name "749"
    phclass "Cancer"
  ]
  node
  [
    id 750
    gene "OGG1"
    name "750"
    phclass "Cancer"
  ]
  node
  [
    id 751
    gene "PRCC"
    name "751"
    phclass "Cancer"
  ]
  node
  [
    id 752
    gene "TFE3"
    name "752"
    phclass "Cancer"
  ]
  node
  [
    id 753
    gene "TAPBP"
    name "753"
    phclass "Immunological"
  ]
  node
  [
    id 754
    gene "TAP2"
    name "754"
    phclass "Immunological"
  ]
  node
  [
    id 755
    gene "RFX5"
    name "755"
    phclass "Immunological"
  ]
  node
  [
    id 756
    gene "RFXAP"
    name "756"
    phclass "Immunological"
  ]
  node
  [
    id 757
    gene "MITF"
    name "757"
    phclass "multiple"
  ]
  node
  [
    id 758
    gene "TYR"
    name "758"
    phclass "Grey"
  ]
  node
  [
    id 759
    gene "SNAI2"
    name "759"
    phclass "multiple"
  ]
  node
  [
    id 760
    gene "SCNN1A"
    name "760"
    phclass "Endocrine"
  ]
  node
  [
    id 761
    gene "SCNN1B"
    name "761"
    phclass "Grey"
  ]
  node
  [
    id 762
    gene "SCNN1G"
    name "762"
    phclass "Grey"
  ]
  node
  [
    id 763
    gene "WNK4"
    name "763"
    phclass "Endocrine"
  ]
  node
  [
    id 764
    gene "WNK1"
    name "764"
    phclass "Endocrine"
  ]
  node
  [
    id 765
    gene "MN1"
    name "765"
    phclass "Cancer"
  ]
  node
  [
    id 766
    gene "NF2"
    name "766"
    phclass "Cancer"
  ]
  node
  [
    id 767
    gene "PDGFB"
    name "767"
    phclass "Cancer"
  ]
  node
  [
    id 768
    gene "MSX1"
    name "768"
    phclass "Grey"
  ]
  node
  [
    id 769
    gene "TBX22"
    name "769"
    phclass "Developmental"
  ]
  node
  [
    id 770
    gene "PAX9"
    name "770"
    phclass "Skeletal"
  ]
  node
  [
    id 771
    gene "ALX4"
    name "771"
    phclass "Skeletal"
  ]
  node
  [
    id 772
    gene "MYH8"
    name "772"
    phclass "multiple"
  ]
  node
  [
    id 773
    gene "NME1"
    name "773"
    phclass "Cancer"
  ]
  node
  [
    id 774
    gene "PROM1"
    name "774"
    phclass "Ophthamological"
  ]
  node
  [
    id 775
    gene "C1QTNF5"
    name "775"
    phclass "Ophthamological"
  ]
  node
  [
    id 776
    gene "LRAT"
    name "776"
    phclass "Ophthamological"
  ]
  node
  [
    id 777
    gene "NTRK1"
    name "777"
    phclass "Grey"
  ]
  node
  [
    id 778
    gene "OCA2"
    name "778"
    phclass "Dermatological"
  ]
  node
  [
    id 779
    gene "TYRP1"
    name "779"
    phclass "Dermatological"
  ]
  node
  [
    id 780
    gene "OPA3"
    name "780"
    phclass "Grey"
  ]
  node
  [
    id 781
    gene "SHH"
    name "781"
    phclass "Grey"
  ]
  node
  [
    id 782
    gene "PC"
    name "782"
    phclass "Metabolic"
  ]
  node
  [
    id 783
    gene "PDHB"
    name "783"
    phclass "Metabolic"
  ]
  node
  [
    id 784
    gene "PPP1R3A"
    name "784"
    phclass "Metabolic"
  ]
  node
  [
    id 785
    gene "PTPN1"
    name "785"
    phclass "Metabolic"
  ]
  node
  [
    id 786
    gene "ALDH4A1"
    name "786"
    phclass "Metabolic"
  ]
  node
  [
    id 787
    gene "RASA1"
    name "787"
    phclass "Grey"
  ]
  node
  [
    id 788
    gene "PTCH2"
    name "788"
    phclass "Cancer"
  ]
  node
  [
    id 789
    gene "SMO"
    name "789"
    phclass "Cancer"
  ]
  node
  [
    id 790
    gene "SIX3"
    name "790"
    phclass "Developmental"
  ]
  node
  [
    id 791
    gene "ZIC2"
    name "791"
    phclass "Developmental"
  ]
  node
  [
    id 792
    gene "RHD"
    name "792"
    phclass "Hematological"
  ]
  node
  [
    id 793
    gene "RNF6"
    name "793"
    phclass "Cancer"
  ]
  node
  [
    id 794
    gene "LZTS1"
    name "794"
    phclass "Cancer"
  ]
  node
  [
    id 795
    gene "WWOX"
    name "795"
    phclass "Cancer"
  ]
  node
  [
    id 796
    gene "SDHB"
    name "796"
    phclass "Cancer"
  ]
  node
  [
    id 797
    gene "SDHD"
    name "797"
    phclass "Cancer"
  ]
  node
  [
    id 798
    gene "SDHC"
    name "798"
    phclass "Cancer"
  ]
  node
  [
    id 799
    gene "TGFBR1"
    name "799"
    phclass "Connective_tissue_disorder"
  ]
  node
  [
    id 800
    gene "THRB"
    name "800"
    phclass "Endocrine"
  ]
  node
  [
    id 801
    gene "SUFU"
    name "801"
    phclass "Cancer"
  ]
  edge
  [
    source 798
    target 797
    measure 6
    diseases "Paragangliomas"
    GOmeasure 6
    id 1
    name "1"
  ]
  edge
  [
    source 798
    target 796
    measure 6
    diseases "Paragangliomas"
    GOmeasure 6
    id 2
    name "2"
  ]
  edge
  [
    source 797
    target 796
    measure 6
    diseases "Paragangliomas,Pheochromocytoma"
    GOmeasure 6
    id 3
    name "3"
  ]
  edge
  [
    source 795
    target 794
    measure 6
    diseases "Esophageal_cancer"
    GOmeasure 6
    id 4
    name "4"
  ]
  edge
  [
    source 795
    target 793
    measure 6
    diseases "Esophageal_cancer"
    GOmeasure 6
    id 5
    name "5"
  ]
  edge
  [
    source 794
    target 793
    measure 5
    diseases "Esophageal_cancer"
    GOmeasure 5
    id 6
    name "6"
  ]
  edge
  [
    source 791
    target 790
    measure 5
    diseases "Holoprosencephaly"
    GOmeasure 5
    id 7
    name "7"
  ]
  edge
  [
    source 801
    target 788
    measure 5
    diseases "Medulloblastoma"
    GOmeasure 5
    id 8
    name "8"
  ]
  edge
  [
    source 789
    target 788
    measure 5
    diseases "Basal_cell_carcinoma"
    GOmeasure 5
    id 9
    name "9"
  ]
  edge
  [
    source 789
    target 787
    measure 5
    diseases "Basal_cell_carcinoma"
    GOmeasure 5
    id 10
    name "10"
  ]
  edge
  [
    source 788
    target 787
    measure 5
    diseases "Basal_cell_carcinoma"
    GOmeasure 5
    id 11
    name "11"
  ]
  edge
  [
    source 785
    target 784
    measure 4
    diseases "Insulin_resistance"
    GOmeasure 4
    id 12
    name "12"
  ]
  edge
  [
    source 783
    target 782
    measure 4
    diseases "Pyruvate_dehydrogenase_deficiency"
    GOmeasure 4
    id 13
    name "13"
  ]
  edge
  [
    source 791
    target 781
    measure 5
    diseases "Holoprosencephaly"
    GOmeasure 5
    id 14
    name "14"
  ]
  edge
  [
    source 790
    target 781
    measure 6
    diseases "Holoprosencephaly"
    GOmeasure 6
    id 15
    name "15"
  ]
  edge
  [
    source 779
    target 778
    measure 5
    diseases "Albinism"
    GOmeasure 5
    id 16
    name "16"
  ]
  edge
  [
    source 776
    target 775
    measure 5
    diseases "Retinal_cone_dsytrophy"
    GOmeasure 5
    id 17
    name "17"
  ]
  edge
  [
    source 776
    target 774
    measure 6
    diseases "Retinal_cone_dsytrophy"
    GOmeasure 6
    id 18
    name "18"
  ]
  edge
  [
    source 775
    target 774
    measure 5
    diseases "Retinal_cone_dsytrophy"
    GOmeasure 5
    id 19
    name "19"
  ]
  edge
  [
    source 770
    target 768
    measure 5
    diseases "Hypodontia"
    GOmeasure 5
    id 20
    name "20"
  ]
  edge
  [
    source 769
    target 768
    measure 5
    diseases "Cleft_palate"
    GOmeasure 5
    id 21
    name "21"
  ]
  edge
  [
    source 767
    target 766
    measure 5
    diseases "Meningioma"
    GOmeasure 5
    id 22
    name "22"
  ]
  edge
  [
    source 767
    target 765
    measure 6
    diseases "Meningioma"
    GOmeasure 6
    id 23
    name "23"
  ]
  edge
  [
    source 766
    target 765
    measure 5
    diseases "Meningioma"
    GOmeasure 5
    id 24
    name "24"
  ]
  edge
  [
    source 764
    target 763
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 25
    name "25"
  ]
  edge
  [
    source 764
    target 762
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 26
    name "26"
  ]
  edge
  [
    source 763
    target 762
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 27
    name "27"
  ]
  edge
  [
    source 764
    target 761
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 28
    name "28"
  ]
  edge
  [
    source 763
    target 761
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 29
    name "29"
  ]
  edge
  [
    source 762
    target 761
    measure 8
    diseases "Liddle_syndrome,Pseudohypoaldosteronism"
    GOmeasure 8
    id 30
    name "30"
  ]
  edge
  [
    source 764
    target 760
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 31
    name "31"
  ]
  edge
  [
    source 763
    target 760
    measure 6
    diseases "Pseudohypoaldosteronism"
    GOmeasure 6
    id 32
    name "32"
  ]
  edge
  [
    source 762
    target 760
    measure 8
    diseases "Pseudohypoaldosteronism"
    GOmeasure 8
    id 33
    name "33"
  ]
  edge
  [
    source 761
    target 760
    measure 8
    diseases "Pseudohypoaldosteronism"
    GOmeasure 8
    id 34
    name "34"
  ]
  edge
  [
    source 779
    target 758
    measure 5
    diseases "Albinism"
    GOmeasure 5
    id 35
    name "35"
  ]
  edge
  [
    source 778
    target 758
    measure 6
    diseases "Albinism"
    GOmeasure 6
    id 36
    name "36"
  ]
  edge
  [
    source 759
    target 758
    measure 5
    diseases "Waardenburg_syndrome"
    GOmeasure 5
    id 37
    name "37"
  ]
  edge
  [
    source 759
    target 757
    measure 6
    diseases "Waardenburg_syndrome"
    GOmeasure 6
    id 38
    name "38"
  ]
  edge
  [
    source 758
    target 757
    measure 4
    diseases "Waardenburg_syndrome"
    GOmeasure 4
    id 39
    name "39"
  ]
  edge
  [
    source 756
    target 755
    measure 6
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 6
    id 40
    name "40"
  ]
  edge
  [
    source 756
    target 754
    measure 4
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 4
    id 41
    name "41"
  ]
  edge
  [
    source 755
    target 754
    measure 5
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 5
    id 42
    name "42"
  ]
  edge
  [
    source 756
    target 753
    measure 6
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 6
    id 43
    name "43"
  ]
  edge
  [
    source 755
    target 753
    measure 7
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 7
    id 44
    name "44"
  ]
  edge
  [
    source 754
    target 753
    measure 5
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 5
    id 45
    name "45"
  ]
  edge
  [
    source 752
    target 751
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 46
    name "46"
  ]
  edge
  [
    source 752
    target 750
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 47
    name "47"
  ]
  edge
  [
    source 751
    target 750
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 48
    name "48"
  ]
  edge
  [
    source 752
    target 749
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 49
    name "49"
  ]
  edge
  [
    source 751
    target 749
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 50
    name "50"
  ]
  edge
  [
    source 750
    target 749
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 51
    name "51"
  ]
  edge
  [
    source 746
    target 745
    measure 6
    diseases "Dementia"
    GOmeasure 6
    id 52
    name "52"
  ]
  edge
  [
    source 746
    target 744
    measure 6
    diseases "Dementia"
    GOmeasure 6
    id 53
    name "53"
  ]
  edge
  [
    source 745
    target 744
    measure 5
    diseases "Dementia"
    GOmeasure 5
    id 54
    name "54"
  ]
  edge
  [
    source 743
    target 742
    measure 5
    diseases "Exudative_vitreoretinopathy"
    GOmeasure 5
    id 55
    name "55"
  ]
  edge
  [
    source 740
    target 739
    measure 6
    diseases "Lipodystrophy"
    GOmeasure 6
    id 56
    name "56"
  ]
  edge
  [
    source 736
    target 735
    measure 5
    diseases "Thrombocythemia"
    GOmeasure 5
    id 57
    name "57"
  ]
  edge
  [
    source 734
    target 733
    measure 6
    diseases "Atopy"
    GOmeasure 6
    id 58
    name "58"
  ]
  edge
  [
    source 734
    target 732
    measure 6
    diseases "Atopy"
    GOmeasure 6
    id 59
    name "59"
  ]
  edge
  [
    source 733
    target 732
    measure 5
    diseases "Atopy"
    GOmeasure 5
    id 60
    name "60"
  ]
  edge
  [
    source 734
    target 731
    measure 5
    diseases "Atopy"
    GOmeasure 5
    id 61
    name "61"
  ]
  edge
  [
    source 733
    target 731
    measure 7
    diseases "Atopy"
    GOmeasure 7
    id 62
    name "62"
  ]
  edge
  [
    source 732
    target 731
    measure 4
    diseases "Atopy"
    GOmeasure 4
    id 63
    name "63"
  ]
  edge
  [
    source 730
    target 729
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 64
    name "64"
  ]
  edge
  [
    source 730
    target 728
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 65
    name "65"
  ]
  edge
  [
    source 729
    target 728
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 66
    name "66"
  ]
  edge
  [
    source 730
    target 727
    measure 6
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 6
    id 67
    name "67"
  ]
  edge
  [
    source 729
    target 727
    measure 6
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 6
    id 68
    name "68"
  ]
  edge
  [
    source 728
    target 727
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 69
    name "69"
  ]
  edge
  [
    source 730
    target 726
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 70
    name "70"
  ]
  edge
  [
    source 729
    target 726
    measure 7
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 7
    id 71
    name "71"
  ]
  edge
  [
    source 728
    target 726
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 72
    name "72"
  ]
  edge
  [
    source 727
    target 726
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 73
    name "73"
  ]
  edge
  [
    source 730
    target 725
    measure 6
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 6
    id 74
    name "74"
  ]
  edge
  [
    source 729
    target 725
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 75
    name "75"
  ]
  edge
  [
    source 728
    target 725
    measure 4
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 4
    id 76
    name "76"
  ]
  edge
  [
    source 727
    target 725
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 77
    name "77"
  ]
  edge
  [
    source 726
    target 725
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 78
    name "78"
  ]
  edge
  [
    source 724
    target 723
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 79
    name "79"
  ]
  edge
  [
    source 772
    target 722
    measure 5
    diseases "Carney_complex"
    GOmeasure 5
    id 80
    name "80"
  ]
  edge
  [
    source 724
    target 722
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 81
    name "81"
  ]
  edge
  [
    source 723
    target 722
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 82
    name "82"
  ]
  edge
  [
    source 724
    target 721
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 83
    name "83"
  ]
  edge
  [
    source 723
    target 721
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 84
    name "84"
  ]
  edge
  [
    source 722
    target 721
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 85
    name "85"
  ]
  edge
  [
    source 724
    target 720
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 86
    name "86"
  ]
  edge
  [
    source 723
    target 720
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 87
    name "87"
  ]
  edge
  [
    source 722
    target 720
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 88
    name "88"
  ]
  edge
  [
    source 721
    target 720
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 89
    name "89"
  ]
  edge
  [
    source 724
    target 719
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 90
    name "90"
  ]
  edge
  [
    source 723
    target 719
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 91
    name "91"
  ]
  edge
  [
    source 722
    target 719
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 92
    name "92"
  ]
  edge
  [
    source 721
    target 719
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 93
    name "93"
  ]
  edge
  [
    source 720
    target 719
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 94
    name "94"
  ]
  edge
  [
    source 724
    target 718
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 95
    name "95"
  ]
  edge
  [
    source 723
    target 718
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 96
    name "96"
  ]
  edge
  [
    source 722
    target 718
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 97
    name "97"
  ]
  edge
  [
    source 721
    target 718
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 98
    name "98"
  ]
  edge
  [
    source 720
    target 718
    measure 7
    diseases "Thyroid_carcinoma"
    GOmeasure 7
    id 99
    name "99"
  ]
  edge
  [
    source 719
    target 718
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 100
    name "100"
  ]
  edge
  [
    source 715
    target 713
    measure 5
    diseases "Acromegaly"
    GOmeasure 5
    id 101
    name "101"
  ]
  edge
  [
    source 714
    target 713
    measure 4
    diseases "Pituitary_ACTH-secreting_adenoma"
    GOmeasure 4
    id 102
    name "102"
  ]
  edge
  [
    source 712
    target 711
    measure 5
    diseases "Autism"
    GOmeasure 5
    id 103
    name "103"
  ]
  edge
  [
    source 716
    target 705
    measure 5
    diseases "Huntington_disease"
    GOmeasure 5
    id 104
    name "104"
  ]
  edge
  [
    source 705
    target 704
    measure 7
    diseases "Insomnia"
    GOmeasure 7
    id 105
    name "105"
  ]
  edge
  [
    source 759
    target 702
    measure 6
    diseases "Waardenburg_syndrome"
    GOmeasure 6
    id 106
    name "106"
  ]
  edge
  [
    source 758
    target 702
    measure 4
    diseases "Waardenburg_syndrome"
    GOmeasure 4
    id 107
    name "107"
  ]
  edge
  [
    source 757
    target 702
    measure 7
    diseases "Waardenburg_syndrome"
    GOmeasure 7
    id 108
    name "108"
  ]
  edge
  [
    source 703
    target 702
    measure 5
    diseases "Rhabdomyosarcoma"
    GOmeasure 5
    id 109
    name "109"
  ]
  edge
  [
    source 771
    target 698
    measure 4
    diseases "Parietal_foramina"
    GOmeasure 4
    id 110
    name "110"
  ]
  edge
  [
    source 724
    target 697
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 111
    name "111"
  ]
  edge
  [
    source 723
    target 697
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 112
    name "112"
  ]
  edge
  [
    source 722
    target 697
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 113
    name "113"
  ]
  edge
  [
    source 721
    target 697
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 114
    name "114"
  ]
  edge
  [
    source 720
    target 697
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 115
    name "115"
  ]
  edge
  [
    source 719
    target 697
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 116
    name "116"
  ]
  edge
  [
    source 718
    target 697
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 117
    name "117"
  ]
  edge
  [
    source 697
    target 696
    measure 6
    diseases "Bladder_cancer"
    GOmeasure 6
    id 118
    name "118"
  ]
  edge
  [
    source 691
    target 690
    measure 4
    diseases "Factor_x_deficiency"
    GOmeasure 4
    id 119
    name "119"
  ]
  edge
  [
    source 691
    target 689
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 120
    name "120"
  ]
  edge
  [
    source 690
    target 689
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 121
    name "121"
  ]
  edge
  [
    source 691
    target 688
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 122
    name "122"
  ]
  edge
  [
    source 690
    target 688
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 123
    name "123"
  ]
  edge
  [
    source 689
    target 688
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 124
    name "124"
  ]
  edge
  [
    source 691
    target 687
    measure 4
    diseases "Factor_x_deficiency"
    GOmeasure 4
    id 125
    name "125"
  ]
  edge
  [
    source 690
    target 687
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 126
    name "126"
  ]
  edge
  [
    source 689
    target 687
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 127
    name "127"
  ]
  edge
  [
    source 688
    target 687
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 128
    name "128"
  ]
  edge
  [
    source 691
    target 686
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 129
    name "129"
  ]
  edge
  [
    source 690
    target 686
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 130
    name "130"
  ]
  edge
  [
    source 689
    target 686
    measure 7
    diseases "Factor_x_deficiency"
    GOmeasure 7
    id 131
    name "131"
  ]
  edge
  [
    source 688
    target 686
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 132
    name "132"
  ]
  edge
  [
    source 687
    target 686
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 133
    name "133"
  ]
  edge
  [
    source 717
    target 685
    measure 5
    diseases "Macular_degeneration"
    GOmeasure 5
    id 134
    name "134"
  ]
  edge
  [
    source 691
    target 685
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 135
    name "135"
  ]
  edge
  [
    source 690
    target 685
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 136
    name "136"
  ]
  edge
  [
    source 689
    target 685
    measure 4
    diseases "Factor_x_deficiency"
    GOmeasure 4
    id 137
    name "137"
  ]
  edge
  [
    source 688
    target 685
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 138
    name "138"
  ]
  edge
  [
    source 687
    target 685
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 139
    name "139"
  ]
  edge
  [
    source 686
    target 685
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 140
    name "140"
  ]
  edge
  [
    source 684
    target 683
    measure 7
    diseases "Thrombocytopenia"
    GOmeasure 7
    id 141
    name "141"
  ]
  edge
  [
    source 701
    target 681
    measure 7
    diseases "Iridogoniodysgenesis,Rieger_syndrome"
    GOmeasure 7
    id 142
    name "142"
  ]
  edge
  [
    source 681
    target 680
    measure 6
    diseases "Anterior_segment_anomalies_and_cataract"
    GOmeasure 6
    id 143
    name "143"
  ]
  edge
  [
    source 684
    target 677
    measure 5
    diseases "Thrombocytopenia"
    GOmeasure 5
    id 144
    name "144"
  ]
  edge
  [
    source 683
    target 677
    measure 4
    diseases "Thrombocytopenia"
    GOmeasure 4
    id 145
    name "145"
  ]
  edge
  [
    source 677
    target 676
    measure 5
    diseases "Neutropenia"
    GOmeasure 5
    id 146
    name "146"
  ]
  edge
  [
    source 693
    target 675
    measure 5
    diseases "Viral_infection"
    GOmeasure 5
    id 147
    name "147"
  ]
  edge
  [
    source 677
    target 675
    measure 5
    diseases "Neutropenia"
    GOmeasure 5
    id 148
    name "148"
  ]
  edge
  [
    source 676
    target 675
    measure 5
    diseases "Neutropenia"
    GOmeasure 5
    id 149
    name "149"
  ]
  edge
  [
    source 674
    target 673
    measure 7
    diseases "Neuropathy"
    GOmeasure 7
    id 150
    name "150"
  ]
  edge
  [
    source 670
    target 669
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 151
    name "151"
  ]
  edge
  [
    source 670
    target 668
    measure 6
    diseases "Ectodermal_dysplasia"
    GOmeasure 6
    id 152
    name "152"
  ]
  edge
  [
    source 669
    target 668
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 153
    name "153"
  ]
  edge
  [
    source 670
    target 667
    measure 7
    diseases "Ectodermal_dysplasia"
    GOmeasure 7
    id 154
    name "154"
  ]
  edge
  [
    source 669
    target 667
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 155
    name "155"
  ]
  edge
  [
    source 668
    target 667
    measure 6
    diseases "Ectodermal_dysplasia"
    GOmeasure 6
    id 156
    name "156"
  ]
  edge
  [
    source 670
    target 666
    measure 4
    diseases "Ectodermal_dysplasia"
    GOmeasure 4
    id 157
    name "157"
  ]
  edge
  [
    source 669
    target 666
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 158
    name "158"
  ]
  edge
  [
    source 668
    target 666
    measure 4
    diseases "Ectodermal_dysplasia"
    GOmeasure 4
    id 159
    name "159"
  ]
  edge
  [
    source 667
    target 666
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 160
    name "160"
  ]
  edge
  [
    source 670
    target 665
    measure 6
    diseases "Ectodermal_dysplasia"
    GOmeasure 6
    id 161
    name "161"
  ]
  edge
  [
    source 669
    target 665
    measure 4
    diseases "Ectodermal_dysplasia"
    GOmeasure 4
    id 162
    name "162"
  ]
  edge
  [
    source 668
    target 665
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 163
    name "163"
  ]
  edge
  [
    source 667
    target 665
    measure 7
    diseases "Ectodermal_dysplasia"
    GOmeasure 7
    id 164
    name "164"
  ]
  edge
  [
    source 666
    target 665
    measure 6
    diseases "Ectodermal_dysplasia"
    GOmeasure 6
    id 165
    name "165"
  ]
  edge
  [
    source 671
    target 664
    measure 5
    diseases "Waardenburg-Shah_syndrome"
    GOmeasure 5
    id 166
    name "166"
  ]
  edge
  [
    source 664
    target 663
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 167
    name "167"
  ]
  edge
  [
    source 659
    target 658
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 168
    name "168"
  ]
  edge
  [
    source 659
    target 657
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 169
    name "169"
  ]
  edge
  [
    source 658
    target 657
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 170
    name "170"
  ]
  edge
  [
    source 659
    target 656
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 171
    name "171"
  ]
  edge
  [
    source 658
    target 656
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 172
    name "172"
  ]
  edge
  [
    source 657
    target 656
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 173
    name "173"
  ]
  edge
  [
    source 659
    target 655
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 174
    name "174"
  ]
  edge
  [
    source 658
    target 655
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 175
    name "175"
  ]
  edge
  [
    source 657
    target 655
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 176
    name "176"
  ]
  edge
  [
    source 656
    target 655
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 177
    name "177"
  ]
  edge
  [
    source 659
    target 654
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 178
    name "178"
  ]
  edge
  [
    source 658
    target 654
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 179
    name "179"
  ]
  edge
  [
    source 657
    target 654
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 180
    name "180"
  ]
  edge
  [
    source 656
    target 654
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 181
    name "181"
  ]
  edge
  [
    source 655
    target 654
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 182
    name "182"
  ]
  edge
  [
    source 659
    target 653
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 183
    name "183"
  ]
  edge
  [
    source 658
    target 653
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 184
    name "184"
  ]
  edge
  [
    source 657
    target 653
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 185
    name "185"
  ]
  edge
  [
    source 656
    target 653
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 186
    name "186"
  ]
  edge
  [
    source 655
    target 653
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 187
    name "187"
  ]
  edge
  [
    source 654
    target 653
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 188
    name "188"
  ]
  edge
  [
    source 674
    target 652
    measure 5
    diseases "Neuropathy"
    GOmeasure 5
    id 189
    name "189"
  ]
  edge
  [
    source 673
    target 652
    measure 5
    diseases "Neuropathy"
    GOmeasure 5
    id 190
    name "190"
  ]
  edge
  [
    source 672
    target 652
    measure 5
    diseases "Dejerine-Sottas_disease"
    GOmeasure 5
    id 191
    name "191"
  ]
  edge
  [
    source 659
    target 652
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 192
    name "192"
  ]
  edge
  [
    source 658
    target 652
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 193
    name "193"
  ]
  edge
  [
    source 657
    target 652
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 194
    name "194"
  ]
  edge
  [
    source 656
    target 652
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 195
    name "195"
  ]
  edge
  [
    source 655
    target 652
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 196
    name "196"
  ]
  edge
  [
    source 654
    target 652
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 197
    name "197"
  ]
  edge
  [
    source 653
    target 652
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 198
    name "198"
  ]
  edge
  [
    source 659
    target 651
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 199
    name "199"
  ]
  edge
  [
    source 658
    target 651
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 200
    name "200"
  ]
  edge
  [
    source 657
    target 651
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 201
    name "201"
  ]
  edge
  [
    source 656
    target 651
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 202
    name "202"
  ]
  edge
  [
    source 655
    target 651
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 203
    name "203"
  ]
  edge
  [
    source 654
    target 651
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 204
    name "204"
  ]
  edge
  [
    source 653
    target 651
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 205
    name "205"
  ]
  edge
  [
    source 652
    target 651
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 206
    name "206"
  ]
  edge
  [
    source 674
    target 650
    measure 4
    diseases "Neuropathy"
    GOmeasure 4
    id 207
    name "207"
  ]
  edge
  [
    source 673
    target 650
    measure 5
    diseases "Neuropathy"
    GOmeasure 5
    id 208
    name "208"
  ]
  edge
  [
    source 672
    target 650
    measure 6
    diseases "Dejerine-Sottas_disease"
    GOmeasure 6
    id 209
    name "209"
  ]
  edge
  [
    source 659
    target 650
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 210
    name "210"
  ]
  edge
  [
    source 658
    target 650
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 211
    name "211"
  ]
  edge
  [
    source 657
    target 650
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 212
    name "212"
  ]
  edge
  [
    source 656
    target 650
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 213
    name "213"
  ]
  edge
  [
    source 655
    target 650
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 214
    name "214"
  ]
  edge
  [
    source 654
    target 650
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 215
    name "215"
  ]
  edge
  [
    source 653
    target 650
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 216
    name "216"
  ]
  edge
  [
    source 652
    target 650
    measure 5
    diseases "Charcot-Marie-Tooth_disease,Dejerine-Sottas_disease,Neuropathy"
    GOmeasure 5
    id 217
    name "217"
  ]
  edge
  [
    source 651
    target 650
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 218
    name "218"
  ]
  edge
  [
    source 659
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 219
    name "219"
  ]
  edge
  [
    source 658
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 220
    name "220"
  ]
  edge
  [
    source 657
    target 649
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 221
    name "221"
  ]
  edge
  [
    source 656
    target 649
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 222
    name "222"
  ]
  edge
  [
    source 655
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 223
    name "223"
  ]
  edge
  [
    source 654
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 224
    name "224"
  ]
  edge
  [
    source 653
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 225
    name "225"
  ]
  edge
  [
    source 652
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 226
    name "226"
  ]
  edge
  [
    source 651
    target 649
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 227
    name "227"
  ]
  edge
  [
    source 650
    target 649
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 228
    name "228"
  ]
  edge
  [
    source 659
    target 648
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 229
    name "229"
  ]
  edge
  [
    source 658
    target 648
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 230
    name "230"
  ]
  edge
  [
    source 657
    target 648
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 231
    name "231"
  ]
  edge
  [
    source 656
    target 648
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 232
    name "232"
  ]
  edge
  [
    source 655
    target 648
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 233
    name "233"
  ]
  edge
  [
    source 654
    target 648
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 234
    name "234"
  ]
  edge
  [
    source 653
    target 648
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 235
    name "235"
  ]
  edge
  [
    source 652
    target 648
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 236
    name "236"
  ]
  edge
  [
    source 651
    target 648
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 237
    name "237"
  ]
  edge
  [
    source 650
    target 648
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 238
    name "238"
  ]
  edge
  [
    source 649
    target 648
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 239
    name "239"
  ]
  edge
  [
    source 674
    target 647
    measure 6
    diseases "Neuropathy"
    GOmeasure 6
    id 240
    name "240"
  ]
  edge
  [
    source 673
    target 647
    measure 7
    diseases "Neuropathy"
    GOmeasure 7
    id 241
    name "241"
  ]
  edge
  [
    source 672
    target 647
    measure 5
    diseases "Dejerine-Sottas_disease"
    GOmeasure 5
    id 242
    name "242"
  ]
  edge
  [
    source 659
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 243
    name "243"
  ]
  edge
  [
    source 658
    target 647
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 244
    name "244"
  ]
  edge
  [
    source 657
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 245
    name "245"
  ]
  edge
  [
    source 656
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 246
    name "246"
  ]
  edge
  [
    source 655
    target 647
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 247
    name "247"
  ]
  edge
  [
    source 654
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 248
    name "248"
  ]
  edge
  [
    source 653
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 249
    name "249"
  ]
  edge
  [
    source 652
    target 647
    measure 4
    diseases "Charcot-Marie-Tooth_disease,Dejerine-Sottas_disease,Neuropathy"
    GOmeasure 4
    id 250
    name "250"
  ]
  edge
  [
    source 651
    target 647
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 251
    name "251"
  ]
  edge
  [
    source 650
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease,Dejerine-Sottas_disease,Neuropathy,Roussy-Levy_syndrome"
    GOmeasure 6
    id 252
    name "252"
  ]
  edge
  [
    source 649
    target 647
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 253
    name "253"
  ]
  edge
  [
    source 648
    target 647
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 254
    name "254"
  ]
  edge
  [
    source 674
    target 646
    measure 5
    diseases "Neuropathy"
    GOmeasure 5
    id 255
    name "255"
  ]
  edge
  [
    source 673
    target 646
    measure 5
    diseases "Neuropathy"
    GOmeasure 5
    id 256
    name "256"
  ]
  edge
  [
    source 659
    target 646
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 257
    name "257"
  ]
  edge
  [
    source 658
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 258
    name "258"
  ]
  edge
  [
    source 657
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 259
    name "259"
  ]
  edge
  [
    source 656
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 260
    name "260"
  ]
  edge
  [
    source 655
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 261
    name "261"
  ]
  edge
  [
    source 654
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 262
    name "262"
  ]
  edge
  [
    source 653
    target 646
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 263
    name "263"
  ]
  edge
  [
    source 652
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease,Neuropathy"
    GOmeasure 5
    id 264
    name "264"
  ]
  edge
  [
    source 651
    target 646
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 265
    name "265"
  ]
  edge
  [
    source 650
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease,Neuropathy"
    GOmeasure 5
    id 266
    name "266"
  ]
  edge
  [
    source 649
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 267
    name "267"
  ]
  edge
  [
    source 648
    target 646
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 268
    name "268"
  ]
  edge
  [
    source 647
    target 646
    measure 5
    diseases "Charcot-Marie-Tooth_disease,Neuropathy"
    GOmeasure 5
    id 269
    name "269"
  ]
  edge
  [
    source 659
    target 645
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 270
    name "270"
  ]
  edge
  [
    source 658
    target 645
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 271
    name "271"
  ]
  edge
  [
    source 657
    target 645
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 272
    name "272"
  ]
  edge
  [
    source 656
    target 645
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 273
    name "273"
  ]
  edge
  [
    source 655
    target 645
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 274
    name "274"
  ]
  edge
  [
    source 654
    target 645
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 275
    name "275"
  ]
  edge
  [
    source 653
    target 645
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 276
    name "276"
  ]
  edge
  [
    source 652
    target 645
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 277
    name "277"
  ]
  edge
  [
    source 651
    target 645
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 278
    name "278"
  ]
  edge
  [
    source 650
    target 645
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 279
    name "279"
  ]
  edge
  [
    source 649
    target 645
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 280
    name "280"
  ]
  edge
  [
    source 648
    target 645
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 281
    name "281"
  ]
  edge
  [
    source 647
    target 645
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 282
    name "282"
  ]
  edge
  [
    source 646
    target 645
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 283
    name "283"
  ]
  edge
  [
    source 643
    target 642
    measure 5
    diseases "Lissencephaly"
    GOmeasure 5
    id 284
    name "284"
  ]
  edge
  [
    source 643
    target 641
    measure 5
    diseases "Lissencephaly"
    GOmeasure 5
    id 285
    name "285"
  ]
  edge
  [
    source 642
    target 641
    measure 4
    diseases "Lissencephaly,Subcortical_laminar_heterotopia"
    GOmeasure 4
    id 286
    name "286"
  ]
  edge
  [
    source 640
    target 639
    measure 5
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 5
    id 287
    name "287"
  ]
  edge
  [
    source 730
    target 638
    measure 4
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 4
    id 288
    name "288"
  ]
  edge
  [
    source 729
    target 638
    measure 6
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 6
    id 289
    name "289"
  ]
  edge
  [
    source 728
    target 638
    measure 5
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 5
    id 290
    name "290"
  ]
  edge
  [
    source 727
    target 638
    measure 4
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 4
    id 291
    name "291"
  ]
  edge
  [
    source 726
    target 638
    measure 6
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 6
    id 292
    name "292"
  ]
  edge
  [
    source 725
    target 638
    measure 4
    diseases "Spastic_ataxia/paraplegia"
    GOmeasure 4
    id 293
    name "293"
  ]
  edge
  [
    source 640
    target 638
    measure 5
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 5
    id 294
    name "294"
  ]
  edge
  [
    source 639
    target 638
    measure 7
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 7
    id 295
    name "295"
  ]
  edge
  [
    source 640
    target 637
    measure 6
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 6
    id 296
    name "296"
  ]
  edge
  [
    source 639
    target 637
    measure 7
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 7
    id 297
    name "297"
  ]
  edge
  [
    source 638
    target 637
    measure 5
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 5
    id 298
    name "298"
  ]
  edge
  [
    source 640
    target 636
    measure 6
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 6
    id 299
    name "299"
  ]
  edge
  [
    source 639
    target 636
    measure 6
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 6
    id 300
    name "300"
  ]
  edge
  [
    source 638
    target 636
    measure 6
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 6
    id 301
    name "301"
  ]
  edge
  [
    source 637
    target 636
    measure 5
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 5
    id 302
    name "302"
  ]
  edge
  [
    source 635
    target 634
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 303
    name "303"
  ]
  edge
  [
    source 635
    target 633
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 304
    name "304"
  ]
  edge
  [
    source 634
    target 633
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 305
    name "305"
  ]
  edge
  [
    source 635
    target 632
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 306
    name "306"
  ]
  edge
  [
    source 634
    target 632
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 307
    name "307"
  ]
  edge
  [
    source 633
    target 632
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 308
    name "308"
  ]
  edge
  [
    source 635
    target 631
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 309
    name "309"
  ]
  edge
  [
    source 634
    target 631
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 310
    name "310"
  ]
  edge
  [
    source 633
    target 631
    measure 7
    diseases "Parkinson_disease"
    GOmeasure 7
    id 311
    name "311"
  ]
  edge
  [
    source 632
    target 631
    measure 7
    diseases "Parkinson_disease"
    GOmeasure 7
    id 312
    name "312"
  ]
  edge
  [
    source 746
    target 630
    measure 9
    diseases "Dementia"
    GOmeasure 9
    id 313
    name "313"
  ]
  edge
  [
    source 745
    target 630
    measure 7
    diseases "Dementia"
    GOmeasure 7
    id 314
    name "314"
  ]
  edge
  [
    source 744
    target 630
    measure 6
    diseases "Dementia"
    GOmeasure 6
    id 315
    name "315"
  ]
  edge
  [
    source 635
    target 630
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 316
    name "316"
  ]
  edge
  [
    source 634
    target 630
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 317
    name "317"
  ]
  edge
  [
    source 633
    target 630
    measure 7
    diseases "Parkinson_disease"
    GOmeasure 7
    id 318
    name "318"
  ]
  edge
  [
    source 632
    target 630
    measure 8
    diseases "Parkinson_disease"
    GOmeasure 8
    id 319
    name "319"
  ]
  edge
  [
    source 631
    target 630
    measure 8
    diseases "Parkinson_disease"
    GOmeasure 8
    id 320
    name "320"
  ]
  edge
  [
    source 635
    target 629
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 321
    name "321"
  ]
  edge
  [
    source 634
    target 629
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 322
    name "322"
  ]
  edge
  [
    source 633
    target 629
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 323
    name "323"
  ]
  edge
  [
    source 632
    target 629
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 324
    name "324"
  ]
  edge
  [
    source 631
    target 629
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 325
    name "325"
  ]
  edge
  [
    source 630
    target 629
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 326
    name "326"
  ]
  edge
  [
    source 635
    target 628
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 327
    name "327"
  ]
  edge
  [
    source 634
    target 628
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 328
    name "328"
  ]
  edge
  [
    source 633
    target 628
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 329
    name "329"
  ]
  edge
  [
    source 632
    target 628
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 330
    name "330"
  ]
  edge
  [
    source 631
    target 628
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 331
    name "331"
  ]
  edge
  [
    source 630
    target 628
    measure 7
    diseases "Parkinson_disease"
    GOmeasure 7
    id 332
    name "332"
  ]
  edge
  [
    source 629
    target 628
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 333
    name "333"
  ]
  edge
  [
    source 635
    target 627
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 334
    name "334"
  ]
  edge
  [
    source 634
    target 627
    measure 7
    diseases "Parkinson_disease"
    GOmeasure 7
    id 335
    name "335"
  ]
  edge
  [
    source 633
    target 627
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 336
    name "336"
  ]
  edge
  [
    source 632
    target 627
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 337
    name "337"
  ]
  edge
  [
    source 631
    target 627
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 338
    name "338"
  ]
  edge
  [
    source 630
    target 627
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 339
    name "339"
  ]
  edge
  [
    source 629
    target 627
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 340
    name "340"
  ]
  edge
  [
    source 628
    target 627
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 341
    name "341"
  ]
  edge
  [
    source 780
    target 626
    measure 6
    diseases "Optic_atrophy"
    GOmeasure 6
    id 342
    name "342"
  ]
  edge
  [
    source 626
    target 625
    measure 5
    diseases "Glaucoma"
    GOmeasure 5
    id 343
    name "343"
  ]
  edge
  [
    source 626
    target 624
    measure 6
    diseases "Glaucoma"
    GOmeasure 6
    id 344
    name "344"
  ]
  edge
  [
    source 625
    target 624
    measure 5
    diseases "Glaucoma"
    GOmeasure 5
    id 345
    name "345"
  ]
  edge
  [
    source 626
    target 623
    measure 5
    diseases "Glaucoma"
    GOmeasure 5
    id 346
    name "346"
  ]
  edge
  [
    source 625
    target 623
    measure 4
    diseases "Glaucoma"
    GOmeasure 4
    id 347
    name "347"
  ]
  edge
  [
    source 624
    target 623
    measure 4
    diseases "Glaucoma"
    GOmeasure 4
    id 348
    name "348"
  ]
  edge
  [
    source 724
    target 621
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 349
    name "349"
  ]
  edge
  [
    source 723
    target 621
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 350
    name "350"
  ]
  edge
  [
    source 722
    target 621
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 351
    name "351"
  ]
  edge
  [
    source 721
    target 621
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 352
    name "352"
  ]
  edge
  [
    source 720
    target 621
    measure 7
    diseases "Thyroid_carcinoma"
    GOmeasure 7
    id 353
    name "353"
  ]
  edge
  [
    source 719
    target 621
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 354
    name "354"
  ]
  edge
  [
    source 718
    target 621
    measure 7
    diseases "Thyroid_carcinoma"
    GOmeasure 7
    id 355
    name "355"
  ]
  edge
  [
    source 697
    target 621
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 356
    name "356"
  ]
  edge
  [
    source 622
    target 621
    measure 5
    diseases "Hypothyroidism"
    GOmeasure 5
    id 357
    name "357"
  ]
  edge
  [
    source 622
    target 620
    measure 6
    diseases "Hypothyroidism"
    GOmeasure 6
    id 358
    name "358"
  ]
  edge
  [
    source 621
    target 620
    measure 5
    diseases "Hypothyroidism"
    GOmeasure 5
    id 359
    name "359"
  ]
  edge
  [
    source 622
    target 619
    measure 4
    diseases "Hypothyroidism"
    GOmeasure 4
    id 360
    name "360"
  ]
  edge
  [
    source 621
    target 619
    measure 6
    diseases "Hypothyroidism"
    GOmeasure 6
    id 361
    name "361"
  ]
  edge
  [
    source 620
    target 619
    measure 4
    diseases "Hypothyroidism"
    GOmeasure 4
    id 362
    name "362"
  ]
  edge
  [
    source 622
    target 617
    measure 5
    diseases "Hypothyroidism"
    GOmeasure 5
    id 363
    name "363"
  ]
  edge
  [
    source 621
    target 617
    measure 5
    diseases "Hypothyroidism"
    GOmeasure 5
    id 364
    name "364"
  ]
  edge
  [
    source 620
    target 617
    measure 4
    diseases "Hypothyroidism"
    GOmeasure 4
    id 365
    name "365"
  ]
  edge
  [
    source 619
    target 617
    measure 5
    diseases "Hypothyroidism"
    GOmeasure 5
    id 366
    name "366"
  ]
  edge
  [
    source 618
    target 617
    measure 5
    diseases "Graves_disease"
    GOmeasure 5
    id 367
    name "367"
  ]
  edge
  [
    source 616
    target 615
    measure 7
    diseases "Atrioventricular_block"
    GOmeasure 7
    id 368
    name "368"
  ]
  edge
  [
    source 613
    target 612
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 369
    name "369"
  ]
  edge
  [
    source 613
    target 611
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 370
    name "370"
  ]
  edge
  [
    source 612
    target 611
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 371
    name "371"
  ]
  edge
  [
    source 613
    target 610
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 372
    name "372"
  ]
  edge
  [
    source 612
    target 610
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 373
    name "373"
  ]
  edge
  [
    source 611
    target 610
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 374
    name "374"
  ]
  edge
  [
    source 613
    target 609
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 375
    name "375"
  ]
  edge
  [
    source 612
    target 609
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 376
    name "376"
  ]
  edge
  [
    source 611
    target 609
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 377
    name "377"
  ]
  edge
  [
    source 610
    target 609
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 378
    name "378"
  ]
  edge
  [
    source 613
    target 608
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 379
    name "379"
  ]
  edge
  [
    source 612
    target 608
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 380
    name "380"
  ]
  edge
  [
    source 611
    target 608
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 381
    name "381"
  ]
  edge
  [
    source 610
    target 608
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 382
    name "382"
  ]
  edge
  [
    source 609
    target 608
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 383
    name "383"
  ]
  edge
  [
    source 613
    target 607
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 384
    name "384"
  ]
  edge
  [
    source 612
    target 607
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 385
    name "385"
  ]
  edge
  [
    source 611
    target 607
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 386
    name "386"
  ]
  edge
  [
    source 610
    target 607
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 387
    name "387"
  ]
  edge
  [
    source 609
    target 607
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 388
    name "388"
  ]
  edge
  [
    source 608
    target 607
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 389
    name "389"
  ]
  edge
  [
    source 613
    target 606
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 390
    name "390"
  ]
  edge
  [
    source 612
    target 606
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 391
    name "391"
  ]
  edge
  [
    source 611
    target 606
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 392
    name "392"
  ]
  edge
  [
    source 610
    target 606
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 393
    name "393"
  ]
  edge
  [
    source 609
    target 606
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 394
    name "394"
  ]
  edge
  [
    source 608
    target 606
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 395
    name "395"
  ]
  edge
  [
    source 607
    target 606
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 396
    name "396"
  ]
  edge
  [
    source 781
    target 605
    measure 5
    diseases "Coloboma,_ocular"
    GOmeasure 5
    id 397
    name "397"
  ]
  edge
  [
    source 623
    target 605
    measure 4
    diseases "Peters_anomaly"
    GOmeasure 4
    id 398
    name "398"
  ]
  edge
  [
    source 613
    target 605
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 399
    name "399"
  ]
  edge
  [
    source 612
    target 605
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 400
    name "400"
  ]
  edge
  [
    source 611
    target 605
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 401
    name "401"
  ]
  edge
  [
    source 610
    target 605
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 402
    name "402"
  ]
  edge
  [
    source 609
    target 605
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 403
    name "403"
  ]
  edge
  [
    source 608
    target 605
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 404
    name "404"
  ]
  edge
  [
    source 607
    target 605
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 405
    name "405"
  ]
  edge
  [
    source 606
    target 605
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 406
    name "406"
  ]
  edge
  [
    source 613
    target 604
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 407
    name "407"
  ]
  edge
  [
    source 612
    target 604
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 408
    name "408"
  ]
  edge
  [
    source 611
    target 604
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 409
    name "409"
  ]
  edge
  [
    source 610
    target 604
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 410
    name "410"
  ]
  edge
  [
    source 609
    target 604
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 411
    name "411"
  ]
  edge
  [
    source 608
    target 604
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 412
    name "412"
  ]
  edge
  [
    source 607
    target 604
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 413
    name "413"
  ]
  edge
  [
    source 606
    target 604
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 414
    name "414"
  ]
  edge
  [
    source 605
    target 604
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 415
    name "415"
  ]
  edge
  [
    source 681
    target 603
    measure 7
    diseases "Anterior_segment_anomalies_and_cataract"
    GOmeasure 7
    id 416
    name "416"
  ]
  edge
  [
    source 680
    target 603
    measure 5
    diseases "Anterior_segment_anomalies_and_cataract"
    GOmeasure 5
    id 417
    name "417"
  ]
  edge
  [
    source 613
    target 603
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 418
    name "418"
  ]
  edge
  [
    source 612
    target 603
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 419
    name "419"
  ]
  edge
  [
    source 611
    target 603
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 420
    name "420"
  ]
  edge
  [
    source 610
    target 603
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 421
    name "421"
  ]
  edge
  [
    source 609
    target 603
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 422
    name "422"
  ]
  edge
  [
    source 608
    target 603
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 423
    name "423"
  ]
  edge
  [
    source 607
    target 603
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 424
    name "424"
  ]
  edge
  [
    source 606
    target 603
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 425
    name "425"
  ]
  edge
  [
    source 605
    target 603
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 426
    name "426"
  ]
  edge
  [
    source 604
    target 603
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 427
    name "427"
  ]
  edge
  [
    source 613
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 428
    name "428"
  ]
  edge
  [
    source 612
    target 602
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 429
    name "429"
  ]
  edge
  [
    source 611
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 430
    name "430"
  ]
  edge
  [
    source 610
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 431
    name "431"
  ]
  edge
  [
    source 609
    target 602
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 432
    name "432"
  ]
  edge
  [
    source 608
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 433
    name "433"
  ]
  edge
  [
    source 607
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 434
    name "434"
  ]
  edge
  [
    source 606
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 435
    name "435"
  ]
  edge
  [
    source 605
    target 602
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 436
    name "436"
  ]
  edge
  [
    source 604
    target 602
    measure 8
    diseases "Cataract"
    GOmeasure 8
    id 437
    name "437"
  ]
  edge
  [
    source 603
    target 602
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 438
    name "438"
  ]
  edge
  [
    source 613
    target 601
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 439
    name "439"
  ]
  edge
  [
    source 612
    target 601
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 440
    name "440"
  ]
  edge
  [
    source 611
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 441
    name "441"
  ]
  edge
  [
    source 610
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 442
    name "442"
  ]
  edge
  [
    source 609
    target 601
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 443
    name "443"
  ]
  edge
  [
    source 608
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 444
    name "444"
  ]
  edge
  [
    source 607
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 445
    name "445"
  ]
  edge
  [
    source 606
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 446
    name "446"
  ]
  edge
  [
    source 605
    target 601
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 447
    name "447"
  ]
  edge
  [
    source 604
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 448
    name "448"
  ]
  edge
  [
    source 603
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 449
    name "449"
  ]
  edge
  [
    source 602
    target 601
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 450
    name "450"
  ]
  edge
  [
    source 597
    target 596
    measure 4
    diseases "Lung_cancer"
    GOmeasure 4
    id 451
    name "451"
  ]
  edge
  [
    source 595
    target 594
    measure 7
    diseases "Epiphyseal_dysplasia,Intervertebral_disc_disease"
    GOmeasure 7
    id 452
    name "452"
  ]
  edge
  [
    source 662
    target 593
    measure 5
    diseases "Atelosteogenesis"
    GOmeasure 5
    id 453
    name "453"
  ]
  edge
  [
    source 595
    target 593
    measure 6
    diseases "Epiphyseal_dysplasia"
    GOmeasure 6
    id 454
    name "454"
  ]
  edge
  [
    source 594
    target 593
    measure 6
    diseases "Epiphyseal_dysplasia"
    GOmeasure 6
    id 455
    name "455"
  ]
  edge
  [
    source 595
    target 592
    measure 4
    diseases "Epiphyseal_dysplasia"
    GOmeasure 4
    id 456
    name "456"
  ]
  edge
  [
    source 594
    target 592
    measure 4
    diseases "Epiphyseal_dysplasia"
    GOmeasure 4
    id 457
    name "457"
  ]
  edge
  [
    source 593
    target 592
    measure 6
    diseases "Epiphyseal_dysplasia"
    GOmeasure 6
    id 458
    name "458"
  ]
  edge
  [
    source 595
    target 591
    measure 7
    diseases "Epiphyseal_dysplasia"
    GOmeasure 7
    id 459
    name "459"
  ]
  edge
  [
    source 594
    target 591
    measure 7
    diseases "Epiphyseal_dysplasia"
    GOmeasure 7
    id 460
    name "460"
  ]
  edge
  [
    source 593
    target 591
    measure 6
    diseases "Epiphyseal_dysplasia"
    GOmeasure 6
    id 461
    name "461"
  ]
  edge
  [
    source 592
    target 591
    measure 4
    diseases "Epiphyseal_dysplasia"
    GOmeasure 4
    id 462
    name "462"
  ]
  edge
  [
    source 590
    target 589
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 463
    name "463"
  ]
  edge
  [
    source 661
    target 588
    measure 4
    diseases "Arrhythmogenic_right_ventricular_dysplasia"
    GOmeasure 4
    id 464
    name "464"
  ]
  edge
  [
    source 660
    target 588
    measure 6
    diseases "Keratosis_palmoplantaria_striata"
    GOmeasure 6
    id 465
    name "465"
  ]
  edge
  [
    source 590
    target 588
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 466
    name "466"
  ]
  edge
  [
    source 589
    target 588
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 467
    name "467"
  ]
  edge
  [
    source 590
    target 587
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 468
    name "468"
  ]
  edge
  [
    source 589
    target 587
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 469
    name "469"
  ]
  edge
  [
    source 588
    target 587
    measure 9
    diseases "Epidermolysis_bullosa"
    GOmeasure 9
    id 470
    name "470"
  ]
  edge
  [
    source 590
    target 586
    measure 8
    diseases "Epidermolysis_bullosa"
    GOmeasure 8
    id 471
    name "471"
  ]
  edge
  [
    source 589
    target 586
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 472
    name "472"
  ]
  edge
  [
    source 588
    target 586
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 473
    name "473"
  ]
  edge
  [
    source 587
    target 586
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 474
    name "474"
  ]
  edge
  [
    source 590
    target 585
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 475
    name "475"
  ]
  edge
  [
    source 589
    target 585
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 476
    name "476"
  ]
  edge
  [
    source 588
    target 585
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 477
    name "477"
  ]
  edge
  [
    source 587
    target 585
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 478
    name "478"
  ]
  edge
  [
    source 586
    target 585
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 479
    name "479"
  ]
  edge
  [
    source 590
    target 584
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 480
    name "480"
  ]
  edge
  [
    source 589
    target 584
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 481
    name "481"
  ]
  edge
  [
    source 588
    target 584
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 482
    name "482"
  ]
  edge
  [
    source 587
    target 584
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 483
    name "483"
  ]
  edge
  [
    source 586
    target 584
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 484
    name "484"
  ]
  edge
  [
    source 585
    target 584
    measure 9
    diseases "Epidermolysis_bullosa"
    GOmeasure 9
    id 485
    name "485"
  ]
  edge
  [
    source 590
    target 583
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 486
    name "486"
  ]
  edge
  [
    source 589
    target 583
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 487
    name "487"
  ]
  edge
  [
    source 588
    target 583
    measure 9
    diseases "Epidermolysis_bullosa"
    GOmeasure 9
    id 488
    name "488"
  ]
  edge
  [
    source 587
    target 583
    measure 9
    diseases "Epidermolysis_bullosa"
    GOmeasure 9
    id 489
    name "489"
  ]
  edge
  [
    source 586
    target 583
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 490
    name "490"
  ]
  edge
  [
    source 585
    target 583
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 491
    name "491"
  ]
  edge
  [
    source 584
    target 583
    measure 7
    diseases "Epidermolysis_bullosa"
    GOmeasure 7
    id 492
    name "492"
  ]
  edge
  [
    source 590
    target 582
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 493
    name "493"
  ]
  edge
  [
    source 589
    target 582
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 494
    name "494"
  ]
  edge
  [
    source 588
    target 582
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 495
    name "495"
  ]
  edge
  [
    source 587
    target 582
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 496
    name "496"
  ]
  edge
  [
    source 586
    target 582
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 497
    name "497"
  ]
  edge
  [
    source 585
    target 582
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 498
    name "498"
  ]
  edge
  [
    source 584
    target 582
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 499
    name "499"
  ]
  edge
  [
    source 583
    target 582
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 500
    name "500"
  ]
  edge
  [
    source 590
    target 581
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 501
    name "501"
  ]
  edge
  [
    source 589
    target 581
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 502
    name "502"
  ]
  edge
  [
    source 588
    target 581
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 503
    name "503"
  ]
  edge
  [
    source 587
    target 581
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 504
    name "504"
  ]
  edge
  [
    source 586
    target 581
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 505
    name "505"
  ]
  edge
  [
    source 585
    target 581
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 506
    name "506"
  ]
  edge
  [
    source 584
    target 581
    measure 6
    diseases "Epidermolysis_bullosa"
    GOmeasure 6
    id 507
    name "507"
  ]
  edge
  [
    source 583
    target 581
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 508
    name "508"
  ]
  edge
  [
    source 582
    target 581
    measure 5
    diseases "Epidermolysis_bullosa"
    GOmeasure 5
    id 509
    name "509"
  ]
  edge
  [
    source 580
    target 578
    measure 5
    diseases "Stickler_syndrome"
    GOmeasure 5
    id 510
    name "510"
  ]
  edge
  [
    source 579
    target 578
    measure 4
    diseases "Osteoarthritis"
    GOmeasure 4
    id 511
    name "511"
  ]
  edge
  [
    source 692
    target 577
    measure 6
    diseases "Weill-Marchesani_syndrome"
    GOmeasure 6
    id 512
    name "512"
  ]
  edge
  [
    source 605
    target 577
    measure 5
    diseases "Ectopia"
    GOmeasure 5
    id 513
    name "513"
  ]
  edge
  [
    source 576
    target 575
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 514
    name "514"
  ]
  edge
  [
    source 576
    target 574
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 515
    name "515"
  ]
  edge
  [
    source 575
    target 574
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 516
    name "516"
  ]
  edge
  [
    source 576
    target 573
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 517
    name "517"
  ]
  edge
  [
    source 575
    target 573
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 518
    name "518"
  ]
  edge
  [
    source 574
    target 573
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 519
    name "519"
  ]
  edge
  [
    source 576
    target 572
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 520
    name "520"
  ]
  edge
  [
    source 575
    target 572
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 521
    name "521"
  ]
  edge
  [
    source 574
    target 572
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 522
    name "522"
  ]
  edge
  [
    source 573
    target 572
    measure 7
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 7
    id 523
    name "523"
  ]
  edge
  [
    source 576
    target 571
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 524
    name "524"
  ]
  edge
  [
    source 575
    target 571
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 525
    name "525"
  ]
  edge
  [
    source 574
    target 571
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 526
    name "526"
  ]
  edge
  [
    source 573
    target 571
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 527
    name "527"
  ]
  edge
  [
    source 572
    target 571
    measure 7
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 7
    id 528
    name "528"
  ]
  edge
  [
    source 576
    target 570
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 529
    name "529"
  ]
  edge
  [
    source 575
    target 570
    measure 4
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 4
    id 530
    name "530"
  ]
  edge
  [
    source 574
    target 570
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 531
    name "531"
  ]
  edge
  [
    source 573
    target 570
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 532
    name "532"
  ]
  edge
  [
    source 572
    target 570
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 533
    name "533"
  ]
  edge
  [
    source 571
    target 570
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 534
    name "534"
  ]
  edge
  [
    source 569
    target 568
    measure 6
    diseases "Achromatopsia"
    GOmeasure 6
    id 535
    name "535"
  ]
  edge
  [
    source 567
    target 566
    measure 6
    diseases "HIV"
    GOmeasure 6
    id 536
    name "536"
  ]
  edge
  [
    source 567
    target 565
    measure 4
    diseases "HIV"
    GOmeasure 4
    id 537
    name "537"
  ]
  edge
  [
    source 566
    target 565
    measure 6
    diseases "HIV"
    GOmeasure 6
    id 538
    name "538"
  ]
  edge
  [
    source 564
    target 563
    measure 5
    diseases "Osteopetrosis"
    GOmeasure 5
    id 539
    name "539"
  ]
  edge
  [
    source 564
    target 562
    measure 5
    diseases "Osteopetrosis"
    GOmeasure 5
    id 540
    name "540"
  ]
  edge
  [
    source 563
    target 562
    measure 6
    diseases "Osteopetrosis"
    GOmeasure 6
    id 541
    name "541"
  ]
  edge
  [
    source 559
    target 558
    measure 8
    diseases "Myasthenic_syndrome"
    GOmeasure 8
    id 542
    name "542"
  ]
  edge
  [
    source 559
    target 557
    measure 5
    diseases "Myasthenic_syndrome"
    GOmeasure 5
    id 543
    name "543"
  ]
  edge
  [
    source 558
    target 557
    measure 5
    diseases "Myasthenic_syndrome"
    GOmeasure 5
    id 544
    name "544"
  ]
  edge
  [
    source 559
    target 556
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 545
    name "545"
  ]
  edge
  [
    source 558
    target 556
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 546
    name "546"
  ]
  edge
  [
    source 557
    target 556
    measure 5
    diseases "Myasthenic_syndrome"
    GOmeasure 5
    id 547
    name "547"
  ]
  edge
  [
    source 559
    target 555
    measure 7
    diseases "Myasthenic_syndrome"
    GOmeasure 7
    id 548
    name "548"
  ]
  edge
  [
    source 558
    target 555
    measure 7
    diseases "Myasthenic_syndrome"
    GOmeasure 7
    id 549
    name "549"
  ]
  edge
  [
    source 557
    target 555
    measure 5
    diseases "Myasthenic_syndrome"
    GOmeasure 5
    id 550
    name "550"
  ]
  edge
  [
    source 556
    target 555
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 551
    name "551"
  ]
  edge
  [
    source 552
    target 551
    measure 8
    diseases "Dysfibrinogenemia,Thrombophilia"
    GOmeasure 8
    id 552
    name "552"
  ]
  edge
  [
    source 552
    target 550
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 553
    name "553"
  ]
  edge
  [
    source 551
    target 550
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 554
    name "554"
  ]
  edge
  [
    source 552
    target 549
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 555
    name "555"
  ]
  edge
  [
    source 551
    target 549
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 556
    name "556"
  ]
  edge
  [
    source 550
    target 549
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 557
    name "557"
  ]
  edge
  [
    source 552
    target 548
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 558
    name "558"
  ]
  edge
  [
    source 551
    target 548
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 559
    name "559"
  ]
  edge
  [
    source 550
    target 548
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 560
    name "560"
  ]
  edge
  [
    source 549
    target 548
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 561
    name "561"
  ]
  edge
  [
    source 684
    target 547
    measure 7
    diseases "Thrombocytopenia"
    GOmeasure 7
    id 562
    name "562"
  ]
  edge
  [
    source 683
    target 547
    measure 6
    diseases "Thrombocytopenia"
    GOmeasure 6
    id 563
    name "563"
  ]
  edge
  [
    source 682
    target 547
    measure 7
    diseases "Hemorrhagic_diathesis"
    GOmeasure 7
    id 564
    name "564"
  ]
  edge
  [
    source 677
    target 547
    measure 4
    diseases "Thrombocytopenia"
    GOmeasure 4
    id 565
    name "565"
  ]
  edge
  [
    source 552
    target 547
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 566
    name "566"
  ]
  edge
  [
    source 551
    target 547
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 567
    name "567"
  ]
  edge
  [
    source 550
    target 547
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 568
    name "568"
  ]
  edge
  [
    source 549
    target 547
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 569
    name "569"
  ]
  edge
  [
    source 548
    target 547
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 570
    name "570"
  ]
  edge
  [
    source 552
    target 545
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 571
    name "571"
  ]
  edge
  [
    source 551
    target 545
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 572
    name "572"
  ]
  edge
  [
    source 550
    target 545
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 573
    name "573"
  ]
  edge
  [
    source 549
    target 545
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 574
    name "574"
  ]
  edge
  [
    source 548
    target 545
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 575
    name "575"
  ]
  edge
  [
    source 547
    target 545
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 576
    name "576"
  ]
  edge
  [
    source 546
    target 545
    measure 6
    diseases "Homocystinuria"
    GOmeasure 6
    id 577
    name "577"
  ]
  edge
  [
    source 544
    target 543
    measure 7
    diseases "Myelogenous_leukemia"
    GOmeasure 7
    id 578
    name "578"
  ]
  edge
  [
    source 544
    target 542
    measure 6
    diseases "Myelogenous_leukemia"
    GOmeasure 6
    id 579
    name "579"
  ]
  edge
  [
    source 543
    target 542
    measure 6
    diseases "Myelogenous_leukemia"
    GOmeasure 6
    id 580
    name "580"
  ]
  edge
  [
    source 541
    target 540
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 581
    name "581"
  ]
  edge
  [
    source 541
    target 539
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 582
    name "582"
  ]
  edge
  [
    source 540
    target 539
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 583
    name "583"
  ]
  edge
  [
    source 541
    target 538
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 584
    name "584"
  ]
  edge
  [
    source 540
    target 538
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 585
    name "585"
  ]
  edge
  [
    source 539
    target 538
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 586
    name "586"
  ]
  edge
  [
    source 756
    target 537
    measure 5
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 5
    id 587
    name "587"
  ]
  edge
  [
    source 755
    target 537
    measure 6
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 6
    id 588
    name "588"
  ]
  edge
  [
    source 754
    target 537
    measure 5
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 5
    id 589
    name "589"
  ]
  edge
  [
    source 753
    target 537
    measure 5
    diseases "Bare_lymphocyte_syndrome"
    GOmeasure 5
    id 590
    name "590"
  ]
  edge
  [
    source 541
    target 537
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 591
    name "591"
  ]
  edge
  [
    source 540
    target 537
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 592
    name "592"
  ]
  edge
  [
    source 539
    target 537
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 593
    name "593"
  ]
  edge
  [
    source 538
    target 537
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 594
    name "594"
  ]
  edge
  [
    source 567
    target 536
    measure 6
    diseases "HIV"
    GOmeasure 6
    id 595
    name "595"
  ]
  edge
  [
    source 566
    target 536
    measure 7
    diseases "HIV"
    GOmeasure 7
    id 596
    name "596"
  ]
  edge
  [
    source 565
    target 536
    measure 5
    diseases "HIV"
    GOmeasure 5
    id 597
    name "597"
  ]
  edge
  [
    source 541
    target 536
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 598
    name "598"
  ]
  edge
  [
    source 540
    target 536
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 599
    name "599"
  ]
  edge
  [
    source 539
    target 536
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 600
    name "600"
  ]
  edge
  [
    source 538
    target 536
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 601
    name "601"
  ]
  edge
  [
    source 537
    target 536
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 602
    name "602"
  ]
  edge
  [
    source 553
    target 535
    measure 6
    diseases "Malaria"
    GOmeasure 6
    id 603
    name "603"
  ]
  edge
  [
    source 535
    target 534
    measure 6
    diseases "Platelet_defect/deficiency"
    GOmeasure 6
    id 604
    name "604"
  ]
  edge
  [
    source 741
    target 532
    measure 7
    diseases "Lipoma"
    GOmeasure 7
    id 605
    name "605"
  ]
  edge
  [
    source 533
    target 532
    measure 5
    diseases "Hyperparathyroidism,Parathyroid_adenoma"
    GOmeasure 5
    id 606
    name "606"
  ]
  edge
  [
    source 533
    target 531
    measure 5
    diseases "Hyperparathyroidism"
    GOmeasure 5
    id 607
    name "607"
  ]
  edge
  [
    source 532
    target 531
    measure 4
    diseases "Hyperparathyroidism"
    GOmeasure 4
    id 608
    name "608"
  ]
  edge
  [
    source 661
    target 530
    measure 4
    diseases "Arrhythmogenic_right_ventricular_dysplasia"
    GOmeasure 4
    id 609
    name "609"
  ]
  edge
  [
    source 588
    target 530
    measure 8
    diseases "Arrhythmogenic_right_ventricular_dysplasia"
    GOmeasure 8
    id 610
    name "610"
  ]
  edge
  [
    source 714
    target 529
    measure 6
    diseases "Pituitary_ACTH-secreting_adenoma"
    GOmeasure 6
    id 611
    name "611"
  ]
  edge
  [
    source 713
    target 529
    measure 6
    diseases "Pituitary_ACTH-secreting_adenoma"
    GOmeasure 6
    id 612
    name "612"
  ]
  edge
  [
    source 530
    target 529
    measure 6
    diseases "Ventricular_tachycardia"
    GOmeasure 6
    id 613
    name "613"
  ]
  edge
  [
    source 530
    target 528
    measure 6
    diseases "Ventricular_tachycardia"
    GOmeasure 6
    id 614
    name "614"
  ]
  edge
  [
    source 529
    target 528
    measure 7
    diseases "Ventricular_tachycardia"
    GOmeasure 7
    id 615
    name "615"
  ]
  edge
  [
    source 752
    target 527
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 616
    name "616"
  ]
  edge
  [
    source 751
    target 527
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 617
    name "617"
  ]
  edge
  [
    source 750
    target 527
    measure 7
    diseases "Renal_cell_carcinoma"
    GOmeasure 7
    id 618
    name "618"
  ]
  edge
  [
    source 749
    target 527
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 619
    name "619"
  ]
  edge
  [
    source 527
    target 526
    measure 5
    diseases "Hepatic_adenoma"
    GOmeasure 5
    id 620
    name "620"
  ]
  edge
  [
    source 527
    target 525
    measure 5
    diseases "Hepatic_adenoma"
    GOmeasure 5
    id 621
    name "621"
  ]
  edge
  [
    source 526
    target 525
    measure 4
    diseases "Hepatic_adenoma"
    GOmeasure 4
    id 622
    name "622"
  ]
  edge
  [
    source 694
    target 523
    measure 6
    diseases "Walker-Warburg_syndrome"
    GOmeasure 6
    id 623
    name "623"
  ]
  edge
  [
    source 524
    target 523
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 624
    name "624"
  ]
  edge
  [
    source 524
    target 522
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 625
    name "625"
  ]
  edge
  [
    source 523
    target 522
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 626
    name "626"
  ]
  edge
  [
    source 524
    target 521
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 627
    name "627"
  ]
  edge
  [
    source 523
    target 521
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 628
    name "628"
  ]
  edge
  [
    source 522
    target 521
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 629
    name "629"
  ]
  edge
  [
    source 524
    target 520
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 630
    name "630"
  ]
  edge
  [
    source 523
    target 520
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 631
    name "631"
  ]
  edge
  [
    source 522
    target 520
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 632
    name "632"
  ]
  edge
  [
    source 521
    target 520
    measure 7
    diseases "Muscular_dystrophy"
    GOmeasure 7
    id 633
    name "633"
  ]
  edge
  [
    source 524
    target 519
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 634
    name "634"
  ]
  edge
  [
    source 523
    target 519
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 635
    name "635"
  ]
  edge
  [
    source 522
    target 519
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 636
    name "636"
  ]
  edge
  [
    source 521
    target 519
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 637
    name "637"
  ]
  edge
  [
    source 520
    target 519
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 638
    name "638"
  ]
  edge
  [
    source 524
    target 518
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 639
    name "639"
  ]
  edge
  [
    source 523
    target 518
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 640
    name "640"
  ]
  edge
  [
    source 522
    target 518
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 641
    name "641"
  ]
  edge
  [
    source 521
    target 518
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 642
    name "642"
  ]
  edge
  [
    source 520
    target 518
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 643
    name "643"
  ]
  edge
  [
    source 519
    target 518
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 644
    name "644"
  ]
  edge
  [
    source 524
    target 517
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 645
    name "645"
  ]
  edge
  [
    source 523
    target 517
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 646
    name "646"
  ]
  edge
  [
    source 522
    target 517
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 647
    name "647"
  ]
  edge
  [
    source 521
    target 517
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 648
    name "648"
  ]
  edge
  [
    source 520
    target 517
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 649
    name "649"
  ]
  edge
  [
    source 519
    target 517
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 650
    name "650"
  ]
  edge
  [
    source 518
    target 517
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 651
    name "651"
  ]
  edge
  [
    source 524
    target 516
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 652
    name "652"
  ]
  edge
  [
    source 523
    target 516
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 653
    name "653"
  ]
  edge
  [
    source 522
    target 516
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 654
    name "654"
  ]
  edge
  [
    source 521
    target 516
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 655
    name "655"
  ]
  edge
  [
    source 520
    target 516
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 656
    name "656"
  ]
  edge
  [
    source 519
    target 516
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 657
    name "657"
  ]
  edge
  [
    source 518
    target 516
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 658
    name "658"
  ]
  edge
  [
    source 517
    target 516
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 659
    name "659"
  ]
  edge
  [
    source 524
    target 515
    measure 7
    diseases "Muscular_dystrophy"
    GOmeasure 7
    id 660
    name "660"
  ]
  edge
  [
    source 523
    target 515
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 661
    name "661"
  ]
  edge
  [
    source 522
    target 515
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 662
    name "662"
  ]
  edge
  [
    source 521
    target 515
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 663
    name "663"
  ]
  edge
  [
    source 520
    target 515
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 664
    name "664"
  ]
  edge
  [
    source 519
    target 515
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 665
    name "665"
  ]
  edge
  [
    source 518
    target 515
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 666
    name "666"
  ]
  edge
  [
    source 517
    target 515
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 667
    name "667"
  ]
  edge
  [
    source 516
    target 515
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 668
    name "668"
  ]
  edge
  [
    source 577
    target 513
    measure 6
    diseases "Marfan_syndrome"
    GOmeasure 6
    id 669
    name "669"
  ]
  edge
  [
    source 576
    target 513
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 670
    name "670"
  ]
  edge
  [
    source 575
    target 513
    measure 7
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 7
    id 671
    name "671"
  ]
  edge
  [
    source 574
    target 513
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 672
    name "672"
  ]
  edge
  [
    source 573
    target 513
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 673
    name "673"
  ]
  edge
  [
    source 572
    target 513
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 674
    name "674"
  ]
  edge
  [
    source 571
    target 513
    measure 7
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 7
    id 675
    name "675"
  ]
  edge
  [
    source 570
    target 513
    measure 4
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 4
    id 676
    name "676"
  ]
  edge
  [
    source 514
    target 513
    measure 6
    diseases "Osteoporosis"
    GOmeasure 6
    id 677
    name "677"
  ]
  edge
  [
    source 743
    target 512
    measure 6
    diseases "Exudative_vitreoretinopathy"
    GOmeasure 6
    id 678
    name "678"
  ]
  edge
  [
    source 742
    target 512
    measure 5
    diseases "Exudative_vitreoretinopathy"
    GOmeasure 5
    id 679
    name "679"
  ]
  edge
  [
    source 564
    target 512
    measure 5
    diseases "Osteopetrosis"
    GOmeasure 5
    id 680
    name "680"
  ]
  edge
  [
    source 563
    target 512
    measure 5
    diseases "Osteopetrosis"
    GOmeasure 5
    id 681
    name "681"
  ]
  edge
  [
    source 562
    target 512
    measure 4
    diseases "Osteopetrosis"
    GOmeasure 4
    id 682
    name "682"
  ]
  edge
  [
    source 514
    target 512
    measure 5
    diseases "Osteoporosis"
    GOmeasure 5
    id 683
    name "683"
  ]
  edge
  [
    source 513
    target 512
    measure 4
    diseases "Osteoporosis"
    GOmeasure 4
    id 684
    name "684"
  ]
  edge
  [
    source 576
    target 511
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 685
    name "685"
  ]
  edge
  [
    source 575
    target 511
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 686
    name "686"
  ]
  edge
  [
    source 574
    target 511
    measure 5
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 5
    id 687
    name "687"
  ]
  edge
  [
    source 573
    target 511
    measure 7
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 7
    id 688
    name "688"
  ]
  edge
  [
    source 572
    target 511
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 689
    name "689"
  ]
  edge
  [
    source 571
    target 511
    measure 6
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 6
    id 690
    name "690"
  ]
  edge
  [
    source 570
    target 511
    measure 7
    diseases "Ehlers-Danlos_syndrome"
    GOmeasure 7
    id 691
    name "691"
  ]
  edge
  [
    source 514
    target 511
    measure 7
    diseases "Osteoporosis"
    GOmeasure 7
    id 692
    name "692"
  ]
  edge
  [
    source 513
    target 511
    measure 5
    diseases "Ehlers-Danlos_syndrome,Osteogenesis_imperfecta,Osteoporosis"
    GOmeasure 5
    id 693
    name "693"
  ]
  edge
  [
    source 512
    target 511
    measure 5
    diseases "Osteoporosis"
    GOmeasure 5
    id 694
    name "694"
  ]
  edge
  [
    source 514
    target 510
    measure 8
    diseases "Osteoporosis"
    GOmeasure 8
    id 695
    name "695"
  ]
  edge
  [
    source 513
    target 510
    measure 6
    diseases "Osteoporosis"
    GOmeasure 6
    id 696
    name "696"
  ]
  edge
  [
    source 512
    target 510
    measure 5
    diseases "Osteoporosis"
    GOmeasure 5
    id 697
    name "697"
  ]
  edge
  [
    source 511
    target 510
    measure 7
    diseases "Osteoporosis"
    GOmeasure 7
    id 698
    name "698"
  ]
  edge
  [
    source 509
    target 508
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 699
    name "699"
  ]
  edge
  [
    source 644
    target 507
    measure 5
    diseases "Glioblastoma"
    GOmeasure 5
    id 700
    name "700"
  ]
  edge
  [
    source 509
    target 507
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 701
    name "701"
  ]
  edge
  [
    source 508
    target 507
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 702
    name "702"
  ]
  edge
  [
    source 560
    target 506
    measure 4
    diseases "Nicotine_addiction"
    GOmeasure 4
    id 703
    name "703"
  ]
  edge
  [
    source 509
    target 506
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 704
    name "704"
  ]
  edge
  [
    source 508
    target 506
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 705
    name "705"
  ]
  edge
  [
    source 507
    target 506
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 706
    name "706"
  ]
  edge
  [
    source 509
    target 505
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 707
    name "707"
  ]
  edge
  [
    source 508
    target 505
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 708
    name "708"
  ]
  edge
  [
    source 507
    target 505
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 709
    name "709"
  ]
  edge
  [
    source 506
    target 505
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 710
    name "710"
  ]
  edge
  [
    source 509
    target 504
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 711
    name "711"
  ]
  edge
  [
    source 508
    target 504
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 712
    name "712"
  ]
  edge
  [
    source 507
    target 504
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 713
    name "713"
  ]
  edge
  [
    source 506
    target 504
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 714
    name "714"
  ]
  edge
  [
    source 505
    target 504
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 715
    name "715"
  ]
  edge
  [
    source 509
    target 503
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 716
    name "716"
  ]
  edge
  [
    source 508
    target 503
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 717
    name "717"
  ]
  edge
  [
    source 507
    target 503
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 718
    name "718"
  ]
  edge
  [
    source 506
    target 503
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 719
    name "719"
  ]
  edge
  [
    source 505
    target 503
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 720
    name "720"
  ]
  edge
  [
    source 504
    target 503
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 721
    name "721"
  ]
  edge
  [
    source 509
    target 502
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 722
    name "722"
  ]
  edge
  [
    source 508
    target 502
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 723
    name "723"
  ]
  edge
  [
    source 507
    target 502
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 724
    name "724"
  ]
  edge
  [
    source 506
    target 502
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 725
    name "725"
  ]
  edge
  [
    source 505
    target 502
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 726
    name "726"
  ]
  edge
  [
    source 504
    target 502
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 727
    name "727"
  ]
  edge
  [
    source 503
    target 502
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 728
    name "728"
  ]
  edge
  [
    source 509
    target 501
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 729
    name "729"
  ]
  edge
  [
    source 508
    target 501
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 730
    name "730"
  ]
  edge
  [
    source 507
    target 501
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 731
    name "731"
  ]
  edge
  [
    source 506
    target 501
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 732
    name "732"
  ]
  edge
  [
    source 505
    target 501
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 733
    name "733"
  ]
  edge
  [
    source 504
    target 501
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 734
    name "734"
  ]
  edge
  [
    source 503
    target 501
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 735
    name "735"
  ]
  edge
  [
    source 502
    target 501
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 736
    name "736"
  ]
  edge
  [
    source 509
    target 500
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 737
    name "737"
  ]
  edge
  [
    source 508
    target 500
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 738
    name "738"
  ]
  edge
  [
    source 507
    target 500
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 739
    name "739"
  ]
  edge
  [
    source 506
    target 500
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 740
    name "740"
  ]
  edge
  [
    source 505
    target 500
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 741
    name "741"
  ]
  edge
  [
    source 504
    target 500
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 742
    name "742"
  ]
  edge
  [
    source 503
    target 500
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 743
    name "743"
  ]
  edge
  [
    source 502
    target 500
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 744
    name "744"
  ]
  edge
  [
    source 501
    target 500
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 745
    name "745"
  ]
  edge
  [
    source 509
    target 499
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 746
    name "746"
  ]
  edge
  [
    source 508
    target 499
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 747
    name "747"
  ]
  edge
  [
    source 507
    target 499
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 748
    name "748"
  ]
  edge
  [
    source 506
    target 499
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 749
    name "749"
  ]
  edge
  [
    source 505
    target 499
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 750
    name "750"
  ]
  edge
  [
    source 504
    target 499
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 751
    name "751"
  ]
  edge
  [
    source 503
    target 499
    measure 8
    diseases "Epilepsy"
    GOmeasure 8
    id 752
    name "752"
  ]
  edge
  [
    source 502
    target 499
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 753
    name "753"
  ]
  edge
  [
    source 501
    target 499
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 754
    name "754"
  ]
  edge
  [
    source 500
    target 499
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 755
    name "755"
  ]
  edge
  [
    source 509
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 756
    name "756"
  ]
  edge
  [
    source 508
    target 498
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 757
    name "757"
  ]
  edge
  [
    source 507
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 758
    name "758"
  ]
  edge
  [
    source 506
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 759
    name "759"
  ]
  edge
  [
    source 505
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 760
    name "760"
  ]
  edge
  [
    source 504
    target 498
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 761
    name "761"
  ]
  edge
  [
    source 503
    target 498
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 762
    name "762"
  ]
  edge
  [
    source 502
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 763
    name "763"
  ]
  edge
  [
    source 501
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 764
    name "764"
  ]
  edge
  [
    source 500
    target 498
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 765
    name "765"
  ]
  edge
  [
    source 499
    target 498
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 766
    name "766"
  ]
  edge
  [
    source 706
    target 497
    measure 5
    diseases "Myoclonic_epilepsy"
    GOmeasure 5
    id 767
    name "767"
  ]
  edge
  [
    source 509
    target 497
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 768
    name "768"
  ]
  edge
  [
    source 508
    target 497
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 769
    name "769"
  ]
  edge
  [
    source 507
    target 497
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 770
    name "770"
  ]
  edge
  [
    source 506
    target 497
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 771
    name "771"
  ]
  edge
  [
    source 505
    target 497
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 772
    name "772"
  ]
  edge
  [
    source 504
    target 497
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 773
    name "773"
  ]
  edge
  [
    source 503
    target 497
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 774
    name "774"
  ]
  edge
  [
    source 502
    target 497
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 775
    name "775"
  ]
  edge
  [
    source 501
    target 497
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 776
    name "776"
  ]
  edge
  [
    source 500
    target 497
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 777
    name "777"
  ]
  edge
  [
    source 499
    target 497
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 778
    name "778"
  ]
  edge
  [
    source 498
    target 497
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 779
    name "779"
  ]
  edge
  [
    source 509
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 780
    name "780"
  ]
  edge
  [
    source 508
    target 496
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 781
    name "781"
  ]
  edge
  [
    source 507
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 782
    name "782"
  ]
  edge
  [
    source 506
    target 496
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 783
    name "783"
  ]
  edge
  [
    source 505
    target 496
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 784
    name "784"
  ]
  edge
  [
    source 504
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 785
    name "785"
  ]
  edge
  [
    source 503
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 786
    name "786"
  ]
  edge
  [
    source 502
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 787
    name "787"
  ]
  edge
  [
    source 501
    target 496
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 788
    name "788"
  ]
  edge
  [
    source 500
    target 496
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 789
    name "789"
  ]
  edge
  [
    source 499
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 790
    name "790"
  ]
  edge
  [
    source 498
    target 496
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 791
    name "791"
  ]
  edge
  [
    source 497
    target 496
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 792
    name "792"
  ]
  edge
  [
    source 509
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 793
    name "793"
  ]
  edge
  [
    source 508
    target 495
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 794
    name "794"
  ]
  edge
  [
    source 507
    target 495
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 795
    name "795"
  ]
  edge
  [
    source 506
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 796
    name "796"
  ]
  edge
  [
    source 505
    target 495
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 797
    name "797"
  ]
  edge
  [
    source 504
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 798
    name "798"
  ]
  edge
  [
    source 503
    target 495
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 799
    name "799"
  ]
  edge
  [
    source 502
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 800
    name "800"
  ]
  edge
  [
    source 501
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 801
    name "801"
  ]
  edge
  [
    source 500
    target 495
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 802
    name "802"
  ]
  edge
  [
    source 499
    target 495
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 803
    name "803"
  ]
  edge
  [
    source 498
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 804
    name "804"
  ]
  edge
  [
    source 497
    target 495
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 805
    name "805"
  ]
  edge
  [
    source 496
    target 495
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 806
    name "806"
  ]
  edge
  [
    source 494
    target 493
    measure 6
    diseases "Ataxia"
    GOmeasure 6
    id 807
    name "807"
  ]
  edge
  [
    source 494
    target 492
    measure 4
    diseases "Ataxia"
    GOmeasure 4
    id 808
    name "808"
  ]
  edge
  [
    source 493
    target 492
    measure 4
    diseases "Ataxia"
    GOmeasure 4
    id 809
    name "809"
  ]
  edge
  [
    source 509
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 810
    name "810"
  ]
  edge
  [
    source 508
    target 491
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 811
    name "811"
  ]
  edge
  [
    source 507
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 812
    name "812"
  ]
  edge
  [
    source 506
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 813
    name "813"
  ]
  edge
  [
    source 505
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 814
    name "814"
  ]
  edge
  [
    source 504
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 815
    name "815"
  ]
  edge
  [
    source 503
    target 491
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 816
    name "816"
  ]
  edge
  [
    source 502
    target 491
    measure 4
    diseases "Epilepsy"
    GOmeasure 4
    id 817
    name "817"
  ]
  edge
  [
    source 501
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 818
    name "818"
  ]
  edge
  [
    source 500
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 819
    name "819"
  ]
  edge
  [
    source 499
    target 491
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 820
    name "820"
  ]
  edge
  [
    source 498
    target 491
    measure 7
    diseases "Epilepsy"
    GOmeasure 7
    id 821
    name "821"
  ]
  edge
  [
    source 497
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 822
    name "822"
  ]
  edge
  [
    source 496
    target 491
    measure 6
    diseases "Epilepsy"
    GOmeasure 6
    id 823
    name "823"
  ]
  edge
  [
    source 495
    target 491
    measure 5
    diseases "Epilepsy"
    GOmeasure 5
    id 824
    name "824"
  ]
  edge
  [
    source 494
    target 491
    measure 5
    diseases "Ataxia"
    GOmeasure 5
    id 825
    name "825"
  ]
  edge
  [
    source 493
    target 491
    measure 4
    diseases "Ataxia"
    GOmeasure 4
    id 826
    name "826"
  ]
  edge
  [
    source 492
    target 491
    measure 5
    diseases "Ataxia"
    GOmeasure 5
    id 827
    name "827"
  ]
  edge
  [
    source 561
    target 489
    measure 5
    diseases "Myotonia_congenita"
    GOmeasure 5
    id 828
    name "828"
  ]
  edge
  [
    source 559
    target 489
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 829
    name "829"
  ]
  edge
  [
    source 558
    target 489
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 830
    name "830"
  ]
  edge
  [
    source 557
    target 489
    measure 5
    diseases "Myasthenic_syndrome"
    GOmeasure 5
    id 831
    name "831"
  ]
  edge
  [
    source 556
    target 489
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 832
    name "832"
  ]
  edge
  [
    source 555
    target 489
    measure 6
    diseases "Myasthenic_syndrome"
    GOmeasure 6
    id 833
    name "833"
  ]
  edge
  [
    source 489
    target 488
    measure 7
    diseases "Hypokalemic_periodic_paralysis"
    GOmeasure 7
    id 834
    name "834"
  ]
  edge
  [
    source 490
    target 487
    measure 5
    diseases "Malignant_hyperthermia_susceptibility"
    GOmeasure 5
    id 835
    name "835"
  ]
  edge
  [
    source 489
    target 487
    measure 6
    diseases "Hypokalemic_periodic_paralysis"
    GOmeasure 6
    id 836
    name "836"
  ]
  edge
  [
    source 488
    target 487
    measure 5
    diseases "Hypokalemic_periodic_paralysis"
    GOmeasure 5
    id 837
    name "837"
  ]
  edge
  [
    source 486
    target 485
    measure 5
    diseases "Night_blindness"
    GOmeasure 5
    id 838
    name "838"
  ]
  edge
  [
    source 486
    target 484
    measure 5
    diseases "Night_blindness"
    GOmeasure 5
    id 839
    name "839"
  ]
  edge
  [
    source 485
    target 484
    measure 5
    diseases "Night_blindness"
    GOmeasure 5
    id 840
    name "840"
  ]
  edge
  [
    source 483
    target 482
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 841
    name "841"
  ]
  edge
  [
    source 483
    target 481
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 842
    name "842"
  ]
  edge
  [
    source 482
    target 481
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 843
    name "843"
  ]
  edge
  [
    source 483
    target 480
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 844
    name "844"
  ]
  edge
  [
    source 482
    target 480
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 845
    name "845"
  ]
  edge
  [
    source 481
    target 480
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 846
    name "846"
  ]
  edge
  [
    source 716
    target 479
    measure 6
    diseases "Huntington_disease"
    GOmeasure 6
    id 847
    name "847"
  ]
  edge
  [
    source 705
    target 479
    measure 6
    diseases "Huntington_disease"
    GOmeasure 6
    id 848
    name "848"
  ]
  edge
  [
    source 635
    target 479
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 849
    name "849"
  ]
  edge
  [
    source 634
    target 479
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 850
    name "850"
  ]
  edge
  [
    source 633
    target 479
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 851
    name "851"
  ]
  edge
  [
    source 632
    target 479
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 852
    name "852"
  ]
  edge
  [
    source 631
    target 479
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 853
    name "853"
  ]
  edge
  [
    source 630
    target 479
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 854
    name "854"
  ]
  edge
  [
    source 629
    target 479
    measure 4
    diseases "Parkinson_disease"
    GOmeasure 4
    id 855
    name "855"
  ]
  edge
  [
    source 628
    target 479
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 856
    name "856"
  ]
  edge
  [
    source 627
    target 479
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 857
    name "857"
  ]
  edge
  [
    source 483
    target 479
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 858
    name "858"
  ]
  edge
  [
    source 482
    target 479
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 859
    name "859"
  ]
  edge
  [
    source 481
    target 479
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 860
    name "860"
  ]
  edge
  [
    source 480
    target 479
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 861
    name "861"
  ]
  edge
  [
    source 483
    target 478
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 862
    name "862"
  ]
  edge
  [
    source 482
    target 478
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 863
    name "863"
  ]
  edge
  [
    source 481
    target 478
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 864
    name "864"
  ]
  edge
  [
    source 480
    target 478
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 865
    name "865"
  ]
  edge
  [
    source 479
    target 478
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 866
    name "866"
  ]
  edge
  [
    source 483
    target 477
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 867
    name "867"
  ]
  edge
  [
    source 482
    target 477
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 868
    name "868"
  ]
  edge
  [
    source 481
    target 477
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 869
    name "869"
  ]
  edge
  [
    source 480
    target 477
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 870
    name "870"
  ]
  edge
  [
    source 479
    target 477
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 871
    name "871"
  ]
  edge
  [
    source 478
    target 477
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 872
    name "872"
  ]
  edge
  [
    source 483
    target 476
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 873
    name "873"
  ]
  edge
  [
    source 482
    target 476
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 874
    name "874"
  ]
  edge
  [
    source 481
    target 476
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 875
    name "875"
  ]
  edge
  [
    source 480
    target 476
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 876
    name "876"
  ]
  edge
  [
    source 479
    target 476
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 877
    name "877"
  ]
  edge
  [
    source 478
    target 476
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 878
    name "878"
  ]
  edge
  [
    source 477
    target 476
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 879
    name "879"
  ]
  edge
  [
    source 483
    target 475
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 880
    name "880"
  ]
  edge
  [
    source 482
    target 475
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 881
    name "881"
  ]
  edge
  [
    source 481
    target 475
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 882
    name "882"
  ]
  edge
  [
    source 480
    target 475
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 883
    name "883"
  ]
  edge
  [
    source 479
    target 475
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 884
    name "884"
  ]
  edge
  [
    source 478
    target 475
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 885
    name "885"
  ]
  edge
  [
    source 477
    target 475
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 886
    name "886"
  ]
  edge
  [
    source 476
    target 475
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 887
    name "887"
  ]
  edge
  [
    source 483
    target 472
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 888
    name "888"
  ]
  edge
  [
    source 482
    target 472
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 889
    name "889"
  ]
  edge
  [
    source 481
    target 472
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 890
    name "890"
  ]
  edge
  [
    source 480
    target 472
    measure 6
    diseases "Spinocereballar_ataxia"
    GOmeasure 6
    id 891
    name "891"
  ]
  edge
  [
    source 479
    target 472
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 892
    name "892"
  ]
  edge
  [
    source 478
    target 472
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 893
    name "893"
  ]
  edge
  [
    source 477
    target 472
    measure 5
    diseases "Spinocereballar_ataxia"
    GOmeasure 5
    id 894
    name "894"
  ]
  edge
  [
    source 476
    target 472
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 895
    name "895"
  ]
  edge
  [
    source 475
    target 472
    measure 4
    diseases "Spinocereballar_ataxia"
    GOmeasure 4
    id 896
    name "896"
  ]
  edge
  [
    source 474
    target 472
    measure 5
    diseases "Episodic_ataxia"
    GOmeasure 5
    id 897
    name "897"
  ]
  edge
  [
    source 473
    target 472
    measure 4
    diseases "Cerebellar_ataxia"
    GOmeasure 4
    id 898
    name "898"
  ]
  edge
  [
    source 471
    target 470
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 899
    name "899"
  ]
  edge
  [
    source 471
    target 469
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 900
    name "900"
  ]
  edge
  [
    source 470
    target 469
    measure 7
    diseases "Complementary_component_deficiency"
    GOmeasure 7
    id 901
    name "901"
  ]
  edge
  [
    source 471
    target 468
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 902
    name "902"
  ]
  edge
  [
    source 470
    target 468
    measure 7
    diseases "Complementary_component_deficiency"
    GOmeasure 7
    id 903
    name "903"
  ]
  edge
  [
    source 469
    target 468
    measure 7
    diseases "Complementary_component_deficiency"
    GOmeasure 7
    id 904
    name "904"
  ]
  edge
  [
    source 471
    target 467
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 905
    name "905"
  ]
  edge
  [
    source 470
    target 467
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 906
    name "906"
  ]
  edge
  [
    source 469
    target 467
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 907
    name "907"
  ]
  edge
  [
    source 468
    target 467
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 908
    name "908"
  ]
  edge
  [
    source 471
    target 466
    measure 7
    diseases "Complementary_component_deficiency"
    GOmeasure 7
    id 909
    name "909"
  ]
  edge
  [
    source 470
    target 466
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 910
    name "910"
  ]
  edge
  [
    source 469
    target 466
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 911
    name "911"
  ]
  edge
  [
    source 468
    target 466
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 912
    name "912"
  ]
  edge
  [
    source 467
    target 466
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 913
    name "913"
  ]
  edge
  [
    source 471
    target 465
    measure 4
    diseases "Complementary_component_deficiency"
    GOmeasure 4
    id 914
    name "914"
  ]
  edge
  [
    source 470
    target 465
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 915
    name "915"
  ]
  edge
  [
    source 469
    target 465
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 916
    name "916"
  ]
  edge
  [
    source 468
    target 465
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 917
    name "917"
  ]
  edge
  [
    source 467
    target 465
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 918
    name "918"
  ]
  edge
  [
    source 466
    target 465
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 919
    name "919"
  ]
  edge
  [
    source 471
    target 464
    measure 4
    diseases "Complementary_component_deficiency"
    GOmeasure 4
    id 920
    name "920"
  ]
  edge
  [
    source 470
    target 464
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 921
    name "921"
  ]
  edge
  [
    source 469
    target 464
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 922
    name "922"
  ]
  edge
  [
    source 468
    target 464
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 923
    name "923"
  ]
  edge
  [
    source 467
    target 464
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 924
    name "924"
  ]
  edge
  [
    source 466
    target 464
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 925
    name "925"
  ]
  edge
  [
    source 465
    target 464
    measure 7
    diseases "Complementary_component_deficiency"
    GOmeasure 7
    id 926
    name "926"
  ]
  edge
  [
    source 471
    target 463
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 927
    name "927"
  ]
  edge
  [
    source 470
    target 463
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 928
    name "928"
  ]
  edge
  [
    source 469
    target 463
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 929
    name "929"
  ]
  edge
  [
    source 468
    target 463
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 930
    name "930"
  ]
  edge
  [
    source 467
    target 463
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 931
    name "931"
  ]
  edge
  [
    source 466
    target 463
    measure 4
    diseases "Complementary_component_deficiency"
    GOmeasure 4
    id 932
    name "932"
  ]
  edge
  [
    source 465
    target 463
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 933
    name "933"
  ]
  edge
  [
    source 464
    target 463
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 934
    name "934"
  ]
  edge
  [
    source 471
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 935
    name "935"
  ]
  edge
  [
    source 470
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 936
    name "936"
  ]
  edge
  [
    source 469
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 937
    name "937"
  ]
  edge
  [
    source 468
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 938
    name "938"
  ]
  edge
  [
    source 467
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 939
    name "939"
  ]
  edge
  [
    source 466
    target 462
    measure 4
    diseases "Complementary_component_deficiency"
    GOmeasure 4
    id 940
    name "940"
  ]
  edge
  [
    source 465
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 941
    name "941"
  ]
  edge
  [
    source 464
    target 462
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 942
    name "942"
  ]
  edge
  [
    source 463
    target 462
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 943
    name "943"
  ]
  edge
  [
    source 471
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 944
    name "944"
  ]
  edge
  [
    source 470
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 945
    name "945"
  ]
  edge
  [
    source 469
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 946
    name "946"
  ]
  edge
  [
    source 468
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 947
    name "947"
  ]
  edge
  [
    source 467
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 948
    name "948"
  ]
  edge
  [
    source 466
    target 461
    measure 4
    diseases "Complementary_component_deficiency"
    GOmeasure 4
    id 949
    name "949"
  ]
  edge
  [
    source 465
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 950
    name "950"
  ]
  edge
  [
    source 464
    target 461
    measure 5
    diseases "Complementary_component_deficiency"
    GOmeasure 5
    id 951
    name "951"
  ]
  edge
  [
    source 463
    target 461
    measure 6
    diseases "Complementary_component_deficiency"
    GOmeasure 6
    id 952
    name "952"
  ]
  edge
  [
    source 462
    target 461
    measure 7
    diseases "Complementary_component_deficiency"
    GOmeasure 7
    id 953
    name "953"
  ]
  edge
  [
    source 460
    target 459
    measure 6
    diseases "Wilms_tumor"
    GOmeasure 6
    id 954
    name "954"
  ]
  edge
  [
    source 458
    target 457
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 955
    name "955"
  ]
  edge
  [
    source 458
    target 456
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 956
    name "956"
  ]
  edge
  [
    source 457
    target 456
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 957
    name "957"
  ]
  edge
  [
    source 458
    target 455
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 958
    name "958"
  ]
  edge
  [
    source 457
    target 455
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 959
    name "959"
  ]
  edge
  [
    source 456
    target 455
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 960
    name "960"
  ]
  edge
  [
    source 458
    target 454
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 961
    name "961"
  ]
  edge
  [
    source 457
    target 454
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 962
    name "962"
  ]
  edge
  [
    source 456
    target 454
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 963
    name "963"
  ]
  edge
  [
    source 455
    target 454
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 964
    name "964"
  ]
  edge
  [
    source 458
    target 453
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 965
    name "965"
  ]
  edge
  [
    source 457
    target 453
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 966
    name "966"
  ]
  edge
  [
    source 456
    target 453
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 967
    name "967"
  ]
  edge
  [
    source 455
    target 453
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 968
    name "968"
  ]
  edge
  [
    source 454
    target 453
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 969
    name "969"
  ]
  edge
  [
    source 458
    target 452
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 970
    name "970"
  ]
  edge
  [
    source 457
    target 452
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 971
    name "971"
  ]
  edge
  [
    source 456
    target 452
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 972
    name "972"
  ]
  edge
  [
    source 455
    target 452
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 973
    name "973"
  ]
  edge
  [
    source 454
    target 452
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 974
    name "974"
  ]
  edge
  [
    source 453
    target 452
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 975
    name "975"
  ]
  edge
  [
    source 458
    target 451
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 976
    name "976"
  ]
  edge
  [
    source 457
    target 451
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 977
    name "977"
  ]
  edge
  [
    source 456
    target 451
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 978
    name "978"
  ]
  edge
  [
    source 455
    target 451
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 979
    name "979"
  ]
  edge
  [
    source 454
    target 451
    measure 7
    diseases "Fanconi_anemia"
    GOmeasure 7
    id 980
    name "980"
  ]
  edge
  [
    source 453
    target 451
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 981
    name "981"
  ]
  edge
  [
    source 452
    target 451
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 982
    name "982"
  ]
  edge
  [
    source 458
    target 450
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 983
    name "983"
  ]
  edge
  [
    source 457
    target 450
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 984
    name "984"
  ]
  edge
  [
    source 456
    target 450
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 985
    name "985"
  ]
  edge
  [
    source 455
    target 450
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 986
    name "986"
  ]
  edge
  [
    source 454
    target 450
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 987
    name "987"
  ]
  edge
  [
    source 453
    target 450
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 988
    name "988"
  ]
  edge
  [
    source 452
    target 450
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 989
    name "989"
  ]
  edge
  [
    source 451
    target 450
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 990
    name "990"
  ]
  edge
  [
    source 635
    target 448
    measure 7
    diseases "Parkinson_disease"
    GOmeasure 7
    id 991
    name "991"
  ]
  edge
  [
    source 634
    target 448
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 992
    name "992"
  ]
  edge
  [
    source 633
    target 448
    measure 4
    diseases "Parkinson_disease"
    GOmeasure 4
    id 993
    name "993"
  ]
  edge
  [
    source 632
    target 448
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 994
    name "994"
  ]
  edge
  [
    source 631
    target 448
    measure 5
    diseases "Parkinson_disease"
    GOmeasure 5
    id 995
    name "995"
  ]
  edge
  [
    source 630
    target 448
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 996
    name "996"
  ]
  edge
  [
    source 629
    target 448
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 997
    name "997"
  ]
  edge
  [
    source 628
    target 448
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 998
    name "998"
  ]
  edge
  [
    source 627
    target 448
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 999
    name "999"
  ]
  edge
  [
    source 479
    target 448
    measure 6
    diseases "Parkinson_disease"
    GOmeasure 6
    id 1000
    name "1000"
  ]
  edge
  [
    source 448
    target 447
    measure 6
    diseases "Adenocarcinoma"
    GOmeasure 6
    id 1001
    name "1001"
  ]
  edge
  [
    source 777
    target 443
    measure 6
    diseases "Medullary_thyroid_carcinoma"
    GOmeasure 6
    id 1002
    name "1002"
  ]
  edge
  [
    source 664
    target 443
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 1003
    name "1003"
  ]
  edge
  [
    source 663
    target 443
    measure 6
    diseases "Hirschsprung_disease"
    GOmeasure 6
    id 1004
    name "1004"
  ]
  edge
  [
    source 532
    target 443
    measure 6
    diseases "Multiple_endocrine_neoplasia"
    GOmeasure 6
    id 1005
    name "1005"
  ]
  edge
  [
    source 773
    target 442
    measure 5
    diseases "Neuroblastoma"
    GOmeasure 5
    id 1006
    name "1006"
  ]
  edge
  [
    source 664
    target 442
    measure 6
    diseases "Hirschsprung_disease"
    GOmeasure 6
    id 1007
    name "1007"
  ]
  edge
  [
    source 663
    target 442
    measure 6
    diseases "Hirschsprung_disease"
    GOmeasure 6
    id 1008
    name "1008"
  ]
  edge
  [
    source 443
    target 442
    measure 6
    diseases "Central_hypoventilation_syndrome,Hirschsprung_disease"
    GOmeasure 6
    id 1009
    name "1009"
  ]
  edge
  [
    source 664
    target 441
    measure 7
    diseases "Hirschsprung_disease"
    GOmeasure 7
    id 1010
    name "1010"
  ]
  edge
  [
    source 663
    target 441
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 1011
    name "1011"
  ]
  edge
  [
    source 443
    target 441
    measure 5
    diseases "Central_hypoventilation_syndrome,Hirschsprung_disease"
    GOmeasure 5
    id 1012
    name "1012"
  ]
  edge
  [
    source 442
    target 441
    measure 7
    diseases "Central_hypoventilation_syndrome,Hirschsprung_disease"
    GOmeasure 7
    id 1013
    name "1013"
  ]
  edge
  [
    source 664
    target 440
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 1014
    name "1014"
  ]
  edge
  [
    source 663
    target 440
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 1015
    name "1015"
  ]
  edge
  [
    source 443
    target 440
    measure 6
    diseases "Central_hypoventilation_syndrome,Hirschsprung_disease"
    GOmeasure 6
    id 1016
    name "1016"
  ]
  edge
  [
    source 442
    target 440
    measure 6
    diseases "Central_hypoventilation_syndrome,Hirschsprung_disease"
    GOmeasure 6
    id 1017
    name "1017"
  ]
  edge
  [
    source 441
    target 440
    measure 5
    diseases "Central_hypoventilation_syndrome,Hirschsprung_disease"
    GOmeasure 5
    id 1018
    name "1018"
  ]
  edge
  [
    source 444
    target 439
    measure 5
    diseases "Obsessive-compulsive_disorder"
    GOmeasure 5
    id 1019
    name "1019"
  ]
  edge
  [
    source 443
    target 439
    measure 6
    diseases "Central_hypoventilation_syndrome"
    GOmeasure 6
    id 1020
    name "1020"
  ]
  edge
  [
    source 442
    target 439
    measure 7
    diseases "Central_hypoventilation_syndrome"
    GOmeasure 7
    id 1021
    name "1021"
  ]
  edge
  [
    source 441
    target 439
    measure 7
    diseases "Central_hypoventilation_syndrome"
    GOmeasure 7
    id 1022
    name "1022"
  ]
  edge
  [
    source 440
    target 439
    measure 7
    diseases "Central_hypoventilation_syndrome"
    GOmeasure 7
    id 1023
    name "1023"
  ]
  edge
  [
    source 438
    target 437
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1024
    name "1024"
  ]
  edge
  [
    source 438
    target 436
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1025
    name "1025"
  ]
  edge
  [
    source 437
    target 436
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1026
    name "1026"
  ]
  edge
  [
    source 783
    target 435
    measure 6
    diseases "Pyruvate_dehydrogenase_deficiency"
    GOmeasure 6
    id 1027
    name "1027"
  ]
  edge
  [
    source 782
    target 435
    measure 4
    diseases "Pyruvate_dehydrogenase_deficiency"
    GOmeasure 4
    id 1028
    name "1028"
  ]
  edge
  [
    source 435
    target 434
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1029
    name "1029"
  ]
  edge
  [
    source 435
    target 433
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1030
    name "1030"
  ]
  edge
  [
    source 434
    target 433
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1031
    name "1031"
  ]
  edge
  [
    source 438
    target 432
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1032
    name "1032"
  ]
  edge
  [
    source 437
    target 432
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1033
    name "1033"
  ]
  edge
  [
    source 436
    target 432
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1034
    name "1034"
  ]
  edge
  [
    source 435
    target 432
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1035
    name "1035"
  ]
  edge
  [
    source 434
    target 432
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1036
    name "1036"
  ]
  edge
  [
    source 433
    target 432
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1037
    name "1037"
  ]
  edge
  [
    source 708
    target 431
    measure 5
    diseases "Alexander_disease"
    GOmeasure 5
    id 1038
    name "1038"
  ]
  edge
  [
    source 438
    target 431
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1039
    name "1039"
  ]
  edge
  [
    source 437
    target 431
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1040
    name "1040"
  ]
  edge
  [
    source 436
    target 431
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1041
    name "1041"
  ]
  edge
  [
    source 435
    target 431
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1042
    name "1042"
  ]
  edge
  [
    source 434
    target 431
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1043
    name "1043"
  ]
  edge
  [
    source 433
    target 431
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1044
    name "1044"
  ]
  edge
  [
    source 432
    target 431
    measure 6
    diseases "Leigh_syndrome,Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1045
    name "1045"
  ]
  edge
  [
    source 435
    target 430
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1046
    name "1046"
  ]
  edge
  [
    source 434
    target 430
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1047
    name "1047"
  ]
  edge
  [
    source 433
    target 430
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1048
    name "1048"
  ]
  edge
  [
    source 432
    target 430
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1049
    name "1049"
  ]
  edge
  [
    source 431
    target 430
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1050
    name "1050"
  ]
  edge
  [
    source 435
    target 429
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1051
    name "1051"
  ]
  edge
  [
    source 434
    target 429
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1052
    name "1052"
  ]
  edge
  [
    source 433
    target 429
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1053
    name "1053"
  ]
  edge
  [
    source 432
    target 429
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1054
    name "1054"
  ]
  edge
  [
    source 431
    target 429
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1055
    name "1055"
  ]
  edge
  [
    source 430
    target 429
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1056
    name "1056"
  ]
  edge
  [
    source 438
    target 428
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1057
    name "1057"
  ]
  edge
  [
    source 437
    target 428
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1058
    name "1058"
  ]
  edge
  [
    source 436
    target 428
    measure 5
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1059
    name "1059"
  ]
  edge
  [
    source 435
    target 428
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1060
    name "1060"
  ]
  edge
  [
    source 434
    target 428
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1061
    name "1061"
  ]
  edge
  [
    source 433
    target 428
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1062
    name "1062"
  ]
  edge
  [
    source 432
    target 428
    measure 5
    diseases "Leigh_syndrome,Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1063
    name "1063"
  ]
  edge
  [
    source 431
    target 428
    measure 4
    diseases "Leigh_syndrome,Mitochondrial_complex_deficiency"
    GOmeasure 4
    id 1064
    name "1064"
  ]
  edge
  [
    source 430
    target 428
    measure 7
    diseases "Leigh_syndrome"
    GOmeasure 7
    id 1065
    name "1065"
  ]
  edge
  [
    source 429
    target 428
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1066
    name "1066"
  ]
  edge
  [
    source 435
    target 427
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1067
    name "1067"
  ]
  edge
  [
    source 434
    target 427
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1068
    name "1068"
  ]
  edge
  [
    source 433
    target 427
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1069
    name "1069"
  ]
  edge
  [
    source 432
    target 427
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1070
    name "1070"
  ]
  edge
  [
    source 431
    target 427
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1071
    name "1071"
  ]
  edge
  [
    source 430
    target 427
    measure 7
    diseases "Leigh_syndrome"
    GOmeasure 7
    id 1072
    name "1072"
  ]
  edge
  [
    source 429
    target 427
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 1073
    name "1073"
  ]
  edge
  [
    source 428
    target 427
    measure 9
    diseases "Leigh_syndrome"
    GOmeasure 9
    id 1074
    name "1074"
  ]
  edge
  [
    source 438
    target 426
    measure 8
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 8
    id 1075
    name "1075"
  ]
  edge
  [
    source 437
    target 426
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1076
    name "1076"
  ]
  edge
  [
    source 436
    target 426
    measure 6
    diseases "Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1077
    name "1077"
  ]
  edge
  [
    source 435
    target 426
    measure 4
    diseases "Leigh_syndrome"
    GOmeasure 4
    id 1078
    name "1078"
  ]
  edge
  [
    source 434
    target 426
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1079
    name "1079"
  ]
  edge
  [
    source 433
    target 426
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1080
    name "1080"
  ]
  edge
  [
    source 432
    target 426
    measure 5
    diseases "Leigh_syndrome,Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1081
    name "1081"
  ]
  edge
  [
    source 431
    target 426
    measure 5
    diseases "Leigh_syndrome,Mitochondrial_complex_deficiency"
    GOmeasure 5
    id 1082
    name "1082"
  ]
  edge
  [
    source 430
    target 426
    measure 7
    diseases "Leigh_syndrome"
    GOmeasure 7
    id 1083
    name "1083"
  ]
  edge
  [
    source 429
    target 426
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1084
    name "1084"
  ]
  edge
  [
    source 428
    target 426
    measure 6
    diseases "Leigh_syndrome,Mitochondrial_complex_deficiency"
    GOmeasure 6
    id 1085
    name "1085"
  ]
  edge
  [
    source 427
    target 426
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 1086
    name "1086"
  ]
  edge
  [
    source 797
    target 425
    measure 6
    diseases "Pheochromocytoma"
    GOmeasure 6
    id 1087
    name "1087"
  ]
  edge
  [
    source 796
    target 425
    measure 6
    diseases "Pheochromocytoma"
    GOmeasure 6
    id 1088
    name "1088"
  ]
  edge
  [
    source 752
    target 425
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 1089
    name "1089"
  ]
  edge
  [
    source 751
    target 425
    measure 4
    diseases "Renal_cell_carcinoma"
    GOmeasure 4
    id 1090
    name "1090"
  ]
  edge
  [
    source 750
    target 425
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 1091
    name "1091"
  ]
  edge
  [
    source 749
    target 425
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 1092
    name "1092"
  ]
  edge
  [
    source 735
    target 425
    measure 6
    diseases "Polycythemia"
    GOmeasure 6
    id 1093
    name "1093"
  ]
  edge
  [
    source 527
    target 425
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 1094
    name "1094"
  ]
  edge
  [
    source 424
    target 423
    measure 5
    diseases "Maple_syrup_urine_disease"
    GOmeasure 5
    id 1095
    name "1095"
  ]
  edge
  [
    source 424
    target 422
    measure 5
    diseases "Maple_syrup_urine_disease"
    GOmeasure 5
    id 1096
    name "1096"
  ]
  edge
  [
    source 423
    target 422
    measure 7
    diseases "Maple_syrup_urine_disease"
    GOmeasure 7
    id 1097
    name "1097"
  ]
  edge
  [
    source 421
    target 420
    measure 5
    diseases "Hypercholanemia"
    GOmeasure 5
    id 1098
    name "1098"
  ]
  edge
  [
    source 717
    target 419
    measure 6
    diseases "Macular_degeneration"
    GOmeasure 6
    id 1099
    name "1099"
  ]
  edge
  [
    source 685
    target 419
    measure 7
    diseases "Macular_degeneration"
    GOmeasure 7
    id 1100
    name "1100"
  ]
  edge
  [
    source 419
    target 418
    measure 6
    diseases "Cutis_laxa"
    GOmeasure 6
    id 1101
    name "1101"
  ]
  edge
  [
    source 419
    target 417
    measure 6
    diseases "Cutis_laxa"
    GOmeasure 6
    id 1102
    name "1102"
  ]
  edge
  [
    source 418
    target 417
    measure 5
    diseases "Cutis_laxa"
    GOmeasure 5
    id 1103
    name "1103"
  ]
  edge
  [
    source 416
    target 415
    measure 6
    diseases "Renal_tubular_acidosis"
    GOmeasure 6
    id 1104
    name "1104"
  ]
  edge
  [
    source 416
    target 414
    measure 4
    diseases "Renal_tubular_acidosis"
    GOmeasure 4
    id 1105
    name "1105"
  ]
  edge
  [
    source 415
    target 414
    measure 5
    diseases "Renal_tubular_acidosis"
    GOmeasure 5
    id 1106
    name "1106"
  ]
  edge
  [
    source 416
    target 413
    measure 7
    diseases "Renal_tubular_acidosis"
    GOmeasure 7
    id 1107
    name "1107"
  ]
  edge
  [
    source 415
    target 413
    measure 6
    diseases "Renal_tubular_acidosis"
    GOmeasure 6
    id 1108
    name "1108"
  ]
  edge
  [
    source 414
    target 413
    measure 4
    diseases "Renal_tubular_acidosis"
    GOmeasure 4
    id 1109
    name "1109"
  ]
  edge
  [
    source 412
    target 411
    measure 5
    diseases "Migraine"
    GOmeasure 5
    id 1110
    name "1110"
  ]
  edge
  [
    source 410
    target 409
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1111
    name "1111"
  ]
  edge
  [
    source 410
    target 408
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1112
    name "1112"
  ]
  edge
  [
    source 409
    target 408
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1113
    name "1113"
  ]
  edge
  [
    source 738
    target 406
    measure 6
    diseases "Epidermolytic_hyperkeratosis"
    GOmeasure 6
    id 1114
    name "1114"
  ]
  edge
  [
    source 660
    target 406
    measure 6
    diseases "Keratosis_palmoplantaria_striata"
    GOmeasure 6
    id 1115
    name "1115"
  ]
  edge
  [
    source 588
    target 406
    measure 5
    diseases "Keratosis_palmoplantaria_striata"
    GOmeasure 5
    id 1116
    name "1116"
  ]
  edge
  [
    source 738
    target 405
    measure 6
    diseases "Epidermolytic_hyperkeratosis"
    GOmeasure 6
    id 1117
    name "1117"
  ]
  edge
  [
    source 406
    target 405
    measure 6
    diseases "Epidermolytic_hyperkeratosis,Ichthyosis"
    GOmeasure 6
    id 1118
    name "1118"
  ]
  edge
  [
    source 404
    target 403
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1119
    name "1119"
  ]
  edge
  [
    source 659
    target 402
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 1120
    name "1120"
  ]
  edge
  [
    source 658
    target 402
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 1121
    name "1121"
  ]
  edge
  [
    source 657
    target 402
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 1122
    name "1122"
  ]
  edge
  [
    source 656
    target 402
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 1123
    name "1123"
  ]
  edge
  [
    source 655
    target 402
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 1124
    name "1124"
  ]
  edge
  [
    source 654
    target 402
    measure 4
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 4
    id 1125
    name "1125"
  ]
  edge
  [
    source 653
    target 402
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 1126
    name "1126"
  ]
  edge
  [
    source 652
    target 402
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 1127
    name "1127"
  ]
  edge
  [
    source 651
    target 402
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 1128
    name "1128"
  ]
  edge
  [
    source 650
    target 402
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 1129
    name "1129"
  ]
  edge
  [
    source 649
    target 402
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 1130
    name "1130"
  ]
  edge
  [
    source 648
    target 402
    measure 6
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 6
    id 1131
    name "1131"
  ]
  edge
  [
    source 647
    target 402
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 1132
    name "1132"
  ]
  edge
  [
    source 646
    target 402
    measure 5
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 5
    id 1133
    name "1133"
  ]
  edge
  [
    source 645
    target 402
    measure 7
    diseases "Charcot-Marie-Tooth_disease"
    GOmeasure 7
    id 1134
    name "1134"
  ]
  edge
  [
    source 404
    target 402
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1135
    name "1135"
  ]
  edge
  [
    source 403
    target 402
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1136
    name "1136"
  ]
  edge
  [
    source 740
    target 401
    measure 4
    diseases "Lipodystrophy"
    GOmeasure 4
    id 1137
    name "1137"
  ]
  edge
  [
    source 739
    target 401
    measure 5
    diseases "Lipodystrophy"
    GOmeasure 5
    id 1138
    name "1138"
  ]
  edge
  [
    source 404
    target 401
    measure 4
    diseases "Spinal_muscular_atrophy"
    GOmeasure 4
    id 1139
    name "1139"
  ]
  edge
  [
    source 403
    target 401
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1140
    name "1140"
  ]
  edge
  [
    source 402
    target 401
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1141
    name "1141"
  ]
  edge
  [
    source 404
    target 400
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1142
    name "1142"
  ]
  edge
  [
    source 403
    target 400
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1143
    name "1143"
  ]
  edge
  [
    source 402
    target 400
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1144
    name "1144"
  ]
  edge
  [
    source 401
    target 400
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1145
    name "1145"
  ]
  edge
  [
    source 640
    target 399
    measure 4
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 4
    id 1146
    name "1146"
  ]
  edge
  [
    source 639
    target 399
    measure 6
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 6
    id 1147
    name "1147"
  ]
  edge
  [
    source 638
    target 399
    measure 4
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 4
    id 1148
    name "1148"
  ]
  edge
  [
    source 637
    target 399
    measure 6
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 6
    id 1149
    name "1149"
  ]
  edge
  [
    source 636
    target 399
    measure 4
    diseases "Amyotrophic_lateral_sclerosis"
    GOmeasure 4
    id 1150
    name "1150"
  ]
  edge
  [
    source 404
    target 399
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1151
    name "1151"
  ]
  edge
  [
    source 403
    target 399
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1152
    name "1152"
  ]
  edge
  [
    source 402
    target 399
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1153
    name "1153"
  ]
  edge
  [
    source 401
    target 399
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1154
    name "1154"
  ]
  edge
  [
    source 400
    target 399
    measure 6
    diseases "Spinal_muscular_atrophy"
    GOmeasure 6
    id 1155
    name "1155"
  ]
  edge
  [
    source 398
    target 397
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1156
    name "1156"
  ]
  edge
  [
    source 410
    target 396
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1157
    name "1157"
  ]
  edge
  [
    source 409
    target 396
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1158
    name "1158"
  ]
  edge
  [
    source 408
    target 396
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1159
    name "1159"
  ]
  edge
  [
    source 398
    target 396
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1160
    name "1160"
  ]
  edge
  [
    source 397
    target 396
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1161
    name "1161"
  ]
  edge
  [
    source 398
    target 395
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 1162
    name "1162"
  ]
  edge
  [
    source 397
    target 395
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1163
    name "1163"
  ]
  edge
  [
    source 396
    target 395
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1164
    name "1164"
  ]
  edge
  [
    source 398
    target 394
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1165
    name "1165"
  ]
  edge
  [
    source 397
    target 394
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1166
    name "1166"
  ]
  edge
  [
    source 396
    target 394
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1167
    name "1167"
  ]
  edge
  [
    source 395
    target 394
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1168
    name "1168"
  ]
  edge
  [
    source 767
    target 393
    measure 5
    diseases "Meningioma"
    GOmeasure 5
    id 1169
    name "1169"
  ]
  edge
  [
    source 766
    target 393
    measure 4
    diseases "Meningioma"
    GOmeasure 4
    id 1170
    name "1170"
  ]
  edge
  [
    source 765
    target 393
    measure 5
    diseases "Meningioma"
    GOmeasure 5
    id 1171
    name "1171"
  ]
  edge
  [
    source 724
    target 393
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 1172
    name "1172"
  ]
  edge
  [
    source 723
    target 393
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 1173
    name "1173"
  ]
  edge
  [
    source 722
    target 393
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 1174
    name "1174"
  ]
  edge
  [
    source 721
    target 393
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 1175
    name "1175"
  ]
  edge
  [
    source 720
    target 393
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 1176
    name "1176"
  ]
  edge
  [
    source 719
    target 393
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 1177
    name "1177"
  ]
  edge
  [
    source 718
    target 393
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 1178
    name "1178"
  ]
  edge
  [
    source 697
    target 393
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 1179
    name "1179"
  ]
  edge
  [
    source 621
    target 393
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 1180
    name "1180"
  ]
  edge
  [
    source 554
    target 393
    measure 6
    diseases "Endometrial_carcinoma"
    GOmeasure 6
    id 1181
    name "1181"
  ]
  edge
  [
    source 445
    target 393
    measure 4
    diseases "Cowden_disease"
    GOmeasure 4
    id 1182
    name "1182"
  ]
  edge
  [
    source 398
    target 393
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1183
    name "1183"
  ]
  edge
  [
    source 397
    target 393
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1184
    name "1184"
  ]
  edge
  [
    source 396
    target 393
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1185
    name "1185"
  ]
  edge
  [
    source 395
    target 393
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1186
    name "1186"
  ]
  edge
  [
    source 394
    target 393
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1187
    name "1187"
  ]
  edge
  [
    source 398
    target 392
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1188
    name "1188"
  ]
  edge
  [
    source 397
    target 392
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1189
    name "1189"
  ]
  edge
  [
    source 396
    target 392
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1190
    name "1190"
  ]
  edge
  [
    source 395
    target 392
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1191
    name "1191"
  ]
  edge
  [
    source 394
    target 392
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1192
    name "1192"
  ]
  edge
  [
    source 393
    target 392
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1193
    name "1193"
  ]
  edge
  [
    source 449
    target 391
    measure 4
    diseases "Melanoma"
    GOmeasure 4
    id 1194
    name "1194"
  ]
  edge
  [
    source 391
    target 390
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1195
    name "1195"
  ]
  edge
  [
    source 391
    target 389
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1196
    name "1196"
  ]
  edge
  [
    source 390
    target 389
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1197
    name "1197"
  ]
  edge
  [
    source 410
    target 388
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1198
    name "1198"
  ]
  edge
  [
    source 409
    target 388
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1199
    name "1199"
  ]
  edge
  [
    source 408
    target 388
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1200
    name "1200"
  ]
  edge
  [
    source 407
    target 388
    measure 5
    diseases "Ataxia-telangiectasia"
    GOmeasure 5
    id 1201
    name "1201"
  ]
  edge
  [
    source 396
    target 388
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1202
    name "1202"
  ]
  edge
  [
    source 391
    target 388
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1203
    name "1203"
  ]
  edge
  [
    source 390
    target 388
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1204
    name "1204"
  ]
  edge
  [
    source 389
    target 388
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1205
    name "1205"
  ]
  edge
  [
    source 391
    target 387
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1206
    name "1206"
  ]
  edge
  [
    source 390
    target 387
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1207
    name "1207"
  ]
  edge
  [
    source 389
    target 387
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1208
    name "1208"
  ]
  edge
  [
    source 388
    target 387
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1209
    name "1209"
  ]
  edge
  [
    source 391
    target 386
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1210
    name "1210"
  ]
  edge
  [
    source 390
    target 386
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1211
    name "1211"
  ]
  edge
  [
    source 389
    target 386
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1212
    name "1212"
  ]
  edge
  [
    source 388
    target 386
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1213
    name "1213"
  ]
  edge
  [
    source 387
    target 386
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1214
    name "1214"
  ]
  edge
  [
    source 458
    target 385
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 1215
    name "1215"
  ]
  edge
  [
    source 457
    target 385
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 1216
    name "1216"
  ]
  edge
  [
    source 456
    target 385
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 1217
    name "1217"
  ]
  edge
  [
    source 455
    target 385
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 1218
    name "1218"
  ]
  edge
  [
    source 454
    target 385
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 1219
    name "1219"
  ]
  edge
  [
    source 453
    target 385
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 1220
    name "1220"
  ]
  edge
  [
    source 452
    target 385
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 1221
    name "1221"
  ]
  edge
  [
    source 451
    target 385
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 1222
    name "1222"
  ]
  edge
  [
    source 450
    target 385
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 1223
    name "1223"
  ]
  edge
  [
    source 391
    target 385
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1224
    name "1224"
  ]
  edge
  [
    source 390
    target 385
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1225
    name "1225"
  ]
  edge
  [
    source 389
    target 385
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1226
    name "1226"
  ]
  edge
  [
    source 388
    target 385
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1227
    name "1227"
  ]
  edge
  [
    source 387
    target 385
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1228
    name "1228"
  ]
  edge
  [
    source 386
    target 385
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1229
    name "1229"
  ]
  edge
  [
    source 391
    target 384
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1230
    name "1230"
  ]
  edge
  [
    source 390
    target 384
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1231
    name "1231"
  ]
  edge
  [
    source 389
    target 384
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1232
    name "1232"
  ]
  edge
  [
    source 388
    target 384
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1233
    name "1233"
  ]
  edge
  [
    source 387
    target 384
    measure 8
    diseases "Breast_cancer"
    GOmeasure 8
    id 1234
    name "1234"
  ]
  edge
  [
    source 386
    target 384
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1235
    name "1235"
  ]
  edge
  [
    source 385
    target 384
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1236
    name "1236"
  ]
  edge
  [
    source 446
    target 383
    measure 5
    diseases "Ovarian_cancer"
    GOmeasure 5
    id 1237
    name "1237"
  ]
  edge
  [
    source 391
    target 383
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1238
    name "1238"
  ]
  edge
  [
    source 390
    target 383
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1239
    name "1239"
  ]
  edge
  [
    source 389
    target 383
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1240
    name "1240"
  ]
  edge
  [
    source 388
    target 383
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1241
    name "1241"
  ]
  edge
  [
    source 387
    target 383
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1242
    name "1242"
  ]
  edge
  [
    source 386
    target 383
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1243
    name "1243"
  ]
  edge
  [
    source 385
    target 383
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1244
    name "1244"
  ]
  edge
  [
    source 384
    target 383
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1245
    name "1245"
  ]
  edge
  [
    source 703
    target 382
    measure 6
    diseases "Rhabdomyosarcoma"
    GOmeasure 6
    id 1246
    name "1246"
  ]
  edge
  [
    source 702
    target 382
    measure 5
    diseases "Rhabdomyosarcoma"
    GOmeasure 5
    id 1247
    name "1247"
  ]
  edge
  [
    source 597
    target 382
    measure 6
    diseases "Lung_cancer"
    GOmeasure 6
    id 1248
    name "1248"
  ]
  edge
  [
    source 596
    target 382
    measure 5
    diseases "Lung_cancer"
    GOmeasure 5
    id 1249
    name "1249"
  ]
  edge
  [
    source 391
    target 382
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1250
    name "1250"
  ]
  edge
  [
    source 390
    target 382
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1251
    name "1251"
  ]
  edge
  [
    source 389
    target 382
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1252
    name "1252"
  ]
  edge
  [
    source 388
    target 382
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1253
    name "1253"
  ]
  edge
  [
    source 387
    target 382
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1254
    name "1254"
  ]
  edge
  [
    source 386
    target 382
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1255
    name "1255"
  ]
  edge
  [
    source 385
    target 382
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1256
    name "1256"
  ]
  edge
  [
    source 384
    target 382
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1257
    name "1257"
  ]
  edge
  [
    source 383
    target 382
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1258
    name "1258"
  ]
  edge
  [
    source 391
    target 381
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1259
    name "1259"
  ]
  edge
  [
    source 390
    target 381
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1260
    name "1260"
  ]
  edge
  [
    source 389
    target 381
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1261
    name "1261"
  ]
  edge
  [
    source 388
    target 381
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1262
    name "1262"
  ]
  edge
  [
    source 387
    target 381
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1263
    name "1263"
  ]
  edge
  [
    source 386
    target 381
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1264
    name "1264"
  ]
  edge
  [
    source 385
    target 381
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1265
    name "1265"
  ]
  edge
  [
    source 384
    target 381
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1266
    name "1266"
  ]
  edge
  [
    source 383
    target 381
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1267
    name "1267"
  ]
  edge
  [
    source 382
    target 381
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1268
    name "1268"
  ]
  edge
  [
    source 398
    target 380
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1269
    name "1269"
  ]
  edge
  [
    source 397
    target 380
    measure 8
    diseases "Prostate_cancer"
    GOmeasure 8
    id 1270
    name "1270"
  ]
  edge
  [
    source 396
    target 380
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1271
    name "1271"
  ]
  edge
  [
    source 395
    target 380
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1272
    name "1272"
  ]
  edge
  [
    source 394
    target 380
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1273
    name "1273"
  ]
  edge
  [
    source 393
    target 380
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1274
    name "1274"
  ]
  edge
  [
    source 392
    target 380
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1275
    name "1275"
  ]
  edge
  [
    source 391
    target 380
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1276
    name "1276"
  ]
  edge
  [
    source 390
    target 380
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1277
    name "1277"
  ]
  edge
  [
    source 389
    target 380
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1278
    name "1278"
  ]
  edge
  [
    source 388
    target 380
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1279
    name "1279"
  ]
  edge
  [
    source 387
    target 380
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1280
    name "1280"
  ]
  edge
  [
    source 386
    target 380
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1281
    name "1281"
  ]
  edge
  [
    source 385
    target 380
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1282
    name "1282"
  ]
  edge
  [
    source 384
    target 380
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1283
    name "1283"
  ]
  edge
  [
    source 383
    target 380
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1284
    name "1284"
  ]
  edge
  [
    source 382
    target 380
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1285
    name "1285"
  ]
  edge
  [
    source 381
    target 380
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1286
    name "1286"
  ]
  edge
  [
    source 404
    target 379
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1287
    name "1287"
  ]
  edge
  [
    source 403
    target 379
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1288
    name "1288"
  ]
  edge
  [
    source 402
    target 379
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1289
    name "1289"
  ]
  edge
  [
    source 401
    target 379
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1290
    name "1290"
  ]
  edge
  [
    source 400
    target 379
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1291
    name "1291"
  ]
  edge
  [
    source 399
    target 379
    measure 5
    diseases "Spinal_muscular_atrophy"
    GOmeasure 5
    id 1292
    name "1292"
  ]
  edge
  [
    source 398
    target 379
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1293
    name "1293"
  ]
  edge
  [
    source 397
    target 379
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 1294
    name "1294"
  ]
  edge
  [
    source 396
    target 379
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 1295
    name "1295"
  ]
  edge
  [
    source 395
    target 379
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 1296
    name "1296"
  ]
  edge
  [
    source 394
    target 379
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1297
    name "1297"
  ]
  edge
  [
    source 393
    target 379
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 1298
    name "1298"
  ]
  edge
  [
    source 392
    target 379
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 1299
    name "1299"
  ]
  edge
  [
    source 391
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1300
    name "1300"
  ]
  edge
  [
    source 390
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1301
    name "1301"
  ]
  edge
  [
    source 389
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1302
    name "1302"
  ]
  edge
  [
    source 388
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1303
    name "1303"
  ]
  edge
  [
    source 387
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1304
    name "1304"
  ]
  edge
  [
    source 386
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1305
    name "1305"
  ]
  edge
  [
    source 385
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1306
    name "1306"
  ]
  edge
  [
    source 384
    target 379
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1307
    name "1307"
  ]
  edge
  [
    source 383
    target 379
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1308
    name "1308"
  ]
  edge
  [
    source 382
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1309
    name "1309"
  ]
  edge
  [
    source 381
    target 379
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1310
    name "1310"
  ]
  edge
  [
    source 380
    target 379
    measure 4
    diseases "Breast_cancer,Prostate_cancer"
    GOmeasure 4
    id 1311
    name "1311"
  ]
  edge
  [
    source 378
    target 377
    measure 5
    diseases "Systemic_lupus_erythematosus"
    GOmeasure 5
    id 1312
    name "1312"
  ]
  edge
  [
    source 378
    target 376
    measure 4
    diseases "Systemic_lupus_erythematosus"
    GOmeasure 4
    id 1313
    name "1313"
  ]
  edge
  [
    source 377
    target 376
    measure 5
    diseases "Systemic_lupus_erythematosus"
    GOmeasure 5
    id 1314
    name "1314"
  ]
  edge
  [
    source 375
    target 374
    measure 5
    diseases "Squamous_cell_carcinoma"
    GOmeasure 5
    id 1315
    name "1315"
  ]
  edge
  [
    source 514
    target 373
    measure 7
    diseases "Osteoporosis"
    GOmeasure 7
    id 1316
    name "1316"
  ]
  edge
  [
    source 513
    target 373
    measure 5
    diseases "Osteoporosis"
    GOmeasure 5
    id 1317
    name "1317"
  ]
  edge
  [
    source 512
    target 373
    measure 5
    diseases "Osteoporosis"
    GOmeasure 5
    id 1318
    name "1318"
  ]
  edge
  [
    source 511
    target 373
    measure 5
    diseases "Osteoporosis"
    GOmeasure 5
    id 1319
    name "1319"
  ]
  edge
  [
    source 510
    target 373
    measure 7
    diseases "Osteoporosis"
    GOmeasure 7
    id 1320
    name "1320"
  ]
  edge
  [
    source 373
    target 372
    measure 5
    diseases "Urolithiasise"
    GOmeasure 5
    id 1321
    name "1321"
  ]
  edge
  [
    source 786
    target 371
    measure 5
    diseases "Hyperprolinemia"
    GOmeasure 5
    id 1322
    name "1322"
  ]
  edge
  [
    source 371
    target 370
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1323
    name "1323"
  ]
  edge
  [
    source 371
    target 369
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1324
    name "1324"
  ]
  edge
  [
    source 370
    target 369
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1325
    name "1325"
  ]
  edge
  [
    source 444
    target 368
    measure 6
    diseases "Obsessive-compulsive_disorder"
    GOmeasure 6
    id 1326
    name "1326"
  ]
  edge
  [
    source 439
    target 368
    measure 7
    diseases "Obsessive-compulsive_disorder"
    GOmeasure 7
    id 1327
    name "1327"
  ]
  edge
  [
    source 371
    target 368
    measure 6
    diseases "Schizophrenia"
    GOmeasure 6
    id 1328
    name "1328"
  ]
  edge
  [
    source 370
    target 368
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1329
    name "1329"
  ]
  edge
  [
    source 369
    target 368
    measure 6
    diseases "Schizophrenia"
    GOmeasure 6
    id 1330
    name "1330"
  ]
  edge
  [
    source 371
    target 367
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1331
    name "1331"
  ]
  edge
  [
    source 370
    target 367
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1332
    name "1332"
  ]
  edge
  [
    source 369
    target 367
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1333
    name "1333"
  ]
  edge
  [
    source 368
    target 367
    measure 6
    diseases "Schizophrenia"
    GOmeasure 6
    id 1334
    name "1334"
  ]
  edge
  [
    source 371
    target 366
    measure 4
    diseases "Schizophrenia"
    GOmeasure 4
    id 1335
    name "1335"
  ]
  edge
  [
    source 370
    target 366
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1336
    name "1336"
  ]
  edge
  [
    source 369
    target 366
    measure 6
    diseases "Schizophrenia"
    GOmeasure 6
    id 1337
    name "1337"
  ]
  edge
  [
    source 368
    target 366
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 1338
    name "1338"
  ]
  edge
  [
    source 367
    target 366
    measure 4
    diseases "Schizophrenia"
    GOmeasure 4
    id 1339
    name "1339"
  ]
  edge
  [
    source 364
    target 363
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1340
    name "1340"
  ]
  edge
  [
    source 364
    target 362
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1341
    name "1341"
  ]
  edge
  [
    source 363
    target 362
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1342
    name "1342"
  ]
  edge
  [
    source 364
    target 361
    measure 6
    diseases "Hypercholesterolemia"
    GOmeasure 6
    id 1343
    name "1343"
  ]
  edge
  [
    source 363
    target 361
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1344
    name "1344"
  ]
  edge
  [
    source 362
    target 361
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1345
    name "1345"
  ]
  edge
  [
    source 364
    target 360
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1346
    name "1346"
  ]
  edge
  [
    source 363
    target 360
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1347
    name "1347"
  ]
  edge
  [
    source 362
    target 360
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1348
    name "1348"
  ]
  edge
  [
    source 361
    target 360
    measure 4
    diseases "Hypercholesterolemia"
    GOmeasure 4
    id 1349
    name "1349"
  ]
  edge
  [
    source 364
    target 359
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1350
    name "1350"
  ]
  edge
  [
    source 363
    target 359
    measure 6
    diseases "Hypercholesterolemia"
    GOmeasure 6
    id 1351
    name "1351"
  ]
  edge
  [
    source 362
    target 359
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1352
    name "1352"
  ]
  edge
  [
    source 361
    target 359
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1353
    name "1353"
  ]
  edge
  [
    source 360
    target 359
    measure 4
    diseases "Hypercholesterolemia"
    GOmeasure 4
    id 1354
    name "1354"
  ]
  edge
  [
    source 357
    target 356
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1355
    name "1355"
  ]
  edge
  [
    source 357
    target 355
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1356
    name "1356"
  ]
  edge
  [
    source 356
    target 355
    measure 4
    diseases "Corneal_dystrophy"
    GOmeasure 4
    id 1357
    name "1357"
  ]
  edge
  [
    source 357
    target 354
    measure 6
    diseases "Corneal_dystrophy"
    GOmeasure 6
    id 1358
    name "1358"
  ]
  edge
  [
    source 356
    target 354
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1359
    name "1359"
  ]
  edge
  [
    source 355
    target 354
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1360
    name "1360"
  ]
  edge
  [
    source 357
    target 353
    measure 4
    diseases "Corneal_dystrophy"
    GOmeasure 4
    id 1361
    name "1361"
  ]
  edge
  [
    source 356
    target 353
    measure 6
    diseases "Corneal_dystrophy"
    GOmeasure 6
    id 1362
    name "1362"
  ]
  edge
  [
    source 355
    target 353
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1363
    name "1363"
  ]
  edge
  [
    source 354
    target 353
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1364
    name "1364"
  ]
  edge
  [
    source 352
    target 351
    measure 6
    diseases "Apolipoprotein_deficiency"
    GOmeasure 6
    id 1365
    name "1365"
  ]
  edge
  [
    source 364
    target 350
    measure 4
    diseases "Hypercholesterolemia"
    GOmeasure 4
    id 1366
    name "1366"
  ]
  edge
  [
    source 363
    target 350
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1367
    name "1367"
  ]
  edge
  [
    source 362
    target 350
    measure 6
    diseases "Hypercholesterolemia"
    GOmeasure 6
    id 1368
    name "1368"
  ]
  edge
  [
    source 361
    target 350
    measure 4
    diseases "Hypercholesterolemia"
    GOmeasure 4
    id 1369
    name "1369"
  ]
  edge
  [
    source 360
    target 350
    measure 6
    diseases "Hypercholesterolemia"
    GOmeasure 6
    id 1370
    name "1370"
  ]
  edge
  [
    source 359
    target 350
    measure 5
    diseases "Hypercholesterolemia"
    GOmeasure 5
    id 1371
    name "1371"
  ]
  edge
  [
    source 352
    target 350
    measure 6
    diseases "Apolipoprotein_deficiency"
    GOmeasure 6
    id 1372
    name "1372"
  ]
  edge
  [
    source 351
    target 350
    measure 5
    diseases "Apolipoprotein_deficiency"
    GOmeasure 5
    id 1373
    name "1373"
  ]
  edge
  [
    source 349
    target 348
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 1374
    name "1374"
  ]
  edge
  [
    source 552
    target 347
    measure 8
    diseases "Dysfibrinogenemia"
    GOmeasure 8
    id 1375
    name "1375"
  ]
  edge
  [
    source 551
    target 347
    measure 8
    diseases "Afibrinogenemia,Dysfibrinogenemia"
    GOmeasure 8
    id 1376
    name "1376"
  ]
  edge
  [
    source 349
    target 347
    measure 7
    diseases "Amyloidosis"
    GOmeasure 7
    id 1377
    name "1377"
  ]
  edge
  [
    source 348
    target 347
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 1378
    name "1378"
  ]
  edge
  [
    source 349
    target 346
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 1379
    name "1379"
  ]
  edge
  [
    source 348
    target 346
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 1380
    name "1380"
  ]
  edge
  [
    source 347
    target 346
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 1381
    name "1381"
  ]
  edge
  [
    source 358
    target 345
    measure 7
    diseases "Hypertriglyceridemia"
    GOmeasure 7
    id 1382
    name "1382"
  ]
  edge
  [
    source 357
    target 345
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1383
    name "1383"
  ]
  edge
  [
    source 356
    target 345
    measure 6
    diseases "Corneal_dystrophy"
    GOmeasure 6
    id 1384
    name "1384"
  ]
  edge
  [
    source 355
    target 345
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1385
    name "1385"
  ]
  edge
  [
    source 354
    target 345
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1386
    name "1386"
  ]
  edge
  [
    source 353
    target 345
    measure 5
    diseases "Corneal_dystrophy"
    GOmeasure 5
    id 1387
    name "1387"
  ]
  edge
  [
    source 352
    target 345
    measure 6
    diseases "Apolipoprotein_deficiency"
    GOmeasure 6
    id 1388
    name "1388"
  ]
  edge
  [
    source 351
    target 345
    measure 6
    diseases "Apolipoprotein_deficiency"
    GOmeasure 6
    id 1389
    name "1389"
  ]
  edge
  [
    source 350
    target 345
    measure 6
    diseases "Apolipoprotein_deficiency"
    GOmeasure 6
    id 1390
    name "1390"
  ]
  edge
  [
    source 349
    target 345
    measure 5
    diseases "Amyloidosis"
    GOmeasure 5
    id 1391
    name "1391"
  ]
  edge
  [
    source 348
    target 345
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 1392
    name "1392"
  ]
  edge
  [
    source 347
    target 345
    measure 5
    diseases "Amyloidosis"
    GOmeasure 5
    id 1393
    name "1393"
  ]
  edge
  [
    source 346
    target 345
    measure 7
    diseases "Amyloidosis"
    GOmeasure 7
    id 1394
    name "1394"
  ]
  edge
  [
    source 527
    target 344
    measure 6
    diseases "Hepatic_adenoma"
    GOmeasure 6
    id 1395
    name "1395"
  ]
  edge
  [
    source 526
    target 344
    measure 5
    diseases "Hepatic_adenoma"
    GOmeasure 5
    id 1396
    name "1396"
  ]
  edge
  [
    source 525
    target 344
    measure 4
    diseases "Hepatic_adenoma"
    GOmeasure 4
    id 1397
    name "1397"
  ]
  edge
  [
    source 375
    target 343
    measure 5
    diseases "Squamous_cell_carcinoma"
    GOmeasure 5
    id 1398
    name "1398"
  ]
  edge
  [
    source 374
    target 343
    measure 5
    diseases "Squamous_cell_carcinoma"
    GOmeasure 5
    id 1399
    name "1399"
  ]
  edge
  [
    source 344
    target 343
    measure 5
    diseases "Autoimmune_disease"
    GOmeasure 5
    id 1400
    name "1400"
  ]
  edge
  [
    source 344
    target 342
    measure 6
    diseases "Autoimmune_disease"
    GOmeasure 6
    id 1401
    name "1401"
  ]
  edge
  [
    source 343
    target 342
    measure 7
    diseases "Autoimmune_disease"
    GOmeasure 7
    id 1402
    name "1402"
  ]
  edge
  [
    source 398
    target 341
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 1403
    name "1403"
  ]
  edge
  [
    source 397
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1404
    name "1404"
  ]
  edge
  [
    source 396
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1405
    name "1405"
  ]
  edge
  [
    source 395
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1406
    name "1406"
  ]
  edge
  [
    source 394
    target 341
    measure 7
    diseases "Prostate_cancer"
    GOmeasure 7
    id 1407
    name "1407"
  ]
  edge
  [
    source 393
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1408
    name "1408"
  ]
  edge
  [
    source 392
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1409
    name "1409"
  ]
  edge
  [
    source 380
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1410
    name "1410"
  ]
  edge
  [
    source 379
    target 341
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 1411
    name "1411"
  ]
  edge
  [
    source 699
    target 340
    measure 5
    diseases "Saethre-Chotzen_syndrome"
    GOmeasure 5
    id 1412
    name "1412"
  ]
  edge
  [
    source 698
    target 340
    measure 5
    diseases "Craniosynostosis"
    GOmeasure 5
    id 1413
    name "1413"
  ]
  edge
  [
    source 695
    target 340
    measure 4
    diseases "Jackson-Weiss_syndrome,Pfeiffer_syndrome"
    GOmeasure 4
    id 1414
    name "1414"
  ]
  edge
  [
    source 341
    target 340
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1415
    name "1415"
  ]
  edge
  [
    source 644
    target 339
    measure 5
    diseases "Glioblastoma"
    GOmeasure 5
    id 1416
    name "1416"
  ]
  edge
  [
    source 507
    target 339
    measure 5
    diseases "Glioblastoma"
    GOmeasure 5
    id 1417
    name "1417"
  ]
  edge
  [
    source 448
    target 339
    measure 4
    diseases "Adenocarcinoma"
    GOmeasure 4
    id 1418
    name "1418"
  ]
  edge
  [
    source 447
    target 339
    measure 5
    diseases "Adenocarcinoma"
    GOmeasure 5
    id 1419
    name "1419"
  ]
  edge
  [
    source 446
    target 339
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1420
    name "1420"
  ]
  edge
  [
    source 383
    target 339
    measure 4
    diseases "Ovarian_cancer"
    GOmeasure 4
    id 1421
    name "1421"
  ]
  edge
  [
    source 341
    target 339
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1422
    name "1422"
  ]
  edge
  [
    source 340
    target 339
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1423
    name "1423"
  ]
  edge
  [
    source 344
    target 338
    measure 7
    diseases "Autoimmune_disease"
    GOmeasure 7
    id 1424
    name "1424"
  ]
  edge
  [
    source 343
    target 338
    measure 5
    diseases "Autoimmune_disease"
    GOmeasure 5
    id 1425
    name "1425"
  ]
  edge
  [
    source 342
    target 338
    measure 6
    diseases "Autoimmune_disease"
    GOmeasure 6
    id 1426
    name "1426"
  ]
  edge
  [
    source 341
    target 338
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1427
    name "1427"
  ]
  edge
  [
    source 340
    target 338
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1428
    name "1428"
  ]
  edge
  [
    source 339
    target 338
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1429
    name "1429"
  ]
  edge
  [
    source 341
    target 337
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1430
    name "1430"
  ]
  edge
  [
    source 340
    target 337
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1431
    name "1431"
  ]
  edge
  [
    source 339
    target 337
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1432
    name "1432"
  ]
  edge
  [
    source 338
    target 337
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1433
    name "1433"
  ]
  edge
  [
    source 341
    target 336
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1434
    name "1434"
  ]
  edge
  [
    source 340
    target 336
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1435
    name "1435"
  ]
  edge
  [
    source 339
    target 336
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1436
    name "1436"
  ]
  edge
  [
    source 338
    target 336
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1437
    name "1437"
  ]
  edge
  [
    source 337
    target 336
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1438
    name "1438"
  ]
  edge
  [
    source 554
    target 335
    measure 6
    diseases "Endometrial_carcinoma"
    GOmeasure 6
    id 1439
    name "1439"
  ]
  edge
  [
    source 446
    target 335
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1440
    name "1440"
  ]
  edge
  [
    source 393
    target 335
    measure 5
    diseases "Endometrial_carcinoma"
    GOmeasure 5
    id 1441
    name "1441"
  ]
  edge
  [
    source 391
    target 335
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1442
    name "1442"
  ]
  edge
  [
    source 390
    target 335
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1443
    name "1443"
  ]
  edge
  [
    source 389
    target 335
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1444
    name "1444"
  ]
  edge
  [
    source 388
    target 335
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1445
    name "1445"
  ]
  edge
  [
    source 387
    target 335
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1446
    name "1446"
  ]
  edge
  [
    source 386
    target 335
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1447
    name "1447"
  ]
  edge
  [
    source 385
    target 335
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1448
    name "1448"
  ]
  edge
  [
    source 384
    target 335
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1449
    name "1449"
  ]
  edge
  [
    source 383
    target 335
    measure 5
    diseases "Breast_cancer,Ovarian_cancer"
    GOmeasure 5
    id 1450
    name "1450"
  ]
  edge
  [
    source 382
    target 335
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1451
    name "1451"
  ]
  edge
  [
    source 381
    target 335
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1452
    name "1452"
  ]
  edge
  [
    source 380
    target 335
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1453
    name "1453"
  ]
  edge
  [
    source 379
    target 335
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1454
    name "1454"
  ]
  edge
  [
    source 341
    target 335
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1455
    name "1455"
  ]
  edge
  [
    source 340
    target 335
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1456
    name "1456"
  ]
  edge
  [
    source 339
    target 335
    measure 4
    diseases "Gastric_cancer,Ovarian_cancer"
    GOmeasure 4
    id 1457
    name "1457"
  ]
  edge
  [
    source 338
    target 335
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1458
    name "1458"
  ]
  edge
  [
    source 337
    target 335
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1459
    name "1459"
  ]
  edge
  [
    source 336
    target 335
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1460
    name "1460"
  ]
  edge
  [
    source 544
    target 334
    measure 6
    diseases "Myelogenous_leukemia"
    GOmeasure 6
    id 1461
    name "1461"
  ]
  edge
  [
    source 543
    target 334
    measure 5
    diseases "Myelodysplastic_syndrome,Myelogenous_leukemia"
    GOmeasure 5
    id 1462
    name "1462"
  ]
  edge
  [
    source 542
    target 334
    measure 6
    diseases "Myelogenous_leukemia"
    GOmeasure 6
    id 1463
    name "1463"
  ]
  edge
  [
    source 447
    target 334
    measure 6
    diseases "Nonsmall_cell_lung_cancer"
    GOmeasure 6
    id 1464
    name "1464"
  ]
  edge
  [
    source 341
    target 334
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1465
    name "1465"
  ]
  edge
  [
    source 340
    target 334
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1466
    name "1466"
  ]
  edge
  [
    source 339
    target 334
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1467
    name "1467"
  ]
  edge
  [
    source 338
    target 334
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1468
    name "1468"
  ]
  edge
  [
    source 337
    target 334
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1469
    name "1469"
  ]
  edge
  [
    source 336
    target 334
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1470
    name "1470"
  ]
  edge
  [
    source 335
    target 334
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1471
    name "1471"
  ]
  edge
  [
    source 333
    target 332
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1472
    name "1472"
  ]
  edge
  [
    source 333
    target 331
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1473
    name "1473"
  ]
  edge
  [
    source 332
    target 331
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1474
    name "1474"
  ]
  edge
  [
    source 449
    target 330
    measure 5
    diseases "Melanoma"
    GOmeasure 5
    id 1475
    name "1475"
  ]
  edge
  [
    source 448
    target 330
    measure 6
    diseases "Adenocarcinoma"
    GOmeasure 6
    id 1476
    name "1476"
  ]
  edge
  [
    source 447
    target 330
    measure 5
    diseases "Adenocarcinoma,Nonsmall_cell_lung_cancer"
    GOmeasure 5
    id 1477
    name "1477"
  ]
  edge
  [
    source 391
    target 330
    measure 5
    diseases "Melanoma"
    GOmeasure 5
    id 1478
    name "1478"
  ]
  edge
  [
    source 339
    target 330
    measure 5
    diseases "Adenocarcinoma"
    GOmeasure 5
    id 1479
    name "1479"
  ]
  edge
  [
    source 334
    target 330
    measure 5
    diseases "Nonsmall_cell_lung_cancer"
    GOmeasure 5
    id 1480
    name "1480"
  ]
  edge
  [
    source 333
    target 330
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1481
    name "1481"
  ]
  edge
  [
    source 332
    target 330
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1482
    name "1482"
  ]
  edge
  [
    source 331
    target 330
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1483
    name "1483"
  ]
  edge
  [
    source 752
    target 329
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 1484
    name "1484"
  ]
  edge
  [
    source 751
    target 329
    measure 7
    diseases "Renal_cell_carcinoma"
    GOmeasure 7
    id 1485
    name "1485"
  ]
  edge
  [
    source 750
    target 329
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 1486
    name "1486"
  ]
  edge
  [
    source 749
    target 329
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 1487
    name "1487"
  ]
  edge
  [
    source 527
    target 329
    measure 6
    diseases "Renal_cell_carcinoma"
    GOmeasure 6
    id 1488
    name "1488"
  ]
  edge
  [
    source 425
    target 329
    measure 5
    diseases "Renal_cell_carcinoma"
    GOmeasure 5
    id 1489
    name "1489"
  ]
  edge
  [
    source 333
    target 329
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1490
    name "1490"
  ]
  edge
  [
    source 332
    target 329
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1491
    name "1491"
  ]
  edge
  [
    source 331
    target 329
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1492
    name "1492"
  ]
  edge
  [
    source 330
    target 329
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1493
    name "1493"
  ]
  edge
  [
    source 697
    target 328
    measure 5
    diseases "Bladder_cancer"
    GOmeasure 5
    id 1494
    name "1494"
  ]
  edge
  [
    source 696
    target 328
    measure 6
    diseases "Bladder_cancer"
    GOmeasure 6
    id 1495
    name "1495"
  ]
  edge
  [
    source 340
    target 328
    measure 6
    diseases "Crouzon_syndrome"
    GOmeasure 6
    id 1496
    name "1496"
  ]
  edge
  [
    source 333
    target 328
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1497
    name "1497"
  ]
  edge
  [
    source 332
    target 328
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1498
    name "1498"
  ]
  edge
  [
    source 331
    target 328
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1499
    name "1499"
  ]
  edge
  [
    source 330
    target 328
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1500
    name "1500"
  ]
  edge
  [
    source 329
    target 328
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1501
    name "1501"
  ]
  edge
  [
    source 700
    target 327
    measure 4
    diseases "Cancer_susceptibility"
    GOmeasure 4
    id 1502
    name "1502"
  ]
  edge
  [
    source 554
    target 327
    measure 7
    diseases "Endometrial_carcinoma"
    GOmeasure 7
    id 1503
    name "1503"
  ]
  edge
  [
    source 446
    target 327
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1504
    name "1504"
  ]
  edge
  [
    source 393
    target 327
    measure 5
    diseases "Endometrial_carcinoma"
    GOmeasure 5
    id 1505
    name "1505"
  ]
  edge
  [
    source 383
    target 327
    measure 5
    diseases "Ovarian_cancer"
    GOmeasure 5
    id 1506
    name "1506"
  ]
  edge
  [
    source 339
    target 327
    measure 5
    diseases "Ovarian_cancer"
    GOmeasure 5
    id 1507
    name "1507"
  ]
  edge
  [
    source 335
    target 327
    measure 5
    diseases "Endometrial_carcinoma,Ovarian_cancer"
    GOmeasure 5
    id 1508
    name "1508"
  ]
  edge
  [
    source 333
    target 327
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1509
    name "1509"
  ]
  edge
  [
    source 332
    target 327
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1510
    name "1510"
  ]
  edge
  [
    source 331
    target 327
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1511
    name "1511"
  ]
  edge
  [
    source 330
    target 327
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1512
    name "1512"
  ]
  edge
  [
    source 329
    target 327
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1513
    name "1513"
  ]
  edge
  [
    source 328
    target 327
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1514
    name "1514"
  ]
  edge
  [
    source 333
    target 326
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1515
    name "1515"
  ]
  edge
  [
    source 332
    target 326
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1516
    name "1516"
  ]
  edge
  [
    source 331
    target 326
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1517
    name "1517"
  ]
  edge
  [
    source 330
    target 326
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1518
    name "1518"
  ]
  edge
  [
    source 329
    target 326
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1519
    name "1519"
  ]
  edge
  [
    source 328
    target 326
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1520
    name "1520"
  ]
  edge
  [
    source 327
    target 326
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1521
    name "1521"
  ]
  edge
  [
    source 333
    target 325
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1522
    name "1522"
  ]
  edge
  [
    source 332
    target 325
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1523
    name "1523"
  ]
  edge
  [
    source 331
    target 325
    measure 8
    diseases "Colon_cancer"
    GOmeasure 8
    id 1524
    name "1524"
  ]
  edge
  [
    source 330
    target 325
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1525
    name "1525"
  ]
  edge
  [
    source 329
    target 325
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1526
    name "1526"
  ]
  edge
  [
    source 328
    target 325
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1527
    name "1527"
  ]
  edge
  [
    source 327
    target 325
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1528
    name "1528"
  ]
  edge
  [
    source 326
    target 325
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1529
    name "1529"
  ]
  edge
  [
    source 333
    target 324
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1530
    name "1530"
  ]
  edge
  [
    source 332
    target 324
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1531
    name "1531"
  ]
  edge
  [
    source 331
    target 324
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1532
    name "1532"
  ]
  edge
  [
    source 330
    target 324
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1533
    name "1533"
  ]
  edge
  [
    source 329
    target 324
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1534
    name "1534"
  ]
  edge
  [
    source 328
    target 324
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1535
    name "1535"
  ]
  edge
  [
    source 327
    target 324
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1536
    name "1536"
  ]
  edge
  [
    source 326
    target 324
    measure 6
    diseases "Colon_cancer,Turcot_syndrome"
    GOmeasure 6
    id 1537
    name "1537"
  ]
  edge
  [
    source 325
    target 324
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1538
    name "1538"
  ]
  edge
  [
    source 766
    target 323
    measure 5
    diseases "Neurofibromatosis"
    GOmeasure 5
    id 1539
    name "1539"
  ]
  edge
  [
    source 644
    target 323
    measure 5
    diseases "Glioblastoma"
    GOmeasure 5
    id 1540
    name "1540"
  ]
  edge
  [
    source 507
    target 323
    measure 4
    diseases "Glioblastoma"
    GOmeasure 4
    id 1541
    name "1541"
  ]
  edge
  [
    source 446
    target 323
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1542
    name "1542"
  ]
  edge
  [
    source 410
    target 323
    measure 4
    diseases "Lymphoma"
    GOmeasure 4
    id 1543
    name "1543"
  ]
  edge
  [
    source 409
    target 323
    measure 7
    diseases "Lymphoma"
    GOmeasure 7
    id 1544
    name "1544"
  ]
  edge
  [
    source 408
    target 323
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1545
    name "1545"
  ]
  edge
  [
    source 396
    target 323
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1546
    name "1546"
  ]
  edge
  [
    source 388
    target 323
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1547
    name "1547"
  ]
  edge
  [
    source 383
    target 323
    measure 5
    diseases "Ovarian_cancer"
    GOmeasure 5
    id 1548
    name "1548"
  ]
  edge
  [
    source 339
    target 323
    measure 5
    diseases "Glioblastoma,Ovarian_cancer"
    GOmeasure 5
    id 1549
    name "1549"
  ]
  edge
  [
    source 335
    target 323
    measure 5
    diseases "Ovarian_cancer"
    GOmeasure 5
    id 1550
    name "1550"
  ]
  edge
  [
    source 333
    target 323
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1551
    name "1551"
  ]
  edge
  [
    source 332
    target 323
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1552
    name "1552"
  ]
  edge
  [
    source 331
    target 323
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1553
    name "1553"
  ]
  edge
  [
    source 330
    target 323
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1554
    name "1554"
  ]
  edge
  [
    source 329
    target 323
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1555
    name "1555"
  ]
  edge
  [
    source 328
    target 323
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1556
    name "1556"
  ]
  edge
  [
    source 327
    target 323
    measure 8
    diseases "Colon_cancer,Ovarian_cancer"
    GOmeasure 8
    id 1557
    name "1557"
  ]
  edge
  [
    source 326
    target 323
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1558
    name "1558"
  ]
  edge
  [
    source 325
    target 323
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1559
    name "1559"
  ]
  edge
  [
    source 324
    target 323
    measure 7
    diseases "Cafe-au-lait_spots,Colon_cancer,Muir-Torre_syndrome"
    GOmeasure 7
    id 1560
    name "1560"
  ]
  edge
  [
    source 333
    target 322
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1561
    name "1561"
  ]
  edge
  [
    source 332
    target 322
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1562
    name "1562"
  ]
  edge
  [
    source 331
    target 322
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1563
    name "1563"
  ]
  edge
  [
    source 330
    target 322
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1564
    name "1564"
  ]
  edge
  [
    source 329
    target 322
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1565
    name "1565"
  ]
  edge
  [
    source 328
    target 322
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1566
    name "1566"
  ]
  edge
  [
    source 327
    target 322
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1567
    name "1567"
  ]
  edge
  [
    source 326
    target 322
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1568
    name "1568"
  ]
  edge
  [
    source 325
    target 322
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1569
    name "1569"
  ]
  edge
  [
    source 324
    target 322
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1570
    name "1570"
  ]
  edge
  [
    source 323
    target 322
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1571
    name "1571"
  ]
  edge
  [
    source 333
    target 321
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1572
    name "1572"
  ]
  edge
  [
    source 332
    target 321
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1573
    name "1573"
  ]
  edge
  [
    source 331
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1574
    name "1574"
  ]
  edge
  [
    source 330
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1575
    name "1575"
  ]
  edge
  [
    source 329
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1576
    name "1576"
  ]
  edge
  [
    source 328
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1577
    name "1577"
  ]
  edge
  [
    source 327
    target 321
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1578
    name "1578"
  ]
  edge
  [
    source 326
    target 321
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1579
    name "1579"
  ]
  edge
  [
    source 325
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1580
    name "1580"
  ]
  edge
  [
    source 324
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1581
    name "1581"
  ]
  edge
  [
    source 323
    target 321
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1582
    name "1582"
  ]
  edge
  [
    source 322
    target 321
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1583
    name "1583"
  ]
  edge
  [
    source 333
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1584
    name "1584"
  ]
  edge
  [
    source 332
    target 320
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1585
    name "1585"
  ]
  edge
  [
    source 331
    target 320
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1586
    name "1586"
  ]
  edge
  [
    source 330
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1587
    name "1587"
  ]
  edge
  [
    source 329
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1588
    name "1588"
  ]
  edge
  [
    source 328
    target 320
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1589
    name "1589"
  ]
  edge
  [
    source 327
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1590
    name "1590"
  ]
  edge
  [
    source 326
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1591
    name "1591"
  ]
  edge
  [
    source 325
    target 320
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1592
    name "1592"
  ]
  edge
  [
    source 324
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1593
    name "1593"
  ]
  edge
  [
    source 323
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1594
    name "1594"
  ]
  edge
  [
    source 322
    target 320
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1595
    name "1595"
  ]
  edge
  [
    source 321
    target 320
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1596
    name "1596"
  ]
  edge
  [
    source 527
    target 319
    measure 6
    diseases "Hepatic_adenoma"
    GOmeasure 6
    id 1597
    name "1597"
  ]
  edge
  [
    source 526
    target 319
    measure 5
    diseases "Hepatic_adenoma"
    GOmeasure 5
    id 1598
    name "1598"
  ]
  edge
  [
    source 525
    target 319
    measure 4
    diseases "Hepatic_adenoma"
    GOmeasure 4
    id 1599
    name "1599"
  ]
  edge
  [
    source 446
    target 319
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1600
    name "1600"
  ]
  edge
  [
    source 383
    target 319
    measure 4
    diseases "Ovarian_cancer"
    GOmeasure 4
    id 1601
    name "1601"
  ]
  edge
  [
    source 344
    target 319
    measure 7
    diseases "Hepatic_adenoma"
    GOmeasure 7
    id 1602
    name "1602"
  ]
  edge
  [
    source 339
    target 319
    measure 5
    diseases "Ovarian_cancer"
    GOmeasure 5
    id 1603
    name "1603"
  ]
  edge
  [
    source 335
    target 319
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1604
    name "1604"
  ]
  edge
  [
    source 333
    target 319
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1605
    name "1605"
  ]
  edge
  [
    source 332
    target 319
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1606
    name "1606"
  ]
  edge
  [
    source 331
    target 319
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1607
    name "1607"
  ]
  edge
  [
    source 330
    target 319
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1608
    name "1608"
  ]
  edge
  [
    source 329
    target 319
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1609
    name "1609"
  ]
  edge
  [
    source 328
    target 319
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1610
    name "1610"
  ]
  edge
  [
    source 327
    target 319
    measure 5
    diseases "Colon_cancer,Ovarian_cancer"
    GOmeasure 5
    id 1611
    name "1611"
  ]
  edge
  [
    source 326
    target 319
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1612
    name "1612"
  ]
  edge
  [
    source 325
    target 319
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1613
    name "1613"
  ]
  edge
  [
    source 324
    target 319
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1614
    name "1614"
  ]
  edge
  [
    source 323
    target 319
    measure 5
    diseases "Colon_cancer,Ovarian_cancer"
    GOmeasure 5
    id 1615
    name "1615"
  ]
  edge
  [
    source 322
    target 319
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1616
    name "1616"
  ]
  edge
  [
    source 321
    target 319
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1617
    name "1617"
  ]
  edge
  [
    source 320
    target 319
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1618
    name "1618"
  ]
  edge
  [
    source 388
    target 318
    measure 6
    diseases "T-cell_lymphoblastic_leukemia"
    GOmeasure 6
    id 1619
    name "1619"
  ]
  edge
  [
    source 333
    target 318
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1620
    name "1620"
  ]
  edge
  [
    source 332
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1621
    name "1621"
  ]
  edge
  [
    source 331
    target 318
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1622
    name "1622"
  ]
  edge
  [
    source 330
    target 318
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1623
    name "1623"
  ]
  edge
  [
    source 329
    target 318
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1624
    name "1624"
  ]
  edge
  [
    source 328
    target 318
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1625
    name "1625"
  ]
  edge
  [
    source 327
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1626
    name "1626"
  ]
  edge
  [
    source 326
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1627
    name "1627"
  ]
  edge
  [
    source 325
    target 318
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1628
    name "1628"
  ]
  edge
  [
    source 324
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1629
    name "1629"
  ]
  edge
  [
    source 323
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1630
    name "1630"
  ]
  edge
  [
    source 322
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1631
    name "1631"
  ]
  edge
  [
    source 321
    target 318
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1632
    name "1632"
  ]
  edge
  [
    source 320
    target 318
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1633
    name "1633"
  ]
  edge
  [
    source 319
    target 318
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1634
    name "1634"
  ]
  edge
  [
    source 446
    target 317
    measure 6
    diseases "Ovarian_cancer"
    GOmeasure 6
    id 1635
    name "1635"
  ]
  edge
  [
    source 391
    target 317
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1636
    name "1636"
  ]
  edge
  [
    source 390
    target 317
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1637
    name "1637"
  ]
  edge
  [
    source 389
    target 317
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1638
    name "1638"
  ]
  edge
  [
    source 388
    target 317
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1639
    name "1639"
  ]
  edge
  [
    source 387
    target 317
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1640
    name "1640"
  ]
  edge
  [
    source 386
    target 317
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1641
    name "1641"
  ]
  edge
  [
    source 385
    target 317
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1642
    name "1642"
  ]
  edge
  [
    source 384
    target 317
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 1643
    name "1643"
  ]
  edge
  [
    source 383
    target 317
    measure 4
    diseases "Breast_cancer,Ovarian_cancer"
    GOmeasure 4
    id 1644
    name "1644"
  ]
  edge
  [
    source 382
    target 317
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1645
    name "1645"
  ]
  edge
  [
    source 381
    target 317
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1646
    name "1646"
  ]
  edge
  [
    source 380
    target 317
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1647
    name "1647"
  ]
  edge
  [
    source 379
    target 317
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1648
    name "1648"
  ]
  edge
  [
    source 339
    target 317
    measure 7
    diseases "Ovarian_cancer"
    GOmeasure 7
    id 1649
    name "1649"
  ]
  edge
  [
    source 335
    target 317
    measure 4
    diseases "Breast_cancer,Ovarian_cancer"
    GOmeasure 4
    id 1650
    name "1650"
  ]
  edge
  [
    source 333
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1651
    name "1651"
  ]
  edge
  [
    source 332
    target 317
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1652
    name "1652"
  ]
  edge
  [
    source 331
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1653
    name "1653"
  ]
  edge
  [
    source 330
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1654
    name "1654"
  ]
  edge
  [
    source 329
    target 317
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1655
    name "1655"
  ]
  edge
  [
    source 328
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1656
    name "1656"
  ]
  edge
  [
    source 327
    target 317
    measure 6
    diseases "Colon_cancer,Ovarian_cancer"
    GOmeasure 6
    id 1657
    name "1657"
  ]
  edge
  [
    source 326
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1658
    name "1658"
  ]
  edge
  [
    source 325
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1659
    name "1659"
  ]
  edge
  [
    source 324
    target 317
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1660
    name "1660"
  ]
  edge
  [
    source 323
    target 317
    measure 6
    diseases "Colon_cancer,Ovarian_cancer"
    GOmeasure 6
    id 1661
    name "1661"
  ]
  edge
  [
    source 322
    target 317
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1662
    name "1662"
  ]
  edge
  [
    source 321
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1663
    name "1663"
  ]
  edge
  [
    source 320
    target 317
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1664
    name "1664"
  ]
  edge
  [
    source 319
    target 317
    measure 5
    diseases "Colon_cancer,Ovarian_cancer"
    GOmeasure 5
    id 1665
    name "1665"
  ]
  edge
  [
    source 318
    target 317
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1666
    name "1666"
  ]
  edge
  [
    source 599
    target 316
    measure 6
    diseases "Rubenstein-Taybi_syndrome"
    GOmeasure 6
    id 1667
    name "1667"
  ]
  edge
  [
    source 333
    target 316
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1668
    name "1668"
  ]
  edge
  [
    source 332
    target 316
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1669
    name "1669"
  ]
  edge
  [
    source 331
    target 316
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1670
    name "1670"
  ]
  edge
  [
    source 330
    target 316
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1671
    name "1671"
  ]
  edge
  [
    source 329
    target 316
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1672
    name "1672"
  ]
  edge
  [
    source 328
    target 316
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1673
    name "1673"
  ]
  edge
  [
    source 327
    target 316
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1674
    name "1674"
  ]
  edge
  [
    source 326
    target 316
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1675
    name "1675"
  ]
  edge
  [
    source 325
    target 316
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1676
    name "1676"
  ]
  edge
  [
    source 324
    target 316
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1677
    name "1677"
  ]
  edge
  [
    source 323
    target 316
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1678
    name "1678"
  ]
  edge
  [
    source 322
    target 316
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1679
    name "1679"
  ]
  edge
  [
    source 321
    target 316
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1680
    name "1680"
  ]
  edge
  [
    source 320
    target 316
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1681
    name "1681"
  ]
  edge
  [
    source 319
    target 316
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1682
    name "1682"
  ]
  edge
  [
    source 318
    target 316
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1683
    name "1683"
  ]
  edge
  [
    source 317
    target 316
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1684
    name "1684"
  ]
  edge
  [
    source 333
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1685
    name "1685"
  ]
  edge
  [
    source 332
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1686
    name "1686"
  ]
  edge
  [
    source 331
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1687
    name "1687"
  ]
  edge
  [
    source 330
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1688
    name "1688"
  ]
  edge
  [
    source 329
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1689
    name "1689"
  ]
  edge
  [
    source 328
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1690
    name "1690"
  ]
  edge
  [
    source 327
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1691
    name "1691"
  ]
  edge
  [
    source 326
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1692
    name "1692"
  ]
  edge
  [
    source 325
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1693
    name "1693"
  ]
  edge
  [
    source 324
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1694
    name "1694"
  ]
  edge
  [
    source 323
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1695
    name "1695"
  ]
  edge
  [
    source 322
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1696
    name "1696"
  ]
  edge
  [
    source 321
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1697
    name "1697"
  ]
  edge
  [
    source 320
    target 315
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1698
    name "1698"
  ]
  edge
  [
    source 319
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1699
    name "1699"
  ]
  edge
  [
    source 318
    target 315
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1700
    name "1700"
  ]
  edge
  [
    source 317
    target 315
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1701
    name "1701"
  ]
  edge
  [
    source 316
    target 315
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1702
    name "1702"
  ]
  edge
  [
    source 333
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1703
    name "1703"
  ]
  edge
  [
    source 332
    target 314
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1704
    name "1704"
  ]
  edge
  [
    source 331
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1705
    name "1705"
  ]
  edge
  [
    source 330
    target 314
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1706
    name "1706"
  ]
  edge
  [
    source 329
    target 314
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1707
    name "1707"
  ]
  edge
  [
    source 328
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1708
    name "1708"
  ]
  edge
  [
    source 327
    target 314
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1709
    name "1709"
  ]
  edge
  [
    source 326
    target 314
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1710
    name "1710"
  ]
  edge
  [
    source 325
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1711
    name "1711"
  ]
  edge
  [
    source 324
    target 314
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1712
    name "1712"
  ]
  edge
  [
    source 323
    target 314
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1713
    name "1713"
  ]
  edge
  [
    source 322
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1714
    name "1714"
  ]
  edge
  [
    source 321
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1715
    name "1715"
  ]
  edge
  [
    source 320
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1716
    name "1716"
  ]
  edge
  [
    source 319
    target 314
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1717
    name "1717"
  ]
  edge
  [
    source 318
    target 314
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1718
    name "1718"
  ]
  edge
  [
    source 317
    target 314
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1719
    name "1719"
  ]
  edge
  [
    source 316
    target 314
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1720
    name "1720"
  ]
  edge
  [
    source 315
    target 314
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1721
    name "1721"
  ]
  edge
  [
    source 333
    target 313
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1722
    name "1722"
  ]
  edge
  [
    source 332
    target 313
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1723
    name "1723"
  ]
  edge
  [
    source 331
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1724
    name "1724"
  ]
  edge
  [
    source 330
    target 313
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1725
    name "1725"
  ]
  edge
  [
    source 329
    target 313
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1726
    name "1726"
  ]
  edge
  [
    source 328
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1727
    name "1727"
  ]
  edge
  [
    source 327
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1728
    name "1728"
  ]
  edge
  [
    source 326
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1729
    name "1729"
  ]
  edge
  [
    source 325
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1730
    name "1730"
  ]
  edge
  [
    source 324
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1731
    name "1731"
  ]
  edge
  [
    source 323
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1732
    name "1732"
  ]
  edge
  [
    source 322
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1733
    name "1733"
  ]
  edge
  [
    source 321
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1734
    name "1734"
  ]
  edge
  [
    source 320
    target 313
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1735
    name "1735"
  ]
  edge
  [
    source 319
    target 313
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1736
    name "1736"
  ]
  edge
  [
    source 318
    target 313
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1737
    name "1737"
  ]
  edge
  [
    source 317
    target 313
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1738
    name "1738"
  ]
  edge
  [
    source 316
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1739
    name "1739"
  ]
  edge
  [
    source 315
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1740
    name "1740"
  ]
  edge
  [
    source 314
    target 313
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1741
    name "1741"
  ]
  edge
  [
    source 333
    target 312
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1742
    name "1742"
  ]
  edge
  [
    source 332
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1743
    name "1743"
  ]
  edge
  [
    source 331
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1744
    name "1744"
  ]
  edge
  [
    source 330
    target 312
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1745
    name "1745"
  ]
  edge
  [
    source 329
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1746
    name "1746"
  ]
  edge
  [
    source 328
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1747
    name "1747"
  ]
  edge
  [
    source 327
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1748
    name "1748"
  ]
  edge
  [
    source 326
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1749
    name "1749"
  ]
  edge
  [
    source 325
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1750
    name "1750"
  ]
  edge
  [
    source 324
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1751
    name "1751"
  ]
  edge
  [
    source 323
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1752
    name "1752"
  ]
  edge
  [
    source 322
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1753
    name "1753"
  ]
  edge
  [
    source 321
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1754
    name "1754"
  ]
  edge
  [
    source 320
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1755
    name "1755"
  ]
  edge
  [
    source 319
    target 312
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1756
    name "1756"
  ]
  edge
  [
    source 318
    target 312
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1757
    name "1757"
  ]
  edge
  [
    source 317
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1758
    name "1758"
  ]
  edge
  [
    source 316
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1759
    name "1759"
  ]
  edge
  [
    source 315
    target 312
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1760
    name "1760"
  ]
  edge
  [
    source 314
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1761
    name "1761"
  ]
  edge
  [
    source 313
    target 312
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1762
    name "1762"
  ]
  edge
  [
    source 333
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1763
    name "1763"
  ]
  edge
  [
    source 332
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1764
    name "1764"
  ]
  edge
  [
    source 331
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1765
    name "1765"
  ]
  edge
  [
    source 330
    target 311
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1766
    name "1766"
  ]
  edge
  [
    source 329
    target 311
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1767
    name "1767"
  ]
  edge
  [
    source 328
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1768
    name "1768"
  ]
  edge
  [
    source 327
    target 311
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1769
    name "1769"
  ]
  edge
  [
    source 326
    target 311
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1770
    name "1770"
  ]
  edge
  [
    source 325
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1771
    name "1771"
  ]
  edge
  [
    source 324
    target 311
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1772
    name "1772"
  ]
  edge
  [
    source 323
    target 311
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1773
    name "1773"
  ]
  edge
  [
    source 322
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1774
    name "1774"
  ]
  edge
  [
    source 321
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1775
    name "1775"
  ]
  edge
  [
    source 320
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1776
    name "1776"
  ]
  edge
  [
    source 319
    target 311
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1777
    name "1777"
  ]
  edge
  [
    source 318
    target 311
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1778
    name "1778"
  ]
  edge
  [
    source 317
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1779
    name "1779"
  ]
  edge
  [
    source 316
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1780
    name "1780"
  ]
  edge
  [
    source 315
    target 311
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1781
    name "1781"
  ]
  edge
  [
    source 314
    target 311
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1782
    name "1782"
  ]
  edge
  [
    source 313
    target 311
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1783
    name "1783"
  ]
  edge
  [
    source 312
    target 311
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1784
    name "1784"
  ]
  edge
  [
    source 333
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1785
    name "1785"
  ]
  edge
  [
    source 332
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1786
    name "1786"
  ]
  edge
  [
    source 331
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1787
    name "1787"
  ]
  edge
  [
    source 330
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1788
    name "1788"
  ]
  edge
  [
    source 329
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1789
    name "1789"
  ]
  edge
  [
    source 328
    target 310
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1790
    name "1790"
  ]
  edge
  [
    source 327
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1791
    name "1791"
  ]
  edge
  [
    source 326
    target 310
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1792
    name "1792"
  ]
  edge
  [
    source 325
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1793
    name "1793"
  ]
  edge
  [
    source 324
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1794
    name "1794"
  ]
  edge
  [
    source 323
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1795
    name "1795"
  ]
  edge
  [
    source 322
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1796
    name "1796"
  ]
  edge
  [
    source 321
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1797
    name "1797"
  ]
  edge
  [
    source 320
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1798
    name "1798"
  ]
  edge
  [
    source 319
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1799
    name "1799"
  ]
  edge
  [
    source 318
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1800
    name "1800"
  ]
  edge
  [
    source 317
    target 310
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1801
    name "1801"
  ]
  edge
  [
    source 316
    target 310
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1802
    name "1802"
  ]
  edge
  [
    source 315
    target 310
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1803
    name "1803"
  ]
  edge
  [
    source 314
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1804
    name "1804"
  ]
  edge
  [
    source 313
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1805
    name "1805"
  ]
  edge
  [
    source 312
    target 310
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1806
    name "1806"
  ]
  edge
  [
    source 311
    target 310
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1807
    name "1807"
  ]
  edge
  [
    source 799
    target 309
    measure 5
    diseases "Loeys-Dietz_syndrome"
    GOmeasure 5
    id 1808
    name "1808"
  ]
  edge
  [
    source 795
    target 309
    measure 6
    diseases "Esophageal_cancer"
    GOmeasure 6
    id 1809
    name "1809"
  ]
  edge
  [
    source 794
    target 309
    measure 4
    diseases "Esophageal_cancer"
    GOmeasure 4
    id 1810
    name "1810"
  ]
  edge
  [
    source 793
    target 309
    measure 7
    diseases "Esophageal_cancer"
    GOmeasure 7
    id 1811
    name "1811"
  ]
  edge
  [
    source 333
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1812
    name "1812"
  ]
  edge
  [
    source 332
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1813
    name "1813"
  ]
  edge
  [
    source 331
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1814
    name "1814"
  ]
  edge
  [
    source 330
    target 309
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1815
    name "1815"
  ]
  edge
  [
    source 329
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1816
    name "1816"
  ]
  edge
  [
    source 328
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1817
    name "1817"
  ]
  edge
  [
    source 327
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1818
    name "1818"
  ]
  edge
  [
    source 326
    target 309
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1819
    name "1819"
  ]
  edge
  [
    source 325
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1820
    name "1820"
  ]
  edge
  [
    source 324
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1821
    name "1821"
  ]
  edge
  [
    source 323
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1822
    name "1822"
  ]
  edge
  [
    source 322
    target 309
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1823
    name "1823"
  ]
  edge
  [
    source 321
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1824
    name "1824"
  ]
  edge
  [
    source 320
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1825
    name "1825"
  ]
  edge
  [
    source 319
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1826
    name "1826"
  ]
  edge
  [
    source 318
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1827
    name "1827"
  ]
  edge
  [
    source 317
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1828
    name "1828"
  ]
  edge
  [
    source 316
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1829
    name "1829"
  ]
  edge
  [
    source 315
    target 309
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1830
    name "1830"
  ]
  edge
  [
    source 314
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1831
    name "1831"
  ]
  edge
  [
    source 313
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1832
    name "1832"
  ]
  edge
  [
    source 312
    target 309
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1833
    name "1833"
  ]
  edge
  [
    source 311
    target 309
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1834
    name "1834"
  ]
  edge
  [
    source 310
    target 309
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1835
    name "1835"
  ]
  edge
  [
    source 333
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1836
    name "1836"
  ]
  edge
  [
    source 332
    target 308
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1837
    name "1837"
  ]
  edge
  [
    source 331
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1838
    name "1838"
  ]
  edge
  [
    source 330
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1839
    name "1839"
  ]
  edge
  [
    source 329
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1840
    name "1840"
  ]
  edge
  [
    source 328
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1841
    name "1841"
  ]
  edge
  [
    source 327
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1842
    name "1842"
  ]
  edge
  [
    source 326
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1843
    name "1843"
  ]
  edge
  [
    source 325
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1844
    name "1844"
  ]
  edge
  [
    source 324
    target 308
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1845
    name "1845"
  ]
  edge
  [
    source 323
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1846
    name "1846"
  ]
  edge
  [
    source 322
    target 308
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1847
    name "1847"
  ]
  edge
  [
    source 321
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1848
    name "1848"
  ]
  edge
  [
    source 320
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1849
    name "1849"
  ]
  edge
  [
    source 319
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1850
    name "1850"
  ]
  edge
  [
    source 318
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1851
    name "1851"
  ]
  edge
  [
    source 317
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1852
    name "1852"
  ]
  edge
  [
    source 316
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1853
    name "1853"
  ]
  edge
  [
    source 315
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1854
    name "1854"
  ]
  edge
  [
    source 314
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1855
    name "1855"
  ]
  edge
  [
    source 313
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1856
    name "1856"
  ]
  edge
  [
    source 312
    target 308
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1857
    name "1857"
  ]
  edge
  [
    source 311
    target 308
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1858
    name "1858"
  ]
  edge
  [
    source 310
    target 308
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1859
    name "1859"
  ]
  edge
  [
    source 309
    target 308
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1860
    name "1860"
  ]
  edge
  [
    source 410
    target 307
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1861
    name "1861"
  ]
  edge
  [
    source 409
    target 307
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1862
    name "1862"
  ]
  edge
  [
    source 408
    target 307
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1863
    name "1863"
  ]
  edge
  [
    source 396
    target 307
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1864
    name "1864"
  ]
  edge
  [
    source 388
    target 307
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1865
    name "1865"
  ]
  edge
  [
    source 333
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1866
    name "1866"
  ]
  edge
  [
    source 332
    target 307
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1867
    name "1867"
  ]
  edge
  [
    source 331
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1868
    name "1868"
  ]
  edge
  [
    source 330
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1869
    name "1869"
  ]
  edge
  [
    source 329
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1870
    name "1870"
  ]
  edge
  [
    source 328
    target 307
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1871
    name "1871"
  ]
  edge
  [
    source 327
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1872
    name "1872"
  ]
  edge
  [
    source 326
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1873
    name "1873"
  ]
  edge
  [
    source 325
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1874
    name "1874"
  ]
  edge
  [
    source 324
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1875
    name "1875"
  ]
  edge
  [
    source 323
    target 307
    measure 6
    diseases "Colon_cancer,Lymphoma"
    GOmeasure 6
    id 1876
    name "1876"
  ]
  edge
  [
    source 322
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1877
    name "1877"
  ]
  edge
  [
    source 321
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1878
    name "1878"
  ]
  edge
  [
    source 320
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1879
    name "1879"
  ]
  edge
  [
    source 319
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1880
    name "1880"
  ]
  edge
  [
    source 318
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1881
    name "1881"
  ]
  edge
  [
    source 317
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1882
    name "1882"
  ]
  edge
  [
    source 316
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1883
    name "1883"
  ]
  edge
  [
    source 315
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1884
    name "1884"
  ]
  edge
  [
    source 314
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1885
    name "1885"
  ]
  edge
  [
    source 313
    target 307
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1886
    name "1886"
  ]
  edge
  [
    source 312
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1887
    name "1887"
  ]
  edge
  [
    source 311
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1888
    name "1888"
  ]
  edge
  [
    source 310
    target 307
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1889
    name "1889"
  ]
  edge
  [
    source 309
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1890
    name "1890"
  ]
  edge
  [
    source 308
    target 307
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1891
    name "1891"
  ]
  edge
  [
    source 410
    target 306
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1892
    name "1892"
  ]
  edge
  [
    source 409
    target 306
    measure 7
    diseases "Lymphoma"
    GOmeasure 7
    id 1893
    name "1893"
  ]
  edge
  [
    source 408
    target 306
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1894
    name "1894"
  ]
  edge
  [
    source 396
    target 306
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1895
    name "1895"
  ]
  edge
  [
    source 391
    target 306
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1896
    name "1896"
  ]
  edge
  [
    source 390
    target 306
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1897
    name "1897"
  ]
  edge
  [
    source 389
    target 306
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1898
    name "1898"
  ]
  edge
  [
    source 388
    target 306
    measure 5
    diseases "Breast_cancer,Lymphoma"
    GOmeasure 5
    id 1899
    name "1899"
  ]
  edge
  [
    source 387
    target 306
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1900
    name "1900"
  ]
  edge
  [
    source 386
    target 306
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1901
    name "1901"
  ]
  edge
  [
    source 385
    target 306
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1902
    name "1902"
  ]
  edge
  [
    source 384
    target 306
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1903
    name "1903"
  ]
  edge
  [
    source 383
    target 306
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1904
    name "1904"
  ]
  edge
  [
    source 382
    target 306
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1905
    name "1905"
  ]
  edge
  [
    source 381
    target 306
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1906
    name "1906"
  ]
  edge
  [
    source 380
    target 306
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 1907
    name "1907"
  ]
  edge
  [
    source 379
    target 306
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 1908
    name "1908"
  ]
  edge
  [
    source 335
    target 306
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 1909
    name "1909"
  ]
  edge
  [
    source 333
    target 306
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1910
    name "1910"
  ]
  edge
  [
    source 332
    target 306
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1911
    name "1911"
  ]
  edge
  [
    source 331
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1912
    name "1912"
  ]
  edge
  [
    source 330
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1913
    name "1913"
  ]
  edge
  [
    source 329
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1914
    name "1914"
  ]
  edge
  [
    source 328
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1915
    name "1915"
  ]
  edge
  [
    source 327
    target 306
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1916
    name "1916"
  ]
  edge
  [
    source 326
    target 306
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1917
    name "1917"
  ]
  edge
  [
    source 325
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1918
    name "1918"
  ]
  edge
  [
    source 324
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1919
    name "1919"
  ]
  edge
  [
    source 323
    target 306
    measure 6
    diseases "Colon_cancer,Lymphoma"
    GOmeasure 6
    id 1920
    name "1920"
  ]
  edge
  [
    source 322
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1921
    name "1921"
  ]
  edge
  [
    source 321
    target 306
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1922
    name "1922"
  ]
  edge
  [
    source 320
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1923
    name "1923"
  ]
  edge
  [
    source 319
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1924
    name "1924"
  ]
  edge
  [
    source 318
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1925
    name "1925"
  ]
  edge
  [
    source 317
    target 306
    measure 5
    diseases "Breast_cancer,Colon_cancer"
    GOmeasure 5
    id 1926
    name "1926"
  ]
  edge
  [
    source 316
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1927
    name "1927"
  ]
  edge
  [
    source 315
    target 306
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1928
    name "1928"
  ]
  edge
  [
    source 314
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1929
    name "1929"
  ]
  edge
  [
    source 313
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1930
    name "1930"
  ]
  edge
  [
    source 312
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1931
    name "1931"
  ]
  edge
  [
    source 311
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1932
    name "1932"
  ]
  edge
  [
    source 310
    target 306
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1933
    name "1933"
  ]
  edge
  [
    source 309
    target 306
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1934
    name "1934"
  ]
  edge
  [
    source 308
    target 306
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1935
    name "1935"
  ]
  edge
  [
    source 307
    target 306
    measure 5
    diseases "Colon_cancer,Lymphoma"
    GOmeasure 5
    id 1936
    name "1936"
  ]
  edge
  [
    source 410
    target 305
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1937
    name "1937"
  ]
  edge
  [
    source 409
    target 305
    measure 6
    diseases "Lymphoma"
    GOmeasure 6
    id 1938
    name "1938"
  ]
  edge
  [
    source 408
    target 305
    measure 7
    diseases "Lymphoma"
    GOmeasure 7
    id 1939
    name "1939"
  ]
  edge
  [
    source 396
    target 305
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1940
    name "1940"
  ]
  edge
  [
    source 388
    target 305
    measure 5
    diseases "Lymphoma"
    GOmeasure 5
    id 1941
    name "1941"
  ]
  edge
  [
    source 333
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1942
    name "1942"
  ]
  edge
  [
    source 332
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1943
    name "1943"
  ]
  edge
  [
    source 331
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1944
    name "1944"
  ]
  edge
  [
    source 330
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1945
    name "1945"
  ]
  edge
  [
    source 329
    target 305
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1946
    name "1946"
  ]
  edge
  [
    source 328
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1947
    name "1947"
  ]
  edge
  [
    source 327
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1948
    name "1948"
  ]
  edge
  [
    source 326
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1949
    name "1949"
  ]
  edge
  [
    source 325
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1950
    name "1950"
  ]
  edge
  [
    source 324
    target 305
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1951
    name "1951"
  ]
  edge
  [
    source 323
    target 305
    measure 5
    diseases "Colon_cancer,Lymphoma"
    GOmeasure 5
    id 1952
    name "1952"
  ]
  edge
  [
    source 322
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1953
    name "1953"
  ]
  edge
  [
    source 321
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1954
    name "1954"
  ]
  edge
  [
    source 320
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1955
    name "1955"
  ]
  edge
  [
    source 319
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1956
    name "1956"
  ]
  edge
  [
    source 318
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1957
    name "1957"
  ]
  edge
  [
    source 317
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1958
    name "1958"
  ]
  edge
  [
    source 316
    target 305
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1959
    name "1959"
  ]
  edge
  [
    source 315
    target 305
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1960
    name "1960"
  ]
  edge
  [
    source 314
    target 305
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1961
    name "1961"
  ]
  edge
  [
    source 313
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1962
    name "1962"
  ]
  edge
  [
    source 312
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1963
    name "1963"
  ]
  edge
  [
    source 311
    target 305
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 1964
    name "1964"
  ]
  edge
  [
    source 310
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1965
    name "1965"
  ]
  edge
  [
    source 309
    target 305
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1966
    name "1966"
  ]
  edge
  [
    source 308
    target 305
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1967
    name "1967"
  ]
  edge
  [
    source 307
    target 305
    measure 5
    diseases "Colon_cancer,Lymphoma"
    GOmeasure 5
    id 1968
    name "1968"
  ]
  edge
  [
    source 306
    target 305
    measure 6
    diseases "Colon_cancer,Lymphoma"
    GOmeasure 6
    id 1969
    name "1969"
  ]
  edge
  [
    source 341
    target 303
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1970
    name "1970"
  ]
  edge
  [
    source 340
    target 303
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 1971
    name "1971"
  ]
  edge
  [
    source 339
    target 303
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1972
    name "1972"
  ]
  edge
  [
    source 338
    target 303
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1973
    name "1973"
  ]
  edge
  [
    source 337
    target 303
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1974
    name "1974"
  ]
  edge
  [
    source 336
    target 303
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1975
    name "1975"
  ]
  edge
  [
    source 335
    target 303
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 1976
    name "1976"
  ]
  edge
  [
    source 334
    target 303
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 1977
    name "1977"
  ]
  edge
  [
    source 333
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1978
    name "1978"
  ]
  edge
  [
    source 332
    target 303
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1979
    name "1979"
  ]
  edge
  [
    source 331
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1980
    name "1980"
  ]
  edge
  [
    source 330
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1981
    name "1981"
  ]
  edge
  [
    source 329
    target 303
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1982
    name "1982"
  ]
  edge
  [
    source 328
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1983
    name "1983"
  ]
  edge
  [
    source 327
    target 303
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1984
    name "1984"
  ]
  edge
  [
    source 326
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1985
    name "1985"
  ]
  edge
  [
    source 325
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1986
    name "1986"
  ]
  edge
  [
    source 324
    target 303
    measure 8
    diseases "Colon_cancer"
    GOmeasure 8
    id 1987
    name "1987"
  ]
  edge
  [
    source 323
    target 303
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1988
    name "1988"
  ]
  edge
  [
    source 322
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1989
    name "1989"
  ]
  edge
  [
    source 321
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1990
    name "1990"
  ]
  edge
  [
    source 320
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1991
    name "1991"
  ]
  edge
  [
    source 319
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1992
    name "1992"
  ]
  edge
  [
    source 318
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1993
    name "1993"
  ]
  edge
  [
    source 317
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1994
    name "1994"
  ]
  edge
  [
    source 316
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1995
    name "1995"
  ]
  edge
  [
    source 315
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1996
    name "1996"
  ]
  edge
  [
    source 314
    target 303
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 1997
    name "1997"
  ]
  edge
  [
    source 313
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 1998
    name "1998"
  ]
  edge
  [
    source 312
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 1999
    name "1999"
  ]
  edge
  [
    source 311
    target 303
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2000
    name "2000"
  ]
  edge
  [
    source 310
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2001
    name "2001"
  ]
  edge
  [
    source 309
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2002
    name "2002"
  ]
  edge
  [
    source 308
    target 303
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 2003
    name "2003"
  ]
  edge
  [
    source 307
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2004
    name "2004"
  ]
  edge
  [
    source 306
    target 303
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2005
    name "2005"
  ]
  edge
  [
    source 305
    target 303
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 2006
    name "2006"
  ]
  edge
  [
    source 304
    target 303
    measure 5
    diseases "Adenomas"
    GOmeasure 5
    id 2007
    name "2007"
  ]
  edge
  [
    source 341
    target 302
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 2008
    name "2008"
  ]
  edge
  [
    source 340
    target 302
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 2009
    name "2009"
  ]
  edge
  [
    source 339
    target 302
    measure 5
    diseases "Gastric_cancer"
    GOmeasure 5
    id 2010
    name "2010"
  ]
  edge
  [
    source 338
    target 302
    measure 7
    diseases "Gastric_cancer"
    GOmeasure 7
    id 2011
    name "2011"
  ]
  edge
  [
    source 337
    target 302
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 2012
    name "2012"
  ]
  edge
  [
    source 336
    target 302
    measure 6
    diseases "Gastric_cancer"
    GOmeasure 6
    id 2013
    name "2013"
  ]
  edge
  [
    source 335
    target 302
    measure 7
    diseases "Gastric_cancer"
    GOmeasure 7
    id 2014
    name "2014"
  ]
  edge
  [
    source 334
    target 302
    measure 4
    diseases "Gastric_cancer"
    GOmeasure 4
    id 2015
    name "2015"
  ]
  edge
  [
    source 333
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2016
    name "2016"
  ]
  edge
  [
    source 332
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2017
    name "2017"
  ]
  edge
  [
    source 331
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2018
    name "2018"
  ]
  edge
  [
    source 330
    target 302
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 2019
    name "2019"
  ]
  edge
  [
    source 329
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2020
    name "2020"
  ]
  edge
  [
    source 328
    target 302
    measure 8
    diseases "Colon_cancer"
    GOmeasure 8
    id 2021
    name "2021"
  ]
  edge
  [
    source 327
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2022
    name "2022"
  ]
  edge
  [
    source 326
    target 302
    measure 6
    diseases "Colon_cancer,Turcot_syndrome"
    GOmeasure 6
    id 2023
    name "2023"
  ]
  edge
  [
    source 325
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2024
    name "2024"
  ]
  edge
  [
    source 324
    target 302
    measure 5
    diseases "Colon_cancer,Turcot_syndrome"
    GOmeasure 5
    id 2025
    name "2025"
  ]
  edge
  [
    source 323
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2026
    name "2026"
  ]
  edge
  [
    source 322
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2027
    name "2027"
  ]
  edge
  [
    source 321
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2028
    name "2028"
  ]
  edge
  [
    source 320
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2029
    name "2029"
  ]
  edge
  [
    source 319
    target 302
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 2030
    name "2030"
  ]
  edge
  [
    source 318
    target 302
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 2031
    name "2031"
  ]
  edge
  [
    source 317
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2032
    name "2032"
  ]
  edge
  [
    source 316
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2033
    name "2033"
  ]
  edge
  [
    source 315
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2034
    name "2034"
  ]
  edge
  [
    source 314
    target 302
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 2035
    name "2035"
  ]
  edge
  [
    source 313
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2036
    name "2036"
  ]
  edge
  [
    source 312
    target 302
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 2037
    name "2037"
  ]
  edge
  [
    source 311
    target 302
    measure 4
    diseases "Colon_cancer"
    GOmeasure 4
    id 2038
    name "2038"
  ]
  edge
  [
    source 310
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2039
    name "2039"
  ]
  edge
  [
    source 309
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2040
    name "2040"
  ]
  edge
  [
    source 308
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2041
    name "2041"
  ]
  edge
  [
    source 307
    target 302
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 2042
    name "2042"
  ]
  edge
  [
    source 306
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2043
    name "2043"
  ]
  edge
  [
    source 305
    target 302
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 2044
    name "2044"
  ]
  edge
  [
    source 304
    target 302
    measure 6
    diseases "Adenomas"
    GOmeasure 6
    id 2045
    name "2045"
  ]
  edge
  [
    source 303
    target 302
    measure 5
    diseases "Adenomas,Colon_cancer,Gastric_cancer"
    GOmeasure 5
    id 2046
    name "2046"
  ]
  edge
  [
    source 614
    target 300
    measure 5
    diseases "Atrial_fibrillation"
    GOmeasure 5
    id 2047
    name "2047"
  ]
  edge
  [
    source 301
    target 300
    measure 5
    diseases "Long_QT_syndrome"
    GOmeasure 5
    id 2048
    name "2048"
  ]
  edge
  [
    source 301
    target 299
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2049
    name "2049"
  ]
  edge
  [
    source 300
    target 299
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2050
    name "2050"
  ]
  edge
  [
    source 301
    target 298
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2051
    name "2051"
  ]
  edge
  [
    source 300
    target 298
    measure 4
    diseases "Long_QT_syndrome"
    GOmeasure 4
    id 2052
    name "2052"
  ]
  edge
  [
    source 299
    target 298
    measure 5
    diseases "Long_QT_syndrome"
    GOmeasure 5
    id 2053
    name "2053"
  ]
  edge
  [
    source 301
    target 297
    measure 5
    diseases "Long_QT_syndrome"
    GOmeasure 5
    id 2054
    name "2054"
  ]
  edge
  [
    source 300
    target 297
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2055
    name "2055"
  ]
  edge
  [
    source 299
    target 297
    measure 7
    diseases "Long_QT_syndrome"
    GOmeasure 7
    id 2056
    name "2056"
  ]
  edge
  [
    source 298
    target 297
    measure 5
    diseases "Long_QT_syndrome"
    GOmeasure 5
    id 2057
    name "2057"
  ]
  edge
  [
    source 614
    target 296
    measure 6
    diseases "Atrial_fibrillation"
    GOmeasure 6
    id 2058
    name "2058"
  ]
  edge
  [
    source 301
    target 296
    measure 5
    diseases "Long_QT_syndrome"
    GOmeasure 5
    id 2059
    name "2059"
  ]
  edge
  [
    source 300
    target 296
    measure 6
    diseases "Atrial_fibrillation,Long_QT_syndrome"
    GOmeasure 6
    id 2060
    name "2060"
  ]
  edge
  [
    source 299
    target 296
    measure 6
    diseases "Jervell_and_Lange-Nielsen_syndrome,Long_QT_syndrome"
    GOmeasure 6
    id 2061
    name "2061"
  ]
  edge
  [
    source 298
    target 296
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2062
    name "2062"
  ]
  edge
  [
    source 297
    target 296
    measure 7
    diseases "Long_QT_syndrome"
    GOmeasure 7
    id 2063
    name "2063"
  ]
  edge
  [
    source 301
    target 295
    measure 5
    diseases "Long_QT_syndrome"
    GOmeasure 5
    id 2064
    name "2064"
  ]
  edge
  [
    source 300
    target 295
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2065
    name "2065"
  ]
  edge
  [
    source 299
    target 295
    measure 7
    diseases "Long_QT_syndrome"
    GOmeasure 7
    id 2066
    name "2066"
  ]
  edge
  [
    source 298
    target 295
    measure 4
    diseases "Long_QT_syndrome"
    GOmeasure 4
    id 2067
    name "2067"
  ]
  edge
  [
    source 297
    target 295
    measure 7
    diseases "Long_QT_syndrome"
    GOmeasure 7
    id 2068
    name "2068"
  ]
  edge
  [
    source 296
    target 295
    measure 6
    diseases "Long_QT_syndrome"
    GOmeasure 6
    id 2069
    name "2069"
  ]
  edge
  [
    source 679
    target 294
    measure 5
    diseases "Elliptocytosis"
    GOmeasure 5
    id 2070
    name "2070"
  ]
  edge
  [
    source 294
    target 293
    measure 6
    diseases "Spherocytosis"
    GOmeasure 6
    id 2071
    name "2071"
  ]
  edge
  [
    source 294
    target 292
    measure 6
    diseases "Spherocytosis"
    GOmeasure 6
    id 2072
    name "2072"
  ]
  edge
  [
    source 293
    target 292
    measure 5
    diseases "Spherocytosis"
    GOmeasure 5
    id 2073
    name "2073"
  ]
  edge
  [
    source 406
    target 290
    measure 6
    diseases "Ichthyosis"
    GOmeasure 6
    id 2074
    name "2074"
  ]
  edge
  [
    source 405
    target 290
    measure 5
    diseases "Ichthyosis"
    GOmeasure 5
    id 2075
    name "2075"
  ]
  edge
  [
    source 291
    target 290
    measure 5
    diseases "Ichthyosiform_erythroderma"
    GOmeasure 5
    id 2076
    name "2076"
  ]
  edge
  [
    source 291
    target 289
    measure 5
    diseases "Ichthyosiform_erythroderma"
    GOmeasure 5
    id 2077
    name "2077"
  ]
  edge
  [
    source 290
    target 289
    measure 7
    diseases "Ichthyosiform_erythroderma"
    GOmeasure 7
    id 2078
    name "2078"
  ]
  edge
  [
    source 552
    target 286
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 2079
    name "2079"
  ]
  edge
  [
    source 551
    target 286
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 2080
    name "2080"
  ]
  edge
  [
    source 550
    target 286
    measure 7
    diseases "Thrombophilia"
    GOmeasure 7
    id 2081
    name "2081"
  ]
  edge
  [
    source 549
    target 286
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 2082
    name "2082"
  ]
  edge
  [
    source 548
    target 286
    measure 5
    diseases "Thrombophilia"
    GOmeasure 5
    id 2083
    name "2083"
  ]
  edge
  [
    source 547
    target 286
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 2084
    name "2084"
  ]
  edge
  [
    source 545
    target 286
    measure 6
    diseases "Thrombophilia"
    GOmeasure 6
    id 2085
    name "2085"
  ]
  edge
  [
    source 287
    target 286
    measure 7
    diseases "Myocardial_infarction"
    GOmeasure 7
    id 2086
    name "2086"
  ]
  edge
  [
    source 287
    target 285
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2087
    name "2087"
  ]
  edge
  [
    source 286
    target 285
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2088
    name "2088"
  ]
  edge
  [
    source 287
    target 284
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2089
    name "2089"
  ]
  edge
  [
    source 286
    target 284
    measure 4
    diseases "Myocardial_infarction"
    GOmeasure 4
    id 2090
    name "2090"
  ]
  edge
  [
    source 285
    target 284
    measure 4
    diseases "Myocardial_infarction"
    GOmeasure 4
    id 2091
    name "2091"
  ]
  edge
  [
    source 287
    target 283
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2092
    name "2092"
  ]
  edge
  [
    source 286
    target 283
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2093
    name "2093"
  ]
  edge
  [
    source 285
    target 283
    measure 4
    diseases "Myocardial_infarction"
    GOmeasure 4
    id 2094
    name "2094"
  ]
  edge
  [
    source 284
    target 283
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2095
    name "2095"
  ]
  edge
  [
    source 691
    target 282
    measure 7
    diseases "Factor_x_deficiency"
    GOmeasure 7
    id 2096
    name "2096"
  ]
  edge
  [
    source 690
    target 282
    measure 4
    diseases "Factor_x_deficiency"
    GOmeasure 4
    id 2097
    name "2097"
  ]
  edge
  [
    source 689
    target 282
    measure 6
    diseases "Factor_x_deficiency"
    GOmeasure 6
    id 2098
    name "2098"
  ]
  edge
  [
    source 688
    target 282
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 2099
    name "2099"
  ]
  edge
  [
    source 687
    target 282
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 2100
    name "2100"
  ]
  edge
  [
    source 686
    target 282
    measure 7
    diseases "Factor_x_deficiency"
    GOmeasure 7
    id 2101
    name "2101"
  ]
  edge
  [
    source 685
    target 282
    measure 5
    diseases "Factor_x_deficiency"
    GOmeasure 5
    id 2102
    name "2102"
  ]
  edge
  [
    source 287
    target 282
    measure 7
    diseases "Myocardial_infarction"
    GOmeasure 7
    id 2103
    name "2103"
  ]
  edge
  [
    source 286
    target 282
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2104
    name "2104"
  ]
  edge
  [
    source 285
    target 282
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2105
    name "2105"
  ]
  edge
  [
    source 284
    target 282
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2106
    name "2106"
  ]
  edge
  [
    source 283
    target 282
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2107
    name "2107"
  ]
  edge
  [
    source 288
    target 281
    measure 5
    diseases "Stroke"
    GOmeasure 5
    id 2108
    name "2108"
  ]
  edge
  [
    source 287
    target 281
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2109
    name "2109"
  ]
  edge
  [
    source 286
    target 281
    measure 4
    diseases "Myocardial_infarction"
    GOmeasure 4
    id 2110
    name "2110"
  ]
  edge
  [
    source 285
    target 281
    measure 4
    diseases "Myocardial_infarction"
    GOmeasure 4
    id 2111
    name "2111"
  ]
  edge
  [
    source 284
    target 281
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2112
    name "2112"
  ]
  edge
  [
    source 283
    target 281
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 2113
    name "2113"
  ]
  edge
  [
    source 282
    target 281
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 2114
    name "2114"
  ]
  edge
  [
    source 280
    target 279
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2115
    name "2115"
  ]
  edge
  [
    source 280
    target 278
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2116
    name "2116"
  ]
  edge
  [
    source 279
    target 278
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2117
    name "2117"
  ]
  edge
  [
    source 280
    target 277
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2118
    name "2118"
  ]
  edge
  [
    source 279
    target 277
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2119
    name "2119"
  ]
  edge
  [
    source 278
    target 277
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2120
    name "2120"
  ]
  edge
  [
    source 280
    target 276
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2121
    name "2121"
  ]
  edge
  [
    source 279
    target 276
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2122
    name "2122"
  ]
  edge
  [
    source 278
    target 276
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2123
    name "2123"
  ]
  edge
  [
    source 277
    target 276
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2124
    name "2124"
  ]
  edge
  [
    source 280
    target 275
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2125
    name "2125"
  ]
  edge
  [
    source 279
    target 275
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2126
    name "2126"
  ]
  edge
  [
    source 278
    target 275
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2127
    name "2127"
  ]
  edge
  [
    source 277
    target 275
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2128
    name "2128"
  ]
  edge
  [
    source 276
    target 275
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2129
    name "2129"
  ]
  edge
  [
    source 280
    target 274
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2130
    name "2130"
  ]
  edge
  [
    source 279
    target 274
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2131
    name "2131"
  ]
  edge
  [
    source 278
    target 274
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2132
    name "2132"
  ]
  edge
  [
    source 277
    target 274
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2133
    name "2133"
  ]
  edge
  [
    source 276
    target 274
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2134
    name "2134"
  ]
  edge
  [
    source 275
    target 274
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2135
    name "2135"
  ]
  edge
  [
    source 280
    target 273
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2136
    name "2136"
  ]
  edge
  [
    source 279
    target 273
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2137
    name "2137"
  ]
  edge
  [
    source 278
    target 273
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2138
    name "2138"
  ]
  edge
  [
    source 277
    target 273
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2139
    name "2139"
  ]
  edge
  [
    source 276
    target 273
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2140
    name "2140"
  ]
  edge
  [
    source 275
    target 273
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2141
    name "2141"
  ]
  edge
  [
    source 274
    target 273
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2142
    name "2142"
  ]
  edge
  [
    source 280
    target 272
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2143
    name "2143"
  ]
  edge
  [
    source 279
    target 272
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2144
    name "2144"
  ]
  edge
  [
    source 278
    target 272
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2145
    name "2145"
  ]
  edge
  [
    source 277
    target 272
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2146
    name "2146"
  ]
  edge
  [
    source 276
    target 272
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2147
    name "2147"
  ]
  edge
  [
    source 275
    target 272
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2148
    name "2148"
  ]
  edge
  [
    source 274
    target 272
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2149
    name "2149"
  ]
  edge
  [
    source 273
    target 272
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2150
    name "2150"
  ]
  edge
  [
    source 280
    target 271
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2151
    name "2151"
  ]
  edge
  [
    source 279
    target 271
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2152
    name "2152"
  ]
  edge
  [
    source 278
    target 271
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2153
    name "2153"
  ]
  edge
  [
    source 277
    target 271
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2154
    name "2154"
  ]
  edge
  [
    source 276
    target 271
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2155
    name "2155"
  ]
  edge
  [
    source 275
    target 271
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2156
    name "2156"
  ]
  edge
  [
    source 274
    target 271
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2157
    name "2157"
  ]
  edge
  [
    source 273
    target 271
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2158
    name "2158"
  ]
  edge
  [
    source 272
    target 271
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2159
    name "2159"
  ]
  edge
  [
    source 707
    target 270
    measure 5
    diseases "MODY"
    GOmeasure 5
    id 2160
    name "2160"
  ]
  edge
  [
    source 280
    target 270
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2161
    name "2161"
  ]
  edge
  [
    source 279
    target 270
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2162
    name "2162"
  ]
  edge
  [
    source 278
    target 270
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2163
    name "2163"
  ]
  edge
  [
    source 277
    target 270
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2164
    name "2164"
  ]
  edge
  [
    source 276
    target 270
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2165
    name "2165"
  ]
  edge
  [
    source 275
    target 270
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2166
    name "2166"
  ]
  edge
  [
    source 274
    target 270
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2167
    name "2167"
  ]
  edge
  [
    source 273
    target 270
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2168
    name "2168"
  ]
  edge
  [
    source 272
    target 270
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2169
    name "2169"
  ]
  edge
  [
    source 271
    target 270
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2170
    name "2170"
  ]
  edge
  [
    source 280
    target 269
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2171
    name "2171"
  ]
  edge
  [
    source 279
    target 269
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2172
    name "2172"
  ]
  edge
  [
    source 278
    target 269
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2173
    name "2173"
  ]
  edge
  [
    source 277
    target 269
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2174
    name "2174"
  ]
  edge
  [
    source 276
    target 269
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2175
    name "2175"
  ]
  edge
  [
    source 275
    target 269
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2176
    name "2176"
  ]
  edge
  [
    source 274
    target 269
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2177
    name "2177"
  ]
  edge
  [
    source 273
    target 269
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2178
    name "2178"
  ]
  edge
  [
    source 272
    target 269
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2179
    name "2179"
  ]
  edge
  [
    source 271
    target 269
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2180
    name "2180"
  ]
  edge
  [
    source 270
    target 269
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2181
    name "2181"
  ]
  edge
  [
    source 280
    target 268
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2182
    name "2182"
  ]
  edge
  [
    source 279
    target 268
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2183
    name "2183"
  ]
  edge
  [
    source 278
    target 268
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2184
    name "2184"
  ]
  edge
  [
    source 277
    target 268
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2185
    name "2185"
  ]
  edge
  [
    source 276
    target 268
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2186
    name "2186"
  ]
  edge
  [
    source 275
    target 268
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2187
    name "2187"
  ]
  edge
  [
    source 274
    target 268
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2188
    name "2188"
  ]
  edge
  [
    source 273
    target 268
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2189
    name "2189"
  ]
  edge
  [
    source 272
    target 268
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2190
    name "2190"
  ]
  edge
  [
    source 271
    target 268
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2191
    name "2191"
  ]
  edge
  [
    source 270
    target 268
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2192
    name "2192"
  ]
  edge
  [
    source 269
    target 268
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2193
    name "2193"
  ]
  edge
  [
    source 707
    target 267
    measure 5
    diseases "MODY"
    GOmeasure 5
    id 2194
    name "2194"
  ]
  edge
  [
    source 280
    target 267
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2195
    name "2195"
  ]
  edge
  [
    source 279
    target 267
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2196
    name "2196"
  ]
  edge
  [
    source 278
    target 267
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2197
    name "2197"
  ]
  edge
  [
    source 277
    target 267
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2198
    name "2198"
  ]
  edge
  [
    source 276
    target 267
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2199
    name "2199"
  ]
  edge
  [
    source 275
    target 267
    measure 8
    diseases "Diabetes_mellitus"
    GOmeasure 8
    id 2200
    name "2200"
  ]
  edge
  [
    source 274
    target 267
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2201
    name "2201"
  ]
  edge
  [
    source 273
    target 267
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2202
    name "2202"
  ]
  edge
  [
    source 272
    target 267
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2203
    name "2203"
  ]
  edge
  [
    source 271
    target 267
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2204
    name "2204"
  ]
  edge
  [
    source 270
    target 267
    measure 7
    diseases "Diabetes_mellitus,MODY"
    GOmeasure 7
    id 2205
    name "2205"
  ]
  edge
  [
    source 269
    target 267
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2206
    name "2206"
  ]
  edge
  [
    source 268
    target 267
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2207
    name "2207"
  ]
  edge
  [
    source 280
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2208
    name "2208"
  ]
  edge
  [
    source 279
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2209
    name "2209"
  ]
  edge
  [
    source 278
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2210
    name "2210"
  ]
  edge
  [
    source 277
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2211
    name "2211"
  ]
  edge
  [
    source 276
    target 266
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2212
    name "2212"
  ]
  edge
  [
    source 275
    target 266
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2213
    name "2213"
  ]
  edge
  [
    source 274
    target 266
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2214
    name "2214"
  ]
  edge
  [
    source 273
    target 266
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2215
    name "2215"
  ]
  edge
  [
    source 272
    target 266
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2216
    name "2216"
  ]
  edge
  [
    source 271
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2217
    name "2217"
  ]
  edge
  [
    source 270
    target 266
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2218
    name "2218"
  ]
  edge
  [
    source 269
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2219
    name "2219"
  ]
  edge
  [
    source 268
    target 266
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2220
    name "2220"
  ]
  edge
  [
    source 267
    target 266
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2221
    name "2221"
  ]
  edge
  [
    source 541
    target 265
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 2222
    name "2222"
  ]
  edge
  [
    source 540
    target 265
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 2223
    name "2223"
  ]
  edge
  [
    source 539
    target 265
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 2224
    name "2224"
  ]
  edge
  [
    source 538
    target 265
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 2225
    name "2225"
  ]
  edge
  [
    source 537
    target 265
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 2226
    name "2226"
  ]
  edge
  [
    source 536
    target 265
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 2227
    name "2227"
  ]
  edge
  [
    source 378
    target 265
    measure 6
    diseases "Systemic_lupus_erythematosus"
    GOmeasure 6
    id 2228
    name "2228"
  ]
  edge
  [
    source 377
    target 265
    measure 7
    diseases "Systemic_lupus_erythematosus"
    GOmeasure 7
    id 2229
    name "2229"
  ]
  edge
  [
    source 376
    target 265
    measure 5
    diseases "Systemic_lupus_erythematosus"
    GOmeasure 5
    id 2230
    name "2230"
  ]
  edge
  [
    source 280
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2231
    name "2231"
  ]
  edge
  [
    source 279
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2232
    name "2232"
  ]
  edge
  [
    source 278
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2233
    name "2233"
  ]
  edge
  [
    source 277
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2234
    name "2234"
  ]
  edge
  [
    source 276
    target 265
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2235
    name "2235"
  ]
  edge
  [
    source 275
    target 265
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2236
    name "2236"
  ]
  edge
  [
    source 274
    target 265
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2237
    name "2237"
  ]
  edge
  [
    source 273
    target 265
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2238
    name "2238"
  ]
  edge
  [
    source 272
    target 265
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2239
    name "2239"
  ]
  edge
  [
    source 271
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2240
    name "2240"
  ]
  edge
  [
    source 270
    target 265
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2241
    name "2241"
  ]
  edge
  [
    source 269
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2242
    name "2242"
  ]
  edge
  [
    source 268
    target 265
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2243
    name "2243"
  ]
  edge
  [
    source 267
    target 265
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2244
    name "2244"
  ]
  edge
  [
    source 266
    target 265
    measure 8
    diseases "Diabetes_mellitus"
    GOmeasure 8
    id 2245
    name "2245"
  ]
  edge
  [
    source 280
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2246
    name "2246"
  ]
  edge
  [
    source 279
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2247
    name "2247"
  ]
  edge
  [
    source 278
    target 264
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2248
    name "2248"
  ]
  edge
  [
    source 277
    target 264
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2249
    name "2249"
  ]
  edge
  [
    source 276
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2250
    name "2250"
  ]
  edge
  [
    source 275
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2251
    name "2251"
  ]
  edge
  [
    source 274
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2252
    name "2252"
  ]
  edge
  [
    source 273
    target 264
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2253
    name "2253"
  ]
  edge
  [
    source 272
    target 264
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2254
    name "2254"
  ]
  edge
  [
    source 271
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2255
    name "2255"
  ]
  edge
  [
    source 270
    target 264
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2256
    name "2256"
  ]
  edge
  [
    source 269
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2257
    name "2257"
  ]
  edge
  [
    source 268
    target 264
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2258
    name "2258"
  ]
  edge
  [
    source 267
    target 264
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2259
    name "2259"
  ]
  edge
  [
    source 266
    target 264
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2260
    name "2260"
  ]
  edge
  [
    source 265
    target 264
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2261
    name "2261"
  ]
  edge
  [
    source 280
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2262
    name "2262"
  ]
  edge
  [
    source 279
    target 263
    measure 8
    diseases "Diabetes_mellitus"
    GOmeasure 8
    id 2263
    name "2263"
  ]
  edge
  [
    source 278
    target 263
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2264
    name "2264"
  ]
  edge
  [
    source 277
    target 263
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2265
    name "2265"
  ]
  edge
  [
    source 276
    target 263
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2266
    name "2266"
  ]
  edge
  [
    source 275
    target 263
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2267
    name "2267"
  ]
  edge
  [
    source 274
    target 263
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2268
    name "2268"
  ]
  edge
  [
    source 273
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2269
    name "2269"
  ]
  edge
  [
    source 272
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2270
    name "2270"
  ]
  edge
  [
    source 271
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2271
    name "2271"
  ]
  edge
  [
    source 270
    target 263
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2272
    name "2272"
  ]
  edge
  [
    source 269
    target 263
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2273
    name "2273"
  ]
  edge
  [
    source 268
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2274
    name "2274"
  ]
  edge
  [
    source 267
    target 263
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2275
    name "2275"
  ]
  edge
  [
    source 266
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2276
    name "2276"
  ]
  edge
  [
    source 265
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2277
    name "2277"
  ]
  edge
  [
    source 264
    target 263
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2278
    name "2278"
  ]
  edge
  [
    source 280
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2279
    name "2279"
  ]
  edge
  [
    source 279
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2280
    name "2280"
  ]
  edge
  [
    source 278
    target 262
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2281
    name "2281"
  ]
  edge
  [
    source 277
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2282
    name "2282"
  ]
  edge
  [
    source 276
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2283
    name "2283"
  ]
  edge
  [
    source 275
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2284
    name "2284"
  ]
  edge
  [
    source 274
    target 262
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2285
    name "2285"
  ]
  edge
  [
    source 273
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2286
    name "2286"
  ]
  edge
  [
    source 272
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2287
    name "2287"
  ]
  edge
  [
    source 271
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2288
    name "2288"
  ]
  edge
  [
    source 270
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2289
    name "2289"
  ]
  edge
  [
    source 269
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2290
    name "2290"
  ]
  edge
  [
    source 268
    target 262
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2291
    name "2291"
  ]
  edge
  [
    source 267
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2292
    name "2292"
  ]
  edge
  [
    source 266
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2293
    name "2293"
  ]
  edge
  [
    source 265
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2294
    name "2294"
  ]
  edge
  [
    source 264
    target 262
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2295
    name "2295"
  ]
  edge
  [
    source 263
    target 262
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2296
    name "2296"
  ]
  edge
  [
    source 261
    target 260
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2297
    name "2297"
  ]
  edge
  [
    source 261
    target 259
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2298
    name "2298"
  ]
  edge
  [
    source 260
    target 259
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2299
    name "2299"
  ]
  edge
  [
    source 261
    target 258
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2300
    name "2300"
  ]
  edge
  [
    source 260
    target 258
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2301
    name "2301"
  ]
  edge
  [
    source 259
    target 258
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2302
    name "2302"
  ]
  edge
  [
    source 261
    target 257
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2303
    name "2303"
  ]
  edge
  [
    source 260
    target 257
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2304
    name "2304"
  ]
  edge
  [
    source 259
    target 257
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2305
    name "2305"
  ]
  edge
  [
    source 258
    target 257
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2306
    name "2306"
  ]
  edge
  [
    source 261
    target 256
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2307
    name "2307"
  ]
  edge
  [
    source 260
    target 256
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2308
    name "2308"
  ]
  edge
  [
    source 259
    target 256
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2309
    name "2309"
  ]
  edge
  [
    source 258
    target 256
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2310
    name "2310"
  ]
  edge
  [
    source 257
    target 256
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2311
    name "2311"
  ]
  edge
  [
    source 261
    target 255
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2312
    name "2312"
  ]
  edge
  [
    source 260
    target 255
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2313
    name "2313"
  ]
  edge
  [
    source 259
    target 255
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2314
    name "2314"
  ]
  edge
  [
    source 258
    target 255
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2315
    name "2315"
  ]
  edge
  [
    source 257
    target 255
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2316
    name "2316"
  ]
  edge
  [
    source 256
    target 255
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2317
    name "2317"
  ]
  edge
  [
    source 261
    target 254
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2318
    name "2318"
  ]
  edge
  [
    source 260
    target 254
    measure 7
    diseases "Hemolytic_anemia"
    GOmeasure 7
    id 2319
    name "2319"
  ]
  edge
  [
    source 259
    target 254
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2320
    name "2320"
  ]
  edge
  [
    source 258
    target 254
    measure 7
    diseases "Hemolytic_anemia"
    GOmeasure 7
    id 2321
    name "2321"
  ]
  edge
  [
    source 257
    target 254
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2322
    name "2322"
  ]
  edge
  [
    source 256
    target 254
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2323
    name "2323"
  ]
  edge
  [
    source 255
    target 254
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2324
    name "2324"
  ]
  edge
  [
    source 261
    target 253
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2325
    name "2325"
  ]
  edge
  [
    source 260
    target 253
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 2326
    name "2326"
  ]
  edge
  [
    source 259
    target 253
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2327
    name "2327"
  ]
  edge
  [
    source 258
    target 253
    measure 7
    diseases "Hemolytic_anemia"
    GOmeasure 7
    id 2328
    name "2328"
  ]
  edge
  [
    source 257
    target 253
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2329
    name "2329"
  ]
  edge
  [
    source 256
    target 253
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 2330
    name "2330"
  ]
  edge
  [
    source 255
    target 253
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 2331
    name "2331"
  ]
  edge
  [
    source 254
    target 253
    measure 7
    diseases "Hemolytic_anemia"
    GOmeasure 7
    id 2332
    name "2332"
  ]
  edge
  [
    source 252
    target 251
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2333
    name "2333"
  ]
  edge
  [
    source 252
    target 250
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2334
    name "2334"
  ]
  edge
  [
    source 251
    target 250
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2335
    name "2335"
  ]
  edge
  [
    source 252
    target 249
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2336
    name "2336"
  ]
  edge
  [
    source 251
    target 249
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2337
    name "2337"
  ]
  edge
  [
    source 250
    target 249
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2338
    name "2338"
  ]
  edge
  [
    source 252
    target 248
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2339
    name "2339"
  ]
  edge
  [
    source 251
    target 248
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2340
    name "2340"
  ]
  edge
  [
    source 250
    target 248
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2341
    name "2341"
  ]
  edge
  [
    source 249
    target 248
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2342
    name "2342"
  ]
  edge
  [
    source 252
    target 247
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2343
    name "2343"
  ]
  edge
  [
    source 251
    target 247
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2344
    name "2344"
  ]
  edge
  [
    source 250
    target 247
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2345
    name "2345"
  ]
  edge
  [
    source 249
    target 247
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2346
    name "2346"
  ]
  edge
  [
    source 248
    target 247
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2347
    name "2347"
  ]
  edge
  [
    source 252
    target 246
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2348
    name "2348"
  ]
  edge
  [
    source 251
    target 246
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2349
    name "2349"
  ]
  edge
  [
    source 250
    target 246
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2350
    name "2350"
  ]
  edge
  [
    source 249
    target 246
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2351
    name "2351"
  ]
  edge
  [
    source 248
    target 246
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2352
    name "2352"
  ]
  edge
  [
    source 247
    target 246
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2353
    name "2353"
  ]
  edge
  [
    source 252
    target 245
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2354
    name "2354"
  ]
  edge
  [
    source 251
    target 245
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2355
    name "2355"
  ]
  edge
  [
    source 250
    target 245
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2356
    name "2356"
  ]
  edge
  [
    source 249
    target 245
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2357
    name "2357"
  ]
  edge
  [
    source 248
    target 245
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2358
    name "2358"
  ]
  edge
  [
    source 247
    target 245
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2359
    name "2359"
  ]
  edge
  [
    source 246
    target 245
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2360
    name "2360"
  ]
  edge
  [
    source 252
    target 244
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2361
    name "2361"
  ]
  edge
  [
    source 251
    target 244
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2362
    name "2362"
  ]
  edge
  [
    source 250
    target 244
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2363
    name "2363"
  ]
  edge
  [
    source 249
    target 244
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2364
    name "2364"
  ]
  edge
  [
    source 248
    target 244
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2365
    name "2365"
  ]
  edge
  [
    source 247
    target 244
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2366
    name "2366"
  ]
  edge
  [
    source 246
    target 244
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2367
    name "2367"
  ]
  edge
  [
    source 245
    target 244
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2368
    name "2368"
  ]
  edge
  [
    source 252
    target 243
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2369
    name "2369"
  ]
  edge
  [
    source 251
    target 243
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2370
    name "2370"
  ]
  edge
  [
    source 250
    target 243
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2371
    name "2371"
  ]
  edge
  [
    source 249
    target 243
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2372
    name "2372"
  ]
  edge
  [
    source 248
    target 243
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2373
    name "2373"
  ]
  edge
  [
    source 247
    target 243
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2374
    name "2374"
  ]
  edge
  [
    source 246
    target 243
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2375
    name "2375"
  ]
  edge
  [
    source 245
    target 243
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2376
    name "2376"
  ]
  edge
  [
    source 244
    target 243
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2377
    name "2377"
  ]
  edge
  [
    source 252
    target 242
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2378
    name "2378"
  ]
  edge
  [
    source 251
    target 242
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2379
    name "2379"
  ]
  edge
  [
    source 250
    target 242
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2380
    name "2380"
  ]
  edge
  [
    source 249
    target 242
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2381
    name "2381"
  ]
  edge
  [
    source 248
    target 242
    measure 8
    diseases "Mental_retardation"
    GOmeasure 8
    id 2382
    name "2382"
  ]
  edge
  [
    source 247
    target 242
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2383
    name "2383"
  ]
  edge
  [
    source 246
    target 242
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2384
    name "2384"
  ]
  edge
  [
    source 245
    target 242
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2385
    name "2385"
  ]
  edge
  [
    source 244
    target 242
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2386
    name "2386"
  ]
  edge
  [
    source 243
    target 242
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2387
    name "2387"
  ]
  edge
  [
    source 252
    target 241
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2388
    name "2388"
  ]
  edge
  [
    source 251
    target 241
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2389
    name "2389"
  ]
  edge
  [
    source 250
    target 241
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2390
    name "2390"
  ]
  edge
  [
    source 249
    target 241
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2391
    name "2391"
  ]
  edge
  [
    source 248
    target 241
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2392
    name "2392"
  ]
  edge
  [
    source 247
    target 241
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2393
    name "2393"
  ]
  edge
  [
    source 246
    target 241
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2394
    name "2394"
  ]
  edge
  [
    source 245
    target 241
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2395
    name "2395"
  ]
  edge
  [
    source 244
    target 241
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2396
    name "2396"
  ]
  edge
  [
    source 243
    target 241
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2397
    name "2397"
  ]
  edge
  [
    source 242
    target 241
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2398
    name "2398"
  ]
  edge
  [
    source 252
    target 240
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2399
    name "2399"
  ]
  edge
  [
    source 251
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2400
    name "2400"
  ]
  edge
  [
    source 250
    target 240
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2401
    name "2401"
  ]
  edge
  [
    source 249
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2402
    name "2402"
  ]
  edge
  [
    source 248
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2403
    name "2403"
  ]
  edge
  [
    source 247
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2404
    name "2404"
  ]
  edge
  [
    source 246
    target 240
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2405
    name "2405"
  ]
  edge
  [
    source 245
    target 240
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2406
    name "2406"
  ]
  edge
  [
    source 244
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2407
    name "2407"
  ]
  edge
  [
    source 243
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2408
    name "2408"
  ]
  edge
  [
    source 242
    target 240
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2409
    name "2409"
  ]
  edge
  [
    source 241
    target 240
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2410
    name "2410"
  ]
  edge
  [
    source 706
    target 239
    measure 6
    diseases "Myoclonic_epilepsy"
    GOmeasure 6
    id 2411
    name "2411"
  ]
  edge
  [
    source 643
    target 239
    measure 5
    diseases "Lissencephaly"
    GOmeasure 5
    id 2412
    name "2412"
  ]
  edge
  [
    source 642
    target 239
    measure 4
    diseases "Lissencephaly"
    GOmeasure 4
    id 2413
    name "2413"
  ]
  edge
  [
    source 641
    target 239
    measure 6
    diseases "Lissencephaly"
    GOmeasure 6
    id 2414
    name "2414"
  ]
  edge
  [
    source 497
    target 239
    measure 5
    diseases "Myoclonic_epilepsy"
    GOmeasure 5
    id 2415
    name "2415"
  ]
  edge
  [
    source 252
    target 239
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2416
    name "2416"
  ]
  edge
  [
    source 251
    target 239
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2417
    name "2417"
  ]
  edge
  [
    source 250
    target 239
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2418
    name "2418"
  ]
  edge
  [
    source 249
    target 239
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2419
    name "2419"
  ]
  edge
  [
    source 248
    target 239
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2420
    name "2420"
  ]
  edge
  [
    source 247
    target 239
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2421
    name "2421"
  ]
  edge
  [
    source 246
    target 239
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2422
    name "2422"
  ]
  edge
  [
    source 245
    target 239
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2423
    name "2423"
  ]
  edge
  [
    source 244
    target 239
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2424
    name "2424"
  ]
  edge
  [
    source 243
    target 239
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2425
    name "2425"
  ]
  edge
  [
    source 242
    target 239
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2426
    name "2426"
  ]
  edge
  [
    source 241
    target 239
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2427
    name "2427"
  ]
  edge
  [
    source 240
    target 239
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2428
    name "2428"
  ]
  edge
  [
    source 252
    target 238
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2429
    name "2429"
  ]
  edge
  [
    source 251
    target 238
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2430
    name "2430"
  ]
  edge
  [
    source 250
    target 238
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2431
    name "2431"
  ]
  edge
  [
    source 249
    target 238
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2432
    name "2432"
  ]
  edge
  [
    source 248
    target 238
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2433
    name "2433"
  ]
  edge
  [
    source 247
    target 238
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2434
    name "2434"
  ]
  edge
  [
    source 246
    target 238
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2435
    name "2435"
  ]
  edge
  [
    source 245
    target 238
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2436
    name "2436"
  ]
  edge
  [
    source 244
    target 238
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2437
    name "2437"
  ]
  edge
  [
    source 243
    target 238
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2438
    name "2438"
  ]
  edge
  [
    source 242
    target 238
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2439
    name "2439"
  ]
  edge
  [
    source 241
    target 238
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2440
    name "2440"
  ]
  edge
  [
    source 240
    target 238
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2441
    name "2441"
  ]
  edge
  [
    source 239
    target 238
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2442
    name "2442"
  ]
  edge
  [
    source 252
    target 237
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2443
    name "2443"
  ]
  edge
  [
    source 251
    target 237
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2444
    name "2444"
  ]
  edge
  [
    source 250
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2445
    name "2445"
  ]
  edge
  [
    source 249
    target 237
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2446
    name "2446"
  ]
  edge
  [
    source 248
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2447
    name "2447"
  ]
  edge
  [
    source 247
    target 237
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2448
    name "2448"
  ]
  edge
  [
    source 246
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2449
    name "2449"
  ]
  edge
  [
    source 245
    target 237
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2450
    name "2450"
  ]
  edge
  [
    source 244
    target 237
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2451
    name "2451"
  ]
  edge
  [
    source 243
    target 237
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2452
    name "2452"
  ]
  edge
  [
    source 242
    target 237
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2453
    name "2453"
  ]
  edge
  [
    source 241
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2454
    name "2454"
  ]
  edge
  [
    source 240
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2455
    name "2455"
  ]
  edge
  [
    source 239
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2456
    name "2456"
  ]
  edge
  [
    source 238
    target 237
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2457
    name "2457"
  ]
  edge
  [
    source 748
    target 236
    measure 6
    diseases "Rett_syndrome"
    GOmeasure 6
    id 2458
    name "2458"
  ]
  edge
  [
    source 747
    target 236
    measure 6
    diseases "Angelman_syndrome"
    GOmeasure 6
    id 2459
    name "2459"
  ]
  edge
  [
    source 712
    target 236
    measure 6
    diseases "Autism"
    GOmeasure 6
    id 2460
    name "2460"
  ]
  edge
  [
    source 711
    target 236
    measure 4
    diseases "Autism"
    GOmeasure 4
    id 2461
    name "2461"
  ]
  edge
  [
    source 252
    target 236
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2462
    name "2462"
  ]
  edge
  [
    source 251
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2463
    name "2463"
  ]
  edge
  [
    source 250
    target 236
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2464
    name "2464"
  ]
  edge
  [
    source 249
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2465
    name "2465"
  ]
  edge
  [
    source 248
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2466
    name "2466"
  ]
  edge
  [
    source 247
    target 236
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2467
    name "2467"
  ]
  edge
  [
    source 246
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2468
    name "2468"
  ]
  edge
  [
    source 245
    target 236
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2469
    name "2469"
  ]
  edge
  [
    source 244
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2470
    name "2470"
  ]
  edge
  [
    source 243
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2471
    name "2471"
  ]
  edge
  [
    source 242
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2472
    name "2472"
  ]
  edge
  [
    source 241
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2473
    name "2473"
  ]
  edge
  [
    source 240
    target 236
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2474
    name "2474"
  ]
  edge
  [
    source 239
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2475
    name "2475"
  ]
  edge
  [
    source 238
    target 236
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2476
    name "2476"
  ]
  edge
  [
    source 237
    target 236
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2477
    name "2477"
  ]
  edge
  [
    source 712
    target 235
    measure 7
    diseases "Asperger_syndrome,Autism"
    GOmeasure 7
    id 2478
    name "2478"
  ]
  edge
  [
    source 711
    target 235
    measure 5
    diseases "Autism"
    GOmeasure 5
    id 2479
    name "2479"
  ]
  edge
  [
    source 252
    target 235
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2480
    name "2480"
  ]
  edge
  [
    source 251
    target 235
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2481
    name "2481"
  ]
  edge
  [
    source 250
    target 235
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2482
    name "2482"
  ]
  edge
  [
    source 249
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2483
    name "2483"
  ]
  edge
  [
    source 248
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2484
    name "2484"
  ]
  edge
  [
    source 247
    target 235
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2485
    name "2485"
  ]
  edge
  [
    source 246
    target 235
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2486
    name "2486"
  ]
  edge
  [
    source 245
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2487
    name "2487"
  ]
  edge
  [
    source 244
    target 235
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2488
    name "2488"
  ]
  edge
  [
    source 243
    target 235
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2489
    name "2489"
  ]
  edge
  [
    source 242
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2490
    name "2490"
  ]
  edge
  [
    source 241
    target 235
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2491
    name "2491"
  ]
  edge
  [
    source 240
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2492
    name "2492"
  ]
  edge
  [
    source 239
    target 235
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2493
    name "2493"
  ]
  edge
  [
    source 238
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2494
    name "2494"
  ]
  edge
  [
    source 237
    target 235
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2495
    name "2495"
  ]
  edge
  [
    source 236
    target 235
    measure 6
    diseases "Autism,Mental_retardation"
    GOmeasure 6
    id 2496
    name "2496"
  ]
  edge
  [
    source 252
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2497
    name "2497"
  ]
  edge
  [
    source 251
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2498
    name "2498"
  ]
  edge
  [
    source 250
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2499
    name "2499"
  ]
  edge
  [
    source 249
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2500
    name "2500"
  ]
  edge
  [
    source 248
    target 234
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2501
    name "2501"
  ]
  edge
  [
    source 247
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2502
    name "2502"
  ]
  edge
  [
    source 246
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2503
    name "2503"
  ]
  edge
  [
    source 245
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2504
    name "2504"
  ]
  edge
  [
    source 244
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2505
    name "2505"
  ]
  edge
  [
    source 243
    target 234
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2506
    name "2506"
  ]
  edge
  [
    source 242
    target 234
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2507
    name "2507"
  ]
  edge
  [
    source 241
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2508
    name "2508"
  ]
  edge
  [
    source 240
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2509
    name "2509"
  ]
  edge
  [
    source 239
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2510
    name "2510"
  ]
  edge
  [
    source 238
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2511
    name "2511"
  ]
  edge
  [
    source 237
    target 234
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2512
    name "2512"
  ]
  edge
  [
    source 236
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2513
    name "2513"
  ]
  edge
  [
    source 235
    target 234
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2514
    name "2514"
  ]
  edge
  [
    source 252
    target 233
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2515
    name "2515"
  ]
  edge
  [
    source 251
    target 233
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2516
    name "2516"
  ]
  edge
  [
    source 250
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2517
    name "2517"
  ]
  edge
  [
    source 249
    target 233
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2518
    name "2518"
  ]
  edge
  [
    source 248
    target 233
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2519
    name "2519"
  ]
  edge
  [
    source 247
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2520
    name "2520"
  ]
  edge
  [
    source 246
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2521
    name "2521"
  ]
  edge
  [
    source 245
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2522
    name "2522"
  ]
  edge
  [
    source 244
    target 233
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2523
    name "2523"
  ]
  edge
  [
    source 243
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2524
    name "2524"
  ]
  edge
  [
    source 242
    target 233
    measure 7
    diseases "Mental_retardation"
    GOmeasure 7
    id 2525
    name "2525"
  ]
  edge
  [
    source 241
    target 233
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2526
    name "2526"
  ]
  edge
  [
    source 240
    target 233
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2527
    name "2527"
  ]
  edge
  [
    source 239
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2528
    name "2528"
  ]
  edge
  [
    source 238
    target 233
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2529
    name "2529"
  ]
  edge
  [
    source 237
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2530
    name "2530"
  ]
  edge
  [
    source 236
    target 233
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2531
    name "2531"
  ]
  edge
  [
    source 235
    target 233
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2532
    name "2532"
  ]
  edge
  [
    source 234
    target 233
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2533
    name "2533"
  ]
  edge
  [
    source 252
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2534
    name "2534"
  ]
  edge
  [
    source 251
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2535
    name "2535"
  ]
  edge
  [
    source 250
    target 232
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2536
    name "2536"
  ]
  edge
  [
    source 249
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2537
    name "2537"
  ]
  edge
  [
    source 248
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2538
    name "2538"
  ]
  edge
  [
    source 247
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2539
    name "2539"
  ]
  edge
  [
    source 246
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2540
    name "2540"
  ]
  edge
  [
    source 245
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2541
    name "2541"
  ]
  edge
  [
    source 244
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2542
    name "2542"
  ]
  edge
  [
    source 243
    target 232
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2543
    name "2543"
  ]
  edge
  [
    source 242
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2544
    name "2544"
  ]
  edge
  [
    source 241
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2545
    name "2545"
  ]
  edge
  [
    source 240
    target 232
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2546
    name "2546"
  ]
  edge
  [
    source 239
    target 232
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2547
    name "2547"
  ]
  edge
  [
    source 238
    target 232
    measure 6
    diseases "Mental_retardation"
    GOmeasure 6
    id 2548
    name "2548"
  ]
  edge
  [
    source 237
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2549
    name "2549"
  ]
  edge
  [
    source 236
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2550
    name "2550"
  ]
  edge
  [
    source 235
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2551
    name "2551"
  ]
  edge
  [
    source 234
    target 232
    measure 5
    diseases "Mental_retardation"
    GOmeasure 5
    id 2552
    name "2552"
  ]
  edge
  [
    source 233
    target 232
    measure 4
    diseases "Mental_retardation"
    GOmeasure 4
    id 2553
    name "2553"
  ]
  edge
  [
    source 421
    target 230
    measure 5
    diseases "Hypercholanemia"
    GOmeasure 5
    id 2554
    name "2554"
  ]
  edge
  [
    source 420
    target 230
    measure 5
    diseases "Hypercholanemia"
    GOmeasure 5
    id 2555
    name "2555"
  ]
  edge
  [
    source 230
    target 229
    measure 6
    diseases "Preeclampsia"
    GOmeasure 6
    id 2556
    name "2556"
  ]
  edge
  [
    source 616
    target 228
    measure 5
    diseases "Atrioventricular_block"
    GOmeasure 5
    id 2557
    name "2557"
  ]
  edge
  [
    source 615
    target 228
    measure 7
    diseases "Atrioventricular_block"
    GOmeasure 7
    id 2558
    name "2558"
  ]
  edge
  [
    source 614
    target 228
    measure 6
    diseases "Atrial_fibrillation"
    GOmeasure 6
    id 2559
    name "2559"
  ]
  edge
  [
    source 300
    target 228
    measure 6
    diseases "Atrial_fibrillation"
    GOmeasure 6
    id 2560
    name "2560"
  ]
  edge
  [
    source 296
    target 228
    measure 6
    diseases "Atrial_fibrillation"
    GOmeasure 6
    id 2561
    name "2561"
  ]
  edge
  [
    source 228
    target 227
    measure 5
    diseases "Tetralogy_of_Fallot"
    GOmeasure 5
    id 2562
    name "2562"
  ]
  edge
  [
    source 226
    target 225
    measure 6
    diseases "Spondyloepiphyseal_dysplasia"
    GOmeasure 6
    id 2563
    name "2563"
  ]
  edge
  [
    source 226
    target 224
    measure 7
    diseases "Spondyloepiphyseal_dysplasia"
    GOmeasure 7
    id 2564
    name "2564"
  ]
  edge
  [
    source 225
    target 224
    measure 6
    diseases "Spondyloepiphyseal_dysplasia"
    GOmeasure 6
    id 2565
    name "2565"
  ]
  edge
  [
    source 595
    target 223
    measure 5
    diseases "Epiphyseal_dysplasia"
    GOmeasure 5
    id 2566
    name "2566"
  ]
  edge
  [
    source 594
    target 223
    measure 5
    diseases "Epiphyseal_dysplasia"
    GOmeasure 5
    id 2567
    name "2567"
  ]
  edge
  [
    source 593
    target 223
    measure 7
    diseases "Epiphyseal_dysplasia"
    GOmeasure 7
    id 2568
    name "2568"
  ]
  edge
  [
    source 592
    target 223
    measure 5
    diseases "Epiphyseal_dysplasia"
    GOmeasure 5
    id 2569
    name "2569"
  ]
  edge
  [
    source 591
    target 223
    measure 5
    diseases "Epiphyseal_dysplasia"
    GOmeasure 5
    id 2570
    name "2570"
  ]
  edge
  [
    source 579
    target 223
    measure 5
    diseases "Osteoarthritis"
    GOmeasure 5
    id 2571
    name "2571"
  ]
  edge
  [
    source 578
    target 223
    measure 5
    diseases "Osteoarthritis"
    GOmeasure 5
    id 2572
    name "2572"
  ]
  edge
  [
    source 226
    target 223
    measure 5
    diseases "Spondyloepiphyseal_dysplasia"
    GOmeasure 5
    id 2573
    name "2573"
  ]
  edge
  [
    source 225
    target 223
    measure 5
    diseases "Spondyloepiphyseal_dysplasia"
    GOmeasure 5
    id 2574
    name "2574"
  ]
  edge
  [
    source 224
    target 223
    measure 5
    diseases "Spondyloepiphyseal_dysplasia"
    GOmeasure 5
    id 2575
    name "2575"
  ]
  edge
  [
    source 222
    target 221
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2576
    name "2576"
  ]
  edge
  [
    source 222
    target 220
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2577
    name "2577"
  ]
  edge
  [
    source 221
    target 220
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2578
    name "2578"
  ]
  edge
  [
    source 222
    target 219
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2579
    name "2579"
  ]
  edge
  [
    source 221
    target 219
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2580
    name "2580"
  ]
  edge
  [
    source 220
    target 219
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2581
    name "2581"
  ]
  edge
  [
    source 280
    target 218
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2582
    name "2582"
  ]
  edge
  [
    source 279
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2583
    name "2583"
  ]
  edge
  [
    source 278
    target 218
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2584
    name "2584"
  ]
  edge
  [
    source 277
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2585
    name "2585"
  ]
  edge
  [
    source 276
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2586
    name "2586"
  ]
  edge
  [
    source 275
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2587
    name "2587"
  ]
  edge
  [
    source 274
    target 218
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2588
    name "2588"
  ]
  edge
  [
    source 273
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2589
    name "2589"
  ]
  edge
  [
    source 272
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2590
    name "2590"
  ]
  edge
  [
    source 271
    target 218
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2591
    name "2591"
  ]
  edge
  [
    source 270
    target 218
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2592
    name "2592"
  ]
  edge
  [
    source 269
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2593
    name "2593"
  ]
  edge
  [
    source 268
    target 218
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2594
    name "2594"
  ]
  edge
  [
    source 267
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2595
    name "2595"
  ]
  edge
  [
    source 266
    target 218
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2596
    name "2596"
  ]
  edge
  [
    source 265
    target 218
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2597
    name "2597"
  ]
  edge
  [
    source 264
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2598
    name "2598"
  ]
  edge
  [
    source 263
    target 218
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2599
    name "2599"
  ]
  edge
  [
    source 262
    target 218
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2600
    name "2600"
  ]
  edge
  [
    source 222
    target 218
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2601
    name "2601"
  ]
  edge
  [
    source 221
    target 218
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2602
    name "2602"
  ]
  edge
  [
    source 220
    target 218
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2603
    name "2603"
  ]
  edge
  [
    source 219
    target 218
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2604
    name "2604"
  ]
  edge
  [
    source 222
    target 217
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2605
    name "2605"
  ]
  edge
  [
    source 221
    target 217
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2606
    name "2606"
  ]
  edge
  [
    source 220
    target 217
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2607
    name "2607"
  ]
  edge
  [
    source 219
    target 217
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2608
    name "2608"
  ]
  edge
  [
    source 218
    target 217
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2609
    name "2609"
  ]
  edge
  [
    source 222
    target 216
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2610
    name "2610"
  ]
  edge
  [
    source 221
    target 216
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2611
    name "2611"
  ]
  edge
  [
    source 220
    target 216
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2612
    name "2612"
  ]
  edge
  [
    source 219
    target 216
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2613
    name "2613"
  ]
  edge
  [
    source 218
    target 216
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2614
    name "2614"
  ]
  edge
  [
    source 217
    target 216
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2615
    name "2615"
  ]
  edge
  [
    source 222
    target 215
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2616
    name "2616"
  ]
  edge
  [
    source 221
    target 215
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2617
    name "2617"
  ]
  edge
  [
    source 220
    target 215
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2618
    name "2618"
  ]
  edge
  [
    source 219
    target 215
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2619
    name "2619"
  ]
  edge
  [
    source 218
    target 215
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2620
    name "2620"
  ]
  edge
  [
    source 217
    target 215
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2621
    name "2621"
  ]
  edge
  [
    source 216
    target 215
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2622
    name "2622"
  ]
  edge
  [
    source 222
    target 214
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2623
    name "2623"
  ]
  edge
  [
    source 221
    target 214
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2624
    name "2624"
  ]
  edge
  [
    source 220
    target 214
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2625
    name "2625"
  ]
  edge
  [
    source 219
    target 214
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2626
    name "2626"
  ]
  edge
  [
    source 218
    target 214
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2627
    name "2627"
  ]
  edge
  [
    source 217
    target 214
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2628
    name "2628"
  ]
  edge
  [
    source 216
    target 214
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2629
    name "2629"
  ]
  edge
  [
    source 215
    target 214
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2630
    name "2630"
  ]
  edge
  [
    source 785
    target 213
    measure 6
    diseases "Insulin_resistance"
    GOmeasure 6
    id 2631
    name "2631"
  ]
  edge
  [
    source 784
    target 213
    measure 5
    diseases "Insulin_resistance"
    GOmeasure 5
    id 2632
    name "2632"
  ]
  edge
  [
    source 740
    target 213
    measure 4
    diseases "Lipodystrophy"
    GOmeasure 4
    id 2633
    name "2633"
  ]
  edge
  [
    source 739
    target 213
    measure 6
    diseases "Lipodystrophy"
    GOmeasure 6
    id 2634
    name "2634"
  ]
  edge
  [
    source 644
    target 213
    measure 4
    diseases "Glioblastoma"
    GOmeasure 4
    id 2635
    name "2635"
  ]
  edge
  [
    source 507
    target 213
    measure 5
    diseases "Glioblastoma"
    GOmeasure 5
    id 2636
    name "2636"
  ]
  edge
  [
    source 401
    target 213
    measure 5
    diseases "Lipodystrophy"
    GOmeasure 5
    id 2637
    name "2637"
  ]
  edge
  [
    source 339
    target 213
    measure 6
    diseases "Glioblastoma"
    GOmeasure 6
    id 2638
    name "2638"
  ]
  edge
  [
    source 323
    target 213
    measure 5
    diseases "Glioblastoma"
    GOmeasure 5
    id 2639
    name "2639"
  ]
  edge
  [
    source 280
    target 213
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2640
    name "2640"
  ]
  edge
  [
    source 279
    target 213
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2641
    name "2641"
  ]
  edge
  [
    source 278
    target 213
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2642
    name "2642"
  ]
  edge
  [
    source 277
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2643
    name "2643"
  ]
  edge
  [
    source 276
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2644
    name "2644"
  ]
  edge
  [
    source 275
    target 213
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2645
    name "2645"
  ]
  edge
  [
    source 274
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2646
    name "2646"
  ]
  edge
  [
    source 273
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2647
    name "2647"
  ]
  edge
  [
    source 272
    target 213
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2648
    name "2648"
  ]
  edge
  [
    source 271
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2649
    name "2649"
  ]
  edge
  [
    source 270
    target 213
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2650
    name "2650"
  ]
  edge
  [
    source 269
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2651
    name "2651"
  ]
  edge
  [
    source 268
    target 213
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2652
    name "2652"
  ]
  edge
  [
    source 267
    target 213
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2653
    name "2653"
  ]
  edge
  [
    source 266
    target 213
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2654
    name "2654"
  ]
  edge
  [
    source 265
    target 213
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2655
    name "2655"
  ]
  edge
  [
    source 264
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2656
    name "2656"
  ]
  edge
  [
    source 263
    target 213
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2657
    name "2657"
  ]
  edge
  [
    source 262
    target 213
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2658
    name "2658"
  ]
  edge
  [
    source 222
    target 213
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2659
    name "2659"
  ]
  edge
  [
    source 221
    target 213
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2660
    name "2660"
  ]
  edge
  [
    source 220
    target 213
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2661
    name "2661"
  ]
  edge
  [
    source 219
    target 213
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2662
    name "2662"
  ]
  edge
  [
    source 218
    target 213
    measure 5
    diseases "Diabetes_mellitus,Obesity"
    GOmeasure 5
    id 2663
    name "2663"
  ]
  edge
  [
    source 217
    target 213
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2664
    name "2664"
  ]
  edge
  [
    source 216
    target 213
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2665
    name "2665"
  ]
  edge
  [
    source 215
    target 213
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2666
    name "2666"
  ]
  edge
  [
    source 214
    target 213
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2667
    name "2667"
  ]
  edge
  [
    source 222
    target 212
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2668
    name "2668"
  ]
  edge
  [
    source 221
    target 212
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2669
    name "2669"
  ]
  edge
  [
    source 220
    target 212
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2670
    name "2670"
  ]
  edge
  [
    source 219
    target 212
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2671
    name "2671"
  ]
  edge
  [
    source 218
    target 212
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2672
    name "2672"
  ]
  edge
  [
    source 217
    target 212
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2673
    name "2673"
  ]
  edge
  [
    source 216
    target 212
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2674
    name "2674"
  ]
  edge
  [
    source 215
    target 212
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2675
    name "2675"
  ]
  edge
  [
    source 214
    target 212
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2676
    name "2676"
  ]
  edge
  [
    source 213
    target 212
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2677
    name "2677"
  ]
  edge
  [
    source 222
    target 211
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2678
    name "2678"
  ]
  edge
  [
    source 221
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2679
    name "2679"
  ]
  edge
  [
    source 220
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2680
    name "2680"
  ]
  edge
  [
    source 219
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2681
    name "2681"
  ]
  edge
  [
    source 218
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2682
    name "2682"
  ]
  edge
  [
    source 217
    target 211
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2683
    name "2683"
  ]
  edge
  [
    source 216
    target 211
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2684
    name "2684"
  ]
  edge
  [
    source 215
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2685
    name "2685"
  ]
  edge
  [
    source 214
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2686
    name "2686"
  ]
  edge
  [
    source 213
    target 211
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2687
    name "2687"
  ]
  edge
  [
    source 212
    target 211
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2688
    name "2688"
  ]
  edge
  [
    source 222
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2689
    name "2689"
  ]
  edge
  [
    source 221
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2690
    name "2690"
  ]
  edge
  [
    source 220
    target 210
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2691
    name "2691"
  ]
  edge
  [
    source 219
    target 210
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2692
    name "2692"
  ]
  edge
  [
    source 218
    target 210
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2693
    name "2693"
  ]
  edge
  [
    source 217
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2694
    name "2694"
  ]
  edge
  [
    source 216
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2695
    name "2695"
  ]
  edge
  [
    source 215
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2696
    name "2696"
  ]
  edge
  [
    source 214
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2697
    name "2697"
  ]
  edge
  [
    source 213
    target 210
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2698
    name "2698"
  ]
  edge
  [
    source 212
    target 210
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2699
    name "2699"
  ]
  edge
  [
    source 211
    target 210
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2700
    name "2700"
  ]
  edge
  [
    source 222
    target 209
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2701
    name "2701"
  ]
  edge
  [
    source 221
    target 209
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2702
    name "2702"
  ]
  edge
  [
    source 220
    target 209
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2703
    name "2703"
  ]
  edge
  [
    source 219
    target 209
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2704
    name "2704"
  ]
  edge
  [
    source 218
    target 209
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2705
    name "2705"
  ]
  edge
  [
    source 217
    target 209
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2706
    name "2706"
  ]
  edge
  [
    source 216
    target 209
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2707
    name "2707"
  ]
  edge
  [
    source 215
    target 209
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2708
    name "2708"
  ]
  edge
  [
    source 214
    target 209
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2709
    name "2709"
  ]
  edge
  [
    source 213
    target 209
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2710
    name "2710"
  ]
  edge
  [
    source 212
    target 209
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2711
    name "2711"
  ]
  edge
  [
    source 211
    target 209
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2712
    name "2712"
  ]
  edge
  [
    source 210
    target 209
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2713
    name "2713"
  ]
  edge
  [
    source 222
    target 208
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2714
    name "2714"
  ]
  edge
  [
    source 221
    target 208
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2715
    name "2715"
  ]
  edge
  [
    source 220
    target 208
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2716
    name "2716"
  ]
  edge
  [
    source 219
    target 208
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2717
    name "2717"
  ]
  edge
  [
    source 218
    target 208
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2718
    name "2718"
  ]
  edge
  [
    source 217
    target 208
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2719
    name "2719"
  ]
  edge
  [
    source 216
    target 208
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2720
    name "2720"
  ]
  edge
  [
    source 215
    target 208
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2721
    name "2721"
  ]
  edge
  [
    source 214
    target 208
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2722
    name "2722"
  ]
  edge
  [
    source 213
    target 208
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2723
    name "2723"
  ]
  edge
  [
    source 212
    target 208
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2724
    name "2724"
  ]
  edge
  [
    source 211
    target 208
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2725
    name "2725"
  ]
  edge
  [
    source 210
    target 208
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2726
    name "2726"
  ]
  edge
  [
    source 209
    target 208
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2727
    name "2727"
  ]
  edge
  [
    source 222
    target 207
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2728
    name "2728"
  ]
  edge
  [
    source 221
    target 207
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2729
    name "2729"
  ]
  edge
  [
    source 220
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2730
    name "2730"
  ]
  edge
  [
    source 219
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2731
    name "2731"
  ]
  edge
  [
    source 218
    target 207
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2732
    name "2732"
  ]
  edge
  [
    source 217
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2733
    name "2733"
  ]
  edge
  [
    source 216
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2734
    name "2734"
  ]
  edge
  [
    source 215
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2735
    name "2735"
  ]
  edge
  [
    source 214
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2736
    name "2736"
  ]
  edge
  [
    source 213
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2737
    name "2737"
  ]
  edge
  [
    source 212
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2738
    name "2738"
  ]
  edge
  [
    source 211
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2739
    name "2739"
  ]
  edge
  [
    source 210
    target 207
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2740
    name "2740"
  ]
  edge
  [
    source 209
    target 207
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2741
    name "2741"
  ]
  edge
  [
    source 208
    target 207
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2742
    name "2742"
  ]
  edge
  [
    source 222
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2743
    name "2743"
  ]
  edge
  [
    source 221
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2744
    name "2744"
  ]
  edge
  [
    source 220
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2745
    name "2745"
  ]
  edge
  [
    source 219
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2746
    name "2746"
  ]
  edge
  [
    source 218
    target 206
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2747
    name "2747"
  ]
  edge
  [
    source 217
    target 206
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2748
    name "2748"
  ]
  edge
  [
    source 216
    target 206
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2749
    name "2749"
  ]
  edge
  [
    source 215
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2750
    name "2750"
  ]
  edge
  [
    source 214
    target 206
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2751
    name "2751"
  ]
  edge
  [
    source 213
    target 206
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2752
    name "2752"
  ]
  edge
  [
    source 212
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2753
    name "2753"
  ]
  edge
  [
    source 211
    target 206
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2754
    name "2754"
  ]
  edge
  [
    source 210
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2755
    name "2755"
  ]
  edge
  [
    source 209
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2756
    name "2756"
  ]
  edge
  [
    source 208
    target 206
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2757
    name "2757"
  ]
  edge
  [
    source 207
    target 206
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2758
    name "2758"
  ]
  edge
  [
    source 222
    target 205
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2759
    name "2759"
  ]
  edge
  [
    source 221
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2760
    name "2760"
  ]
  edge
  [
    source 220
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2761
    name "2761"
  ]
  edge
  [
    source 219
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2762
    name "2762"
  ]
  edge
  [
    source 218
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2763
    name "2763"
  ]
  edge
  [
    source 217
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2764
    name "2764"
  ]
  edge
  [
    source 216
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2765
    name "2765"
  ]
  edge
  [
    source 215
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2766
    name "2766"
  ]
  edge
  [
    source 214
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2767
    name "2767"
  ]
  edge
  [
    source 213
    target 205
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2768
    name "2768"
  ]
  edge
  [
    source 212
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2769
    name "2769"
  ]
  edge
  [
    source 211
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2770
    name "2770"
  ]
  edge
  [
    source 210
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2771
    name "2771"
  ]
  edge
  [
    source 209
    target 205
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2772
    name "2772"
  ]
  edge
  [
    source 208
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2773
    name "2773"
  ]
  edge
  [
    source 207
    target 205
    measure 4
    diseases "Obesity"
    GOmeasure 4
    id 2774
    name "2774"
  ]
  edge
  [
    source 206
    target 205
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2775
    name "2775"
  ]
  edge
  [
    source 746
    target 203
    measure 7
    diseases "Dementia"
    GOmeasure 7
    id 2776
    name "2776"
  ]
  edge
  [
    source 745
    target 203
    measure 6
    diseases "Dementia"
    GOmeasure 6
    id 2777
    name "2777"
  ]
  edge
  [
    source 744
    target 203
    measure 5
    diseases "Dementia"
    GOmeasure 5
    id 2778
    name "2778"
  ]
  edge
  [
    source 630
    target 203
    measure 7
    diseases "Dementia"
    GOmeasure 7
    id 2779
    name "2779"
  ]
  edge
  [
    source 553
    target 203
    measure 6
    diseases "Malaria"
    GOmeasure 6
    id 2780
    name "2780"
  ]
  edge
  [
    source 535
    target 203
    measure 6
    diseases "Malaria"
    GOmeasure 6
    id 2781
    name "2781"
  ]
  edge
  [
    source 412
    target 203
    measure 5
    diseases "Migraine"
    GOmeasure 5
    id 2782
    name "2782"
  ]
  edge
  [
    source 411
    target 203
    measure 6
    diseases "Migraine"
    GOmeasure 6
    id 2783
    name "2783"
  ]
  edge
  [
    source 204
    target 203
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2784
    name "2784"
  ]
  edge
  [
    source 204
    target 202
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2785
    name "2785"
  ]
  edge
  [
    source 203
    target 202
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2786
    name "2786"
  ]
  edge
  [
    source 734
    target 201
    measure 5
    diseases "Atopy"
    GOmeasure 5
    id 2787
    name "2787"
  ]
  edge
  [
    source 733
    target 201
    measure 5
    diseases "Atopy"
    GOmeasure 5
    id 2788
    name "2788"
  ]
  edge
  [
    source 732
    target 201
    measure 5
    diseases "Atopy"
    GOmeasure 5
    id 2789
    name "2789"
  ]
  edge
  [
    source 731
    target 201
    measure 5
    diseases "Atopy"
    GOmeasure 5
    id 2790
    name "2790"
  ]
  edge
  [
    source 535
    target 201
    measure 6
    diseases "Platelet_defect/deficiency"
    GOmeasure 6
    id 2791
    name "2791"
  ]
  edge
  [
    source 534
    target 201
    measure 6
    diseases "Platelet_defect/deficiency"
    GOmeasure 6
    id 2792
    name "2792"
  ]
  edge
  [
    source 204
    target 201
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2793
    name "2793"
  ]
  edge
  [
    source 203
    target 201
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2794
    name "2794"
  ]
  edge
  [
    source 202
    target 201
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2795
    name "2795"
  ]
  edge
  [
    source 204
    target 200
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2796
    name "2796"
  ]
  edge
  [
    source 203
    target 200
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2797
    name "2797"
  ]
  edge
  [
    source 202
    target 200
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2798
    name "2798"
  ]
  edge
  [
    source 201
    target 200
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2799
    name "2799"
  ]
  edge
  [
    source 204
    target 199
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2800
    name "2800"
  ]
  edge
  [
    source 203
    target 199
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2801
    name "2801"
  ]
  edge
  [
    source 202
    target 199
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2802
    name "2802"
  ]
  edge
  [
    source 201
    target 199
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2803
    name "2803"
  ]
  edge
  [
    source 200
    target 199
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2804
    name "2804"
  ]
  edge
  [
    source 204
    target 198
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2805
    name "2805"
  ]
  edge
  [
    source 203
    target 198
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2806
    name "2806"
  ]
  edge
  [
    source 202
    target 198
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2807
    name "2807"
  ]
  edge
  [
    source 201
    target 198
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2808
    name "2808"
  ]
  edge
  [
    source 200
    target 198
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2809
    name "2809"
  ]
  edge
  [
    source 199
    target 198
    measure 8
    diseases "Asthma"
    GOmeasure 8
    id 2810
    name "2810"
  ]
  edge
  [
    source 204
    target 197
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2811
    name "2811"
  ]
  edge
  [
    source 203
    target 197
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2812
    name "2812"
  ]
  edge
  [
    source 202
    target 197
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2813
    name "2813"
  ]
  edge
  [
    source 201
    target 197
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2814
    name "2814"
  ]
  edge
  [
    source 200
    target 197
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2815
    name "2815"
  ]
  edge
  [
    source 199
    target 197
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2816
    name "2816"
  ]
  edge
  [
    source 198
    target 197
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2817
    name "2817"
  ]
  edge
  [
    source 204
    target 196
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2818
    name "2818"
  ]
  edge
  [
    source 203
    target 196
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2819
    name "2819"
  ]
  edge
  [
    source 202
    target 196
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2820
    name "2820"
  ]
  edge
  [
    source 201
    target 196
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2821
    name "2821"
  ]
  edge
  [
    source 200
    target 196
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2822
    name "2822"
  ]
  edge
  [
    source 199
    target 196
    measure 8
    diseases "Asthma"
    GOmeasure 8
    id 2823
    name "2823"
  ]
  edge
  [
    source 198
    target 196
    measure 8
    diseases "Asthma"
    GOmeasure 8
    id 2824
    name "2824"
  ]
  edge
  [
    source 197
    target 196
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2825
    name "2825"
  ]
  edge
  [
    source 204
    target 195
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2826
    name "2826"
  ]
  edge
  [
    source 203
    target 195
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2827
    name "2827"
  ]
  edge
  [
    source 202
    target 195
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2828
    name "2828"
  ]
  edge
  [
    source 201
    target 195
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2829
    name "2829"
  ]
  edge
  [
    source 200
    target 195
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2830
    name "2830"
  ]
  edge
  [
    source 199
    target 195
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2831
    name "2831"
  ]
  edge
  [
    source 198
    target 195
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2832
    name "2832"
  ]
  edge
  [
    source 197
    target 195
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2833
    name "2833"
  ]
  edge
  [
    source 196
    target 195
    measure 4
    diseases "Asthma"
    GOmeasure 4
    id 2834
    name "2834"
  ]
  edge
  [
    source 222
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2835
    name "2835"
  ]
  edge
  [
    source 221
    target 194
    measure 8
    diseases "Obesity"
    GOmeasure 8
    id 2836
    name "2836"
  ]
  edge
  [
    source 220
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2837
    name "2837"
  ]
  edge
  [
    source 219
    target 194
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2838
    name "2838"
  ]
  edge
  [
    source 218
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2839
    name "2839"
  ]
  edge
  [
    source 217
    target 194
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2840
    name "2840"
  ]
  edge
  [
    source 216
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2841
    name "2841"
  ]
  edge
  [
    source 215
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2842
    name "2842"
  ]
  edge
  [
    source 214
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2843
    name "2843"
  ]
  edge
  [
    source 213
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2844
    name "2844"
  ]
  edge
  [
    source 212
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2845
    name "2845"
  ]
  edge
  [
    source 211
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2846
    name "2846"
  ]
  edge
  [
    source 210
    target 194
    measure 5
    diseases "Obesity"
    GOmeasure 5
    id 2847
    name "2847"
  ]
  edge
  [
    source 209
    target 194
    measure 7
    diseases "Obesity"
    GOmeasure 7
    id 2848
    name "2848"
  ]
  edge
  [
    source 208
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2849
    name "2849"
  ]
  edge
  [
    source 207
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2850
    name "2850"
  ]
  edge
  [
    source 206
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2851
    name "2851"
  ]
  edge
  [
    source 205
    target 194
    measure 6
    diseases "Obesity"
    GOmeasure 6
    id 2852
    name "2852"
  ]
  edge
  [
    source 204
    target 194
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2853
    name "2853"
  ]
  edge
  [
    source 203
    target 194
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2854
    name "2854"
  ]
  edge
  [
    source 202
    target 194
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2855
    name "2855"
  ]
  edge
  [
    source 201
    target 194
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2856
    name "2856"
  ]
  edge
  [
    source 200
    target 194
    measure 8
    diseases "Asthma"
    GOmeasure 8
    id 2857
    name "2857"
  ]
  edge
  [
    source 199
    target 194
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2858
    name "2858"
  ]
  edge
  [
    source 198
    target 194
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2859
    name "2859"
  ]
  edge
  [
    source 197
    target 194
    measure 6
    diseases "Asthma"
    GOmeasure 6
    id 2860
    name "2860"
  ]
  edge
  [
    source 196
    target 194
    measure 7
    diseases "Asthma"
    GOmeasure 7
    id 2861
    name "2861"
  ]
  edge
  [
    source 195
    target 194
    measure 5
    diseases "Asthma"
    GOmeasure 5
    id 2862
    name "2862"
  ]
  edge
  [
    source 193
    target 192
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2863
    name "2863"
  ]
  edge
  [
    source 280
    target 191
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2864
    name "2864"
  ]
  edge
  [
    source 279
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2865
    name "2865"
  ]
  edge
  [
    source 278
    target 191
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2866
    name "2866"
  ]
  edge
  [
    source 277
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2867
    name "2867"
  ]
  edge
  [
    source 276
    target 191
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 2868
    name "2868"
  ]
  edge
  [
    source 275
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2869
    name "2869"
  ]
  edge
  [
    source 274
    target 191
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2870
    name "2870"
  ]
  edge
  [
    source 273
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2871
    name "2871"
  ]
  edge
  [
    source 272
    target 191
    measure 4
    diseases "Diabetes_mellitus"
    GOmeasure 4
    id 2872
    name "2872"
  ]
  edge
  [
    source 271
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2873
    name "2873"
  ]
  edge
  [
    source 270
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2874
    name "2874"
  ]
  edge
  [
    source 269
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2875
    name "2875"
  ]
  edge
  [
    source 268
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2876
    name "2876"
  ]
  edge
  [
    source 267
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2877
    name "2877"
  ]
  edge
  [
    source 266
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2878
    name "2878"
  ]
  edge
  [
    source 265
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2879
    name "2879"
  ]
  edge
  [
    source 264
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2880
    name "2880"
  ]
  edge
  [
    source 263
    target 191
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 2881
    name "2881"
  ]
  edge
  [
    source 262
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2882
    name "2882"
  ]
  edge
  [
    source 218
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2883
    name "2883"
  ]
  edge
  [
    source 213
    target 191
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 2884
    name "2884"
  ]
  edge
  [
    source 193
    target 191
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2885
    name "2885"
  ]
  edge
  [
    source 192
    target 191
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2886
    name "2886"
  ]
  edge
  [
    source 193
    target 190
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2887
    name "2887"
  ]
  edge
  [
    source 192
    target 190
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2888
    name "2888"
  ]
  edge
  [
    source 191
    target 190
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2889
    name "2889"
  ]
  edge
  [
    source 664
    target 189
    measure 4
    diseases "Hirschsprung_disease"
    GOmeasure 4
    id 2890
    name "2890"
  ]
  edge
  [
    source 663
    target 189
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 2891
    name "2891"
  ]
  edge
  [
    source 443
    target 189
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 2892
    name "2892"
  ]
  edge
  [
    source 442
    target 189
    measure 6
    diseases "Hirschsprung_disease"
    GOmeasure 6
    id 2893
    name "2893"
  ]
  edge
  [
    source 441
    target 189
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 2894
    name "2894"
  ]
  edge
  [
    source 440
    target 189
    measure 5
    diseases "Hirschsprung_disease"
    GOmeasure 5
    id 2895
    name "2895"
  ]
  edge
  [
    source 193
    target 189
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2896
    name "2896"
  ]
  edge
  [
    source 192
    target 189
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2897
    name "2897"
  ]
  edge
  [
    source 191
    target 189
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2898
    name "2898"
  ]
  edge
  [
    source 190
    target 189
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2899
    name "2899"
  ]
  edge
  [
    source 231
    target 188
    measure 5
    diseases "Renal_tubular_dysgenesis"
    GOmeasure 5
    id 2900
    name "2900"
  ]
  edge
  [
    source 230
    target 188
    measure 7
    diseases "Preeclampsia"
    GOmeasure 7
    id 2901
    name "2901"
  ]
  edge
  [
    source 229
    target 188
    measure 7
    diseases "Preeclampsia"
    GOmeasure 7
    id 2902
    name "2902"
  ]
  edge
  [
    source 193
    target 188
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2903
    name "2903"
  ]
  edge
  [
    source 192
    target 188
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2904
    name "2904"
  ]
  edge
  [
    source 191
    target 188
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2905
    name "2905"
  ]
  edge
  [
    source 190
    target 188
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2906
    name "2906"
  ]
  edge
  [
    source 189
    target 188
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2907
    name "2907"
  ]
  edge
  [
    source 193
    target 187
    measure 4
    diseases "Hypertension"
    GOmeasure 4
    id 2908
    name "2908"
  ]
  edge
  [
    source 192
    target 187
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2909
    name "2909"
  ]
  edge
  [
    source 191
    target 187
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2910
    name "2910"
  ]
  edge
  [
    source 190
    target 187
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2911
    name "2911"
  ]
  edge
  [
    source 189
    target 187
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2912
    name "2912"
  ]
  edge
  [
    source 188
    target 187
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2913
    name "2913"
  ]
  edge
  [
    source 231
    target 186
    measure 4
    diseases "Renal_tubular_dysgenesis"
    GOmeasure 4
    id 2914
    name "2914"
  ]
  edge
  [
    source 193
    target 186
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2915
    name "2915"
  ]
  edge
  [
    source 192
    target 186
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2916
    name "2916"
  ]
  edge
  [
    source 191
    target 186
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2917
    name "2917"
  ]
  edge
  [
    source 190
    target 186
    measure 4
    diseases "Hypertension"
    GOmeasure 4
    id 2918
    name "2918"
  ]
  edge
  [
    source 189
    target 186
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2919
    name "2919"
  ]
  edge
  [
    source 188
    target 186
    measure 6
    diseases "Hypertension,Renal_tubular_dysgenesis"
    GOmeasure 6
    id 2920
    name "2920"
  ]
  edge
  [
    source 187
    target 186
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2921
    name "2921"
  ]
  edge
  [
    source 764
    target 185
    measure 5
    diseases "Pseudohypoaldosteronism"
    GOmeasure 5
    id 2922
    name "2922"
  ]
  edge
  [
    source 763
    target 185
    measure 5
    diseases "Pseudohypoaldosteronism"
    GOmeasure 5
    id 2923
    name "2923"
  ]
  edge
  [
    source 762
    target 185
    measure 5
    diseases "Pseudohypoaldosteronism"
    GOmeasure 5
    id 2924
    name "2924"
  ]
  edge
  [
    source 761
    target 185
    measure 5
    diseases "Pseudohypoaldosteronism"
    GOmeasure 5
    id 2925
    name "2925"
  ]
  edge
  [
    source 760
    target 185
    measure 5
    diseases "Pseudohypoaldosteronism"
    GOmeasure 5
    id 2926
    name "2926"
  ]
  edge
  [
    source 193
    target 185
    measure 4
    diseases "Hypertension"
    GOmeasure 4
    id 2927
    name "2927"
  ]
  edge
  [
    source 192
    target 185
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2928
    name "2928"
  ]
  edge
  [
    source 191
    target 185
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2929
    name "2929"
  ]
  edge
  [
    source 190
    target 185
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2930
    name "2930"
  ]
  edge
  [
    source 189
    target 185
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2931
    name "2931"
  ]
  edge
  [
    source 188
    target 185
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2932
    name "2932"
  ]
  edge
  [
    source 187
    target 185
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2933
    name "2933"
  ]
  edge
  [
    source 186
    target 185
    measure 4
    diseases "Hypertension"
    GOmeasure 4
    id 2934
    name "2934"
  ]
  edge
  [
    source 193
    target 184
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2935
    name "2935"
  ]
  edge
  [
    source 192
    target 184
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2936
    name "2936"
  ]
  edge
  [
    source 191
    target 184
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2937
    name "2937"
  ]
  edge
  [
    source 190
    target 184
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2938
    name "2938"
  ]
  edge
  [
    source 189
    target 184
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2939
    name "2939"
  ]
  edge
  [
    source 188
    target 184
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2940
    name "2940"
  ]
  edge
  [
    source 187
    target 184
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2941
    name "2941"
  ]
  edge
  [
    source 186
    target 184
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2942
    name "2942"
  ]
  edge
  [
    source 185
    target 184
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2943
    name "2943"
  ]
  edge
  [
    source 193
    target 183
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 2944
    name "2944"
  ]
  edge
  [
    source 192
    target 183
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2945
    name "2945"
  ]
  edge
  [
    source 191
    target 183
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2946
    name "2946"
  ]
  edge
  [
    source 190
    target 183
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2947
    name "2947"
  ]
  edge
  [
    source 189
    target 183
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 2948
    name "2948"
  ]
  edge
  [
    source 188
    target 183
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2949
    name "2949"
  ]
  edge
  [
    source 187
    target 183
    measure 4
    diseases "Hypertension"
    GOmeasure 4
    id 2950
    name "2950"
  ]
  edge
  [
    source 186
    target 183
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2951
    name "2951"
  ]
  edge
  [
    source 185
    target 183
    measure 4
    diseases "Hypertension"
    GOmeasure 4
    id 2952
    name "2952"
  ]
  edge
  [
    source 184
    target 183
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 2953
    name "2953"
  ]
  edge
  [
    source 468
    target 182
    measure 6
    diseases "Combined_immunodeficiency"
    GOmeasure 6
    id 2954
    name "2954"
  ]
  edge
  [
    source 182
    target 181
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2955
    name "2955"
  ]
  edge
  [
    source 182
    target 180
    measure 6
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 6
    id 2956
    name "2956"
  ]
  edge
  [
    source 181
    target 180
    measure 4
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 4
    id 2957
    name "2957"
  ]
  edge
  [
    source 537
    target 179
    measure 5
    diseases "Multiple_sclerosis"
    GOmeasure 5
    id 2958
    name "2958"
  ]
  edge
  [
    source 182
    target 179
    measure 4
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 4
    id 2959
    name "2959"
  ]
  edge
  [
    source 181
    target 179
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2960
    name "2960"
  ]
  edge
  [
    source 180
    target 179
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2961
    name "2961"
  ]
  edge
  [
    source 182
    target 178
    measure 6
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 6
    id 2962
    name "2962"
  ]
  edge
  [
    source 181
    target 178
    measure 4
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 4
    id 2963
    name "2963"
  ]
  edge
  [
    source 180
    target 178
    measure 6
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 6
    id 2964
    name "2964"
  ]
  edge
  [
    source 179
    target 178
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2965
    name "2965"
  ]
  edge
  [
    source 182
    target 177
    measure 6
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 6
    id 2966
    name "2966"
  ]
  edge
  [
    source 181
    target 177
    measure 4
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 4
    id 2967
    name "2967"
  ]
  edge
  [
    source 180
    target 177
    measure 6
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 6
    id 2968
    name "2968"
  ]
  edge
  [
    source 179
    target 177
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2969
    name "2969"
  ]
  edge
  [
    source 178
    target 177
    measure 7
    diseases "Omenn_syndrome,Severe_combined_immunodeficiency"
    GOmeasure 7
    id 2970
    name "2970"
  ]
  edge
  [
    source 182
    target 176
    measure 7
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 7
    id 2971
    name "2971"
  ]
  edge
  [
    source 181
    target 176
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2972
    name "2972"
  ]
  edge
  [
    source 180
    target 176
    measure 6
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 6
    id 2973
    name "2973"
  ]
  edge
  [
    source 179
    target 176
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2974
    name "2974"
  ]
  edge
  [
    source 178
    target 176
    measure 7
    diseases "Omenn_syndrome,Severe_combined_immunodeficiency"
    GOmeasure 7
    id 2975
    name "2975"
  ]
  edge
  [
    source 177
    target 176
    measure 7
    diseases "Omenn_syndrome,Severe_combined_immunodeficiency"
    GOmeasure 7
    id 2976
    name "2976"
  ]
  edge
  [
    source 182
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2977
    name "2977"
  ]
  edge
  [
    source 181
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2978
    name "2978"
  ]
  edge
  [
    source 180
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2979
    name "2979"
  ]
  edge
  [
    source 179
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2980
    name "2980"
  ]
  edge
  [
    source 178
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2981
    name "2981"
  ]
  edge
  [
    source 177
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2982
    name "2982"
  ]
  edge
  [
    source 176
    target 175
    measure 5
    diseases "Severe_combined_immunodeficiency"
    GOmeasure 5
    id 2983
    name "2983"
  ]
  edge
  [
    source 449
    target 173
    measure 6
    diseases "Melanoma"
    GOmeasure 6
    id 2984
    name "2984"
  ]
  edge
  [
    source 391
    target 173
    measure 5
    diseases "Melanoma"
    GOmeasure 5
    id 2985
    name "2985"
  ]
  edge
  [
    source 330
    target 173
    measure 5
    diseases "Melanoma"
    GOmeasure 5
    id 2986
    name "2986"
  ]
  edge
  [
    source 174
    target 173
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 2987
    name "2987"
  ]
  edge
  [
    source 449
    target 172
    measure 4
    diseases "Melanoma"
    GOmeasure 4
    id 2988
    name "2988"
  ]
  edge
  [
    source 391
    target 172
    measure 4
    diseases "Melanoma"
    GOmeasure 4
    id 2989
    name "2989"
  ]
  edge
  [
    source 330
    target 172
    measure 5
    diseases "Melanoma"
    GOmeasure 5
    id 2990
    name "2990"
  ]
  edge
  [
    source 174
    target 172
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 2991
    name "2991"
  ]
  edge
  [
    source 173
    target 172
    measure 5
    diseases "Melanoma,Pancreatic_cancer"
    GOmeasure 5
    id 2992
    name "2992"
  ]
  edge
  [
    source 445
    target 171
    measure 6
    diseases "Polyposis"
    GOmeasure 6
    id 2993
    name "2993"
  ]
  edge
  [
    source 174
    target 171
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 2994
    name "2994"
  ]
  edge
  [
    source 173
    target 171
    measure 5
    diseases "Pancreatic_cancer"
    GOmeasure 5
    id 2995
    name "2995"
  ]
  edge
  [
    source 172
    target 171
    measure 5
    diseases "Pancreatic_cancer"
    GOmeasure 5
    id 2996
    name "2996"
  ]
  edge
  [
    source 724
    target 170
    measure 7
    diseases "Thyroid_carcinoma"
    GOmeasure 7
    id 2997
    name "2997"
  ]
  edge
  [
    source 723
    target 170
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 2998
    name "2998"
  ]
  edge
  [
    source 722
    target 170
    measure 6
    diseases "Thyroid_carcinoma"
    GOmeasure 6
    id 2999
    name "2999"
  ]
  edge
  [
    source 721
    target 170
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 3000
    name "3000"
  ]
  edge
  [
    source 720
    target 170
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 3001
    name "3001"
  ]
  edge
  [
    source 719
    target 170
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 3002
    name "3002"
  ]
  edge
  [
    source 718
    target 170
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 3003
    name "3003"
  ]
  edge
  [
    source 697
    target 170
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 3004
    name "3004"
  ]
  edge
  [
    source 621
    target 170
    measure 4
    diseases "Thyroid_carcinoma"
    GOmeasure 4
    id 3005
    name "3005"
  ]
  edge
  [
    source 527
    target 170
    measure 7
    diseases "Hepatic_adenoma"
    GOmeasure 7
    id 3006
    name "3006"
  ]
  edge
  [
    source 526
    target 170
    measure 5
    diseases "Hepatic_adenoma"
    GOmeasure 5
    id 3007
    name "3007"
  ]
  edge
  [
    source 525
    target 170
    measure 5
    diseases "Hepatic_adenoma"
    GOmeasure 5
    id 3008
    name "3008"
  ]
  edge
  [
    source 393
    target 170
    measure 5
    diseases "Thyroid_carcinoma"
    GOmeasure 5
    id 3009
    name "3009"
  ]
  edge
  [
    source 391
    target 170
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3010
    name "3010"
  ]
  edge
  [
    source 390
    target 170
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3011
    name "3011"
  ]
  edge
  [
    source 389
    target 170
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3012
    name "3012"
  ]
  edge
  [
    source 388
    target 170
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 3013
    name "3013"
  ]
  edge
  [
    source 387
    target 170
    measure 8
    diseases "Breast_cancer"
    GOmeasure 8
    id 3014
    name "3014"
  ]
  edge
  [
    source 386
    target 170
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3015
    name "3015"
  ]
  edge
  [
    source 385
    target 170
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3016
    name "3016"
  ]
  edge
  [
    source 384
    target 170
    measure 8
    diseases "Breast_cancer"
    GOmeasure 8
    id 3017
    name "3017"
  ]
  edge
  [
    source 383
    target 170
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3018
    name "3018"
  ]
  edge
  [
    source 382
    target 170
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3019
    name "3019"
  ]
  edge
  [
    source 381
    target 170
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3020
    name "3020"
  ]
  edge
  [
    source 380
    target 170
    measure 7
    diseases "Breast_cancer,Li-Fraumeni_syndrome,Osteosarcoma"
    GOmeasure 7
    id 3021
    name "3021"
  ]
  edge
  [
    source 379
    target 170
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3022
    name "3022"
  ]
  edge
  [
    source 344
    target 170
    measure 7
    diseases "Hepatic_adenoma"
    GOmeasure 7
    id 3023
    name "3023"
  ]
  edge
  [
    source 335
    target 170
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3024
    name "3024"
  ]
  edge
  [
    source 333
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3025
    name "3025"
  ]
  edge
  [
    source 332
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3026
    name "3026"
  ]
  edge
  [
    source 331
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3027
    name "3027"
  ]
  edge
  [
    source 330
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3028
    name "3028"
  ]
  edge
  [
    source 329
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3029
    name "3029"
  ]
  edge
  [
    source 328
    target 170
    measure 8
    diseases "Colon_cancer"
    GOmeasure 8
    id 3030
    name "3030"
  ]
  edge
  [
    source 327
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3031
    name "3031"
  ]
  edge
  [
    source 326
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3032
    name "3032"
  ]
  edge
  [
    source 325
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3033
    name "3033"
  ]
  edge
  [
    source 324
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3034
    name "3034"
  ]
  edge
  [
    source 323
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3035
    name "3035"
  ]
  edge
  [
    source 322
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3036
    name "3036"
  ]
  edge
  [
    source 321
    target 170
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 3037
    name "3037"
  ]
  edge
  [
    source 320
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3038
    name "3038"
  ]
  edge
  [
    source 319
    target 170
    measure 7
    diseases "Colon_cancer,Hepatic_adenoma"
    GOmeasure 7
    id 3039
    name "3039"
  ]
  edge
  [
    source 318
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3040
    name "3040"
  ]
  edge
  [
    source 317
    target 170
    measure 6
    diseases "Breast_cancer,Colon_cancer"
    GOmeasure 6
    id 3041
    name "3041"
  ]
  edge
  [
    source 316
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3042
    name "3042"
  ]
  edge
  [
    source 315
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3043
    name "3043"
  ]
  edge
  [
    source 314
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3044
    name "3044"
  ]
  edge
  [
    source 313
    target 170
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 3045
    name "3045"
  ]
  edge
  [
    source 312
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3046
    name "3046"
  ]
  edge
  [
    source 311
    target 170
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 3047
    name "3047"
  ]
  edge
  [
    source 310
    target 170
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 3048
    name "3048"
  ]
  edge
  [
    source 309
    target 170
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 3049
    name "3049"
  ]
  edge
  [
    source 308
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3050
    name "3050"
  ]
  edge
  [
    source 307
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3051
    name "3051"
  ]
  edge
  [
    source 306
    target 170
    measure 5
    diseases "Breast_cancer,Colon_cancer"
    GOmeasure 5
    id 3052
    name "3052"
  ]
  edge
  [
    source 305
    target 170
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 3053
    name "3053"
  ]
  edge
  [
    source 303
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3054
    name "3054"
  ]
  edge
  [
    source 302
    target 170
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 3055
    name "3055"
  ]
  edge
  [
    source 174
    target 170
    measure 8
    diseases "Pancreatic_cancer"
    GOmeasure 8
    id 3056
    name "3056"
  ]
  edge
  [
    source 173
    target 170
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 3057
    name "3057"
  ]
  edge
  [
    source 172
    target 170
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 3058
    name "3058"
  ]
  edge
  [
    source 171
    target 170
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 3059
    name "3059"
  ]
  edge
  [
    source 460
    target 169
    measure 7
    diseases "Wilms_tumor"
    GOmeasure 7
    id 3060
    name "3060"
  ]
  edge
  [
    source 459
    target 169
    measure 7
    diseases "Wilms_tumor"
    GOmeasure 7
    id 3061
    name "3061"
  ]
  edge
  [
    source 458
    target 169
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 3062
    name "3062"
  ]
  edge
  [
    source 457
    target 169
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 3063
    name "3063"
  ]
  edge
  [
    source 456
    target 169
    measure 4
    diseases "Fanconi_anemia"
    GOmeasure 4
    id 3064
    name "3064"
  ]
  edge
  [
    source 455
    target 169
    measure 5
    diseases "Fanconi_anemia"
    GOmeasure 5
    id 3065
    name "3065"
  ]
  edge
  [
    source 454
    target 169
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 3066
    name "3066"
  ]
  edge
  [
    source 453
    target 169
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 3067
    name "3067"
  ]
  edge
  [
    source 452
    target 169
    measure 7
    diseases "Fanconi_anemia"
    GOmeasure 7
    id 3068
    name "3068"
  ]
  edge
  [
    source 451
    target 169
    measure 6
    diseases "Fanconi_anemia"
    GOmeasure 6
    id 3069
    name "3069"
  ]
  edge
  [
    source 450
    target 169
    measure 7
    diseases "Fanconi_anemia"
    GOmeasure 7
    id 3070
    name "3070"
  ]
  edge
  [
    source 398
    target 169
    measure 7
    diseases "Prostate_cancer"
    GOmeasure 7
    id 3071
    name "3071"
  ]
  edge
  [
    source 397
    target 169
    measure 7
    diseases "Prostate_cancer"
    GOmeasure 7
    id 3072
    name "3072"
  ]
  edge
  [
    source 396
    target 169
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 3073
    name "3073"
  ]
  edge
  [
    source 395
    target 169
    measure 4
    diseases "Prostate_cancer"
    GOmeasure 4
    id 3074
    name "3074"
  ]
  edge
  [
    source 394
    target 169
    measure 6
    diseases "Prostate_cancer"
    GOmeasure 6
    id 3075
    name "3075"
  ]
  edge
  [
    source 393
    target 169
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 3076
    name "3076"
  ]
  edge
  [
    source 392
    target 169
    measure 7
    diseases "Prostate_cancer"
    GOmeasure 7
    id 3077
    name "3077"
  ]
  edge
  [
    source 391
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3078
    name "3078"
  ]
  edge
  [
    source 390
    target 169
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3079
    name "3079"
  ]
  edge
  [
    source 389
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3080
    name "3080"
  ]
  edge
  [
    source 388
    target 169
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 3081
    name "3081"
  ]
  edge
  [
    source 387
    target 169
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 3082
    name "3082"
  ]
  edge
  [
    source 386
    target 169
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3083
    name "3083"
  ]
  edge
  [
    source 385
    target 169
    measure 6
    diseases "Breast_cancer,Fanconi_anemia"
    GOmeasure 6
    id 3084
    name "3084"
  ]
  edge
  [
    source 384
    target 169
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 3085
    name "3085"
  ]
  edge
  [
    source 383
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3086
    name "3086"
  ]
  edge
  [
    source 382
    target 169
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 3087
    name "3087"
  ]
  edge
  [
    source 381
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3088
    name "3088"
  ]
  edge
  [
    source 380
    target 169
    measure 6
    diseases "Breast_cancer,Prostate_cancer"
    GOmeasure 6
    id 3089
    name "3089"
  ]
  edge
  [
    source 379
    target 169
    measure 5
    diseases "Breast_cancer,Prostate_cancer"
    GOmeasure 5
    id 3090
    name "3090"
  ]
  edge
  [
    source 341
    target 169
    measure 5
    diseases "Prostate_cancer"
    GOmeasure 5
    id 3091
    name "3091"
  ]
  edge
  [
    source 335
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3092
    name "3092"
  ]
  edge
  [
    source 317
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3093
    name "3093"
  ]
  edge
  [
    source 306
    target 169
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 3094
    name "3094"
  ]
  edge
  [
    source 174
    target 169
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 3095
    name "3095"
  ]
  edge
  [
    source 173
    target 169
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 3096
    name "3096"
  ]
  edge
  [
    source 172
    target 169
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 3097
    name "3097"
  ]
  edge
  [
    source 171
    target 169
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 3098
    name "3098"
  ]
  edge
  [
    source 170
    target 169
    measure 8
    diseases "Breast_cancer,Pancreatic_cancer"
    GOmeasure 8
    id 3099
    name "3099"
  ]
  edge
  [
    source 174
    target 168
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 3100
    name "3100"
  ]
  edge
  [
    source 173
    target 168
    measure 5
    diseases "Pancreatic_cancer"
    GOmeasure 5
    id 3101
    name "3101"
  ]
  edge
  [
    source 172
    target 168
    measure 5
    diseases "Pancreatic_cancer"
    GOmeasure 5
    id 3102
    name "3102"
  ]
  edge
  [
    source 171
    target 168
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 3103
    name "3103"
  ]
  edge
  [
    source 170
    target 168
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 3104
    name "3104"
  ]
  edge
  [
    source 169
    target 168
    measure 7
    diseases "Pancreatic_cancer"
    GOmeasure 7
    id 3105
    name "3105"
  ]
  edge
  [
    source 228
    target 166
    measure 6
    diseases "Tetralogy_of_Fallot"
    GOmeasure 6
    id 3106
    name "3106"
  ]
  edge
  [
    source 227
    target 166
    measure 5
    diseases "Tetralogy_of_Fallot"
    GOmeasure 5
    id 3107
    name "3107"
  ]
  edge
  [
    source 167
    target 166
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3108
    name "3108"
  ]
  edge
  [
    source 167
    target 165
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3109
    name "3109"
  ]
  edge
  [
    source 166
    target 165
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3110
    name "3110"
  ]
  edge
  [
    source 167
    target 164
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3111
    name "3111"
  ]
  edge
  [
    source 166
    target 164
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3112
    name "3112"
  ]
  edge
  [
    source 165
    target 164
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3113
    name "3113"
  ]
  edge
  [
    source 167
    target 163
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3114
    name "3114"
  ]
  edge
  [
    source 166
    target 163
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3115
    name "3115"
  ]
  edge
  [
    source 165
    target 163
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3116
    name "3116"
  ]
  edge
  [
    source 164
    target 163
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3117
    name "3117"
  ]
  edge
  [
    source 167
    target 162
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3118
    name "3118"
  ]
  edge
  [
    source 166
    target 162
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3119
    name "3119"
  ]
  edge
  [
    source 165
    target 162
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3120
    name "3120"
  ]
  edge
  [
    source 164
    target 162
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3121
    name "3121"
  ]
  edge
  [
    source 163
    target 162
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3122
    name "3122"
  ]
  edge
  [
    source 167
    target 161
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3123
    name "3123"
  ]
  edge
  [
    source 166
    target 161
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3124
    name "3124"
  ]
  edge
  [
    source 165
    target 161
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3125
    name "3125"
  ]
  edge
  [
    source 164
    target 161
    measure 8
    diseases "Deafness"
    GOmeasure 8
    id 3126
    name "3126"
  ]
  edge
  [
    source 163
    target 161
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3127
    name "3127"
  ]
  edge
  [
    source 162
    target 161
    measure 5
    diseases "Deafness,Usher_syndrome"
    GOmeasure 5
    id 3128
    name "3128"
  ]
  edge
  [
    source 167
    target 160
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3129
    name "3129"
  ]
  edge
  [
    source 166
    target 160
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3130
    name "3130"
  ]
  edge
  [
    source 165
    target 160
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3131
    name "3131"
  ]
  edge
  [
    source 164
    target 160
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3132
    name "3132"
  ]
  edge
  [
    source 163
    target 160
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3133
    name "3133"
  ]
  edge
  [
    source 162
    target 160
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3134
    name "3134"
  ]
  edge
  [
    source 161
    target 160
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3135
    name "3135"
  ]
  edge
  [
    source 167
    target 159
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3136
    name "3136"
  ]
  edge
  [
    source 166
    target 159
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3137
    name "3137"
  ]
  edge
  [
    source 165
    target 159
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3138
    name "3138"
  ]
  edge
  [
    source 164
    target 159
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3139
    name "3139"
  ]
  edge
  [
    source 163
    target 159
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3140
    name "3140"
  ]
  edge
  [
    source 162
    target 159
    measure 7
    diseases "Deafness,Usher_syndrome"
    GOmeasure 7
    id 3141
    name "3141"
  ]
  edge
  [
    source 161
    target 159
    measure 5
    diseases "Deafness,Usher_syndrome"
    GOmeasure 5
    id 3142
    name "3142"
  ]
  edge
  [
    source 160
    target 159
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3143
    name "3143"
  ]
  edge
  [
    source 167
    target 158
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3144
    name "3144"
  ]
  edge
  [
    source 166
    target 158
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3145
    name "3145"
  ]
  edge
  [
    source 165
    target 158
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3146
    name "3146"
  ]
  edge
  [
    source 164
    target 158
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3147
    name "3147"
  ]
  edge
  [
    source 163
    target 158
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3148
    name "3148"
  ]
  edge
  [
    source 162
    target 158
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3149
    name "3149"
  ]
  edge
  [
    source 161
    target 158
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3150
    name "3150"
  ]
  edge
  [
    source 160
    target 158
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3151
    name "3151"
  ]
  edge
  [
    source 159
    target 158
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3152
    name "3152"
  ]
  edge
  [
    source 167
    target 157
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3153
    name "3153"
  ]
  edge
  [
    source 166
    target 157
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3154
    name "3154"
  ]
  edge
  [
    source 165
    target 157
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3155
    name "3155"
  ]
  edge
  [
    source 164
    target 157
    measure 8
    diseases "Deafness"
    GOmeasure 8
    id 3156
    name "3156"
  ]
  edge
  [
    source 163
    target 157
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3157
    name "3157"
  ]
  edge
  [
    source 162
    target 157
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3158
    name "3158"
  ]
  edge
  [
    source 161
    target 157
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3159
    name "3159"
  ]
  edge
  [
    source 160
    target 157
    measure 8
    diseases "Deafness"
    GOmeasure 8
    id 3160
    name "3160"
  ]
  edge
  [
    source 159
    target 157
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3161
    name "3161"
  ]
  edge
  [
    source 158
    target 157
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3162
    name "3162"
  ]
  edge
  [
    source 167
    target 156
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3163
    name "3163"
  ]
  edge
  [
    source 166
    target 156
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3164
    name "3164"
  ]
  edge
  [
    source 165
    target 156
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3165
    name "3165"
  ]
  edge
  [
    source 164
    target 156
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3166
    name "3166"
  ]
  edge
  [
    source 163
    target 156
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3167
    name "3167"
  ]
  edge
  [
    source 162
    target 156
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3168
    name "3168"
  ]
  edge
  [
    source 161
    target 156
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3169
    name "3169"
  ]
  edge
  [
    source 160
    target 156
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3170
    name "3170"
  ]
  edge
  [
    source 159
    target 156
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3171
    name "3171"
  ]
  edge
  [
    source 158
    target 156
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3172
    name "3172"
  ]
  edge
  [
    source 157
    target 156
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3173
    name "3173"
  ]
  edge
  [
    source 167
    target 155
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3174
    name "3174"
  ]
  edge
  [
    source 166
    target 155
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3175
    name "3175"
  ]
  edge
  [
    source 165
    target 155
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3176
    name "3176"
  ]
  edge
  [
    source 164
    target 155
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3177
    name "3177"
  ]
  edge
  [
    source 163
    target 155
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3178
    name "3178"
  ]
  edge
  [
    source 162
    target 155
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3179
    name "3179"
  ]
  edge
  [
    source 161
    target 155
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3180
    name "3180"
  ]
  edge
  [
    source 160
    target 155
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3181
    name "3181"
  ]
  edge
  [
    source 159
    target 155
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3182
    name "3182"
  ]
  edge
  [
    source 158
    target 155
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3183
    name "3183"
  ]
  edge
  [
    source 157
    target 155
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3184
    name "3184"
  ]
  edge
  [
    source 156
    target 155
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3185
    name "3185"
  ]
  edge
  [
    source 167
    target 154
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3186
    name "3186"
  ]
  edge
  [
    source 166
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3187
    name "3187"
  ]
  edge
  [
    source 165
    target 154
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3188
    name "3188"
  ]
  edge
  [
    source 164
    target 154
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3189
    name "3189"
  ]
  edge
  [
    source 163
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3190
    name "3190"
  ]
  edge
  [
    source 162
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3191
    name "3191"
  ]
  edge
  [
    source 161
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3192
    name "3192"
  ]
  edge
  [
    source 160
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3193
    name "3193"
  ]
  edge
  [
    source 159
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3194
    name "3194"
  ]
  edge
  [
    source 158
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3195
    name "3195"
  ]
  edge
  [
    source 157
    target 154
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3196
    name "3196"
  ]
  edge
  [
    source 156
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3197
    name "3197"
  ]
  edge
  [
    source 155
    target 154
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3198
    name "3198"
  ]
  edge
  [
    source 167
    target 153
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3199
    name "3199"
  ]
  edge
  [
    source 166
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3200
    name "3200"
  ]
  edge
  [
    source 165
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3201
    name "3201"
  ]
  edge
  [
    source 164
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3202
    name "3202"
  ]
  edge
  [
    source 163
    target 153
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3203
    name "3203"
  ]
  edge
  [
    source 162
    target 153
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3204
    name "3204"
  ]
  edge
  [
    source 161
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3205
    name "3205"
  ]
  edge
  [
    source 160
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3206
    name "3206"
  ]
  edge
  [
    source 159
    target 153
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3207
    name "3207"
  ]
  edge
  [
    source 158
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3208
    name "3208"
  ]
  edge
  [
    source 157
    target 153
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3209
    name "3209"
  ]
  edge
  [
    source 156
    target 153
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3210
    name "3210"
  ]
  edge
  [
    source 155
    target 153
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3211
    name "3211"
  ]
  edge
  [
    source 154
    target 153
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3212
    name "3212"
  ]
  edge
  [
    source 167
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3213
    name "3213"
  ]
  edge
  [
    source 166
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3214
    name "3214"
  ]
  edge
  [
    source 165
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3215
    name "3215"
  ]
  edge
  [
    source 164
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3216
    name "3216"
  ]
  edge
  [
    source 163
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3217
    name "3217"
  ]
  edge
  [
    source 162
    target 152
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3218
    name "3218"
  ]
  edge
  [
    source 161
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3219
    name "3219"
  ]
  edge
  [
    source 160
    target 152
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3220
    name "3220"
  ]
  edge
  [
    source 159
    target 152
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3221
    name "3221"
  ]
  edge
  [
    source 158
    target 152
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3222
    name "3222"
  ]
  edge
  [
    source 157
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3223
    name "3223"
  ]
  edge
  [
    source 156
    target 152
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3224
    name "3224"
  ]
  edge
  [
    source 155
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3225
    name "3225"
  ]
  edge
  [
    source 154
    target 152
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3226
    name "3226"
  ]
  edge
  [
    source 153
    target 152
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3227
    name "3227"
  ]
  edge
  [
    source 670
    target 151
    measure 7
    diseases "Ectodermal_dysplasia"
    GOmeasure 7
    id 3228
    name "3228"
  ]
  edge
  [
    source 669
    target 151
    measure 5
    diseases "Ectodermal_dysplasia"
    GOmeasure 5
    id 3229
    name "3229"
  ]
  edge
  [
    source 668
    target 151
    measure 7
    diseases "Ectodermal_dysplasia"
    GOmeasure 7
    id 3230
    name "3230"
  ]
  edge
  [
    source 667
    target 151
    measure 7
    diseases "Ectodermal_dysplasia"
    GOmeasure 7
    id 3231
    name "3231"
  ]
  edge
  [
    source 666
    target 151
    measure 4
    diseases "Ectodermal_dysplasia"
    GOmeasure 4
    id 3232
    name "3232"
  ]
  edge
  [
    source 665
    target 151
    measure 6
    diseases "Ectodermal_dysplasia"
    GOmeasure 6
    id 3233
    name "3233"
  ]
  edge
  [
    source 167
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3234
    name "3234"
  ]
  edge
  [
    source 166
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3235
    name "3235"
  ]
  edge
  [
    source 165
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3236
    name "3236"
  ]
  edge
  [
    source 164
    target 151
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3237
    name "3237"
  ]
  edge
  [
    source 163
    target 151
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3238
    name "3238"
  ]
  edge
  [
    source 162
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3239
    name "3239"
  ]
  edge
  [
    source 161
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3240
    name "3240"
  ]
  edge
  [
    source 160
    target 151
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3241
    name "3241"
  ]
  edge
  [
    source 159
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3242
    name "3242"
  ]
  edge
  [
    source 158
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3243
    name "3243"
  ]
  edge
  [
    source 157
    target 151
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3244
    name "3244"
  ]
  edge
  [
    source 156
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3245
    name "3245"
  ]
  edge
  [
    source 155
    target 151
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3246
    name "3246"
  ]
  edge
  [
    source 154
    target 151
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3247
    name "3247"
  ]
  edge
  [
    source 153
    target 151
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3248
    name "3248"
  ]
  edge
  [
    source 152
    target 151
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3249
    name "3249"
  ]
  edge
  [
    source 710
    target 150
    measure 5
    diseases "Vohwinkel_syndrome"
    GOmeasure 5
    id 3250
    name "3250"
  ]
  edge
  [
    source 167
    target 150
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3251
    name "3251"
  ]
  edge
  [
    source 166
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3252
    name "3252"
  ]
  edge
  [
    source 165
    target 150
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3253
    name "3253"
  ]
  edge
  [
    source 164
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3254
    name "3254"
  ]
  edge
  [
    source 163
    target 150
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3255
    name "3255"
  ]
  edge
  [
    source 162
    target 150
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3256
    name "3256"
  ]
  edge
  [
    source 161
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3257
    name "3257"
  ]
  edge
  [
    source 160
    target 150
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3258
    name "3258"
  ]
  edge
  [
    source 159
    target 150
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3259
    name "3259"
  ]
  edge
  [
    source 158
    target 150
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3260
    name "3260"
  ]
  edge
  [
    source 157
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3261
    name "3261"
  ]
  edge
  [
    source 156
    target 150
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3262
    name "3262"
  ]
  edge
  [
    source 155
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3263
    name "3263"
  ]
  edge
  [
    source 154
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3264
    name "3264"
  ]
  edge
  [
    source 153
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3265
    name "3265"
  ]
  edge
  [
    source 152
    target 150
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3266
    name "3266"
  ]
  edge
  [
    source 151
    target 150
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3267
    name "3267"
  ]
  edge
  [
    source 167
    target 149
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3268
    name "3268"
  ]
  edge
  [
    source 166
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3269
    name "3269"
  ]
  edge
  [
    source 165
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3270
    name "3270"
  ]
  edge
  [
    source 164
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3271
    name "3271"
  ]
  edge
  [
    source 163
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3272
    name "3272"
  ]
  edge
  [
    source 162
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3273
    name "3273"
  ]
  edge
  [
    source 161
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3274
    name "3274"
  ]
  edge
  [
    source 160
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3275
    name "3275"
  ]
  edge
  [
    source 159
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3276
    name "3276"
  ]
  edge
  [
    source 158
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3277
    name "3277"
  ]
  edge
  [
    source 157
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3278
    name "3278"
  ]
  edge
  [
    source 156
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3279
    name "3279"
  ]
  edge
  [
    source 155
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3280
    name "3280"
  ]
  edge
  [
    source 154
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3281
    name "3281"
  ]
  edge
  [
    source 153
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3282
    name "3282"
  ]
  edge
  [
    source 152
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3283
    name "3283"
  ]
  edge
  [
    source 151
    target 149
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3284
    name "3284"
  ]
  edge
  [
    source 150
    target 149
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3285
    name "3285"
  ]
  edge
  [
    source 167
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3286
    name "3286"
  ]
  edge
  [
    source 166
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3287
    name "3287"
  ]
  edge
  [
    source 165
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3288
    name "3288"
  ]
  edge
  [
    source 164
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3289
    name "3289"
  ]
  edge
  [
    source 163
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3290
    name "3290"
  ]
  edge
  [
    source 162
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3291
    name "3291"
  ]
  edge
  [
    source 161
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3292
    name "3292"
  ]
  edge
  [
    source 160
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3293
    name "3293"
  ]
  edge
  [
    source 159
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3294
    name "3294"
  ]
  edge
  [
    source 158
    target 148
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3295
    name "3295"
  ]
  edge
  [
    source 157
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3296
    name "3296"
  ]
  edge
  [
    source 156
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3297
    name "3297"
  ]
  edge
  [
    source 155
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3298
    name "3298"
  ]
  edge
  [
    source 154
    target 148
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3299
    name "3299"
  ]
  edge
  [
    source 153
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3300
    name "3300"
  ]
  edge
  [
    source 152
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3301
    name "3301"
  ]
  edge
  [
    source 151
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3302
    name "3302"
  ]
  edge
  [
    source 150
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3303
    name "3303"
  ]
  edge
  [
    source 149
    target 148
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3304
    name "3304"
  ]
  edge
  [
    source 710
    target 147
    measure 5
    diseases "Erythrokeratoderma"
    GOmeasure 5
    id 3305
    name "3305"
  ]
  edge
  [
    source 167
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3306
    name "3306"
  ]
  edge
  [
    source 166
    target 147
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3307
    name "3307"
  ]
  edge
  [
    source 165
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3308
    name "3308"
  ]
  edge
  [
    source 164
    target 147
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3309
    name "3309"
  ]
  edge
  [
    source 163
    target 147
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3310
    name "3310"
  ]
  edge
  [
    source 162
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3311
    name "3311"
  ]
  edge
  [
    source 161
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3312
    name "3312"
  ]
  edge
  [
    source 160
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3313
    name "3313"
  ]
  edge
  [
    source 159
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3314
    name "3314"
  ]
  edge
  [
    source 158
    target 147
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3315
    name "3315"
  ]
  edge
  [
    source 157
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3316
    name "3316"
  ]
  edge
  [
    source 156
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3317
    name "3317"
  ]
  edge
  [
    source 155
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3318
    name "3318"
  ]
  edge
  [
    source 154
    target 147
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3319
    name "3319"
  ]
  edge
  [
    source 153
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3320
    name "3320"
  ]
  edge
  [
    source 152
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3321
    name "3321"
  ]
  edge
  [
    source 151
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3322
    name "3322"
  ]
  edge
  [
    source 150
    target 147
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3323
    name "3323"
  ]
  edge
  [
    source 149
    target 147
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3324
    name "3324"
  ]
  edge
  [
    source 148
    target 147
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3325
    name "3325"
  ]
  edge
  [
    source 167
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3326
    name "3326"
  ]
  edge
  [
    source 166
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3327
    name "3327"
  ]
  edge
  [
    source 165
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3328
    name "3328"
  ]
  edge
  [
    source 164
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3329
    name "3329"
  ]
  edge
  [
    source 163
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3330
    name "3330"
  ]
  edge
  [
    source 162
    target 146
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3331
    name "3331"
  ]
  edge
  [
    source 161
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3332
    name "3332"
  ]
  edge
  [
    source 160
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3333
    name "3333"
  ]
  edge
  [
    source 159
    target 146
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3334
    name "3334"
  ]
  edge
  [
    source 158
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3335
    name "3335"
  ]
  edge
  [
    source 157
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3336
    name "3336"
  ]
  edge
  [
    source 156
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3337
    name "3337"
  ]
  edge
  [
    source 155
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3338
    name "3338"
  ]
  edge
  [
    source 154
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3339
    name "3339"
  ]
  edge
  [
    source 153
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3340
    name "3340"
  ]
  edge
  [
    source 152
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3341
    name "3341"
  ]
  edge
  [
    source 151
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3342
    name "3342"
  ]
  edge
  [
    source 150
    target 146
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3343
    name "3343"
  ]
  edge
  [
    source 149
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3344
    name "3344"
  ]
  edge
  [
    source 148
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3345
    name "3345"
  ]
  edge
  [
    source 147
    target 146
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3346
    name "3346"
  ]
  edge
  [
    source 167
    target 145
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3347
    name "3347"
  ]
  edge
  [
    source 166
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3348
    name "3348"
  ]
  edge
  [
    source 165
    target 145
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3349
    name "3349"
  ]
  edge
  [
    source 164
    target 145
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3350
    name "3350"
  ]
  edge
  [
    source 163
    target 145
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3351
    name "3351"
  ]
  edge
  [
    source 162
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3352
    name "3352"
  ]
  edge
  [
    source 161
    target 145
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3353
    name "3353"
  ]
  edge
  [
    source 160
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3354
    name "3354"
  ]
  edge
  [
    source 159
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3355
    name "3355"
  ]
  edge
  [
    source 158
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3356
    name "3356"
  ]
  edge
  [
    source 157
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3357
    name "3357"
  ]
  edge
  [
    source 156
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3358
    name "3358"
  ]
  edge
  [
    source 155
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3359
    name "3359"
  ]
  edge
  [
    source 154
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3360
    name "3360"
  ]
  edge
  [
    source 153
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3361
    name "3361"
  ]
  edge
  [
    source 152
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3362
    name "3362"
  ]
  edge
  [
    source 151
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3363
    name "3363"
  ]
  edge
  [
    source 150
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3364
    name "3364"
  ]
  edge
  [
    source 149
    target 145
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3365
    name "3365"
  ]
  edge
  [
    source 148
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3366
    name "3366"
  ]
  edge
  [
    source 147
    target 145
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3367
    name "3367"
  ]
  edge
  [
    source 146
    target 145
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3368
    name "3368"
  ]
  edge
  [
    source 580
    target 144
    measure 7
    diseases "Stickler_syndrome"
    GOmeasure 7
    id 3369
    name "3369"
  ]
  edge
  [
    source 578
    target 144
    measure 5
    diseases "Stickler_syndrome"
    GOmeasure 5
    id 3370
    name "3370"
  ]
  edge
  [
    source 167
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3371
    name "3371"
  ]
  edge
  [
    source 166
    target 144
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3372
    name "3372"
  ]
  edge
  [
    source 165
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3373
    name "3373"
  ]
  edge
  [
    source 164
    target 144
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3374
    name "3374"
  ]
  edge
  [
    source 163
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3375
    name "3375"
  ]
  edge
  [
    source 162
    target 144
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3376
    name "3376"
  ]
  edge
  [
    source 161
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3377
    name "3377"
  ]
  edge
  [
    source 160
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3378
    name "3378"
  ]
  edge
  [
    source 159
    target 144
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3379
    name "3379"
  ]
  edge
  [
    source 158
    target 144
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3380
    name "3380"
  ]
  edge
  [
    source 157
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3381
    name "3381"
  ]
  edge
  [
    source 156
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3382
    name "3382"
  ]
  edge
  [
    source 155
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3383
    name "3383"
  ]
  edge
  [
    source 154
    target 144
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3384
    name "3384"
  ]
  edge
  [
    source 153
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3385
    name "3385"
  ]
  edge
  [
    source 152
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3386
    name "3386"
  ]
  edge
  [
    source 151
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3387
    name "3387"
  ]
  edge
  [
    source 150
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3388
    name "3388"
  ]
  edge
  [
    source 149
    target 144
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3389
    name "3389"
  ]
  edge
  [
    source 148
    target 144
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3390
    name "3390"
  ]
  edge
  [
    source 147
    target 144
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3391
    name "3391"
  ]
  edge
  [
    source 146
    target 144
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3392
    name "3392"
  ]
  edge
  [
    source 145
    target 144
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3393
    name "3393"
  ]
  edge
  [
    source 167
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3394
    name "3394"
  ]
  edge
  [
    source 166
    target 143
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3395
    name "3395"
  ]
  edge
  [
    source 165
    target 143
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3396
    name "3396"
  ]
  edge
  [
    source 164
    target 143
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3397
    name "3397"
  ]
  edge
  [
    source 163
    target 143
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3398
    name "3398"
  ]
  edge
  [
    source 162
    target 143
    measure 7
    diseases "Deafness,Usher_syndrome"
    GOmeasure 7
    id 3399
    name "3399"
  ]
  edge
  [
    source 161
    target 143
    measure 5
    diseases "Deafness,Usher_syndrome"
    GOmeasure 5
    id 3400
    name "3400"
  ]
  edge
  [
    source 160
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3401
    name "3401"
  ]
  edge
  [
    source 159
    target 143
    measure 7
    diseases "Deafness,Usher_syndrome"
    GOmeasure 7
    id 3402
    name "3402"
  ]
  edge
  [
    source 158
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3403
    name "3403"
  ]
  edge
  [
    source 157
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3404
    name "3404"
  ]
  edge
  [
    source 156
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3405
    name "3405"
  ]
  edge
  [
    source 155
    target 143
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3406
    name "3406"
  ]
  edge
  [
    source 154
    target 143
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3407
    name "3407"
  ]
  edge
  [
    source 153
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3408
    name "3408"
  ]
  edge
  [
    source 152
    target 143
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3409
    name "3409"
  ]
  edge
  [
    source 151
    target 143
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3410
    name "3410"
  ]
  edge
  [
    source 150
    target 143
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3411
    name "3411"
  ]
  edge
  [
    source 149
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3412
    name "3412"
  ]
  edge
  [
    source 148
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3413
    name "3413"
  ]
  edge
  [
    source 147
    target 143
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3414
    name "3414"
  ]
  edge
  [
    source 146
    target 143
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3415
    name "3415"
  ]
  edge
  [
    source 145
    target 143
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3416
    name "3416"
  ]
  edge
  [
    source 144
    target 143
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3417
    name "3417"
  ]
  edge
  [
    source 167
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3418
    name "3418"
  ]
  edge
  [
    source 166
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3419
    name "3419"
  ]
  edge
  [
    source 165
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3420
    name "3420"
  ]
  edge
  [
    source 164
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3421
    name "3421"
  ]
  edge
  [
    source 163
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3422
    name "3422"
  ]
  edge
  [
    source 162
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3423
    name "3423"
  ]
  edge
  [
    source 161
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3424
    name "3424"
  ]
  edge
  [
    source 160
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3425
    name "3425"
  ]
  edge
  [
    source 159
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3426
    name "3426"
  ]
  edge
  [
    source 158
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3427
    name "3427"
  ]
  edge
  [
    source 157
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3428
    name "3428"
  ]
  edge
  [
    source 156
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3429
    name "3429"
  ]
  edge
  [
    source 155
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3430
    name "3430"
  ]
  edge
  [
    source 154
    target 142
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3431
    name "3431"
  ]
  edge
  [
    source 153
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3432
    name "3432"
  ]
  edge
  [
    source 152
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3433
    name "3433"
  ]
  edge
  [
    source 151
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3434
    name "3434"
  ]
  edge
  [
    source 150
    target 142
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3435
    name "3435"
  ]
  edge
  [
    source 149
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3436
    name "3436"
  ]
  edge
  [
    source 148
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3437
    name "3437"
  ]
  edge
  [
    source 147
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3438
    name "3438"
  ]
  edge
  [
    source 146
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3439
    name "3439"
  ]
  edge
  [
    source 145
    target 142
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3440
    name "3440"
  ]
  edge
  [
    source 144
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3441
    name "3441"
  ]
  edge
  [
    source 143
    target 142
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3442
    name "3442"
  ]
  edge
  [
    source 167
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3443
    name "3443"
  ]
  edge
  [
    source 166
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3444
    name "3444"
  ]
  edge
  [
    source 165
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3445
    name "3445"
  ]
  edge
  [
    source 164
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3446
    name "3446"
  ]
  edge
  [
    source 163
    target 141
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3447
    name "3447"
  ]
  edge
  [
    source 162
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3448
    name "3448"
  ]
  edge
  [
    source 161
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3449
    name "3449"
  ]
  edge
  [
    source 160
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3450
    name "3450"
  ]
  edge
  [
    source 159
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3451
    name "3451"
  ]
  edge
  [
    source 158
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3452
    name "3452"
  ]
  edge
  [
    source 157
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3453
    name "3453"
  ]
  edge
  [
    source 156
    target 141
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3454
    name "3454"
  ]
  edge
  [
    source 155
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3455
    name "3455"
  ]
  edge
  [
    source 154
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3456
    name "3456"
  ]
  edge
  [
    source 153
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3457
    name "3457"
  ]
  edge
  [
    source 152
    target 141
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3458
    name "3458"
  ]
  edge
  [
    source 151
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3459
    name "3459"
  ]
  edge
  [
    source 150
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3460
    name "3460"
  ]
  edge
  [
    source 149
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3461
    name "3461"
  ]
  edge
  [
    source 148
    target 141
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3462
    name "3462"
  ]
  edge
  [
    source 147
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3463
    name "3463"
  ]
  edge
  [
    source 146
    target 141
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3464
    name "3464"
  ]
  edge
  [
    source 145
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3465
    name "3465"
  ]
  edge
  [
    source 144
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3466
    name "3466"
  ]
  edge
  [
    source 143
    target 141
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3467
    name "3467"
  ]
  edge
  [
    source 142
    target 141
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3468
    name "3468"
  ]
  edge
  [
    source 140
    target 139
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3469
    name "3469"
  ]
  edge
  [
    source 140
    target 138
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3470
    name "3470"
  ]
  edge
  [
    source 139
    target 138
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3471
    name "3471"
  ]
  edge
  [
    source 140
    target 137
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3472
    name "3472"
  ]
  edge
  [
    source 139
    target 137
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3473
    name "3473"
  ]
  edge
  [
    source 138
    target 137
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3474
    name "3474"
  ]
  edge
  [
    source 435
    target 136
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 3475
    name "3475"
  ]
  edge
  [
    source 434
    target 136
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 3476
    name "3476"
  ]
  edge
  [
    source 433
    target 136
    measure 7
    diseases "Leigh_syndrome"
    GOmeasure 7
    id 3477
    name "3477"
  ]
  edge
  [
    source 432
    target 136
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 3478
    name "3478"
  ]
  edge
  [
    source 431
    target 136
    measure 5
    diseases "Leigh_syndrome"
    GOmeasure 5
    id 3479
    name "3479"
  ]
  edge
  [
    source 430
    target 136
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 3480
    name "3480"
  ]
  edge
  [
    source 429
    target 136
    measure 7
    diseases "Leigh_syndrome"
    GOmeasure 7
    id 3481
    name "3481"
  ]
  edge
  [
    source 428
    target 136
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 3482
    name "3482"
  ]
  edge
  [
    source 427
    target 136
    measure 6
    diseases "Leigh_syndrome"
    GOmeasure 6
    id 3483
    name "3483"
  ]
  edge
  [
    source 426
    target 136
    measure 7
    diseases "Leigh_syndrome"
    GOmeasure 7
    id 3484
    name "3484"
  ]
  edge
  [
    source 140
    target 136
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3485
    name "3485"
  ]
  edge
  [
    source 139
    target 136
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3486
    name "3486"
  ]
  edge
  [
    source 138
    target 136
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3487
    name "3487"
  ]
  edge
  [
    source 137
    target 136
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3488
    name "3488"
  ]
  edge
  [
    source 140
    target 135
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3489
    name "3489"
  ]
  edge
  [
    source 139
    target 135
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3490
    name "3490"
  ]
  edge
  [
    source 138
    target 135
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3491
    name "3491"
  ]
  edge
  [
    source 137
    target 135
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3492
    name "3492"
  ]
  edge
  [
    source 136
    target 135
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3493
    name "3493"
  ]
  edge
  [
    source 140
    target 134
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3494
    name "3494"
  ]
  edge
  [
    source 139
    target 134
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3495
    name "3495"
  ]
  edge
  [
    source 138
    target 134
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3496
    name "3496"
  ]
  edge
  [
    source 137
    target 134
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3497
    name "3497"
  ]
  edge
  [
    source 136
    target 134
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3498
    name "3498"
  ]
  edge
  [
    source 135
    target 134
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3499
    name "3499"
  ]
  edge
  [
    source 140
    target 133
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3500
    name "3500"
  ]
  edge
  [
    source 139
    target 133
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3501
    name "3501"
  ]
  edge
  [
    source 138
    target 133
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3502
    name "3502"
  ]
  edge
  [
    source 137
    target 133
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3503
    name "3503"
  ]
  edge
  [
    source 136
    target 133
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3504
    name "3504"
  ]
  edge
  [
    source 135
    target 133
    measure 8
    diseases "Cardiomyopathy"
    GOmeasure 8
    id 3505
    name "3505"
  ]
  edge
  [
    source 134
    target 133
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3506
    name "3506"
  ]
  edge
  [
    source 140
    target 132
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3507
    name "3507"
  ]
  edge
  [
    source 139
    target 132
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3508
    name "3508"
  ]
  edge
  [
    source 138
    target 132
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3509
    name "3509"
  ]
  edge
  [
    source 137
    target 132
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3510
    name "3510"
  ]
  edge
  [
    source 136
    target 132
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3511
    name "3511"
  ]
  edge
  [
    source 135
    target 132
    measure 8
    diseases "Cardiomyopathy"
    GOmeasure 8
    id 3512
    name "3512"
  ]
  edge
  [
    source 134
    target 132
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3513
    name "3513"
  ]
  edge
  [
    source 133
    target 132
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3514
    name "3514"
  ]
  edge
  [
    source 614
    target 131
    measure 6
    diseases "Atrial_fibrillation"
    GOmeasure 6
    id 3515
    name "3515"
  ]
  edge
  [
    source 300
    target 131
    measure 5
    diseases "Atrial_fibrillation"
    GOmeasure 5
    id 3516
    name "3516"
  ]
  edge
  [
    source 296
    target 131
    measure 6
    diseases "Atrial_fibrillation"
    GOmeasure 6
    id 3517
    name "3517"
  ]
  edge
  [
    source 228
    target 131
    measure 5
    diseases "Atrial_fibrillation"
    GOmeasure 5
    id 3518
    name "3518"
  ]
  edge
  [
    source 140
    target 131
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3519
    name "3519"
  ]
  edge
  [
    source 139
    target 131
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3520
    name "3520"
  ]
  edge
  [
    source 138
    target 131
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3521
    name "3521"
  ]
  edge
  [
    source 137
    target 131
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3522
    name "3522"
  ]
  edge
  [
    source 136
    target 131
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3523
    name "3523"
  ]
  edge
  [
    source 135
    target 131
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3524
    name "3524"
  ]
  edge
  [
    source 134
    target 131
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3525
    name "3525"
  ]
  edge
  [
    source 133
    target 131
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3526
    name "3526"
  ]
  edge
  [
    source 132
    target 131
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3527
    name "3527"
  ]
  edge
  [
    source 140
    target 130
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3528
    name "3528"
  ]
  edge
  [
    source 139
    target 130
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3529
    name "3529"
  ]
  edge
  [
    source 138
    target 130
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3530
    name "3530"
  ]
  edge
  [
    source 137
    target 130
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3531
    name "3531"
  ]
  edge
  [
    source 136
    target 130
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3532
    name "3532"
  ]
  edge
  [
    source 135
    target 130
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3533
    name "3533"
  ]
  edge
  [
    source 134
    target 130
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3534
    name "3534"
  ]
  edge
  [
    source 133
    target 130
    measure 8
    diseases "Cardiomyopathy"
    GOmeasure 8
    id 3535
    name "3535"
  ]
  edge
  [
    source 132
    target 130
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3536
    name "3536"
  ]
  edge
  [
    source 131
    target 130
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3537
    name "3537"
  ]
  edge
  [
    source 140
    target 129
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3538
    name "3538"
  ]
  edge
  [
    source 139
    target 129
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3539
    name "3539"
  ]
  edge
  [
    source 138
    target 129
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3540
    name "3540"
  ]
  edge
  [
    source 137
    target 129
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3541
    name "3541"
  ]
  edge
  [
    source 136
    target 129
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3542
    name "3542"
  ]
  edge
  [
    source 135
    target 129
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3543
    name "3543"
  ]
  edge
  [
    source 134
    target 129
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3544
    name "3544"
  ]
  edge
  [
    source 133
    target 129
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3545
    name "3545"
  ]
  edge
  [
    source 132
    target 129
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3546
    name "3546"
  ]
  edge
  [
    source 131
    target 129
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3547
    name "3547"
  ]
  edge
  [
    source 130
    target 129
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3548
    name "3548"
  ]
  edge
  [
    source 140
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3549
    name "3549"
  ]
  edge
  [
    source 139
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3550
    name "3550"
  ]
  edge
  [
    source 138
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3551
    name "3551"
  ]
  edge
  [
    source 137
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3552
    name "3552"
  ]
  edge
  [
    source 136
    target 128
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3553
    name "3553"
  ]
  edge
  [
    source 135
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3554
    name "3554"
  ]
  edge
  [
    source 134
    target 128
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3555
    name "3555"
  ]
  edge
  [
    source 133
    target 128
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3556
    name "3556"
  ]
  edge
  [
    source 132
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3557
    name "3557"
  ]
  edge
  [
    source 131
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3558
    name "3558"
  ]
  edge
  [
    source 130
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3559
    name "3559"
  ]
  edge
  [
    source 129
    target 128
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3560
    name "3560"
  ]
  edge
  [
    source 524
    target 127
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3561
    name "3561"
  ]
  edge
  [
    source 523
    target 127
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3562
    name "3562"
  ]
  edge
  [
    source 522
    target 127
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3563
    name "3563"
  ]
  edge
  [
    source 521
    target 127
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3564
    name "3564"
  ]
  edge
  [
    source 520
    target 127
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3565
    name "3565"
  ]
  edge
  [
    source 519
    target 127
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3566
    name "3566"
  ]
  edge
  [
    source 518
    target 127
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3567
    name "3567"
  ]
  edge
  [
    source 517
    target 127
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3568
    name "3568"
  ]
  edge
  [
    source 516
    target 127
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3569
    name "3569"
  ]
  edge
  [
    source 515
    target 127
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3570
    name "3570"
  ]
  edge
  [
    source 140
    target 127
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3571
    name "3571"
  ]
  edge
  [
    source 139
    target 127
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3572
    name "3572"
  ]
  edge
  [
    source 138
    target 127
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3573
    name "3573"
  ]
  edge
  [
    source 137
    target 127
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3574
    name "3574"
  ]
  edge
  [
    source 136
    target 127
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3575
    name "3575"
  ]
  edge
  [
    source 135
    target 127
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3576
    name "3576"
  ]
  edge
  [
    source 134
    target 127
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3577
    name "3577"
  ]
  edge
  [
    source 133
    target 127
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3578
    name "3578"
  ]
  edge
  [
    source 132
    target 127
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3579
    name "3579"
  ]
  edge
  [
    source 131
    target 127
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3580
    name "3580"
  ]
  edge
  [
    source 130
    target 127
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3581
    name "3581"
  ]
  edge
  [
    source 129
    target 127
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3582
    name "3582"
  ]
  edge
  [
    source 128
    target 127
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3583
    name "3583"
  ]
  edge
  [
    source 140
    target 126
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3584
    name "3584"
  ]
  edge
  [
    source 139
    target 126
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3585
    name "3585"
  ]
  edge
  [
    source 138
    target 126
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3586
    name "3586"
  ]
  edge
  [
    source 137
    target 126
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3587
    name "3587"
  ]
  edge
  [
    source 136
    target 126
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3588
    name "3588"
  ]
  edge
  [
    source 135
    target 126
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3589
    name "3589"
  ]
  edge
  [
    source 134
    target 126
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3590
    name "3590"
  ]
  edge
  [
    source 133
    target 126
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3591
    name "3591"
  ]
  edge
  [
    source 132
    target 126
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3592
    name "3592"
  ]
  edge
  [
    source 131
    target 126
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3593
    name "3593"
  ]
  edge
  [
    source 130
    target 126
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3594
    name "3594"
  ]
  edge
  [
    source 129
    target 126
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3595
    name "3595"
  ]
  edge
  [
    source 128
    target 126
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3596
    name "3596"
  ]
  edge
  [
    source 127
    target 126
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3597
    name "3597"
  ]
  edge
  [
    source 524
    target 125
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3598
    name "3598"
  ]
  edge
  [
    source 523
    target 125
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3599
    name "3599"
  ]
  edge
  [
    source 522
    target 125
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3600
    name "3600"
  ]
  edge
  [
    source 521
    target 125
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3601
    name "3601"
  ]
  edge
  [
    source 520
    target 125
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3602
    name "3602"
  ]
  edge
  [
    source 519
    target 125
    measure 8
    diseases "Muscular_dystrophy"
    GOmeasure 8
    id 3603
    name "3603"
  ]
  edge
  [
    source 518
    target 125
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3604
    name "3604"
  ]
  edge
  [
    source 517
    target 125
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3605
    name "3605"
  ]
  edge
  [
    source 516
    target 125
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3606
    name "3606"
  ]
  edge
  [
    source 515
    target 125
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3607
    name "3607"
  ]
  edge
  [
    source 140
    target 125
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3608
    name "3608"
  ]
  edge
  [
    source 139
    target 125
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3609
    name "3609"
  ]
  edge
  [
    source 138
    target 125
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3610
    name "3610"
  ]
  edge
  [
    source 137
    target 125
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3611
    name "3611"
  ]
  edge
  [
    source 136
    target 125
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3612
    name "3612"
  ]
  edge
  [
    source 135
    target 125
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3613
    name "3613"
  ]
  edge
  [
    source 134
    target 125
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3614
    name "3614"
  ]
  edge
  [
    source 133
    target 125
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3615
    name "3615"
  ]
  edge
  [
    source 132
    target 125
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3616
    name "3616"
  ]
  edge
  [
    source 131
    target 125
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3617
    name "3617"
  ]
  edge
  [
    source 130
    target 125
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3618
    name "3618"
  ]
  edge
  [
    source 129
    target 125
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3619
    name "3619"
  ]
  edge
  [
    source 128
    target 125
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3620
    name "3620"
  ]
  edge
  [
    source 127
    target 125
    measure 6
    diseases "Cardiomyopathy,Muscular_dystrophy"
    GOmeasure 6
    id 3621
    name "3621"
  ]
  edge
  [
    source 126
    target 125
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3622
    name "3622"
  ]
  edge
  [
    source 167
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3623
    name "3623"
  ]
  edge
  [
    source 166
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3624
    name "3624"
  ]
  edge
  [
    source 165
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3625
    name "3625"
  ]
  edge
  [
    source 164
    target 124
    measure 7
    diseases "Deafness"
    GOmeasure 7
    id 3626
    name "3626"
  ]
  edge
  [
    source 163
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3627
    name "3627"
  ]
  edge
  [
    source 162
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3628
    name "3628"
  ]
  edge
  [
    source 161
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3629
    name "3629"
  ]
  edge
  [
    source 160
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3630
    name "3630"
  ]
  edge
  [
    source 159
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3631
    name "3631"
  ]
  edge
  [
    source 158
    target 124
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3632
    name "3632"
  ]
  edge
  [
    source 157
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3633
    name "3633"
  ]
  edge
  [
    source 156
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3634
    name "3634"
  ]
  edge
  [
    source 155
    target 124
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3635
    name "3635"
  ]
  edge
  [
    source 154
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3636
    name "3636"
  ]
  edge
  [
    source 153
    target 124
    measure 4
    diseases "Deafness"
    GOmeasure 4
    id 3637
    name "3637"
  ]
  edge
  [
    source 152
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3638
    name "3638"
  ]
  edge
  [
    source 151
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3639
    name "3639"
  ]
  edge
  [
    source 150
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3640
    name "3640"
  ]
  edge
  [
    source 149
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3641
    name "3641"
  ]
  edge
  [
    source 148
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3642
    name "3642"
  ]
  edge
  [
    source 147
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3643
    name "3643"
  ]
  edge
  [
    source 146
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3644
    name "3644"
  ]
  edge
  [
    source 145
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3645
    name "3645"
  ]
  edge
  [
    source 144
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3646
    name "3646"
  ]
  edge
  [
    source 143
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3647
    name "3647"
  ]
  edge
  [
    source 142
    target 124
    measure 5
    diseases "Deafness"
    GOmeasure 5
    id 3648
    name "3648"
  ]
  edge
  [
    source 141
    target 124
    measure 6
    diseases "Deafness"
    GOmeasure 6
    id 3649
    name "3649"
  ]
  edge
  [
    source 140
    target 124
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3650
    name "3650"
  ]
  edge
  [
    source 139
    target 124
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3651
    name "3651"
  ]
  edge
  [
    source 138
    target 124
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3652
    name "3652"
  ]
  edge
  [
    source 137
    target 124
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3653
    name "3653"
  ]
  edge
  [
    source 136
    target 124
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3654
    name "3654"
  ]
  edge
  [
    source 135
    target 124
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3655
    name "3655"
  ]
  edge
  [
    source 134
    target 124
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3656
    name "3656"
  ]
  edge
  [
    source 133
    target 124
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3657
    name "3657"
  ]
  edge
  [
    source 132
    target 124
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3658
    name "3658"
  ]
  edge
  [
    source 131
    target 124
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3659
    name "3659"
  ]
  edge
  [
    source 130
    target 124
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3660
    name "3660"
  ]
  edge
  [
    source 129
    target 124
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3661
    name "3661"
  ]
  edge
  [
    source 128
    target 124
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3662
    name "3662"
  ]
  edge
  [
    source 127
    target 124
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3663
    name "3663"
  ]
  edge
  [
    source 126
    target 124
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3664
    name "3664"
  ]
  edge
  [
    source 125
    target 124
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3665
    name "3665"
  ]
  edge
  [
    source 524
    target 123
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3666
    name "3666"
  ]
  edge
  [
    source 523
    target 123
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3667
    name "3667"
  ]
  edge
  [
    source 522
    target 123
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3668
    name "3668"
  ]
  edge
  [
    source 521
    target 123
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3669
    name "3669"
  ]
  edge
  [
    source 520
    target 123
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3670
    name "3670"
  ]
  edge
  [
    source 519
    target 123
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3671
    name "3671"
  ]
  edge
  [
    source 518
    target 123
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3672
    name "3672"
  ]
  edge
  [
    source 517
    target 123
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3673
    name "3673"
  ]
  edge
  [
    source 516
    target 123
    measure 7
    diseases "Muscular_dystrophy"
    GOmeasure 7
    id 3674
    name "3674"
  ]
  edge
  [
    source 515
    target 123
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3675
    name "3675"
  ]
  edge
  [
    source 140
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3676
    name "3676"
  ]
  edge
  [
    source 139
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3677
    name "3677"
  ]
  edge
  [
    source 138
    target 123
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3678
    name "3678"
  ]
  edge
  [
    source 137
    target 123
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3679
    name "3679"
  ]
  edge
  [
    source 136
    target 123
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3680
    name "3680"
  ]
  edge
  [
    source 135
    target 123
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3681
    name "3681"
  ]
  edge
  [
    source 134
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3682
    name "3682"
  ]
  edge
  [
    source 133
    target 123
    measure 8
    diseases "Cardiomyopathy"
    GOmeasure 8
    id 3683
    name "3683"
  ]
  edge
  [
    source 132
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3684
    name "3684"
  ]
  edge
  [
    source 131
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3685
    name "3685"
  ]
  edge
  [
    source 130
    target 123
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3686
    name "3686"
  ]
  edge
  [
    source 129
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3687
    name "3687"
  ]
  edge
  [
    source 128
    target 123
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3688
    name "3688"
  ]
  edge
  [
    source 127
    target 123
    measure 5
    diseases "Cardiomyopathy,Muscular_dystrophy"
    GOmeasure 5
    id 3689
    name "3689"
  ]
  edge
  [
    source 126
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3690
    name "3690"
  ]
  edge
  [
    source 125
    target 123
    measure 6
    diseases "Cardiomyopathy,Muscular_dystrophy"
    GOmeasure 6
    id 3691
    name "3691"
  ]
  edge
  [
    source 124
    target 123
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3692
    name "3692"
  ]
  edge
  [
    source 140
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3693
    name "3693"
  ]
  edge
  [
    source 139
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3694
    name "3694"
  ]
  edge
  [
    source 138
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3695
    name "3695"
  ]
  edge
  [
    source 137
    target 122
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3696
    name "3696"
  ]
  edge
  [
    source 136
    target 122
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3697
    name "3697"
  ]
  edge
  [
    source 135
    target 122
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3698
    name "3698"
  ]
  edge
  [
    source 134
    target 122
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3699
    name "3699"
  ]
  edge
  [
    source 133
    target 122
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3700
    name "3700"
  ]
  edge
  [
    source 132
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3701
    name "3701"
  ]
  edge
  [
    source 131
    target 122
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3702
    name "3702"
  ]
  edge
  [
    source 130
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3703
    name "3703"
  ]
  edge
  [
    source 129
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3704
    name "3704"
  ]
  edge
  [
    source 128
    target 122
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3705
    name "3705"
  ]
  edge
  [
    source 127
    target 122
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3706
    name "3706"
  ]
  edge
  [
    source 126
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3707
    name "3707"
  ]
  edge
  [
    source 125
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3708
    name "3708"
  ]
  edge
  [
    source 124
    target 122
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3709
    name "3709"
  ]
  edge
  [
    source 123
    target 122
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3710
    name "3710"
  ]
  edge
  [
    source 740
    target 121
    measure 4
    diseases "Lipodystrophy"
    GOmeasure 4
    id 3711
    name "3711"
  ]
  edge
  [
    source 739
    target 121
    measure 7
    diseases "Lipodystrophy"
    GOmeasure 7
    id 3712
    name "3712"
  ]
  edge
  [
    source 678
    target 121
    measure 5
    diseases "Emery-Dreifuss_muscular_dystrophy"
    GOmeasure 5
    id 3713
    name "3713"
  ]
  edge
  [
    source 401
    target 121
    measure 5
    diseases "Lipodystrophy"
    GOmeasure 5
    id 3714
    name "3714"
  ]
  edge
  [
    source 213
    target 121
    measure 6
    diseases "Lipodystrophy"
    GOmeasure 6
    id 3715
    name "3715"
  ]
  edge
  [
    source 140
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3716
    name "3716"
  ]
  edge
  [
    source 139
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3717
    name "3717"
  ]
  edge
  [
    source 138
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3718
    name "3718"
  ]
  edge
  [
    source 137
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3719
    name "3719"
  ]
  edge
  [
    source 136
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3720
    name "3720"
  ]
  edge
  [
    source 135
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3721
    name "3721"
  ]
  edge
  [
    source 134
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3722
    name "3722"
  ]
  edge
  [
    source 133
    target 121
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3723
    name "3723"
  ]
  edge
  [
    source 132
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3724
    name "3724"
  ]
  edge
  [
    source 131
    target 121
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3725
    name "3725"
  ]
  edge
  [
    source 130
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3726
    name "3726"
  ]
  edge
  [
    source 129
    target 121
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3727
    name "3727"
  ]
  edge
  [
    source 128
    target 121
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3728
    name "3728"
  ]
  edge
  [
    source 127
    target 121
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3729
    name "3729"
  ]
  edge
  [
    source 126
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3730
    name "3730"
  ]
  edge
  [
    source 125
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3731
    name "3731"
  ]
  edge
  [
    source 124
    target 121
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3732
    name "3732"
  ]
  edge
  [
    source 123
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3733
    name "3733"
  ]
  edge
  [
    source 122
    target 121
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3734
    name "3734"
  ]
  edge
  [
    source 140
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3735
    name "3735"
  ]
  edge
  [
    source 139
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3736
    name "3736"
  ]
  edge
  [
    source 138
    target 120
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3737
    name "3737"
  ]
  edge
  [
    source 137
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3738
    name "3738"
  ]
  edge
  [
    source 136
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3739
    name "3739"
  ]
  edge
  [
    source 135
    target 120
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3740
    name "3740"
  ]
  edge
  [
    source 134
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3741
    name "3741"
  ]
  edge
  [
    source 133
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3742
    name "3742"
  ]
  edge
  [
    source 132
    target 120
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3743
    name "3743"
  ]
  edge
  [
    source 131
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3744
    name "3744"
  ]
  edge
  [
    source 130
    target 120
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3745
    name "3745"
  ]
  edge
  [
    source 129
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3746
    name "3746"
  ]
  edge
  [
    source 128
    target 120
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3747
    name "3747"
  ]
  edge
  [
    source 127
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3748
    name "3748"
  ]
  edge
  [
    source 126
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3749
    name "3749"
  ]
  edge
  [
    source 125
    target 120
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3750
    name "3750"
  ]
  edge
  [
    source 124
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3751
    name "3751"
  ]
  edge
  [
    source 123
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3752
    name "3752"
  ]
  edge
  [
    source 122
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3753
    name "3753"
  ]
  edge
  [
    source 121
    target 120
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3754
    name "3754"
  ]
  edge
  [
    source 490
    target 119
    measure 5
    diseases "Central_core_disease"
    GOmeasure 5
    id 3755
    name "3755"
  ]
  edge
  [
    source 140
    target 119
    measure 8
    diseases "Cardiomyopathy"
    GOmeasure 8
    id 3756
    name "3756"
  ]
  edge
  [
    source 139
    target 119
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3757
    name "3757"
  ]
  edge
  [
    source 138
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3758
    name "3758"
  ]
  edge
  [
    source 137
    target 119
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3759
    name "3759"
  ]
  edge
  [
    source 136
    target 119
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3760
    name "3760"
  ]
  edge
  [
    source 135
    target 119
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3761
    name "3761"
  ]
  edge
  [
    source 134
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3762
    name "3762"
  ]
  edge
  [
    source 133
    target 119
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3763
    name "3763"
  ]
  edge
  [
    source 132
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3764
    name "3764"
  ]
  edge
  [
    source 131
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3765
    name "3765"
  ]
  edge
  [
    source 130
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3766
    name "3766"
  ]
  edge
  [
    source 129
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3767
    name "3767"
  ]
  edge
  [
    source 128
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3768
    name "3768"
  ]
  edge
  [
    source 127
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3769
    name "3769"
  ]
  edge
  [
    source 126
    target 119
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3770
    name "3770"
  ]
  edge
  [
    source 125
    target 119
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3771
    name "3771"
  ]
  edge
  [
    source 124
    target 119
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3772
    name "3772"
  ]
  edge
  [
    source 123
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3773
    name "3773"
  ]
  edge
  [
    source 122
    target 119
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3774
    name "3774"
  ]
  edge
  [
    source 121
    target 119
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3775
    name "3775"
  ]
  edge
  [
    source 120
    target 119
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3776
    name "3776"
  ]
  edge
  [
    source 119
    target 118
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3777
    name "3777"
  ]
  edge
  [
    source 598
    target 117
    measure 7
    diseases "CPT_deficiency,_hepatic"
    GOmeasure 7
    id 3778
    name "3778"
  ]
  edge
  [
    source 119
    target 117
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3779
    name "3779"
  ]
  edge
  [
    source 118
    target 117
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3780
    name "3780"
  ]
  edge
  [
    source 524
    target 116
    measure 7
    diseases "Muscular_dystrophy"
    GOmeasure 7
    id 3781
    name "3781"
  ]
  edge
  [
    source 523
    target 116
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3782
    name "3782"
  ]
  edge
  [
    source 522
    target 116
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3783
    name "3783"
  ]
  edge
  [
    source 521
    target 116
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3784
    name "3784"
  ]
  edge
  [
    source 520
    target 116
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3785
    name "3785"
  ]
  edge
  [
    source 519
    target 116
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3786
    name "3786"
  ]
  edge
  [
    source 518
    target 116
    measure 7
    diseases "Muscular_dystrophy"
    GOmeasure 7
    id 3787
    name "3787"
  ]
  edge
  [
    source 517
    target 116
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3788
    name "3788"
  ]
  edge
  [
    source 516
    target 116
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3789
    name "3789"
  ]
  edge
  [
    source 515
    target 116
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3790
    name "3790"
  ]
  edge
  [
    source 140
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3791
    name "3791"
  ]
  edge
  [
    source 139
    target 116
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3792
    name "3792"
  ]
  edge
  [
    source 138
    target 116
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3793
    name "3793"
  ]
  edge
  [
    source 137
    target 116
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3794
    name "3794"
  ]
  edge
  [
    source 136
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3795
    name "3795"
  ]
  edge
  [
    source 135
    target 116
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3796
    name "3796"
  ]
  edge
  [
    source 134
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3797
    name "3797"
  ]
  edge
  [
    source 133
    target 116
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3798
    name "3798"
  ]
  edge
  [
    source 132
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3799
    name "3799"
  ]
  edge
  [
    source 131
    target 116
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3800
    name "3800"
  ]
  edge
  [
    source 130
    target 116
    measure 7
    diseases "Cardiomyopathy"
    GOmeasure 7
    id 3801
    name "3801"
  ]
  edge
  [
    source 129
    target 116
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3802
    name "3802"
  ]
  edge
  [
    source 128
    target 116
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3803
    name "3803"
  ]
  edge
  [
    source 127
    target 116
    measure 5
    diseases "Cardiomyopathy,Muscular_dystrophy"
    GOmeasure 5
    id 3804
    name "3804"
  ]
  edge
  [
    source 126
    target 116
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3805
    name "3805"
  ]
  edge
  [
    source 125
    target 116
    measure 6
    diseases "Cardiomyopathy,Muscular_dystrophy"
    GOmeasure 6
    id 3806
    name "3806"
  ]
  edge
  [
    source 124
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3807
    name "3807"
  ]
  edge
  [
    source 123
    target 116
    measure 6
    diseases "Cardiomyopathy,Muscular_dystrophy"
    GOmeasure 6
    id 3808
    name "3808"
  ]
  edge
  [
    source 122
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3809
    name "3809"
  ]
  edge
  [
    source 121
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3810
    name "3810"
  ]
  edge
  [
    source 120
    target 116
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3811
    name "3811"
  ]
  edge
  [
    source 119
    target 116
    measure 6
    diseases "Cardiomyopathy,Myopathy"
    GOmeasure 6
    id 3812
    name "3812"
  ]
  edge
  [
    source 118
    target 116
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3813
    name "3813"
  ]
  edge
  [
    source 117
    target 116
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3814
    name "3814"
  ]
  edge
  [
    source 524
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3815
    name "3815"
  ]
  edge
  [
    source 523
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3816
    name "3816"
  ]
  edge
  [
    source 522
    target 115
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3817
    name "3817"
  ]
  edge
  [
    source 521
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3818
    name "3818"
  ]
  edge
  [
    source 520
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3819
    name "3819"
  ]
  edge
  [
    source 519
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3820
    name "3820"
  ]
  edge
  [
    source 518
    target 115
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3821
    name "3821"
  ]
  edge
  [
    source 517
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3822
    name "3822"
  ]
  edge
  [
    source 516
    target 115
    measure 6
    diseases "Muscular_dystrophy"
    GOmeasure 6
    id 3823
    name "3823"
  ]
  edge
  [
    source 515
    target 115
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3824
    name "3824"
  ]
  edge
  [
    source 127
    target 115
    measure 4
    diseases "Muscular_dystrophy"
    GOmeasure 4
    id 3825
    name "3825"
  ]
  edge
  [
    source 125
    target 115
    measure 5
    diseases "Muscular_dystrophy"
    GOmeasure 5
    id 3826
    name "3826"
  ]
  edge
  [
    source 123
    target 115
    measure 7
    diseases "Muscular_dystrophy"
    GOmeasure 7
    id 3827
    name "3827"
  ]
  edge
  [
    source 119
    target 115
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3828
    name "3828"
  ]
  edge
  [
    source 118
    target 115
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3829
    name "3829"
  ]
  edge
  [
    source 117
    target 115
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3830
    name "3830"
  ]
  edge
  [
    source 116
    target 115
    measure 6
    diseases "Muscular_dystrophy,Myopathy"
    GOmeasure 6
    id 3831
    name "3831"
  ]
  edge
  [
    source 140
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3832
    name "3832"
  ]
  edge
  [
    source 139
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3833
    name "3833"
  ]
  edge
  [
    source 138
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3834
    name "3834"
  ]
  edge
  [
    source 137
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3835
    name "3835"
  ]
  edge
  [
    source 136
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3836
    name "3836"
  ]
  edge
  [
    source 135
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3837
    name "3837"
  ]
  edge
  [
    source 134
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3838
    name "3838"
  ]
  edge
  [
    source 133
    target 114
    measure 8
    diseases "Cardiomyopathy"
    GOmeasure 8
    id 3839
    name "3839"
  ]
  edge
  [
    source 132
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3840
    name "3840"
  ]
  edge
  [
    source 131
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3841
    name "3841"
  ]
  edge
  [
    source 130
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3842
    name "3842"
  ]
  edge
  [
    source 129
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3843
    name "3843"
  ]
  edge
  [
    source 128
    target 114
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3844
    name "3844"
  ]
  edge
  [
    source 127
    target 114
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3845
    name "3845"
  ]
  edge
  [
    source 126
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3846
    name "3846"
  ]
  edge
  [
    source 125
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3847
    name "3847"
  ]
  edge
  [
    source 124
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3848
    name "3848"
  ]
  edge
  [
    source 123
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3849
    name "3849"
  ]
  edge
  [
    source 122
    target 114
    measure 5
    diseases "Cardiomyopathy"
    GOmeasure 5
    id 3850
    name "3850"
  ]
  edge
  [
    source 121
    target 114
    measure 6
    diseases "Cardiomyopathy"
    GOmeasure 6
    id 3851
    name "3851"
  ]
  edge
  [
    source 120
    target 114
    measure 4
    diseases "Cardiomyopathy"
    GOmeasure 4
    id 3852
    name "3852"
  ]
  edge
  [
    source 119
    target 114
    measure 6
    diseases "Cardiomyopathy,Myopathy"
    GOmeasure 6
    id 3853
    name "3853"
  ]
  edge
  [
    source 118
    target 114
    measure 7
    diseases "Myopathy"
    GOmeasure 7
    id 3854
    name "3854"
  ]
  edge
  [
    source 117
    target 114
    measure 7
    diseases "Myopathy"
    GOmeasure 7
    id 3855
    name "3855"
  ]
  edge
  [
    source 116
    target 114
    measure 6
    diseases "Cardiomyopathy,Myopathy"
    GOmeasure 6
    id 3856
    name "3856"
  ]
  edge
  [
    source 115
    target 114
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3857
    name "3857"
  ]
  edge
  [
    source 119
    target 113
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3858
    name "3858"
  ]
  edge
  [
    source 118
    target 113
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3859
    name "3859"
  ]
  edge
  [
    source 117
    target 113
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3860
    name "3860"
  ]
  edge
  [
    source 116
    target 113
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3861
    name "3861"
  ]
  edge
  [
    source 115
    target 113
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3862
    name "3862"
  ]
  edge
  [
    source 114
    target 113
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3863
    name "3863"
  ]
  edge
  [
    source 129
    target 112
    measure 4
    diseases "Becker_muscular_dystrophy"
    GOmeasure 4
    id 3864
    name "3864"
  ]
  edge
  [
    source 119
    target 112
    measure 7
    diseases "Myopathy"
    GOmeasure 7
    id 3865
    name "3865"
  ]
  edge
  [
    source 118
    target 112
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3866
    name "3866"
  ]
  edge
  [
    source 117
    target 112
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3867
    name "3867"
  ]
  edge
  [
    source 116
    target 112
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3868
    name "3868"
  ]
  edge
  [
    source 115
    target 112
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3869
    name "3869"
  ]
  edge
  [
    source 114
    target 112
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3870
    name "3870"
  ]
  edge
  [
    source 113
    target 112
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3871
    name "3871"
  ]
  edge
  [
    source 613
    target 111
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 3872
    name "3872"
  ]
  edge
  [
    source 612
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3873
    name "3873"
  ]
  edge
  [
    source 611
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3874
    name "3874"
  ]
  edge
  [
    source 610
    target 111
    measure 7
    diseases "Cataract"
    GOmeasure 7
    id 3875
    name "3875"
  ]
  edge
  [
    source 609
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3876
    name "3876"
  ]
  edge
  [
    source 608
    target 111
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 3877
    name "3877"
  ]
  edge
  [
    source 607
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3878
    name "3878"
  ]
  edge
  [
    source 606
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3879
    name "3879"
  ]
  edge
  [
    source 605
    target 111
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 3880
    name "3880"
  ]
  edge
  [
    source 604
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3881
    name "3881"
  ]
  edge
  [
    source 603
    target 111
    measure 6
    diseases "Cataract"
    GOmeasure 6
    id 3882
    name "3882"
  ]
  edge
  [
    source 602
    target 111
    measure 5
    diseases "Cataract"
    GOmeasure 5
    id 3883
    name "3883"
  ]
  edge
  [
    source 601
    target 111
    measure 4
    diseases "Cataract"
    GOmeasure 4
    id 3884
    name "3884"
  ]
  edge
  [
    source 119
    target 111
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3885
    name "3885"
  ]
  edge
  [
    source 118
    target 111
    measure 7
    diseases "Myopathy"
    GOmeasure 7
    id 3886
    name "3886"
  ]
  edge
  [
    source 117
    target 111
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3887
    name "3887"
  ]
  edge
  [
    source 116
    target 111
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3888
    name "3888"
  ]
  edge
  [
    source 115
    target 111
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3889
    name "3889"
  ]
  edge
  [
    source 114
    target 111
    measure 7
    diseases "Myopathy"
    GOmeasure 7
    id 3890
    name "3890"
  ]
  edge
  [
    source 113
    target 111
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3891
    name "3891"
  ]
  edge
  [
    source 112
    target 111
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3892
    name "3892"
  ]
  edge
  [
    source 119
    target 110
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3893
    name "3893"
  ]
  edge
  [
    source 118
    target 110
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3894
    name "3894"
  ]
  edge
  [
    source 117
    target 110
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3895
    name "3895"
  ]
  edge
  [
    source 116
    target 110
    measure 7
    diseases "Myopathy"
    GOmeasure 7
    id 3896
    name "3896"
  ]
  edge
  [
    source 115
    target 110
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3897
    name "3897"
  ]
  edge
  [
    source 114
    target 110
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3898
    name "3898"
  ]
  edge
  [
    source 113
    target 110
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3899
    name "3899"
  ]
  edge
  [
    source 112
    target 110
    measure 5
    diseases "Myopathy"
    GOmeasure 5
    id 3900
    name "3900"
  ]
  edge
  [
    source 111
    target 110
    measure 6
    diseases "Myopathy"
    GOmeasure 6
    id 3901
    name "3901"
  ]
  edge
  [
    source 109
    target 108
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3902
    name "3902"
  ]
  edge
  [
    source 109
    target 107
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3903
    name "3903"
  ]
  edge
  [
    source 108
    target 107
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3904
    name "3904"
  ]
  edge
  [
    source 109
    target 106
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3905
    name "3905"
  ]
  edge
  [
    source 108
    target 106
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3906
    name "3906"
  ]
  edge
  [
    source 107
    target 106
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3907
    name "3907"
  ]
  edge
  [
    source 109
    target 105
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3908
    name "3908"
  ]
  edge
  [
    source 108
    target 105
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3909
    name "3909"
  ]
  edge
  [
    source 107
    target 105
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3910
    name "3910"
  ]
  edge
  [
    source 106
    target 105
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3911
    name "3911"
  ]
  edge
  [
    source 109
    target 104
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3912
    name "3912"
  ]
  edge
  [
    source 108
    target 104
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3913
    name "3913"
  ]
  edge
  [
    source 107
    target 104
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3914
    name "3914"
  ]
  edge
  [
    source 106
    target 104
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3915
    name "3915"
  ]
  edge
  [
    source 105
    target 104
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3916
    name "3916"
  ]
  edge
  [
    source 109
    target 103
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3917
    name "3917"
  ]
  edge
  [
    source 108
    target 103
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3918
    name "3918"
  ]
  edge
  [
    source 107
    target 103
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3919
    name "3919"
  ]
  edge
  [
    source 106
    target 103
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3920
    name "3920"
  ]
  edge
  [
    source 105
    target 103
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3921
    name "3921"
  ]
  edge
  [
    source 104
    target 103
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3922
    name "3922"
  ]
  edge
  [
    source 109
    target 102
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3923
    name "3923"
  ]
  edge
  [
    source 108
    target 102
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3924
    name "3924"
  ]
  edge
  [
    source 107
    target 102
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3925
    name "3925"
  ]
  edge
  [
    source 106
    target 102
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3926
    name "3926"
  ]
  edge
  [
    source 105
    target 102
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3927
    name "3927"
  ]
  edge
  [
    source 104
    target 102
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3928
    name "3928"
  ]
  edge
  [
    source 103
    target 102
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3929
    name "3929"
  ]
  edge
  [
    source 109
    target 101
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3930
    name "3930"
  ]
  edge
  [
    source 108
    target 101
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3931
    name "3931"
  ]
  edge
  [
    source 107
    target 101
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3932
    name "3932"
  ]
  edge
  [
    source 106
    target 101
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3933
    name "3933"
  ]
  edge
  [
    source 105
    target 101
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3934
    name "3934"
  ]
  edge
  [
    source 104
    target 101
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3935
    name "3935"
  ]
  edge
  [
    source 103
    target 101
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3936
    name "3936"
  ]
  edge
  [
    source 102
    target 101
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3937
    name "3937"
  ]
  edge
  [
    source 109
    target 100
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3938
    name "3938"
  ]
  edge
  [
    source 108
    target 100
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3939
    name "3939"
  ]
  edge
  [
    source 107
    target 100
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3940
    name "3940"
  ]
  edge
  [
    source 106
    target 100
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3941
    name "3941"
  ]
  edge
  [
    source 105
    target 100
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3942
    name "3942"
  ]
  edge
  [
    source 104
    target 100
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3943
    name "3943"
  ]
  edge
  [
    source 103
    target 100
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3944
    name "3944"
  ]
  edge
  [
    source 102
    target 100
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3945
    name "3945"
  ]
  edge
  [
    source 101
    target 100
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3946
    name "3946"
  ]
  edge
  [
    source 109
    target 99
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3947
    name "3947"
  ]
  edge
  [
    source 108
    target 99
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3948
    name "3948"
  ]
  edge
  [
    source 107
    target 99
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3949
    name "3949"
  ]
  edge
  [
    source 106
    target 99
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3950
    name "3950"
  ]
  edge
  [
    source 105
    target 99
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3951
    name "3951"
  ]
  edge
  [
    source 104
    target 99
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3952
    name "3952"
  ]
  edge
  [
    source 103
    target 99
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3953
    name "3953"
  ]
  edge
  [
    source 102
    target 99
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3954
    name "3954"
  ]
  edge
  [
    source 101
    target 99
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3955
    name "3955"
  ]
  edge
  [
    source 100
    target 99
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3956
    name "3956"
  ]
  edge
  [
    source 553
    target 98
    measure 5
    diseases "Malaria"
    GOmeasure 5
    id 3957
    name "3957"
  ]
  edge
  [
    source 535
    target 98
    measure 6
    diseases "Malaria"
    GOmeasure 6
    id 3958
    name "3958"
  ]
  edge
  [
    source 203
    target 98
    measure 6
    diseases "Malaria"
    GOmeasure 6
    id 3959
    name "3959"
  ]
  edge
  [
    source 109
    target 98
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3960
    name "3960"
  ]
  edge
  [
    source 108
    target 98
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3961
    name "3961"
  ]
  edge
  [
    source 107
    target 98
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3962
    name "3962"
  ]
  edge
  [
    source 106
    target 98
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3963
    name "3963"
  ]
  edge
  [
    source 105
    target 98
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3964
    name "3964"
  ]
  edge
  [
    source 104
    target 98
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3965
    name "3965"
  ]
  edge
  [
    source 103
    target 98
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3966
    name "3966"
  ]
  edge
  [
    source 102
    target 98
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3967
    name "3967"
  ]
  edge
  [
    source 101
    target 98
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3968
    name "3968"
  ]
  edge
  [
    source 100
    target 98
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3969
    name "3969"
  ]
  edge
  [
    source 99
    target 98
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3970
    name "3970"
  ]
  edge
  [
    source 679
    target 97
    measure 7
    diseases "Elliptocytosis"
    GOmeasure 7
    id 3971
    name "3971"
  ]
  edge
  [
    source 416
    target 97
    measure 5
    diseases "Renal_tubular_acidosis"
    GOmeasure 5
    id 3972
    name "3972"
  ]
  edge
  [
    source 415
    target 97
    measure 4
    diseases "Renal_tubular_acidosis"
    GOmeasure 4
    id 3973
    name "3973"
  ]
  edge
  [
    source 414
    target 97
    measure 4
    diseases "Renal_tubular_acidosis"
    GOmeasure 4
    id 3974
    name "3974"
  ]
  edge
  [
    source 413
    target 97
    measure 5
    diseases "Renal_tubular_acidosis"
    GOmeasure 5
    id 3975
    name "3975"
  ]
  edge
  [
    source 294
    target 97
    measure 5
    diseases "Elliptocytosis,Spherocytosis"
    GOmeasure 5
    id 3976
    name "3976"
  ]
  edge
  [
    source 293
    target 97
    measure 5
    diseases "Spherocytosis"
    GOmeasure 5
    id 3977
    name "3977"
  ]
  edge
  [
    source 292
    target 97
    measure 5
    diseases "Spherocytosis"
    GOmeasure 5
    id 3978
    name "3978"
  ]
  edge
  [
    source 261
    target 97
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 3979
    name "3979"
  ]
  edge
  [
    source 260
    target 97
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 3980
    name "3980"
  ]
  edge
  [
    source 259
    target 97
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 3981
    name "3981"
  ]
  edge
  [
    source 258
    target 97
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 3982
    name "3982"
  ]
  edge
  [
    source 257
    target 97
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 3983
    name "3983"
  ]
  edge
  [
    source 256
    target 97
    measure 4
    diseases "Hemolytic_anemia"
    GOmeasure 4
    id 3984
    name "3984"
  ]
  edge
  [
    source 255
    target 97
    measure 5
    diseases "Hemolytic_anemia"
    GOmeasure 5
    id 3985
    name "3985"
  ]
  edge
  [
    source 254
    target 97
    measure 7
    diseases "Hemolytic_anemia"
    GOmeasure 7
    id 3986
    name "3986"
  ]
  edge
  [
    source 253
    target 97
    measure 6
    diseases "Hemolytic_anemia"
    GOmeasure 6
    id 3987
    name "3987"
  ]
  edge
  [
    source 109
    target 97
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3988
    name "3988"
  ]
  edge
  [
    source 108
    target 97
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3989
    name "3989"
  ]
  edge
  [
    source 107
    target 97
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3990
    name "3990"
  ]
  edge
  [
    source 106
    target 97
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3991
    name "3991"
  ]
  edge
  [
    source 105
    target 97
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3992
    name "3992"
  ]
  edge
  [
    source 104
    target 97
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3993
    name "3993"
  ]
  edge
  [
    source 103
    target 97
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3994
    name "3994"
  ]
  edge
  [
    source 102
    target 97
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3995
    name "3995"
  ]
  edge
  [
    source 101
    target 97
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 3996
    name "3996"
  ]
  edge
  [
    source 100
    target 97
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 3997
    name "3997"
  ]
  edge
  [
    source 99
    target 97
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 3998
    name "3998"
  ]
  edge
  [
    source 98
    target 97
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 3999
    name "3999"
  ]
  edge
  [
    source 109
    target 96
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4000
    name "4000"
  ]
  edge
  [
    source 108
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4001
    name "4001"
  ]
  edge
  [
    source 107
    target 96
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 4002
    name "4002"
  ]
  edge
  [
    source 106
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4003
    name "4003"
  ]
  edge
  [
    source 105
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4004
    name "4004"
  ]
  edge
  [
    source 104
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4005
    name "4005"
  ]
  edge
  [
    source 103
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4006
    name "4006"
  ]
  edge
  [
    source 102
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4007
    name "4007"
  ]
  edge
  [
    source 101
    target 96
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 4008
    name "4008"
  ]
  edge
  [
    source 100
    target 96
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4009
    name "4009"
  ]
  edge
  [
    source 99
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4010
    name "4010"
  ]
  edge
  [
    source 98
    target 96
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4011
    name "4011"
  ]
  edge
  [
    source 97
    target 96
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4012
    name "4012"
  ]
  edge
  [
    source 109
    target 95
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 4013
    name "4013"
  ]
  edge
  [
    source 108
    target 95
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4014
    name "4014"
  ]
  edge
  [
    source 107
    target 95
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4015
    name "4015"
  ]
  edge
  [
    source 106
    target 95
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4016
    name "4016"
  ]
  edge
  [
    source 105
    target 95
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4017
    name "4017"
  ]
  edge
  [
    source 104
    target 95
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4018
    name "4018"
  ]
  edge
  [
    source 103
    target 95
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4019
    name "4019"
  ]
  edge
  [
    source 102
    target 95
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 4020
    name "4020"
  ]
  edge
  [
    source 101
    target 95
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4021
    name "4021"
  ]
  edge
  [
    source 100
    target 95
    measure 7
    diseases "Blood_group"
    GOmeasure 7
    id 4022
    name "4022"
  ]
  edge
  [
    source 99
    target 95
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 4023
    name "4023"
  ]
  edge
  [
    source 98
    target 95
    measure 5
    diseases "Blood_group"
    GOmeasure 5
    id 4024
    name "4024"
  ]
  edge
  [
    source 97
    target 95
    measure 6
    diseases "Blood_group"
    GOmeasure 6
    id 4025
    name "4025"
  ]
  edge
  [
    source 96
    target 95
    measure 4
    diseases "Blood_group"
    GOmeasure 4
    id 4026
    name "4026"
  ]
  edge
  [
    source 94
    target 93
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4027
    name "4027"
  ]
  edge
  [
    source 425
    target 92
    measure 7
    diseases "von_Hippel-Lindau_syndrome"
    GOmeasure 7
    id 4028
    name "4028"
  ]
  edge
  [
    source 333
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4029
    name "4029"
  ]
  edge
  [
    source 332
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4030
    name "4030"
  ]
  edge
  [
    source 331
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4031
    name "4031"
  ]
  edge
  [
    source 330
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4032
    name "4032"
  ]
  edge
  [
    source 329
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4033
    name "4033"
  ]
  edge
  [
    source 328
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4034
    name "4034"
  ]
  edge
  [
    source 327
    target 92
    measure 8
    diseases "Colon_cancer"
    GOmeasure 8
    id 4035
    name "4035"
  ]
  edge
  [
    source 326
    target 92
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 4036
    name "4036"
  ]
  edge
  [
    source 325
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4037
    name "4037"
  ]
  edge
  [
    source 324
    target 92
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 4038
    name "4038"
  ]
  edge
  [
    source 323
    target 92
    measure 8
    diseases "Colon_cancer"
    GOmeasure 8
    id 4039
    name "4039"
  ]
  edge
  [
    source 322
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4040
    name "4040"
  ]
  edge
  [
    source 321
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4041
    name "4041"
  ]
  edge
  [
    source 320
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4042
    name "4042"
  ]
  edge
  [
    source 319
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4043
    name "4043"
  ]
  edge
  [
    source 318
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4044
    name "4044"
  ]
  edge
  [
    source 317
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4045
    name "4045"
  ]
  edge
  [
    source 316
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4046
    name "4046"
  ]
  edge
  [
    source 315
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4047
    name "4047"
  ]
  edge
  [
    source 314
    target 92
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 4048
    name "4048"
  ]
  edge
  [
    source 313
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4049
    name "4049"
  ]
  edge
  [
    source 312
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4050
    name "4050"
  ]
  edge
  [
    source 311
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4051
    name "4051"
  ]
  edge
  [
    source 310
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4052
    name "4052"
  ]
  edge
  [
    source 309
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4053
    name "4053"
  ]
  edge
  [
    source 308
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4054
    name "4054"
  ]
  edge
  [
    source 307
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4055
    name "4055"
  ]
  edge
  [
    source 306
    target 92
    measure 6
    diseases "Colon_cancer"
    GOmeasure 6
    id 4056
    name "4056"
  ]
  edge
  [
    source 305
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4057
    name "4057"
  ]
  edge
  [
    source 303
    target 92
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 4058
    name "4058"
  ]
  edge
  [
    source 302
    target 92
    measure 5
    diseases "Colon_cancer"
    GOmeasure 5
    id 4059
    name "4059"
  ]
  edge
  [
    source 170
    target 92
    measure 7
    diseases "Colon_cancer"
    GOmeasure 7
    id 4060
    name "4060"
  ]
  edge
  [
    source 94
    target 92
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4061
    name "4061"
  ]
  edge
  [
    source 93
    target 92
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4062
    name "4062"
  ]
  edge
  [
    source 94
    target 91
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4063
    name "4063"
  ]
  edge
  [
    source 93
    target 91
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4064
    name "4064"
  ]
  edge
  [
    source 92
    target 91
    measure 8
    diseases "Leukemia"
    GOmeasure 8
    id 4065
    name "4065"
  ]
  edge
  [
    source 94
    target 90
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4066
    name "4066"
  ]
  edge
  [
    source 93
    target 90
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4067
    name "4067"
  ]
  edge
  [
    source 92
    target 90
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4068
    name "4068"
  ]
  edge
  [
    source 91
    target 90
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4069
    name "4069"
  ]
  edge
  [
    source 766
    target 89
    measure 5
    diseases "Neurofibromatosis"
    GOmeasure 5
    id 4070
    name "4070"
  ]
  edge
  [
    source 323
    target 89
    measure 6
    diseases "Neurofibromatosis"
    GOmeasure 6
    id 4071
    name "4071"
  ]
  edge
  [
    source 94
    target 89
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4072
    name "4072"
  ]
  edge
  [
    source 93
    target 89
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4073
    name "4073"
  ]
  edge
  [
    source 92
    target 89
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4074
    name "4074"
  ]
  edge
  [
    source 91
    target 89
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4075
    name "4075"
  ]
  edge
  [
    source 90
    target 89
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4076
    name "4076"
  ]
  edge
  [
    source 94
    target 88
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4077
    name "4077"
  ]
  edge
  [
    source 93
    target 88
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4078
    name "4078"
  ]
  edge
  [
    source 92
    target 88
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4079
    name "4079"
  ]
  edge
  [
    source 91
    target 88
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4080
    name "4080"
  ]
  edge
  [
    source 90
    target 88
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4081
    name "4081"
  ]
  edge
  [
    source 89
    target 88
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4082
    name "4082"
  ]
  edge
  [
    source 94
    target 87
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4083
    name "4083"
  ]
  edge
  [
    source 93
    target 87
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4084
    name "4084"
  ]
  edge
  [
    source 92
    target 87
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4085
    name "4085"
  ]
  edge
  [
    source 91
    target 87
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4086
    name "4086"
  ]
  edge
  [
    source 90
    target 87
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4087
    name "4087"
  ]
  edge
  [
    source 89
    target 87
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4088
    name "4088"
  ]
  edge
  [
    source 88
    target 87
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4089
    name "4089"
  ]
  edge
  [
    source 94
    target 86
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4090
    name "4090"
  ]
  edge
  [
    source 93
    target 86
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4091
    name "4091"
  ]
  edge
  [
    source 92
    target 86
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4092
    name "4092"
  ]
  edge
  [
    source 91
    target 86
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4093
    name "4093"
  ]
  edge
  [
    source 90
    target 86
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4094
    name "4094"
  ]
  edge
  [
    source 89
    target 86
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4095
    name "4095"
  ]
  edge
  [
    source 88
    target 86
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4096
    name "4096"
  ]
  edge
  [
    source 87
    target 86
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4097
    name "4097"
  ]
  edge
  [
    source 709
    target 85
    measure 5
    diseases "Growth_hormone"
    GOmeasure 5
    id 4098
    name "4098"
  ]
  edge
  [
    source 94
    target 85
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4099
    name "4099"
  ]
  edge
  [
    source 93
    target 85
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4100
    name "4100"
  ]
  edge
  [
    source 92
    target 85
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4101
    name "4101"
  ]
  edge
  [
    source 91
    target 85
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4102
    name "4102"
  ]
  edge
  [
    source 90
    target 85
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4103
    name "4103"
  ]
  edge
  [
    source 89
    target 85
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4104
    name "4104"
  ]
  edge
  [
    source 88
    target 85
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4105
    name "4105"
  ]
  edge
  [
    source 87
    target 85
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4106
    name "4106"
  ]
  edge
  [
    source 86
    target 85
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4107
    name "4107"
  ]
  edge
  [
    source 94
    target 84
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4108
    name "4108"
  ]
  edge
  [
    source 93
    target 84
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4109
    name "4109"
  ]
  edge
  [
    source 92
    target 84
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4110
    name "4110"
  ]
  edge
  [
    source 91
    target 84
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4111
    name "4111"
  ]
  edge
  [
    source 90
    target 84
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4112
    name "4112"
  ]
  edge
  [
    source 89
    target 84
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4113
    name "4113"
  ]
  edge
  [
    source 88
    target 84
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4114
    name "4114"
  ]
  edge
  [
    source 87
    target 84
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4115
    name "4115"
  ]
  edge
  [
    source 86
    target 84
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4116
    name "4116"
  ]
  edge
  [
    source 85
    target 84
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4117
    name "4117"
  ]
  edge
  [
    source 94
    target 83
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4118
    name "4118"
  ]
  edge
  [
    source 93
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4119
    name "4119"
  ]
  edge
  [
    source 92
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4120
    name "4120"
  ]
  edge
  [
    source 91
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4121
    name "4121"
  ]
  edge
  [
    source 90
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4122
    name "4122"
  ]
  edge
  [
    source 89
    target 83
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4123
    name "4123"
  ]
  edge
  [
    source 88
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4124
    name "4124"
  ]
  edge
  [
    source 87
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4125
    name "4125"
  ]
  edge
  [
    source 86
    target 83
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4126
    name "4126"
  ]
  edge
  [
    source 85
    target 83
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4127
    name "4127"
  ]
  edge
  [
    source 84
    target 83
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4128
    name "4128"
  ]
  edge
  [
    source 94
    target 82
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4129
    name "4129"
  ]
  edge
  [
    source 93
    target 82
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4130
    name "4130"
  ]
  edge
  [
    source 92
    target 82
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4131
    name "4131"
  ]
  edge
  [
    source 91
    target 82
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4132
    name "4132"
  ]
  edge
  [
    source 90
    target 82
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4133
    name "4133"
  ]
  edge
  [
    source 89
    target 82
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4134
    name "4134"
  ]
  edge
  [
    source 88
    target 82
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4135
    name "4135"
  ]
  edge
  [
    source 87
    target 82
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4136
    name "4136"
  ]
  edge
  [
    source 86
    target 82
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4137
    name "4137"
  ]
  edge
  [
    source 85
    target 82
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4138
    name "4138"
  ]
  edge
  [
    source 84
    target 82
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4139
    name "4139"
  ]
  edge
  [
    source 83
    target 82
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4140
    name "4140"
  ]
  edge
  [
    source 94
    target 81
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4141
    name "4141"
  ]
  edge
  [
    source 93
    target 81
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4142
    name "4142"
  ]
  edge
  [
    source 92
    target 81
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4143
    name "4143"
  ]
  edge
  [
    source 91
    target 81
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4144
    name "4144"
  ]
  edge
  [
    source 90
    target 81
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4145
    name "4145"
  ]
  edge
  [
    source 89
    target 81
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4146
    name "4146"
  ]
  edge
  [
    source 88
    target 81
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4147
    name "4147"
  ]
  edge
  [
    source 87
    target 81
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4148
    name "4148"
  ]
  edge
  [
    source 86
    target 81
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4149
    name "4149"
  ]
  edge
  [
    source 85
    target 81
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4150
    name "4150"
  ]
  edge
  [
    source 84
    target 81
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4151
    name "4151"
  ]
  edge
  [
    source 83
    target 81
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4152
    name "4152"
  ]
  edge
  [
    source 82
    target 81
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4153
    name "4153"
  ]
  edge
  [
    source 94
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4154
    name "4154"
  ]
  edge
  [
    source 93
    target 80
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4155
    name "4155"
  ]
  edge
  [
    source 92
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4156
    name "4156"
  ]
  edge
  [
    source 91
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4157
    name "4157"
  ]
  edge
  [
    source 90
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4158
    name "4158"
  ]
  edge
  [
    source 89
    target 80
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4159
    name "4159"
  ]
  edge
  [
    source 88
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4160
    name "4160"
  ]
  edge
  [
    source 87
    target 80
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4161
    name "4161"
  ]
  edge
  [
    source 86
    target 80
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4162
    name "4162"
  ]
  edge
  [
    source 85
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4163
    name "4163"
  ]
  edge
  [
    source 84
    target 80
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4164
    name "4164"
  ]
  edge
  [
    source 83
    target 80
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4165
    name "4165"
  ]
  edge
  [
    source 82
    target 80
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4166
    name "4166"
  ]
  edge
  [
    source 81
    target 80
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4167
    name "4167"
  ]
  edge
  [
    source 541
    target 79
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 4168
    name "4168"
  ]
  edge
  [
    source 540
    target 79
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 4169
    name "4169"
  ]
  edge
  [
    source 539
    target 79
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 4170
    name "4170"
  ]
  edge
  [
    source 538
    target 79
    measure 6
    diseases "Rheumatoid_arthritis"
    GOmeasure 6
    id 4171
    name "4171"
  ]
  edge
  [
    source 537
    target 79
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 4172
    name "4172"
  ]
  edge
  [
    source 536
    target 79
    measure 5
    diseases "Rheumatoid_arthritis"
    GOmeasure 5
    id 4173
    name "4173"
  ]
  edge
  [
    source 535
    target 79
    measure 7
    diseases "Platelet_defect/deficiency"
    GOmeasure 7
    id 4174
    name "4174"
  ]
  edge
  [
    source 534
    target 79
    measure 7
    diseases "Platelet_defect/deficiency"
    GOmeasure 7
    id 4175
    name "4175"
  ]
  edge
  [
    source 265
    target 79
    measure 7
    diseases "Rheumatoid_arthritis"
    GOmeasure 7
    id 4176
    name "4176"
  ]
  edge
  [
    source 201
    target 79
    measure 6
    diseases "Platelet_defect/deficiency"
    GOmeasure 6
    id 4177
    name "4177"
  ]
  edge
  [
    source 94
    target 79
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4178
    name "4178"
  ]
  edge
  [
    source 93
    target 79
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4179
    name "4179"
  ]
  edge
  [
    source 92
    target 79
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4180
    name "4180"
  ]
  edge
  [
    source 91
    target 79
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4181
    name "4181"
  ]
  edge
  [
    source 90
    target 79
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4182
    name "4182"
  ]
  edge
  [
    source 89
    target 79
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4183
    name "4183"
  ]
  edge
  [
    source 88
    target 79
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4184
    name "4184"
  ]
  edge
  [
    source 87
    target 79
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4185
    name "4185"
  ]
  edge
  [
    source 86
    target 79
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4186
    name "4186"
  ]
  edge
  [
    source 85
    target 79
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4187
    name "4187"
  ]
  edge
  [
    source 84
    target 79
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4188
    name "4188"
  ]
  edge
  [
    source 83
    target 79
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4189
    name "4189"
  ]
  edge
  [
    source 82
    target 79
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4190
    name "4190"
  ]
  edge
  [
    source 81
    target 79
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4191
    name "4191"
  ]
  edge
  [
    source 80
    target 79
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4192
    name "4192"
  ]
  edge
  [
    source 94
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4193
    name "4193"
  ]
  edge
  [
    source 93
    target 78
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4194
    name "4194"
  ]
  edge
  [
    source 92
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4195
    name "4195"
  ]
  edge
  [
    source 91
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4196
    name "4196"
  ]
  edge
  [
    source 90
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4197
    name "4197"
  ]
  edge
  [
    source 89
    target 78
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4198
    name "4198"
  ]
  edge
  [
    source 88
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4199
    name "4199"
  ]
  edge
  [
    source 87
    target 78
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4200
    name "4200"
  ]
  edge
  [
    source 86
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4201
    name "4201"
  ]
  edge
  [
    source 85
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4202
    name "4202"
  ]
  edge
  [
    source 84
    target 78
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4203
    name "4203"
  ]
  edge
  [
    source 83
    target 78
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4204
    name "4204"
  ]
  edge
  [
    source 82
    target 78
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4205
    name "4205"
  ]
  edge
  [
    source 81
    target 78
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4206
    name "4206"
  ]
  edge
  [
    source 80
    target 78
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4207
    name "4207"
  ]
  edge
  [
    source 79
    target 78
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4208
    name "4208"
  ]
  edge
  [
    source 94
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4209
    name "4209"
  ]
  edge
  [
    source 93
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4210
    name "4210"
  ]
  edge
  [
    source 92
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4211
    name "4211"
  ]
  edge
  [
    source 91
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4212
    name "4212"
  ]
  edge
  [
    source 90
    target 77
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4213
    name "4213"
  ]
  edge
  [
    source 89
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4214
    name "4214"
  ]
  edge
  [
    source 88
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4215
    name "4215"
  ]
  edge
  [
    source 87
    target 77
    measure 8
    diseases "Leukemia"
    GOmeasure 8
    id 4216
    name "4216"
  ]
  edge
  [
    source 86
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4217
    name "4217"
  ]
  edge
  [
    source 85
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4218
    name "4218"
  ]
  edge
  [
    source 84
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4219
    name "4219"
  ]
  edge
  [
    source 83
    target 77
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4220
    name "4220"
  ]
  edge
  [
    source 82
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4221
    name "4221"
  ]
  edge
  [
    source 81
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4222
    name "4222"
  ]
  edge
  [
    source 80
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4223
    name "4223"
  ]
  edge
  [
    source 79
    target 77
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4224
    name "4224"
  ]
  edge
  [
    source 78
    target 77
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4225
    name "4225"
  ]
  edge
  [
    source 741
    target 76
    measure 5
    diseases "Lipoma"
    GOmeasure 5
    id 4226
    name "4226"
  ]
  edge
  [
    source 532
    target 76
    measure 5
    diseases "Lipoma"
    GOmeasure 5
    id 4227
    name "4227"
  ]
  edge
  [
    source 94
    target 76
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4228
    name "4228"
  ]
  edge
  [
    source 93
    target 76
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4229
    name "4229"
  ]
  edge
  [
    source 92
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4230
    name "4230"
  ]
  edge
  [
    source 91
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4231
    name "4231"
  ]
  edge
  [
    source 90
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4232
    name "4232"
  ]
  edge
  [
    source 89
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4233
    name "4233"
  ]
  edge
  [
    source 88
    target 76
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4234
    name "4234"
  ]
  edge
  [
    source 87
    target 76
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4235
    name "4235"
  ]
  edge
  [
    source 86
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4236
    name "4236"
  ]
  edge
  [
    source 85
    target 76
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4237
    name "4237"
  ]
  edge
  [
    source 84
    target 76
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4238
    name "4238"
  ]
  edge
  [
    source 83
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4239
    name "4239"
  ]
  edge
  [
    source 82
    target 76
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4240
    name "4240"
  ]
  edge
  [
    source 81
    target 76
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4241
    name "4241"
  ]
  edge
  [
    source 80
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4242
    name "4242"
  ]
  edge
  [
    source 79
    target 76
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4243
    name "4243"
  ]
  edge
  [
    source 78
    target 76
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4244
    name "4244"
  ]
  edge
  [
    source 77
    target 76
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4245
    name "4245"
  ]
  edge
  [
    source 737
    target 75
    measure 5
    diseases "Gastrointestinal_stromal_tumor"
    GOmeasure 5
    id 4246
    name "4246"
  ]
  edge
  [
    source 307
    target 75
    measure 6
    diseases "Germ_cell_tumor"
    GOmeasure 6
    id 4247
    name "4247"
  ]
  edge
  [
    source 94
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4248
    name "4248"
  ]
  edge
  [
    source 93
    target 75
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4249
    name "4249"
  ]
  edge
  [
    source 92
    target 75
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4250
    name "4250"
  ]
  edge
  [
    source 91
    target 75
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4251
    name "4251"
  ]
  edge
  [
    source 90
    target 75
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4252
    name "4252"
  ]
  edge
  [
    source 89
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4253
    name "4253"
  ]
  edge
  [
    source 88
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4254
    name "4254"
  ]
  edge
  [
    source 87
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4255
    name "4255"
  ]
  edge
  [
    source 86
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4256
    name "4256"
  ]
  edge
  [
    source 85
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4257
    name "4257"
  ]
  edge
  [
    source 84
    target 75
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4258
    name "4258"
  ]
  edge
  [
    source 83
    target 75
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4259
    name "4259"
  ]
  edge
  [
    source 82
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4260
    name "4260"
  ]
  edge
  [
    source 81
    target 75
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4261
    name "4261"
  ]
  edge
  [
    source 80
    target 75
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4262
    name "4262"
  ]
  edge
  [
    source 79
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4263
    name "4263"
  ]
  edge
  [
    source 78
    target 75
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4264
    name "4264"
  ]
  edge
  [
    source 77
    target 75
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4265
    name "4265"
  ]
  edge
  [
    source 76
    target 75
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4266
    name "4266"
  ]
  edge
  [
    source 94
    target 74
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4267
    name "4267"
  ]
  edge
  [
    source 93
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4268
    name "4268"
  ]
  edge
  [
    source 92
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4269
    name "4269"
  ]
  edge
  [
    source 91
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4270
    name "4270"
  ]
  edge
  [
    source 90
    target 74
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4271
    name "4271"
  ]
  edge
  [
    source 89
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4272
    name "4272"
  ]
  edge
  [
    source 88
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4273
    name "4273"
  ]
  edge
  [
    source 87
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4274
    name "4274"
  ]
  edge
  [
    source 86
    target 74
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4275
    name "4275"
  ]
  edge
  [
    source 85
    target 74
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4276
    name "4276"
  ]
  edge
  [
    source 84
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4277
    name "4277"
  ]
  edge
  [
    source 83
    target 74
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4278
    name "4278"
  ]
  edge
  [
    source 82
    target 74
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4279
    name "4279"
  ]
  edge
  [
    source 81
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4280
    name "4280"
  ]
  edge
  [
    source 80
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4281
    name "4281"
  ]
  edge
  [
    source 79
    target 74
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4282
    name "4282"
  ]
  edge
  [
    source 78
    target 74
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4283
    name "4283"
  ]
  edge
  [
    source 77
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4284
    name "4284"
  ]
  edge
  [
    source 76
    target 74
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4285
    name "4285"
  ]
  edge
  [
    source 75
    target 74
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4286
    name "4286"
  ]
  edge
  [
    source 94
    target 73
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4287
    name "4287"
  ]
  edge
  [
    source 93
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4288
    name "4288"
  ]
  edge
  [
    source 92
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4289
    name "4289"
  ]
  edge
  [
    source 91
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4290
    name "4290"
  ]
  edge
  [
    source 90
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4291
    name "4291"
  ]
  edge
  [
    source 89
    target 73
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4292
    name "4292"
  ]
  edge
  [
    source 88
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4293
    name "4293"
  ]
  edge
  [
    source 87
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4294
    name "4294"
  ]
  edge
  [
    source 86
    target 73
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4295
    name "4295"
  ]
  edge
  [
    source 85
    target 73
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4296
    name "4296"
  ]
  edge
  [
    source 84
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4297
    name "4297"
  ]
  edge
  [
    source 83
    target 73
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4298
    name "4298"
  ]
  edge
  [
    source 82
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4299
    name "4299"
  ]
  edge
  [
    source 81
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4300
    name "4300"
  ]
  edge
  [
    source 80
    target 73
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4301
    name "4301"
  ]
  edge
  [
    source 79
    target 73
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4302
    name "4302"
  ]
  edge
  [
    source 78
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4303
    name "4303"
  ]
  edge
  [
    source 77
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4304
    name "4304"
  ]
  edge
  [
    source 76
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4305
    name "4305"
  ]
  edge
  [
    source 75
    target 73
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4306
    name "4306"
  ]
  edge
  [
    source 74
    target 73
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4307
    name "4307"
  ]
  edge
  [
    source 94
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4308
    name "4308"
  ]
  edge
  [
    source 93
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4309
    name "4309"
  ]
  edge
  [
    source 92
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4310
    name "4310"
  ]
  edge
  [
    source 91
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4311
    name "4311"
  ]
  edge
  [
    source 90
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4312
    name "4312"
  ]
  edge
  [
    source 89
    target 72
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4313
    name "4313"
  ]
  edge
  [
    source 88
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4314
    name "4314"
  ]
  edge
  [
    source 87
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4315
    name "4315"
  ]
  edge
  [
    source 86
    target 72
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4316
    name "4316"
  ]
  edge
  [
    source 85
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4317
    name "4317"
  ]
  edge
  [
    source 84
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4318
    name "4318"
  ]
  edge
  [
    source 83
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4319
    name "4319"
  ]
  edge
  [
    source 82
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4320
    name "4320"
  ]
  edge
  [
    source 81
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4321
    name "4321"
  ]
  edge
  [
    source 80
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4322
    name "4322"
  ]
  edge
  [
    source 79
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4323
    name "4323"
  ]
  edge
  [
    source 78
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4324
    name "4324"
  ]
  edge
  [
    source 77
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4325
    name "4325"
  ]
  edge
  [
    source 76
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4326
    name "4326"
  ]
  edge
  [
    source 75
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4327
    name "4327"
  ]
  edge
  [
    source 74
    target 72
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4328
    name "4328"
  ]
  edge
  [
    source 73
    target 72
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4329
    name "4329"
  ]
  edge
  [
    source 94
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4330
    name "4330"
  ]
  edge
  [
    source 93
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4331
    name "4331"
  ]
  edge
  [
    source 92
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4332
    name "4332"
  ]
  edge
  [
    source 91
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4333
    name "4333"
  ]
  edge
  [
    source 90
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4334
    name "4334"
  ]
  edge
  [
    source 89
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4335
    name "4335"
  ]
  edge
  [
    source 88
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4336
    name "4336"
  ]
  edge
  [
    source 87
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4337
    name "4337"
  ]
  edge
  [
    source 86
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4338
    name "4338"
  ]
  edge
  [
    source 85
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4339
    name "4339"
  ]
  edge
  [
    source 84
    target 71
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4340
    name "4340"
  ]
  edge
  [
    source 83
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4341
    name "4341"
  ]
  edge
  [
    source 82
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4342
    name "4342"
  ]
  edge
  [
    source 81
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4343
    name "4343"
  ]
  edge
  [
    source 80
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4344
    name "4344"
  ]
  edge
  [
    source 79
    target 71
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4345
    name "4345"
  ]
  edge
  [
    source 78
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4346
    name "4346"
  ]
  edge
  [
    source 77
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4347
    name "4347"
  ]
  edge
  [
    source 76
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4348
    name "4348"
  ]
  edge
  [
    source 75
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4349
    name "4349"
  ]
  edge
  [
    source 74
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4350
    name "4350"
  ]
  edge
  [
    source 73
    target 71
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4351
    name "4351"
  ]
  edge
  [
    source 72
    target 71
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4352
    name "4352"
  ]
  edge
  [
    source 94
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4353
    name "4353"
  ]
  edge
  [
    source 93
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4354
    name "4354"
  ]
  edge
  [
    source 92
    target 70
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4355
    name "4355"
  ]
  edge
  [
    source 91
    target 70
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4356
    name "4356"
  ]
  edge
  [
    source 90
    target 70
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4357
    name "4357"
  ]
  edge
  [
    source 89
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4358
    name "4358"
  ]
  edge
  [
    source 88
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4359
    name "4359"
  ]
  edge
  [
    source 87
    target 70
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4360
    name "4360"
  ]
  edge
  [
    source 86
    target 70
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4361
    name "4361"
  ]
  edge
  [
    source 85
    target 70
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4362
    name "4362"
  ]
  edge
  [
    source 84
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4363
    name "4363"
  ]
  edge
  [
    source 83
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4364
    name "4364"
  ]
  edge
  [
    source 82
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4365
    name "4365"
  ]
  edge
  [
    source 81
    target 70
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4366
    name "4366"
  ]
  edge
  [
    source 80
    target 70
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4367
    name "4367"
  ]
  edge
  [
    source 79
    target 70
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4368
    name "4368"
  ]
  edge
  [
    source 78
    target 70
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4369
    name "4369"
  ]
  edge
  [
    source 77
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4370
    name "4370"
  ]
  edge
  [
    source 76
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4371
    name "4371"
  ]
  edge
  [
    source 75
    target 70
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4372
    name "4372"
  ]
  edge
  [
    source 74
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4373
    name "4373"
  ]
  edge
  [
    source 73
    target 70
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4374
    name "4374"
  ]
  edge
  [
    source 72
    target 70
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4375
    name "4375"
  ]
  edge
  [
    source 71
    target 70
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4376
    name "4376"
  ]
  edge
  [
    source 94
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4377
    name "4377"
  ]
  edge
  [
    source 93
    target 69
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4378
    name "4378"
  ]
  edge
  [
    source 92
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4379
    name "4379"
  ]
  edge
  [
    source 91
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4380
    name "4380"
  ]
  edge
  [
    source 90
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4381
    name "4381"
  ]
  edge
  [
    source 89
    target 69
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4382
    name "4382"
  ]
  edge
  [
    source 88
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4383
    name "4383"
  ]
  edge
  [
    source 87
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4384
    name "4384"
  ]
  edge
  [
    source 86
    target 69
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4385
    name "4385"
  ]
  edge
  [
    source 85
    target 69
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4386
    name "4386"
  ]
  edge
  [
    source 84
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4387
    name "4387"
  ]
  edge
  [
    source 83
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4388
    name "4388"
  ]
  edge
  [
    source 82
    target 69
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4389
    name "4389"
  ]
  edge
  [
    source 81
    target 69
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4390
    name "4390"
  ]
  edge
  [
    source 80
    target 69
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4391
    name "4391"
  ]
  edge
  [
    source 79
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4392
    name "4392"
  ]
  edge
  [
    source 78
    target 69
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4393
    name "4393"
  ]
  edge
  [
    source 77
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4394
    name "4394"
  ]
  edge
  [
    source 76
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4395
    name "4395"
  ]
  edge
  [
    source 75
    target 69
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4396
    name "4396"
  ]
  edge
  [
    source 74
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4397
    name "4397"
  ]
  edge
  [
    source 73
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4398
    name "4398"
  ]
  edge
  [
    source 72
    target 69
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4399
    name "4399"
  ]
  edge
  [
    source 71
    target 69
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4400
    name "4400"
  ]
  edge
  [
    source 70
    target 69
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4401
    name "4401"
  ]
  edge
  [
    source 697
    target 68
    measure 6
    diseases "Bladder_cancer"
    GOmeasure 6
    id 4402
    name "4402"
  ]
  edge
  [
    source 696
    target 68
    measure 7
    diseases "Bladder_cancer"
    GOmeasure 7
    id 4403
    name "4403"
  ]
  edge
  [
    source 597
    target 68
    measure 5
    diseases "Lung_cancer"
    GOmeasure 5
    id 4404
    name "4404"
  ]
  edge
  [
    source 596
    target 68
    measure 4
    diseases "Lung_cancer"
    GOmeasure 4
    id 4405
    name "4405"
  ]
  edge
  [
    source 391
    target 68
    measure 4
    diseases "Breast_cancer"
    GOmeasure 4
    id 4406
    name "4406"
  ]
  edge
  [
    source 390
    target 68
    measure 7
    diseases "Breast_cancer"
    GOmeasure 7
    id 4407
    name "4407"
  ]
  edge
  [
    source 389
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4408
    name "4408"
  ]
  edge
  [
    source 388
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4409
    name "4409"
  ]
  edge
  [
    source 387
    target 68
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 4410
    name "4410"
  ]
  edge
  [
    source 386
    target 68
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 4411
    name "4411"
  ]
  edge
  [
    source 385
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4412
    name "4412"
  ]
  edge
  [
    source 384
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4413
    name "4413"
  ]
  edge
  [
    source 383
    target 68
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 4414
    name "4414"
  ]
  edge
  [
    source 382
    target 68
    measure 6
    diseases "Breast_cancer,Lung_cancer"
    GOmeasure 6
    id 4415
    name "4415"
  ]
  edge
  [
    source 381
    target 68
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 4416
    name "4416"
  ]
  edge
  [
    source 380
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4417
    name "4417"
  ]
  edge
  [
    source 379
    target 68
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 4418
    name "4418"
  ]
  edge
  [
    source 335
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4419
    name "4419"
  ]
  edge
  [
    source 328
    target 68
    measure 7
    diseases "Bladder_cancer"
    GOmeasure 7
    id 4420
    name "4420"
  ]
  edge
  [
    source 317
    target 68
    measure 5
    diseases "Breast_cancer"
    GOmeasure 5
    id 4421
    name "4421"
  ]
  edge
  [
    source 306
    target 68
    measure 6
    diseases "Breast_cancer"
    GOmeasure 6
    id 4422
    name "4422"
  ]
  edge
  [
    source 174
    target 68
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 4423
    name "4423"
  ]
  edge
  [
    source 173
    target 68
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 4424
    name "4424"
  ]
  edge
  [
    source 172
    target 68
    measure 6
    diseases "Pancreatic_cancer"
    GOmeasure 6
    id 4425
    name "4425"
  ]
  edge
  [
    source 171
    target 68
    measure 5
    diseases "Pancreatic_cancer"
    GOmeasure 5
    id 4426
    name "4426"
  ]
  edge
  [
    source 170
    target 68
    measure 6
    diseases "Breast_cancer,Pancreatic_cancer"
    GOmeasure 6
    id 4427
    name "4427"
  ]
  edge
  [
    source 169
    target 68
    measure 6
    diseases "Breast_cancer,Pancreatic_cancer"
    GOmeasure 6
    id 4428
    name "4428"
  ]
  edge
  [
    source 168
    target 68
    measure 5
    diseases "Pancreatic_cancer"
    GOmeasure 5
    id 4429
    name "4429"
  ]
  edge
  [
    source 94
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4430
    name "4430"
  ]
  edge
  [
    source 93
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4431
    name "4431"
  ]
  edge
  [
    source 92
    target 68
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4432
    name "4432"
  ]
  edge
  [
    source 91
    target 68
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4433
    name "4433"
  ]
  edge
  [
    source 90
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4434
    name "4434"
  ]
  edge
  [
    source 89
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4435
    name "4435"
  ]
  edge
  [
    source 88
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4436
    name "4436"
  ]
  edge
  [
    source 87
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4437
    name "4437"
  ]
  edge
  [
    source 86
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4438
    name "4438"
  ]
  edge
  [
    source 85
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4439
    name "4439"
  ]
  edge
  [
    source 84
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4440
    name "4440"
  ]
  edge
  [
    source 83
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4441
    name "4441"
  ]
  edge
  [
    source 82
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4442
    name "4442"
  ]
  edge
  [
    source 81
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4443
    name "4443"
  ]
  edge
  [
    source 80
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4444
    name "4444"
  ]
  edge
  [
    source 79
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4445
    name "4445"
  ]
  edge
  [
    source 78
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4446
    name "4446"
  ]
  edge
  [
    source 77
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4447
    name "4447"
  ]
  edge
  [
    source 76
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4448
    name "4448"
  ]
  edge
  [
    source 75
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4449
    name "4449"
  ]
  edge
  [
    source 74
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4450
    name "4450"
  ]
  edge
  [
    source 73
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4451
    name "4451"
  ]
  edge
  [
    source 72
    target 68
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4452
    name "4452"
  ]
  edge
  [
    source 71
    target 68
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4453
    name "4453"
  ]
  edge
  [
    source 70
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4454
    name "4454"
  ]
  edge
  [
    source 69
    target 68
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4455
    name "4455"
  ]
  edge
  [
    source 94
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4456
    name "4456"
  ]
  edge
  [
    source 93
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4457
    name "4457"
  ]
  edge
  [
    source 92
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4458
    name "4458"
  ]
  edge
  [
    source 91
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4459
    name "4459"
  ]
  edge
  [
    source 90
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4460
    name "4460"
  ]
  edge
  [
    source 89
    target 67
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4461
    name "4461"
  ]
  edge
  [
    source 88
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4462
    name "4462"
  ]
  edge
  [
    source 87
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4463
    name "4463"
  ]
  edge
  [
    source 86
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4464
    name "4464"
  ]
  edge
  [
    source 85
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4465
    name "4465"
  ]
  edge
  [
    source 84
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4466
    name "4466"
  ]
  edge
  [
    source 83
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4467
    name "4467"
  ]
  edge
  [
    source 82
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4468
    name "4468"
  ]
  edge
  [
    source 81
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4469
    name "4469"
  ]
  edge
  [
    source 80
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4470
    name "4470"
  ]
  edge
  [
    source 79
    target 67
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4471
    name "4471"
  ]
  edge
  [
    source 78
    target 67
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4472
    name "4472"
  ]
  edge
  [
    source 77
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4473
    name "4473"
  ]
  edge
  [
    source 76
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4474
    name "4474"
  ]
  edge
  [
    source 75
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4475
    name "4475"
  ]
  edge
  [
    source 74
    target 67
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4476
    name "4476"
  ]
  edge
  [
    source 73
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4477
    name "4477"
  ]
  edge
  [
    source 72
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4478
    name "4478"
  ]
  edge
  [
    source 71
    target 67
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4479
    name "4479"
  ]
  edge
  [
    source 70
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4480
    name "4480"
  ]
  edge
  [
    source 69
    target 67
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4481
    name "4481"
  ]
  edge
  [
    source 68
    target 67
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4482
    name "4482"
  ]
  edge
  [
    source 94
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4483
    name "4483"
  ]
  edge
  [
    source 93
    target 66
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4484
    name "4484"
  ]
  edge
  [
    source 92
    target 66
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4485
    name "4485"
  ]
  edge
  [
    source 91
    target 66
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4486
    name "4486"
  ]
  edge
  [
    source 90
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4487
    name "4487"
  ]
  edge
  [
    source 89
    target 66
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4488
    name "4488"
  ]
  edge
  [
    source 88
    target 66
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4489
    name "4489"
  ]
  edge
  [
    source 87
    target 66
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4490
    name "4490"
  ]
  edge
  [
    source 86
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4491
    name "4491"
  ]
  edge
  [
    source 85
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4492
    name "4492"
  ]
  edge
  [
    source 84
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4493
    name "4493"
  ]
  edge
  [
    source 83
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4494
    name "4494"
  ]
  edge
  [
    source 82
    target 66
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4495
    name "4495"
  ]
  edge
  [
    source 81
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4496
    name "4496"
  ]
  edge
  [
    source 80
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4497
    name "4497"
  ]
  edge
  [
    source 79
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4498
    name "4498"
  ]
  edge
  [
    source 78
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4499
    name "4499"
  ]
  edge
  [
    source 77
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4500
    name "4500"
  ]
  edge
  [
    source 76
    target 66
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4501
    name "4501"
  ]
  edge
  [
    source 75
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4502
    name "4502"
  ]
  edge
  [
    source 74
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4503
    name "4503"
  ]
  edge
  [
    source 73
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4504
    name "4504"
  ]
  edge
  [
    source 72
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4505
    name "4505"
  ]
  edge
  [
    source 71
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4506
    name "4506"
  ]
  edge
  [
    source 70
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4507
    name "4507"
  ]
  edge
  [
    source 69
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4508
    name "4508"
  ]
  edge
  [
    source 68
    target 66
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4509
    name "4509"
  ]
  edge
  [
    source 67
    target 66
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4510
    name "4510"
  ]
  edge
  [
    source 94
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4511
    name "4511"
  ]
  edge
  [
    source 93
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4512
    name "4512"
  ]
  edge
  [
    source 92
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4513
    name "4513"
  ]
  edge
  [
    source 91
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4514
    name "4514"
  ]
  edge
  [
    source 90
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4515
    name "4515"
  ]
  edge
  [
    source 89
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4516
    name "4516"
  ]
  edge
  [
    source 88
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4517
    name "4517"
  ]
  edge
  [
    source 87
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4518
    name "4518"
  ]
  edge
  [
    source 86
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4519
    name "4519"
  ]
  edge
  [
    source 85
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4520
    name "4520"
  ]
  edge
  [
    source 84
    target 65
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4521
    name "4521"
  ]
  edge
  [
    source 83
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4522
    name "4522"
  ]
  edge
  [
    source 82
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4523
    name "4523"
  ]
  edge
  [
    source 81
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4524
    name "4524"
  ]
  edge
  [
    source 80
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4525
    name "4525"
  ]
  edge
  [
    source 79
    target 65
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4526
    name "4526"
  ]
  edge
  [
    source 78
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4527
    name "4527"
  ]
  edge
  [
    source 77
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4528
    name "4528"
  ]
  edge
  [
    source 76
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4529
    name "4529"
  ]
  edge
  [
    source 75
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4530
    name "4530"
  ]
  edge
  [
    source 74
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4531
    name "4531"
  ]
  edge
  [
    source 73
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4532
    name "4532"
  ]
  edge
  [
    source 72
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4533
    name "4533"
  ]
  edge
  [
    source 71
    target 65
    measure 8
    diseases "Leukemia"
    GOmeasure 8
    id 4534
    name "4534"
  ]
  edge
  [
    source 70
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4535
    name "4535"
  ]
  edge
  [
    source 69
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4536
    name "4536"
  ]
  edge
  [
    source 68
    target 65
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4537
    name "4537"
  ]
  edge
  [
    source 67
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4538
    name "4538"
  ]
  edge
  [
    source 66
    target 65
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4539
    name "4539"
  ]
  edge
  [
    source 94
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4540
    name "4540"
  ]
  edge
  [
    source 93
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4541
    name "4541"
  ]
  edge
  [
    source 92
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4542
    name "4542"
  ]
  edge
  [
    source 91
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4543
    name "4543"
  ]
  edge
  [
    source 90
    target 64
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4544
    name "4544"
  ]
  edge
  [
    source 89
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4545
    name "4545"
  ]
  edge
  [
    source 88
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4546
    name "4546"
  ]
  edge
  [
    source 87
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4547
    name "4547"
  ]
  edge
  [
    source 86
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4548
    name "4548"
  ]
  edge
  [
    source 85
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4549
    name "4549"
  ]
  edge
  [
    source 84
    target 64
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4550
    name "4550"
  ]
  edge
  [
    source 83
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4551
    name "4551"
  ]
  edge
  [
    source 82
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4552
    name "4552"
  ]
  edge
  [
    source 81
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4553
    name "4553"
  ]
  edge
  [
    source 80
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4554
    name "4554"
  ]
  edge
  [
    source 79
    target 64
    measure 8
    diseases "Leukemia"
    GOmeasure 8
    id 4555
    name "4555"
  ]
  edge
  [
    source 78
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4556
    name "4556"
  ]
  edge
  [
    source 77
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4557
    name "4557"
  ]
  edge
  [
    source 76
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4558
    name "4558"
  ]
  edge
  [
    source 75
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4559
    name "4559"
  ]
  edge
  [
    source 74
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4560
    name "4560"
  ]
  edge
  [
    source 73
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4561
    name "4561"
  ]
  edge
  [
    source 72
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4562
    name "4562"
  ]
  edge
  [
    source 71
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4563
    name "4563"
  ]
  edge
  [
    source 70
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4564
    name "4564"
  ]
  edge
  [
    source 69
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4565
    name "4565"
  ]
  edge
  [
    source 68
    target 64
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4566
    name "4566"
  ]
  edge
  [
    source 67
    target 64
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4567
    name "4567"
  ]
  edge
  [
    source 66
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4568
    name "4568"
  ]
  edge
  [
    source 65
    target 64
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4569
    name "4569"
  ]
  edge
  [
    source 94
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4570
    name "4570"
  ]
  edge
  [
    source 93
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4571
    name "4571"
  ]
  edge
  [
    source 92
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4572
    name "4572"
  ]
  edge
  [
    source 91
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4573
    name "4573"
  ]
  edge
  [
    source 90
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4574
    name "4574"
  ]
  edge
  [
    source 89
    target 63
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4575
    name "4575"
  ]
  edge
  [
    source 88
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4576
    name "4576"
  ]
  edge
  [
    source 87
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4577
    name "4577"
  ]
  edge
  [
    source 86
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4578
    name "4578"
  ]
  edge
  [
    source 85
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4579
    name "4579"
  ]
  edge
  [
    source 84
    target 63
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4580
    name "4580"
  ]
  edge
  [
    source 83
    target 63
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4581
    name "4581"
  ]
  edge
  [
    source 82
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4582
    name "4582"
  ]
  edge
  [
    source 81
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4583
    name "4583"
  ]
  edge
  [
    source 80
    target 63
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4584
    name "4584"
  ]
  edge
  [
    source 79
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4585
    name "4585"
  ]
  edge
  [
    source 78
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4586
    name "4586"
  ]
  edge
  [
    source 77
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4587
    name "4587"
  ]
  edge
  [
    source 76
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4588
    name "4588"
  ]
  edge
  [
    source 75
    target 63
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4589
    name "4589"
  ]
  edge
  [
    source 74
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4590
    name "4590"
  ]
  edge
  [
    source 73
    target 63
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4591
    name "4591"
  ]
  edge
  [
    source 72
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4592
    name "4592"
  ]
  edge
  [
    source 71
    target 63
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4593
    name "4593"
  ]
  edge
  [
    source 70
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4594
    name "4594"
  ]
  edge
  [
    source 69
    target 63
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4595
    name "4595"
  ]
  edge
  [
    source 68
    target 63
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4596
    name "4596"
  ]
  edge
  [
    source 67
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4597
    name "4597"
  ]
  edge
  [
    source 66
    target 63
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4598
    name "4598"
  ]
  edge
  [
    source 65
    target 63
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4599
    name "4599"
  ]
  edge
  [
    source 64
    target 63
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4600
    name "4600"
  ]
  edge
  [
    source 94
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4601
    name "4601"
  ]
  edge
  [
    source 93
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4602
    name "4602"
  ]
  edge
  [
    source 92
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4603
    name "4603"
  ]
  edge
  [
    source 91
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4604
    name "4604"
  ]
  edge
  [
    source 90
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4605
    name "4605"
  ]
  edge
  [
    source 89
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4606
    name "4606"
  ]
  edge
  [
    source 88
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4607
    name "4607"
  ]
  edge
  [
    source 87
    target 62
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4608
    name "4608"
  ]
  edge
  [
    source 86
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4609
    name "4609"
  ]
  edge
  [
    source 85
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4610
    name "4610"
  ]
  edge
  [
    source 84
    target 62
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4611
    name "4611"
  ]
  edge
  [
    source 83
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4612
    name "4612"
  ]
  edge
  [
    source 82
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4613
    name "4613"
  ]
  edge
  [
    source 81
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4614
    name "4614"
  ]
  edge
  [
    source 80
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4615
    name "4615"
  ]
  edge
  [
    source 79
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4616
    name "4616"
  ]
  edge
  [
    source 78
    target 62
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4617
    name "4617"
  ]
  edge
  [
    source 77
    target 62
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4618
    name "4618"
  ]
  edge
  [
    source 76
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4619
    name "4619"
  ]
  edge
  [
    source 75
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4620
    name "4620"
  ]
  edge
  [
    source 74
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4621
    name "4621"
  ]
  edge
  [
    source 73
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4622
    name "4622"
  ]
  edge
  [
    source 72
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4623
    name "4623"
  ]
  edge
  [
    source 71
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4624
    name "4624"
  ]
  edge
  [
    source 70
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4625
    name "4625"
  ]
  edge
  [
    source 69
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4626
    name "4626"
  ]
  edge
  [
    source 68
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4627
    name "4627"
  ]
  edge
  [
    source 67
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4628
    name "4628"
  ]
  edge
  [
    source 66
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4629
    name "4629"
  ]
  edge
  [
    source 65
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4630
    name "4630"
  ]
  edge
  [
    source 64
    target 62
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4631
    name "4631"
  ]
  edge
  [
    source 63
    target 62
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4632
    name "4632"
  ]
  edge
  [
    source 94
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4633
    name "4633"
  ]
  edge
  [
    source 93
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4634
    name "4634"
  ]
  edge
  [
    source 92
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4635
    name "4635"
  ]
  edge
  [
    source 91
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4636
    name "4636"
  ]
  edge
  [
    source 90
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4637
    name "4637"
  ]
  edge
  [
    source 89
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4638
    name "4638"
  ]
  edge
  [
    source 88
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4639
    name "4639"
  ]
  edge
  [
    source 87
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4640
    name "4640"
  ]
  edge
  [
    source 86
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4641
    name "4641"
  ]
  edge
  [
    source 85
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4642
    name "4642"
  ]
  edge
  [
    source 84
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4643
    name "4643"
  ]
  edge
  [
    source 83
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4644
    name "4644"
  ]
  edge
  [
    source 82
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4645
    name "4645"
  ]
  edge
  [
    source 81
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4646
    name "4646"
  ]
  edge
  [
    source 80
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4647
    name "4647"
  ]
  edge
  [
    source 79
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4648
    name "4648"
  ]
  edge
  [
    source 78
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4649
    name "4649"
  ]
  edge
  [
    source 77
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4650
    name "4650"
  ]
  edge
  [
    source 76
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4651
    name "4651"
  ]
  edge
  [
    source 75
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4652
    name "4652"
  ]
  edge
  [
    source 74
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4653
    name "4653"
  ]
  edge
  [
    source 73
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4654
    name "4654"
  ]
  edge
  [
    source 72
    target 61
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4655
    name "4655"
  ]
  edge
  [
    source 71
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4656
    name "4656"
  ]
  edge
  [
    source 70
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4657
    name "4657"
  ]
  edge
  [
    source 69
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4658
    name "4658"
  ]
  edge
  [
    source 68
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4659
    name "4659"
  ]
  edge
  [
    source 67
    target 61
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4660
    name "4660"
  ]
  edge
  [
    source 66
    target 61
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4661
    name "4661"
  ]
  edge
  [
    source 65
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4662
    name "4662"
  ]
  edge
  [
    source 64
    target 61
    measure 7
    diseases "Leukemia"
    GOmeasure 7
    id 4663
    name "4663"
  ]
  edge
  [
    source 63
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4664
    name "4664"
  ]
  edge
  [
    source 62
    target 61
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4665
    name "4665"
  ]
  edge
  [
    source 94
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4666
    name "4666"
  ]
  edge
  [
    source 93
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4667
    name "4667"
  ]
  edge
  [
    source 92
    target 60
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4668
    name "4668"
  ]
  edge
  [
    source 91
    target 60
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4669
    name "4669"
  ]
  edge
  [
    source 90
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4670
    name "4670"
  ]
  edge
  [
    source 89
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4671
    name "4671"
  ]
  edge
  [
    source 88
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4672
    name "4672"
  ]
  edge
  [
    source 87
    target 60
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4673
    name "4673"
  ]
  edge
  [
    source 86
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4674
    name "4674"
  ]
  edge
  [
    source 85
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4675
    name "4675"
  ]
  edge
  [
    source 84
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4676
    name "4676"
  ]
  edge
  [
    source 83
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4677
    name "4677"
  ]
  edge
  [
    source 82
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4678
    name "4678"
  ]
  edge
  [
    source 81
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4679
    name "4679"
  ]
  edge
  [
    source 80
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4680
    name "4680"
  ]
  edge
  [
    source 79
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4681
    name "4681"
  ]
  edge
  [
    source 78
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4682
    name "4682"
  ]
  edge
  [
    source 77
    target 60
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4683
    name "4683"
  ]
  edge
  [
    source 76
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4684
    name "4684"
  ]
  edge
  [
    source 75
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4685
    name "4685"
  ]
  edge
  [
    source 74
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4686
    name "4686"
  ]
  edge
  [
    source 73
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4687
    name "4687"
  ]
  edge
  [
    source 72
    target 60
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4688
    name "4688"
  ]
  edge
  [
    source 71
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4689
    name "4689"
  ]
  edge
  [
    source 70
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4690
    name "4690"
  ]
  edge
  [
    source 69
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4691
    name "4691"
  ]
  edge
  [
    source 68
    target 60
    measure 6
    diseases "Leukemia"
    GOmeasure 6
    id 4692
    name "4692"
  ]
  edge
  [
    source 67
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4693
    name "4693"
  ]
  edge
  [
    source 66
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4694
    name "4694"
  ]
  edge
  [
    source 65
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4695
    name "4695"
  ]
  edge
  [
    source 64
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4696
    name "4696"
  ]
  edge
  [
    source 63
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4697
    name "4697"
  ]
  edge
  [
    source 62
    target 60
    measure 4
    diseases "Leukemia"
    GOmeasure 4
    id 4698
    name "4698"
  ]
  edge
  [
    source 61
    target 60
    measure 5
    diseases "Leukemia"
    GOmeasure 5
    id 4699
    name "4699"
  ]
  edge
  [
    source 58
    target 57
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4700
    name "4700"
  ]
  edge
  [
    source 58
    target 56
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4701
    name "4701"
  ]
  edge
  [
    source 57
    target 56
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4702
    name "4702"
  ]
  edge
  [
    source 486
    target 55
    measure 5
    diseases "Night_blindness"
    GOmeasure 5
    id 4703
    name "4703"
  ]
  edge
  [
    source 485
    target 55
    measure 6
    diseases "Night_blindness"
    GOmeasure 6
    id 4704
    name "4704"
  ]
  edge
  [
    source 484
    target 55
    measure 6
    diseases "Night_blindness"
    GOmeasure 6
    id 4705
    name "4705"
  ]
  edge
  [
    source 58
    target 55
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4706
    name "4706"
  ]
  edge
  [
    source 57
    target 55
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4707
    name "4707"
  ]
  edge
  [
    source 56
    target 55
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4708
    name "4708"
  ]
  edge
  [
    source 58
    target 54
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4709
    name "4709"
  ]
  edge
  [
    source 57
    target 54
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4710
    name "4710"
  ]
  edge
  [
    source 56
    target 54
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4711
    name "4711"
  ]
  edge
  [
    source 55
    target 54
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4712
    name "4712"
  ]
  edge
  [
    source 58
    target 53
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4713
    name "4713"
  ]
  edge
  [
    source 57
    target 53
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4714
    name "4714"
  ]
  edge
  [
    source 56
    target 53
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4715
    name "4715"
  ]
  edge
  [
    source 55
    target 53
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4716
    name "4716"
  ]
  edge
  [
    source 54
    target 53
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4717
    name "4717"
  ]
  edge
  [
    source 58
    target 52
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4718
    name "4718"
  ]
  edge
  [
    source 57
    target 52
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4719
    name "4719"
  ]
  edge
  [
    source 56
    target 52
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4720
    name "4720"
  ]
  edge
  [
    source 55
    target 52
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4721
    name "4721"
  ]
  edge
  [
    source 54
    target 52
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4722
    name "4722"
  ]
  edge
  [
    source 53
    target 52
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4723
    name "4723"
  ]
  edge
  [
    source 58
    target 51
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4724
    name "4724"
  ]
  edge
  [
    source 57
    target 51
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4725
    name "4725"
  ]
  edge
  [
    source 56
    target 51
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4726
    name "4726"
  ]
  edge
  [
    source 55
    target 51
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4727
    name "4727"
  ]
  edge
  [
    source 54
    target 51
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4728
    name "4728"
  ]
  edge
  [
    source 53
    target 51
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4729
    name "4729"
  ]
  edge
  [
    source 52
    target 51
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4730
    name "4730"
  ]
  edge
  [
    source 162
    target 50
    measure 4
    diseases "Usher_syndrome"
    GOmeasure 4
    id 4731
    name "4731"
  ]
  edge
  [
    source 161
    target 50
    measure 6
    diseases "Usher_syndrome"
    GOmeasure 6
    id 4732
    name "4732"
  ]
  edge
  [
    source 159
    target 50
    measure 4
    diseases "Usher_syndrome"
    GOmeasure 4
    id 4733
    name "4733"
  ]
  edge
  [
    source 143
    target 50
    measure 4
    diseases "Usher_syndrome"
    GOmeasure 4
    id 4734
    name "4734"
  ]
  edge
  [
    source 58
    target 50
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4735
    name "4735"
  ]
  edge
  [
    source 57
    target 50
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4736
    name "4736"
  ]
  edge
  [
    source 56
    target 50
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4737
    name "4737"
  ]
  edge
  [
    source 55
    target 50
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4738
    name "4738"
  ]
  edge
  [
    source 54
    target 50
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4739
    name "4739"
  ]
  edge
  [
    source 53
    target 50
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4740
    name "4740"
  ]
  edge
  [
    source 52
    target 50
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4741
    name "4741"
  ]
  edge
  [
    source 51
    target 50
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4742
    name "4742"
  ]
  edge
  [
    source 58
    target 49
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4743
    name "4743"
  ]
  edge
  [
    source 57
    target 49
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4744
    name "4744"
  ]
  edge
  [
    source 56
    target 49
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4745
    name "4745"
  ]
  edge
  [
    source 55
    target 49
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4746
    name "4746"
  ]
  edge
  [
    source 54
    target 49
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4747
    name "4747"
  ]
  edge
  [
    source 53
    target 49
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4748
    name "4748"
  ]
  edge
  [
    source 52
    target 49
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4749
    name "4749"
  ]
  edge
  [
    source 51
    target 49
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4750
    name "4750"
  ]
  edge
  [
    source 50
    target 49
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4751
    name "4751"
  ]
  edge
  [
    source 486
    target 48
    measure 7
    diseases "Night_blindness"
    GOmeasure 7
    id 4752
    name "4752"
  ]
  edge
  [
    source 485
    target 48
    measure 6
    diseases "Night_blindness"
    GOmeasure 6
    id 4753
    name "4753"
  ]
  edge
  [
    source 484
    target 48
    measure 5
    diseases "Night_blindness"
    GOmeasure 5
    id 4754
    name "4754"
  ]
  edge
  [
    source 58
    target 48
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4755
    name "4755"
  ]
  edge
  [
    source 57
    target 48
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4756
    name "4756"
  ]
  edge
  [
    source 56
    target 48
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4757
    name "4757"
  ]
  edge
  [
    source 55
    target 48
    measure 5
    diseases "Night_blindness,Retinitis_pigmentosa"
    GOmeasure 5
    id 4758
    name "4758"
  ]
  edge
  [
    source 54
    target 48
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4759
    name "4759"
  ]
  edge
  [
    source 53
    target 48
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4760
    name "4760"
  ]
  edge
  [
    source 52
    target 48
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4761
    name "4761"
  ]
  edge
  [
    source 51
    target 48
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4762
    name "4762"
  ]
  edge
  [
    source 50
    target 48
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4763
    name "4763"
  ]
  edge
  [
    source 49
    target 48
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4764
    name "4764"
  ]
  edge
  [
    source 776
    target 47
    measure 4
    diseases "Retinal_cone_dsytrophy"
    GOmeasure 4
    id 4765
    name "4765"
  ]
  edge
  [
    source 775
    target 47
    measure 6
    diseases "Retinal_cone_dsytrophy"
    GOmeasure 6
    id 4766
    name "4766"
  ]
  edge
  [
    source 774
    target 47
    measure 6
    diseases "Retinal_cone_dsytrophy"
    GOmeasure 6
    id 4767
    name "4767"
  ]
  edge
  [
    source 58
    target 47
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4768
    name "4768"
  ]
  edge
  [
    source 57
    target 47
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4769
    name "4769"
  ]
  edge
  [
    source 56
    target 47
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4770
    name "4770"
  ]
  edge
  [
    source 55
    target 47
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4771
    name "4771"
  ]
  edge
  [
    source 54
    target 47
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4772
    name "4772"
  ]
  edge
  [
    source 53
    target 47
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4773
    name "4773"
  ]
  edge
  [
    source 52
    target 47
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4774
    name "4774"
  ]
  edge
  [
    source 51
    target 47
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4775
    name "4775"
  ]
  edge
  [
    source 50
    target 47
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4776
    name "4776"
  ]
  edge
  [
    source 49
    target 47
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4777
    name "4777"
  ]
  edge
  [
    source 48
    target 47
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4778
    name "4778"
  ]
  edge
  [
    source 58
    target 46
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4779
    name "4779"
  ]
  edge
  [
    source 57
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4780
    name "4780"
  ]
  edge
  [
    source 56
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4781
    name "4781"
  ]
  edge
  [
    source 55
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4782
    name "4782"
  ]
  edge
  [
    source 54
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4783
    name "4783"
  ]
  edge
  [
    source 53
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4784
    name "4784"
  ]
  edge
  [
    source 52
    target 46
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4785
    name "4785"
  ]
  edge
  [
    source 51
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4786
    name "4786"
  ]
  edge
  [
    source 50
    target 46
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4787
    name "4787"
  ]
  edge
  [
    source 49
    target 46
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4788
    name "4788"
  ]
  edge
  [
    source 48
    target 46
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4789
    name "4789"
  ]
  edge
  [
    source 47
    target 46
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4790
    name "4790"
  ]
  edge
  [
    source 58
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4791
    name "4791"
  ]
  edge
  [
    source 57
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4792
    name "4792"
  ]
  edge
  [
    source 56
    target 45
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4793
    name "4793"
  ]
  edge
  [
    source 55
    target 45
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4794
    name "4794"
  ]
  edge
  [
    source 54
    target 45
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4795
    name "4795"
  ]
  edge
  [
    source 53
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4796
    name "4796"
  ]
  edge
  [
    source 52
    target 45
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4797
    name "4797"
  ]
  edge
  [
    source 51
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4798
    name "4798"
  ]
  edge
  [
    source 50
    target 45
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4799
    name "4799"
  ]
  edge
  [
    source 49
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4800
    name "4800"
  ]
  edge
  [
    source 48
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4801
    name "4801"
  ]
  edge
  [
    source 47
    target 45
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4802
    name "4802"
  ]
  edge
  [
    source 46
    target 45
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4803
    name "4803"
  ]
  edge
  [
    source 600
    target 44
    measure 6
    diseases "Leber_congenital_amaurosis"
    GOmeasure 6
    id 4804
    name "4804"
  ]
  edge
  [
    source 58
    target 44
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4805
    name "4805"
  ]
  edge
  [
    source 57
    target 44
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4806
    name "4806"
  ]
  edge
  [
    source 56
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4807
    name "4807"
  ]
  edge
  [
    source 55
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4808
    name "4808"
  ]
  edge
  [
    source 54
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4809
    name "4809"
  ]
  edge
  [
    source 53
    target 44
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4810
    name "4810"
  ]
  edge
  [
    source 52
    target 44
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4811
    name "4811"
  ]
  edge
  [
    source 51
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4812
    name "4812"
  ]
  edge
  [
    source 50
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4813
    name "4813"
  ]
  edge
  [
    source 49
    target 44
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4814
    name "4814"
  ]
  edge
  [
    source 48
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4815
    name "4815"
  ]
  edge
  [
    source 47
    target 44
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4816
    name "4816"
  ]
  edge
  [
    source 46
    target 44
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4817
    name "4817"
  ]
  edge
  [
    source 45
    target 44
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4818
    name "4818"
  ]
  edge
  [
    source 58
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4819
    name "4819"
  ]
  edge
  [
    source 57
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4820
    name "4820"
  ]
  edge
  [
    source 56
    target 43
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4821
    name "4821"
  ]
  edge
  [
    source 55
    target 43
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4822
    name "4822"
  ]
  edge
  [
    source 54
    target 43
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4823
    name "4823"
  ]
  edge
  [
    source 53
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4824
    name "4824"
  ]
  edge
  [
    source 52
    target 43
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4825
    name "4825"
  ]
  edge
  [
    source 51
    target 43
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4826
    name "4826"
  ]
  edge
  [
    source 50
    target 43
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4827
    name "4827"
  ]
  edge
  [
    source 49
    target 43
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4828
    name "4828"
  ]
  edge
  [
    source 48
    target 43
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4829
    name "4829"
  ]
  edge
  [
    source 47
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4830
    name "4830"
  ]
  edge
  [
    source 46
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4831
    name "4831"
  ]
  edge
  [
    source 45
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4832
    name "4832"
  ]
  edge
  [
    source 44
    target 43
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4833
    name "4833"
  ]
  edge
  [
    source 58
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4834
    name "4834"
  ]
  edge
  [
    source 57
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4835
    name "4835"
  ]
  edge
  [
    source 56
    target 42
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4836
    name "4836"
  ]
  edge
  [
    source 55
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4837
    name "4837"
  ]
  edge
  [
    source 54
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4838
    name "4838"
  ]
  edge
  [
    source 53
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4839
    name "4839"
  ]
  edge
  [
    source 52
    target 42
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4840
    name "4840"
  ]
  edge
  [
    source 51
    target 42
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4841
    name "4841"
  ]
  edge
  [
    source 50
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4842
    name "4842"
  ]
  edge
  [
    source 49
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4843
    name "4843"
  ]
  edge
  [
    source 48
    target 42
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4844
    name "4844"
  ]
  edge
  [
    source 47
    target 42
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4845
    name "4845"
  ]
  edge
  [
    source 46
    target 42
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4846
    name "4846"
  ]
  edge
  [
    source 45
    target 42
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4847
    name "4847"
  ]
  edge
  [
    source 44
    target 42
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4848
    name "4848"
  ]
  edge
  [
    source 43
    target 42
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4849
    name "4849"
  ]
  edge
  [
    source 58
    target 41
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4850
    name "4850"
  ]
  edge
  [
    source 57
    target 41
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4851
    name "4851"
  ]
  edge
  [
    source 56
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4852
    name "4852"
  ]
  edge
  [
    source 55
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4853
    name "4853"
  ]
  edge
  [
    source 54
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4854
    name "4854"
  ]
  edge
  [
    source 53
    target 41
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4855
    name "4855"
  ]
  edge
  [
    source 52
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4856
    name "4856"
  ]
  edge
  [
    source 51
    target 41
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4857
    name "4857"
  ]
  edge
  [
    source 50
    target 41
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4858
    name "4858"
  ]
  edge
  [
    source 49
    target 41
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4859
    name "4859"
  ]
  edge
  [
    source 48
    target 41
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4860
    name "4860"
  ]
  edge
  [
    source 47
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4861
    name "4861"
  ]
  edge
  [
    source 46
    target 41
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4862
    name "4862"
  ]
  edge
  [
    source 45
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4863
    name "4863"
  ]
  edge
  [
    source 44
    target 41
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4864
    name "4864"
  ]
  edge
  [
    source 43
    target 41
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4865
    name "4865"
  ]
  edge
  [
    source 42
    target 41
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4866
    name "4866"
  ]
  edge
  [
    source 600
    target 40
    measure 6
    diseases "Leber_congenital_amaurosis"
    GOmeasure 6
    id 4867
    name "4867"
  ]
  edge
  [
    source 58
    target 40
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4868
    name "4868"
  ]
  edge
  [
    source 57
    target 40
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4869
    name "4869"
  ]
  edge
  [
    source 56
    target 40
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4870
    name "4870"
  ]
  edge
  [
    source 55
    target 40
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4871
    name "4871"
  ]
  edge
  [
    source 54
    target 40
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4872
    name "4872"
  ]
  edge
  [
    source 53
    target 40
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4873
    name "4873"
  ]
  edge
  [
    source 52
    target 40
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4874
    name "4874"
  ]
  edge
  [
    source 51
    target 40
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4875
    name "4875"
  ]
  edge
  [
    source 50
    target 40
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4876
    name "4876"
  ]
  edge
  [
    source 49
    target 40
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4877
    name "4877"
  ]
  edge
  [
    source 48
    target 40
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4878
    name "4878"
  ]
  edge
  [
    source 47
    target 40
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4879
    name "4879"
  ]
  edge
  [
    source 46
    target 40
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4880
    name "4880"
  ]
  edge
  [
    source 45
    target 40
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4881
    name "4881"
  ]
  edge
  [
    source 44
    target 40
    measure 6
    diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
    GOmeasure 6
    id 4882
    name "4882"
  ]
  edge
  [
    source 43
    target 40
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4883
    name "4883"
  ]
  edge
  [
    source 42
    target 40
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4884
    name "4884"
  ]
  edge
  [
    source 41
    target 40
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4885
    name "4885"
  ]
  edge
  [
    source 358
    target 39
    measure 4
    diseases "Hypertriglyceridemia"
    GOmeasure 4
    id 4886
    name "4886"
  ]
  edge
  [
    source 345
    target 39
    measure 4
    diseases "Hypertriglyceridemia"
    GOmeasure 4
    id 4887
    name "4887"
  ]
  edge
  [
    source 58
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4888
    name "4888"
  ]
  edge
  [
    source 57
    target 39
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4889
    name "4889"
  ]
  edge
  [
    source 56
    target 39
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4890
    name "4890"
  ]
  edge
  [
    source 55
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4891
    name "4891"
  ]
  edge
  [
    source 54
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4892
    name "4892"
  ]
  edge
  [
    source 53
    target 39
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4893
    name "4893"
  ]
  edge
  [
    source 52
    target 39
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4894
    name "4894"
  ]
  edge
  [
    source 51
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4895
    name "4895"
  ]
  edge
  [
    source 50
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4896
    name "4896"
  ]
  edge
  [
    source 49
    target 39
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4897
    name "4897"
  ]
  edge
  [
    source 48
    target 39
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4898
    name "4898"
  ]
  edge
  [
    source 47
    target 39
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4899
    name "4899"
  ]
  edge
  [
    source 46
    target 39
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4900
    name "4900"
  ]
  edge
  [
    source 45
    target 39
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4901
    name "4901"
  ]
  edge
  [
    source 44
    target 39
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4902
    name "4902"
  ]
  edge
  [
    source 43
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4903
    name "4903"
  ]
  edge
  [
    source 42
    target 39
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4904
    name "4904"
  ]
  edge
  [
    source 41
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4905
    name "4905"
  ]
  edge
  [
    source 40
    target 39
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4906
    name "4906"
  ]
  edge
  [
    source 58
    target 38
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4907
    name "4907"
  ]
  edge
  [
    source 57
    target 38
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4908
    name "4908"
  ]
  edge
  [
    source 56
    target 38
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4909
    name "4909"
  ]
  edge
  [
    source 55
    target 38
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4910
    name "4910"
  ]
  edge
  [
    source 54
    target 38
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4911
    name "4911"
  ]
  edge
  [
    source 53
    target 38
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4912
    name "4912"
  ]
  edge
  [
    source 52
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4913
    name "4913"
  ]
  edge
  [
    source 51
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4914
    name "4914"
  ]
  edge
  [
    source 50
    target 38
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4915
    name "4915"
  ]
  edge
  [
    source 49
    target 38
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4916
    name "4916"
  ]
  edge
  [
    source 48
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4917
    name "4917"
  ]
  edge
  [
    source 47
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4918
    name "4918"
  ]
  edge
  [
    source 46
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4919
    name "4919"
  ]
  edge
  [
    source 45
    target 38
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4920
    name "4920"
  ]
  edge
  [
    source 44
    target 38
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4921
    name "4921"
  ]
  edge
  [
    source 43
    target 38
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4922
    name "4922"
  ]
  edge
  [
    source 42
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4923
    name "4923"
  ]
  edge
  [
    source 41
    target 38
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4924
    name "4924"
  ]
  edge
  [
    source 40
    target 38
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4925
    name "4925"
  ]
  edge
  [
    source 39
    target 38
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4926
    name "4926"
  ]
  edge
  [
    source 58
    target 37
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4927
    name "4927"
  ]
  edge
  [
    source 57
    target 37
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4928
    name "4928"
  ]
  edge
  [
    source 56
    target 37
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4929
    name "4929"
  ]
  edge
  [
    source 55
    target 37
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4930
    name "4930"
  ]
  edge
  [
    source 54
    target 37
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4931
    name "4931"
  ]
  edge
  [
    source 53
    target 37
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4932
    name "4932"
  ]
  edge
  [
    source 52
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4933
    name "4933"
  ]
  edge
  [
    source 51
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4934
    name "4934"
  ]
  edge
  [
    source 50
    target 37
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4935
    name "4935"
  ]
  edge
  [
    source 49
    target 37
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4936
    name "4936"
  ]
  edge
  [
    source 48
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4937
    name "4937"
  ]
  edge
  [
    source 47
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4938
    name "4938"
  ]
  edge
  [
    source 46
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4939
    name "4939"
  ]
  edge
  [
    source 45
    target 37
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4940
    name "4940"
  ]
  edge
  [
    source 44
    target 37
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4941
    name "4941"
  ]
  edge
  [
    source 43
    target 37
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4942
    name "4942"
  ]
  edge
  [
    source 42
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4943
    name "4943"
  ]
  edge
  [
    source 41
    target 37
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4944
    name "4944"
  ]
  edge
  [
    source 40
    target 37
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4945
    name "4945"
  ]
  edge
  [
    source 39
    target 37
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4946
    name "4946"
  ]
  edge
  [
    source 38
    target 37
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4947
    name "4947"
  ]
  edge
  [
    source 58
    target 36
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4948
    name "4948"
  ]
  edge
  [
    source 57
    target 36
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4949
    name "4949"
  ]
  edge
  [
    source 56
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4950
    name "4950"
  ]
  edge
  [
    source 55
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4951
    name "4951"
  ]
  edge
  [
    source 54
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4952
    name "4952"
  ]
  edge
  [
    source 53
    target 36
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4953
    name "4953"
  ]
  edge
  [
    source 52
    target 36
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4954
    name "4954"
  ]
  edge
  [
    source 51
    target 36
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4955
    name "4955"
  ]
  edge
  [
    source 50
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4956
    name "4956"
  ]
  edge
  [
    source 49
    target 36
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4957
    name "4957"
  ]
  edge
  [
    source 48
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4958
    name "4958"
  ]
  edge
  [
    source 47
    target 36
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4959
    name "4959"
  ]
  edge
  [
    source 46
    target 36
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4960
    name "4960"
  ]
  edge
  [
    source 45
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4961
    name "4961"
  ]
  edge
  [
    source 44
    target 36
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4962
    name "4962"
  ]
  edge
  [
    source 43
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4963
    name "4963"
  ]
  edge
  [
    source 42
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4964
    name "4964"
  ]
  edge
  [
    source 41
    target 36
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4965
    name "4965"
  ]
  edge
  [
    source 40
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4966
    name "4966"
  ]
  edge
  [
    source 39
    target 36
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4967
    name "4967"
  ]
  edge
  [
    source 38
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4968
    name "4968"
  ]
  edge
  [
    source 37
    target 36
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4969
    name "4969"
  ]
  edge
  [
    source 36
    target 35
    measure 5
    diseases "Fundus_albipunctatus"
    GOmeasure 5
    id 4970
    name "4970"
  ]
  edge
  [
    source 600
    target 34
    measure 7
    diseases "Leber_congenital_amaurosis"
    GOmeasure 7
    id 4971
    name "4971"
  ]
  edge
  [
    source 58
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4972
    name "4972"
  ]
  edge
  [
    source 57
    target 34
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4973
    name "4973"
  ]
  edge
  [
    source 56
    target 34
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4974
    name "4974"
  ]
  edge
  [
    source 55
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4975
    name "4975"
  ]
  edge
  [
    source 54
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4976
    name "4976"
  ]
  edge
  [
    source 53
    target 34
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4977
    name "4977"
  ]
  edge
  [
    source 52
    target 34
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4978
    name "4978"
  ]
  edge
  [
    source 51
    target 34
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4979
    name "4979"
  ]
  edge
  [
    source 50
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4980
    name "4980"
  ]
  edge
  [
    source 49
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4981
    name "4981"
  ]
  edge
  [
    source 48
    target 34
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4982
    name "4982"
  ]
  edge
  [
    source 47
    target 34
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4983
    name "4983"
  ]
  edge
  [
    source 46
    target 34
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4984
    name "4984"
  ]
  edge
  [
    source 45
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4985
    name "4985"
  ]
  edge
  [
    source 44
    target 34
    measure 5
    diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
    GOmeasure 5
    id 4986
    name "4986"
  ]
  edge
  [
    source 43
    target 34
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4987
    name "4987"
  ]
  edge
  [
    source 42
    target 34
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4988
    name "4988"
  ]
  edge
  [
    source 41
    target 34
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 4989
    name "4989"
  ]
  edge
  [
    source 40
    target 34
    measure 5
    diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
    GOmeasure 5
    id 4990
    name "4990"
  ]
  edge
  [
    source 39
    target 34
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 4991
    name "4991"
  ]
  edge
  [
    source 38
    target 34
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4992
    name "4992"
  ]
  edge
  [
    source 37
    target 34
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 4993
    name "4993"
  ]
  edge
  [
    source 36
    target 34
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 4994
    name "4994"
  ]
  edge
  [
    source 600
    target 33
    measure 6
    diseases "Leber_congenital_amaurosis"
    GOmeasure 6
    id 4995
    name "4995"
  ]
  edge
  [
    source 44
    target 33
    measure 7
    diseases "Leber_congenital_amaurosis"
    GOmeasure 7
    id 4996
    name "4996"
  ]
  edge
  [
    source 40
    target 33
    measure 6
    diseases "Leber_congenital_amaurosis"
    GOmeasure 6
    id 4997
    name "4997"
  ]
  edge
  [
    source 34
    target 33
    measure 6
    diseases "Cone_dystrophy,Leber_congenital_amaurosis"
    GOmeasure 6
    id 4998
    name "4998"
  ]
  edge
  [
    source 600
    target 32
    measure 6
    diseases "Leber_congenital_amaurosis"
    GOmeasure 6
    id 4999
    name "4999"
  ]
  edge
  [
    source 44
    target 32
    measure 5
    diseases "Leber_congenital_amaurosis"
    GOmeasure 5
    id 5000
    name "5000"
  ]
  edge
  [
    source 40
    target 32
    measure 5
    diseases "Leber_congenital_amaurosis"
    GOmeasure 5
    id 5001
    name "5001"
  ]
  edge
  [
    source 34
    target 32
    measure 6
    diseases "Cone_dystrophy,Leber_congenital_amaurosis"
    GOmeasure 6
    id 5002
    name "5002"
  ]
  edge
  [
    source 33
    target 32
    measure 4
    diseases "Cone_dystrophy,Leber_congenital_amaurosis"
    GOmeasure 4
    id 5003
    name "5003"
  ]
  edge
  [
    source 600
    target 31
    measure 4
    diseases "Leber_congenital_amaurosis"
    GOmeasure 4
    id 5004
    name "5004"
  ]
  edge
  [
    source 58
    target 31
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5005
    name "5005"
  ]
  edge
  [
    source 57
    target 31
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5006
    name "5006"
  ]
  edge
  [
    source 56
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5007
    name "5007"
  ]
  edge
  [
    source 55
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5008
    name "5008"
  ]
  edge
  [
    source 54
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5009
    name "5009"
  ]
  edge
  [
    source 53
    target 31
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5010
    name "5010"
  ]
  edge
  [
    source 52
    target 31
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5011
    name "5011"
  ]
  edge
  [
    source 51
    target 31
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5012
    name "5012"
  ]
  edge
  [
    source 50
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5013
    name "5013"
  ]
  edge
  [
    source 49
    target 31
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5014
    name "5014"
  ]
  edge
  [
    source 48
    target 31
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5015
    name "5015"
  ]
  edge
  [
    source 47
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5016
    name "5016"
  ]
  edge
  [
    source 46
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5017
    name "5017"
  ]
  edge
  [
    source 45
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5018
    name "5018"
  ]
  edge
  [
    source 44
    target 31
    measure 6
    diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
    GOmeasure 6
    id 5019
    name "5019"
  ]
  edge
  [
    source 43
    target 31
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5020
    name "5020"
  ]
  edge
  [
    source 42
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5021
    name "5021"
  ]
  edge
  [
    source 41
    target 31
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5022
    name "5022"
  ]
  edge
  [
    source 40
    target 31
    measure 5
    diseases "Leber_congenital_amaurosis,Retinitis_pigmentosa"
    GOmeasure 5
    id 5023
    name "5023"
  ]
  edge
  [
    source 39
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5024
    name "5024"
  ]
  edge
  [
    source 38
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5025
    name "5025"
  ]
  edge
  [
    source 37
    target 31
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5026
    name "5026"
  ]
  edge
  [
    source 36
    target 31
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5027
    name "5027"
  ]
  edge
  [
    source 34
    target 31
    measure 4
    diseases "Cone_dystrophy,Leber_congenital_amaurosis,Retinitis_pigmentosa"
    GOmeasure 4
    id 5028
    name "5028"
  ]
  edge
  [
    source 33
    target 31
    measure 6
    diseases "Cone_dystrophy,Leber_congenital_amaurosis"
    GOmeasure 6
    id 5029
    name "5029"
  ]
  edge
  [
    source 32
    target 31
    measure 4
    diseases "Cone_dystrophy,Leber_congenital_amaurosis"
    GOmeasure 4
    id 5030
    name "5030"
  ]
  edge
  [
    source 34
    target 30
    measure 7
    diseases "Cone_dystrophy"
    GOmeasure 7
    id 5031
    name "5031"
  ]
  edge
  [
    source 33
    target 30
    measure 7
    diseases "Cone_dystrophy"
    GOmeasure 7
    id 5032
    name "5032"
  ]
  edge
  [
    source 32
    target 30
    measure 5
    diseases "Cone_dystrophy"
    GOmeasure 5
    id 5033
    name "5033"
  ]
  edge
  [
    source 31
    target 30
    measure 5
    diseases "Cone_dystrophy"
    GOmeasure 5
    id 5034
    name "5034"
  ]
  edge
  [
    source 717
    target 29
    measure 5
    diseases "Macular_degeneration"
    GOmeasure 5
    id 5035
    name "5035"
  ]
  edge
  [
    source 685
    target 29
    measure 5
    diseases "Macular_degeneration"
    GOmeasure 5
    id 5036
    name "5036"
  ]
  edge
  [
    source 419
    target 29
    measure 6
    diseases "Macular_degeneration"
    GOmeasure 6
    id 5037
    name "5037"
  ]
  edge
  [
    source 58
    target 29
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5038
    name "5038"
  ]
  edge
  [
    source 57
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5039
    name "5039"
  ]
  edge
  [
    source 56
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5040
    name "5040"
  ]
  edge
  [
    source 55
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5041
    name "5041"
  ]
  edge
  [
    source 54
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5042
    name "5042"
  ]
  edge
  [
    source 53
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5043
    name "5043"
  ]
  edge
  [
    source 52
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5044
    name "5044"
  ]
  edge
  [
    source 51
    target 29
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5045
    name "5045"
  ]
  edge
  [
    source 50
    target 29
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5046
    name "5046"
  ]
  edge
  [
    source 49
    target 29
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5047
    name "5047"
  ]
  edge
  [
    source 48
    target 29
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5048
    name "5048"
  ]
  edge
  [
    source 47
    target 29
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5049
    name "5049"
  ]
  edge
  [
    source 46
    target 29
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5050
    name "5050"
  ]
  edge
  [
    source 45
    target 29
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5051
    name "5051"
  ]
  edge
  [
    source 44
    target 29
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5052
    name "5052"
  ]
  edge
  [
    source 43
    target 29
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5053
    name "5053"
  ]
  edge
  [
    source 42
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5054
    name "5054"
  ]
  edge
  [
    source 41
    target 29
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5055
    name "5055"
  ]
  edge
  [
    source 40
    target 29
    measure 7
    diseases "Retinitis_pigmentosa"
    GOmeasure 7
    id 5056
    name "5056"
  ]
  edge
  [
    source 39
    target 29
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5057
    name "5057"
  ]
  edge
  [
    source 38
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5058
    name "5058"
  ]
  edge
  [
    source 37
    target 29
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5059
    name "5059"
  ]
  edge
  [
    source 36
    target 29
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5060
    name "5060"
  ]
  edge
  [
    source 34
    target 29
    measure 6
    diseases "Cone_dystrophy,Retinitis_pigmentosa"
    GOmeasure 6
    id 5061
    name "5061"
  ]
  edge
  [
    source 33
    target 29
    measure 7
    diseases "Cone_dystrophy"
    GOmeasure 7
    id 5062
    name "5062"
  ]
  edge
  [
    source 32
    target 29
    measure 5
    diseases "Cone_dystrophy"
    GOmeasure 5
    id 5063
    name "5063"
  ]
  edge
  [
    source 31
    target 29
    measure 5
    diseases "Cone_dystrophy,Retinitis_pigmentosa"
    GOmeasure 5
    id 5064
    name "5064"
  ]
  edge
  [
    source 30
    target 29
    measure 6
    diseases "Cone_dystrophy"
    GOmeasure 6
    id 5065
    name "5065"
  ]
  edge
  [
    source 59
    target 28
    measure 6
    diseases "Stargardt_disease,Macular_dystrophy"
    GOmeasure 6
    id 5066
    name "5066"
  ]
  edge
  [
    source 58
    target 28
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5067
    name "5067"
  ]
  edge
  [
    source 57
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5068
    name "5068"
  ]
  edge
  [
    source 56
    target 28
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5069
    name "5069"
  ]
  edge
  [
    source 55
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5070
    name "5070"
  ]
  edge
  [
    source 54
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5071
    name "5071"
  ]
  edge
  [
    source 53
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5072
    name "5072"
  ]
  edge
  [
    source 52
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5073
    name "5073"
  ]
  edge
  [
    source 51
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5074
    name "5074"
  ]
  edge
  [
    source 50
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5075
    name "5075"
  ]
  edge
  [
    source 49
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5076
    name "5076"
  ]
  edge
  [
    source 48
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5077
    name "5077"
  ]
  edge
  [
    source 47
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5078
    name "5078"
  ]
  edge
  [
    source 46
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5079
    name "5079"
  ]
  edge
  [
    source 45
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5080
    name "5080"
  ]
  edge
  [
    source 44
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5081
    name "5081"
  ]
  edge
  [
    source 43
    target 28
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5082
    name "5082"
  ]
  edge
  [
    source 42
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5083
    name "5083"
  ]
  edge
  [
    source 41
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5084
    name "5084"
  ]
  edge
  [
    source 40
    target 28
    measure 5
    diseases "Retinitis_pigmentosa"
    GOmeasure 5
    id 5085
    name "5085"
  ]
  edge
  [
    source 39
    target 28
    measure 6
    diseases "Retinitis_pigmentosa"
    GOmeasure 6
    id 5086
    name "5086"
  ]
  edge
  [
    source 38
    target 28
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5087
    name "5087"
  ]
  edge
  [
    source 37
    target 28
    measure 4
    diseases "Retinitis_pigmentosa"
    GOmeasure 4
    id 5088
    name "5088"
  ]
  edge
  [
    source 36
    target 28
    measure 5
    diseases "Fundus_albipunctatus,Retinitis_pigmentosa"
    GOmeasure 5
    id 5089
    name "5089"
  ]
  edge
  [
    source 35
    target 28
    measure 5
    diseases "Fundus_albipunctatus"
    GOmeasure 5
    id 5090
    name "5090"
  ]
  edge
  [
    source 34
    target 28
    measure 6
    diseases "Cone_dystrophy,Retinitis_pigmentosa"
    GOmeasure 6
    id 5091
    name "5091"
  ]
  edge
  [
    source 33
    target 28
    measure 6
    diseases "Cone_dystrophy"
    GOmeasure 6
    id 5092
    name "5092"
  ]
  edge
  [
    source 32
    target 28
    measure 5
    diseases "Cone_dystrophy"
    GOmeasure 5
    id 5093
    name "5093"
  ]
  edge
  [
    source 31
    target 28
    measure 5
    diseases "Cone_dystrophy,Retinitis_pigmentosa"
    GOmeasure 5
    id 5094
    name "5094"
  ]
  edge
  [
    source 30
    target 28
    measure 6
    diseases "Cone_dystrophy"
    GOmeasure 6
    id 5095
    name "5095"
  ]
  edge
  [
    source 29
    target 28
    measure 5
    diseases "Cone_dystrophy,Retinitis_pigmentosa"
    GOmeasure 5
    id 5096
    name "5096"
  ]
  edge
  [
    source 679
    target 26
    measure 5
    diseases "Elliptocytosis"
    GOmeasure 5
    id 5097
    name "5097"
  ]
  edge
  [
    source 294
    target 26
    measure 8
    diseases "Elliptocytosis,Spherocytosis"
    GOmeasure 8
    id 5098
    name "5098"
  ]
  edge
  [
    source 293
    target 26
    measure 6
    diseases "Spherocytosis"
    GOmeasure 6
    id 5099
    name "5099"
  ]
  edge
  [
    source 292
    target 26
    measure 6
    diseases "Spherocytosis"
    GOmeasure 6
    id 5100
    name "5100"
  ]
  edge
  [
    source 97
    target 26
    measure 5
    diseases "Elliptocytosis,Spherocytosis"
    GOmeasure 5
    id 5101
    name "5101"
  ]
  edge
  [
    source 27
    target 26
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5102
    name "5102"
  ]
  edge
  [
    source 27
    target 25
    measure 7
    diseases "Anemia"
    GOmeasure 7
    id 5103
    name "5103"
  ]
  edge
  [
    source 26
    target 25
    measure 8
    diseases "Anemia"
    GOmeasure 8
    id 5104
    name "5104"
  ]
  edge
  [
    source 27
    target 24
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5105
    name "5105"
  ]
  edge
  [
    source 26
    target 24
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5106
    name "5106"
  ]
  edge
  [
    source 25
    target 24
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5107
    name "5107"
  ]
  edge
  [
    source 27
    target 23
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5108
    name "5108"
  ]
  edge
  [
    source 26
    target 23
    measure 7
    diseases "Anemia"
    GOmeasure 7
    id 5109
    name "5109"
  ]
  edge
  [
    source 25
    target 23
    measure 7
    diseases "Anemia"
    GOmeasure 7
    id 5110
    name "5110"
  ]
  edge
  [
    source 24
    target 23
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5111
    name "5111"
  ]
  edge
  [
    source 27
    target 22
    measure 4
    diseases "Anemia"
    GOmeasure 4
    id 5112
    name "5112"
  ]
  edge
  [
    source 26
    target 22
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5113
    name "5113"
  ]
  edge
  [
    source 25
    target 22
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5114
    name "5114"
  ]
  edge
  [
    source 24
    target 22
    measure 4
    diseases "Anemia"
    GOmeasure 4
    id 5115
    name "5115"
  ]
  edge
  [
    source 23
    target 22
    measure 4
    diseases "Anemia"
    GOmeasure 4
    id 5116
    name "5116"
  ]
  edge
  [
    source 27
    target 21
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5117
    name "5117"
  ]
  edge
  [
    source 26
    target 21
    measure 8
    diseases "Anemia"
    GOmeasure 8
    id 5118
    name "5118"
  ]
  edge
  [
    source 25
    target 21
    measure 9
    diseases "Anemia"
    GOmeasure 9
    id 5119
    name "5119"
  ]
  edge
  [
    source 24
    target 21
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5120
    name "5120"
  ]
  edge
  [
    source 23
    target 21
    measure 7
    diseases "Anemia"
    GOmeasure 7
    id 5121
    name "5121"
  ]
  edge
  [
    source 22
    target 21
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5122
    name "5122"
  ]
  edge
  [
    source 27
    target 20
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5123
    name "5123"
  ]
  edge
  [
    source 26
    target 20
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5124
    name "5124"
  ]
  edge
  [
    source 25
    target 20
    measure 6
    diseases "Anemia"
    GOmeasure 6
    id 5125
    name "5125"
  ]
  edge
  [
    source 24
    target 20
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5126
    name "5126"
  ]
  edge
  [
    source 23
    target 20
    measure 4
    diseases "Anemia"
    GOmeasure 4
    id 5127
    name "5127"
  ]
  edge
  [
    source 22
    target 20
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5128
    name "5128"
  ]
  edge
  [
    source 21
    target 20
    measure 5
    diseases "Anemia"
    GOmeasure 5
    id 5129
    name "5129"
  ]
  edge
  [
    source 412
    target 19
    measure 5
    diseases "Migraine"
    GOmeasure 5
    id 5130
    name "5130"
  ]
  edge
  [
    source 411
    target 19
    measure 7
    diseases "Migraine"
    GOmeasure 7
    id 5131
    name "5131"
  ]
  edge
  [
    source 203
    target 19
    measure 6
    diseases "Migraine"
    GOmeasure 6
    id 5132
    name "5132"
  ]
  edge
  [
    source 18
    target 17
    measure 6
    diseases "Coronary_artery_disease"
    GOmeasure 6
    id 5133
    name "5133"
  ]
  edge
  [
    source 18
    target 16
    measure 6
    diseases "Coronary_artery_disease"
    GOmeasure 6
    id 5134
    name "5134"
  ]
  edge
  [
    source 17
    target 16
    measure 7
    diseases "Coronary_artery_disease"
    GOmeasure 7
    id 5135
    name "5135"
  ]
  edge
  [
    source 18
    target 15
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5136
    name "5136"
  ]
  edge
  [
    source 17
    target 15
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5137
    name "5137"
  ]
  edge
  [
    source 16
    target 15
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5138
    name "5138"
  ]
  edge
  [
    source 19
    target 13
    measure 5
    diseases "HDL_cholesterol_level_QTL"
    GOmeasure 5
    id 5139
    name "5139"
  ]
  edge
  [
    source 18
    target 13
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5140
    name "5140"
  ]
  edge
  [
    source 17
    target 13
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5141
    name "5141"
  ]
  edge
  [
    source 16
    target 13
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5142
    name "5142"
  ]
  edge
  [
    source 15
    target 13
    measure 5
    diseases "Coronary_artery_disease"
    GOmeasure 5
    id 5143
    name "5143"
  ]
  edge
  [
    source 14
    target 13
    measure 6
    diseases "Cerebral_amyloid_angiopathy"
    GOmeasure 6
    id 5144
    name "5144"
  ]
  edge
  [
    source 746
    target 12
    measure 7
    diseases "Dementia"
    GOmeasure 7
    id 5145
    name "5145"
  ]
  edge
  [
    source 745
    target 12
    measure 5
    diseases "Dementia"
    GOmeasure 5
    id 5146
    name "5146"
  ]
  edge
  [
    source 744
    target 12
    measure 6
    diseases "Dementia"
    GOmeasure 6
    id 5147
    name "5147"
  ]
  edge
  [
    source 630
    target 12
    measure 7
    diseases "Dementia"
    GOmeasure 7
    id 5148
    name "5148"
  ]
  edge
  [
    source 203
    target 12
    measure 5
    diseases "Dementia"
    GOmeasure 5
    id 5149
    name "5149"
  ]
  edge
  [
    source 12
    target 11
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5150
    name "5150"
  ]
  edge
  [
    source 12
    target 10
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5151
    name "5151"
  ]
  edge
  [
    source 11
    target 10
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5152
    name "5152"
  ]
  edge
  [
    source 12
    target 9
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5153
    name "5153"
  ]
  edge
  [
    source 11
    target 9
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5154
    name "5154"
  ]
  edge
  [
    source 10
    target 9
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5155
    name "5155"
  ]
  edge
  [
    source 287
    target 8
    measure 7
    diseases "Myocardial_infarction"
    GOmeasure 7
    id 5156
    name "5156"
  ]
  edge
  [
    source 286
    target 8
    measure 8
    diseases "Myocardial_infarction"
    GOmeasure 8
    id 5157
    name "5157"
  ]
  edge
  [
    source 285
    target 8
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 5158
    name "5158"
  ]
  edge
  [
    source 284
    target 8
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 5159
    name "5159"
  ]
  edge
  [
    source 283
    target 8
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 5160
    name "5160"
  ]
  edge
  [
    source 282
    target 8
    measure 7
    diseases "Myocardial_infarction"
    GOmeasure 7
    id 5161
    name "5161"
  ]
  edge
  [
    source 281
    target 8
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 5162
    name "5162"
  ]
  edge
  [
    source 280
    target 8
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 5163
    name "5163"
  ]
  edge
  [
    source 279
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5164
    name "5164"
  ]
  edge
  [
    source 278
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5165
    name "5165"
  ]
  edge
  [
    source 277
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5166
    name "5166"
  ]
  edge
  [
    source 276
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5167
    name "5167"
  ]
  edge
  [
    source 275
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5168
    name "5168"
  ]
  edge
  [
    source 274
    target 8
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 5169
    name "5169"
  ]
  edge
  [
    source 273
    target 8
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 5170
    name "5170"
  ]
  edge
  [
    source 272
    target 8
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 5171
    name "5171"
  ]
  edge
  [
    source 271
    target 8
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 5172
    name "5172"
  ]
  edge
  [
    source 270
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5173
    name "5173"
  ]
  edge
  [
    source 269
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5174
    name "5174"
  ]
  edge
  [
    source 268
    target 8
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 5175
    name "5175"
  ]
  edge
  [
    source 267
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5176
    name "5176"
  ]
  edge
  [
    source 266
    target 8
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 5177
    name "5177"
  ]
  edge
  [
    source 265
    target 8
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 5178
    name "5178"
  ]
  edge
  [
    source 264
    target 8
    measure 6
    diseases "Diabetes_mellitus"
    GOmeasure 6
    id 5179
    name "5179"
  ]
  edge
  [
    source 263
    target 8
    measure 7
    diseases "Diabetes_mellitus"
    GOmeasure 7
    id 5180
    name "5180"
  ]
  edge
  [
    source 262
    target 8
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 5181
    name "5181"
  ]
  edge
  [
    source 231
    target 8
    measure 5
    diseases "Renal_tubular_dysgenesis"
    GOmeasure 5
    id 5182
    name "5182"
  ]
  edge
  [
    source 218
    target 8
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 5183
    name "5183"
  ]
  edge
  [
    source 213
    target 8
    measure 5
    diseases "Diabetes_mellitus"
    GOmeasure 5
    id 5184
    name "5184"
  ]
  edge
  [
    source 191
    target 8
    measure 8
    diseases "Diabetes_mellitus"
    GOmeasure 8
    id 5185
    name "5185"
  ]
  edge
  [
    source 188
    target 8
    measure 8
    diseases "Renal_tubular_dysgenesis"
    GOmeasure 8
    id 5186
    name "5186"
  ]
  edge
  [
    source 186
    target 8
    measure 6
    diseases "Renal_tubular_dysgenesis"
    GOmeasure 6
    id 5187
    name "5187"
  ]
  edge
  [
    source 12
    target 8
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5188
    name "5188"
  ]
  edge
  [
    source 11
    target 8
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5189
    name "5189"
  ]
  edge
  [
    source 10
    target 8
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5190
    name "5190"
  ]
  edge
  [
    source 9
    target 8
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5191
    name "5191"
  ]
  edge
  [
    source 12
    target 7
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5192
    name "5192"
  ]
  edge
  [
    source 11
    target 7
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5193
    name "5193"
  ]
  edge
  [
    source 10
    target 7
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5194
    name "5194"
  ]
  edge
  [
    source 9
    target 7
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5195
    name "5195"
  ]
  edge
  [
    source 8
    target 7
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5196
    name "5196"
  ]
  edge
  [
    source 193
    target 6
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 5197
    name "5197"
  ]
  edge
  [
    source 192
    target 6
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 5198
    name "5198"
  ]
  edge
  [
    source 191
    target 6
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 5199
    name "5199"
  ]
  edge
  [
    source 190
    target 6
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 5200
    name "5200"
  ]
  edge
  [
    source 189
    target 6
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 5201
    name "5201"
  ]
  edge
  [
    source 188
    target 6
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 5202
    name "5202"
  ]
  edge
  [
    source 187
    target 6
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 5203
    name "5203"
  ]
  edge
  [
    source 186
    target 6
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 5204
    name "5204"
  ]
  edge
  [
    source 185
    target 6
    measure 7
    diseases "Hypertension"
    GOmeasure 7
    id 5205
    name "5205"
  ]
  edge
  [
    source 184
    target 6
    measure 6
    diseases "Hypertension"
    GOmeasure 6
    id 5206
    name "5206"
  ]
  edge
  [
    source 183
    target 6
    measure 5
    diseases "Hypertension"
    GOmeasure 5
    id 5207
    name "5207"
  ]
  edge
  [
    source 12
    target 6
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5208
    name "5208"
  ]
  edge
  [
    source 11
    target 6
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5209
    name "5209"
  ]
  edge
  [
    source 10
    target 6
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5210
    name "5210"
  ]
  edge
  [
    source 9
    target 6
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5211
    name "5211"
  ]
  edge
  [
    source 8
    target 6
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5212
    name "5212"
  ]
  edge
  [
    source 7
    target 6
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5213
    name "5213"
  ]
  edge
  [
    source 12
    target 5
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5214
    name "5214"
  ]
  edge
  [
    source 11
    target 5
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5215
    name "5215"
  ]
  edge
  [
    source 10
    target 5
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5216
    name "5216"
  ]
  edge
  [
    source 9
    target 5
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5217
    name "5217"
  ]
  edge
  [
    source 8
    target 5
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5218
    name "5218"
  ]
  edge
  [
    source 7
    target 5
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5219
    name "5219"
  ]
  edge
  [
    source 6
    target 5
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5220
    name "5220"
  ]
  edge
  [
    source 12
    target 4
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5221
    name "5221"
  ]
  edge
  [
    source 11
    target 4
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5222
    name "5222"
  ]
  edge
  [
    source 10
    target 4
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5223
    name "5223"
  ]
  edge
  [
    source 9
    target 4
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5224
    name "5224"
  ]
  edge
  [
    source 8
    target 4
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5225
    name "5225"
  ]
  edge
  [
    source 7
    target 4
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5226
    name "5226"
  ]
  edge
  [
    source 6
    target 4
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5227
    name "5227"
  ]
  edge
  [
    source 5
    target 4
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5228
    name "5228"
  ]
  edge
  [
    source 365
    target 3
    measure 5
    diseases "Hyperlipoproteinemia"
    GOmeasure 5
    id 5229
    name "5229"
  ]
  edge
  [
    source 287
    target 3
    measure 7
    diseases "Myocardial_infarction"
    GOmeasure 7
    id 5230
    name "5230"
  ]
  edge
  [
    source 286
    target 3
    measure 7
    diseases "Myocardial_infarction"
    GOmeasure 7
    id 5231
    name "5231"
  ]
  edge
  [
    source 285
    target 3
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 5232
    name "5232"
  ]
  edge
  [
    source 284
    target 3
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 5233
    name "5233"
  ]
  edge
  [
    source 283
    target 3
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 5234
    name "5234"
  ]
  edge
  [
    source 282
    target 3
    measure 6
    diseases "Myocardial_infarction"
    GOmeasure 6
    id 5235
    name "5235"
  ]
  edge
  [
    source 281
    target 3
    measure 5
    diseases "Myocardial_infarction"
    GOmeasure 5
    id 5236
    name "5236"
  ]
  edge
  [
    source 12
    target 3
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5237
    name "5237"
  ]
  edge
  [
    source 11
    target 3
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5238
    name "5238"
  ]
  edge
  [
    source 10
    target 3
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5239
    name "5239"
  ]
  edge
  [
    source 9
    target 3
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5240
    name "5240"
  ]
  edge
  [
    source 8
    target 3
    measure 7
    diseases "Alzheimer_disease,Myocardial_infarction"
    GOmeasure 7
    id 5241
    name "5241"
  ]
  edge
  [
    source 7
    target 3
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5242
    name "5242"
  ]
  edge
  [
    source 6
    target 3
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5243
    name "5243"
  ]
  edge
  [
    source 5
    target 3
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5244
    name "5244"
  ]
  edge
  [
    source 4
    target 3
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5245
    name "5245"
  ]
  edge
  [
    source 371
    target 2
    measure 6
    diseases "Schizophrenia"
    GOmeasure 6
    id 5246
    name "5246"
  ]
  edge
  [
    source 370
    target 2
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 5247
    name "5247"
  ]
  edge
  [
    source 369
    target 2
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 5248
    name "5248"
  ]
  edge
  [
    source 368
    target 2
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 5249
    name "5249"
  ]
  edge
  [
    source 367
    target 2
    measure 5
    diseases "Schizophrenia"
    GOmeasure 5
    id 5250
    name "5250"
  ]
  edge
  [
    source 366
    target 2
    measure 4
    diseases "Schizophrenia"
    GOmeasure 4
    id 5251
    name "5251"
  ]
  edge
  [
    source 349
    target 2
    measure 4
    diseases "Amyloidosis"
    GOmeasure 4
    id 5252
    name "5252"
  ]
  edge
  [
    source 348
    target 2
    measure 5
    diseases "Amyloidosis"
    GOmeasure 5
    id 5253
    name "5253"
  ]
  edge
  [
    source 347
    target 2
    measure 4
    diseases "Amyloidosis"
    GOmeasure 4
    id 5254
    name "5254"
  ]
  edge
  [
    source 346
    target 2
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 5255
    name "5255"
  ]
  edge
  [
    source 345
    target 2
    measure 6
    diseases "Amyloidosis"
    GOmeasure 6
    id 5256
    name "5256"
  ]
  edge
  [
    source 12
    target 2
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5257
    name "5257"
  ]
  edge
  [
    source 11
    target 2
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5258
    name "5258"
  ]
  edge
  [
    source 10
    target 2
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5259
    name "5259"
  ]
  edge
  [
    source 9
    target 2
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5260
    name "5260"
  ]
  edge
  [
    source 8
    target 2
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5261
    name "5261"
  ]
  edge
  [
    source 7
    target 2
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5262
    name "5262"
  ]
  edge
  [
    source 6
    target 2
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5263
    name "5263"
  ]
  edge
  [
    source 5
    target 2
    measure 4
    diseases "Alzheimer_disease"
    GOmeasure 4
    id 5264
    name "5264"
  ]
  edge
  [
    source 4
    target 2
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5265
    name "5265"
  ]
  edge
  [
    source 3
    target 2
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5266
    name "5266"
  ]
  edge
  [
    source 12
    target 1
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5267
    name "5267"
  ]
  edge
  [
    source 11
    target 1
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5268
    name "5268"
  ]
  edge
  [
    source 10
    target 1
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5269
    name "5269"
  ]
  edge
  [
    source 9
    target 1
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5270
    name "5270"
  ]
  edge
  [
    source 8
    target 1
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5271
    name "5271"
  ]
  edge
  [
    source 7
    target 1
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5272
    name "5272"
  ]
  edge
  [
    source 6
    target 1
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5273
    name "5273"
  ]
  edge
  [
    source 5
    target 1
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5274
    name "5274"
  ]
  edge
  [
    source 4
    target 1
    measure 6
    diseases "Alzheimer_disease"
    GOmeasure 6
    id 5275
    name "5275"
  ]
  edge
  [
    source 3
    target 1
    measure 7
    diseases "Alzheimer_disease"
    GOmeasure 7
    id 5276
    name "5276"
  ]
  edge
  [
    source 2
    target 1
    measure 5
    diseases "Alzheimer_disease"
    GOmeasure 5
    id 5277
    name "5277"
  ]
]
